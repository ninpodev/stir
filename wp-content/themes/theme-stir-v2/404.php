<?php get_header(); ?>
          
<div class="box fwidth fleft supertitulo">
    <div class="container">
        <div class="col-lg-12">
            <h1>:/</h1>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div class="page-wrapper box fleft fwidth">
  <section id="page" class="container">
      
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-column text-center">
        <p>It looks like you've reached a dead end. <br>But don't worry, go back to the <a href="<?php echo get_option('siteurl'); ?>">homepage</a> to start over. </p>
        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/404.jpg" alt="404">
         

    </div>
    
  </section>
</div>

<?php get_footer(); ?>