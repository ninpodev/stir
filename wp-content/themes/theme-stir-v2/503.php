<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php bloginfo('name'); ?>&nbsp;<?php if ( is_home() ) { ?><? } else { ?> / <?php
echo empty( $post->post_parent ) ? get_the_title( $post->ID ) : get_the_title( $post->post_parent );
?><? } ?></title>
    <meta property="og:url"           content="http://causeastir.com.au" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Stir" />
    <meta property="og:description"   content="Crowd-voted Microgrants for Creative Projects" />
    <meta property="og:image"         content="http://causeastir.com.au/mails/mail-bienvenida/welcome-mail_01.jpg" />      
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/normalize.css" rel="stylesheet">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/animate.css" rel="stylesheet">      
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/bootstrap.css" rel="stylesheet">      
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/magicwall.min.css" rel="stylesheet">            
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/materialize.css" rel="stylesheet">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/flexslider.css" rel="stylesheet">
    
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/theme.css" rel="stylesheet">
    <link href="<?php bloginfo('stylesheet_url'); ?>?v=<?php echo rand(0,1000);?>" rel="stylesheet">
      <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/frontpage.css" rel="stylesheet">
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/modernizr.js" type="text/javascript"></script>    
      
    <script src="//use.typekit.net/myc2zqg.js"></script>
    <script>try{Typekit.load();}catch(e){}</script>
    <script src="https://www.google.com/recaptcha/api.js"></script>
      
  </head>
  <body>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1560221257581148',
      xfbml      : true,
      version    : 'v2.2'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.4&appId=342690685862200";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>      
    
<header id="head-main" class="container-fluid no-column">
    
    <div id="logo" class="col-lg-2 col-md-2 col-sm-4 col-xs-4 no-column">
        <a href="<?php echo get_option('siteurl'); ?>">&nbsp;</a>
    </div>
    <div id="top-menu" class="col-lg-5 col-md-5 hidden-sm hidden-xs no-column">
        <ul>
            <?php if ( is_user_logged_in() ) { ?>
                &nbsp;
                <? } else { ?>
                <li><a href="<?php echo get_option('siteurl'); ?>/welcome/">Create</a></li>
                <? }?>
                <li><a href="<?php echo get_option('siteurl'); ?>/support/" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>Support</a></li>
                <?php /* <li><a href="<?php echo get_option('siteurl'); ?>/toolshed" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>Toolshed</a></li>
                <li><a href="<?php echo get_option('siteurl'); ?>/About" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>About</a></li>
                <li><a href="<?php echo get_option('siteurl'); ?>/News" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>News</a></li> */ ?>
            <li><a href="https://www.facebook.com/causeastirCBR" target="_blank"><i class="fa fa-facebook-square fa-lg"></i></a></li>
            <li><a href="https://twitter.com/StirCBR" target="_blank"><i class="fa fa-twitter fa-lg"></i></a></li>
        </ul>
    </div>
    <div id="top-meta" class="col-lg-5 col-md-5 hidden-sm hidden-xs text-right no-column">
        <?php if ( is_user_logged_in() ) { ?>
        <?php } else { ?>
        <? } ?>
        
        
        <?php if ( is_user_logged_in() ) { ?>
			<?php   $current_user = wp_get_current_user();?>
            <p>Hello <?php echo $current_user->user_login;?></p>
            <?php if(have_draft()){?>
            <a href="<?php echo esc_url( home_url() );?>/basics/" class="btn-gris-small">Complete project <i class="fa fa-pencil fa-lg"></i></a>   
            <?php } else if(user_publishproject()){ ?>
            <a href="<?php echo esc_url( home_url() );?>/basics/?draft" class="btn-gris-small">Edit my project <i class="fa fa-pencil fa-lg"></i></a>   
            <?php  } else{?>
            <a href="<?php echo esc_url( home_url() );?>/eligibility/" class="btn-gris-small">Create project <i class="fa fa-plus fa-lg"></i></a> 
            <?php } ?>
            <p><a href="<?php echo wp_logout_url();?>">Log Out -></a></p>
        <?php ; } else{?>
            
            <a href="<?php echo get_option('siteurl'); ?>/welcome/" class="btn-gris-small">Start a Project &nbsp;<i class="fa fa-plus fa-lg"></i></a>
        <p style="font-size:10px;">or <a href="<?php echo get_option('siteurl'); ?>/login/">Log in</a> here</p>
        <? } ?>        

        
        
        
        
        
    </div>
    
    <div id="hamburger-mob-icon" class="hidden-lg hidden-md col-sm-8 col-xs-8 text-right no-column">
        <a href="#menu" class="menu-link">
            <div id="nav-icon1">
                <span></span>
                <span></span>
                <span></span>
            </div>
            
        </a>
    </div>
    
    
    
    <?php /*
    
    
    
    
    <div id="logo-head" class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
        <a href="<?php echo get_option('siteurl'); ?>"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-head-2.png" alt=""></a>
    </div>
    <!--logo-->
    
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-10">
        <nav>
            <ul class="menu">
                
                <?php if ( is_user_logged_in() ) { ?>
                <li><a href="<?php echo get_option('siteurl'); ?>/eligibility">Create</a></li>
                <? } else { ?>
                <li><a href="<?php echo get_option('siteurl'); ?>/register">Create</a></li>
                <? }?>
                <li><a href="<?php echo get_option('siteurl'); ?>/support" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>Support</a></li>
                <li><a href="<?php echo get_option('siteurl'); ?>/toolshed" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>Toolshed</a></li>
                <li><a href="<?php echo get_option('siteurl'); ?>/About" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>About</a></li>
                <li><a href="<?php echo get_option('siteurl'); ?>/News" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>News</a></li>
            </ul>
        </nav>
    </div>
    <!--menu--->
    
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 login-search">
        <form method="get" action="<?php echo esc_url( home_url() );?>">
          <?php    
            extract($_GET,EXTR_SKIP);
            $s = strip_tags(htmlentities($s));
          ?>
            <input type="text" name="s" placeholder="type &amp; enter" <?php if(isset($s)){?>value="<?php echo $s;?>"<?php } ?>>
        </form>
     </div>
     <!--form-->
        
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 user-info">
        <?php if ( is_user_logged_in() ) { ?>
			<?php   $current_user = wp_get_current_user();?>
            <a href="#" class="welcome">Hello <?php echo $current_user->user_login;?></a>
            <div class="clearfix"></div>
            <?php if(have_draft()){?>
            <a href="<?php echo esc_url( home_url() );?>/basics/" class="link">Complete project</a>   
            <?php } else if(user_publishproject()){ ?>
            <a href="<?php echo esc_url( home_url() );?>/basics/?draft" class="link">Edit my project</a>   
            <?php  } else{?>
            <a href="<?php echo esc_url( home_url() );?>/eligibility/" class="link">Create project</a> 
            <?php } ?>
            <span style="color:#FFF;">|</span>&nbsp;<a href="<?php echo wp_logout_url();?>" class="link">Log Out</a>
        <?php ; } else{?>
            <a href="<?php echo get_option('siteurl'); ?>/register" class="login">Log in or register</a>
        <? } ?>
    
    </div>
    
    <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs sm-head text-center">
        <a href="https://www.facebook.com/causeastirCBR" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
        </span>
        </a>
        
        <a href="https://twitter.com/StirCBR" target="_blank">
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
        </span>
        </a>
    </div>
    */ ?>
    
  <div id="mensaje" style="display:none;">
    <p>Saving</p><div class="spinner"></div>
  </div>
    
    
    
</header>
      <!--<div class="pixeles"></div>-->
<nav id="menu" class="panel" role="navigation">

 <?php if ( is_user_logged_in() ) { ?>
			<?php   $current_user = wp_get_current_user();?>
            <p>Hello <?php echo $current_user->user_login;?></p><br>
            <?php if(have_draft()){?>
            <a href="<?php echo esc_url( home_url() );?>/basics/" class="btn-gris-small">Complete project <i class="fa fa-pencil fa-lg"></i></a>   
            <?php } else if(user_publishproject()){ ?>
            <a href="<?php echo esc_url( home_url() );?>/basics/?draft" class="btn-gris-small">Edit my project <i class="fa fa-pencil fa-lg"></i></a>   
            <?php  } else{?>
            <a href="<?php echo esc_url( home_url() );?>/eligibility/" class="btn-gris-small">Create project <i class="fa fa-plus fa-lg"></i></a> 
            <?php } ?>
            <p class="logout-link-mob"><a href="<?php echo wp_logout_url();?>">Log Out -></a></p>
            <?php ; } else{?>
            <a href="<?php echo get_option('siteurl'); ?>/welcome/" class="btn-gris-small">Start a project &nbsp;<i class="fa fa-plus fa-lg"></i></a>
    <p style="font-size:10px;">or <a href="<?php echo get_option('siteurl'); ?>/login/">Log in</a> here</p>
        <? } ?>         
    
    <ul>
        <?php if ( is_user_logged_in() ) { ?>
                <? } else { ?>
                <? }?>
                <li><a href="<?php echo get_option('siteurl'); ?>/support/" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>Support</a></li>
                <li><a href="<?php echo get_option('siteurl'); ?>/toolshed/" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>Toolshed</a></li>
                <li><a href="<?php echo get_option('siteurl'); ?>/About" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>About</a></li>
                <li><a href="<?php echo get_option('siteurl'); ?>/blog/" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>News</a></li>
            <li><a href="https://www.facebook.com/causeastirCBR" target="_blank"><i class="fa fa-facebook-square fa-lg"></i></a>&nbsp;&nbsp;<a href="https://twitter.com/StirCBR" target="_blank"><i class="fa fa-twitter fa-lg"></i></a></li>
    </ul>
</nav>      
		<div id="maintenance" class="container-fluid no-column">
					
			<a id="determine-user-type" class="in-page-link"></a>

			<section id="open-cta" class="cta cta-8">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-left">
                            <h1>CROWD VOTED MICROGRANTS<h1>
                            <h2>For creative projects</h2><br><br>
							<a class="btn waves-effect waves-light btn" href="<?php echo get_option('siteurl'); ?>/support/" target="_blank">Browse Projects</a>
							<a class="btn waves-effect waves-light btn" href="<?php echo get_option('siteurl'); ?>/welcome/">Create Project</a>
						</div>
					</div>
				</div>
			</section>
			
			<section id="what-is-stir" class="features features-13">
                <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 text-left">
                                <h2>&nbsp;What is Stir?</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="feature col-sm-4 text-left">
                                <h5>LAUNCHED IN 2015, STIR PROVIDES CREATIVE YOUTH WITH A PLATFORM TO DEVELOP THEIR PASSIONS INTO PROJECT PROPOSALS BY PROVIDING EDUCATION, EXPOSURE AND THE POTENTIAL FOR FUNDING.</h5>
                            </div>
                            <div class="col-sm-4 feature text-left">
                                <p>
                                    Stir is a CBRIN initiative; a local crowd-voted grants platform that provides a simple process to help them structure, write and present their personal projects in a way that teaches them about grant applications. </p>

                            </div>
                           <div class="col-sm-4 feature text-left">
                                <p>
                                    Stir Season One ran between April and June, awarding 15 $1k grants. Two of these projects progressed to win a total of $20,000 in ANU’s startup program InnovationACT.

                                </p>
                            </div>
                        </div>
				</div>
			</section>
            <section id="how-it-works" class="features features-13">
                <div class="container">
                    <div class="row">
                            <div class="col-md-12 col-sm-12 text-left">
                                <h2>&nbsp;How it works?</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 text-center no-column">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/img-diagrama.jpg" alt="">
                            </div>
                        </div>

                    
                        <div class="row">
                            <div class="feature col-sm-4 text-left">
                                <h5>Upload</h5>
                                <p>Stir provides a simple process that guides people through the steps required to develop a project proposal. Each step is supported by tools that help in answering some of the common questions asked of entrepreneurial projects.</p>
                            </div>
                            <div class="col-sm-4 feature text-left">
                                <h5>Comment</h5>
                                <p>Once a project is published it can begin receiving comments and feedback from the Stir community. These can help the project creator to improve their proposal and increase their chances of receiving funding. </p>
                            </div>
                           <div class="col-sm-4 feature text-left">
                                <h5>Vote</h5>
                                <p>On the 1st of May, crowd-voting will commence. Project creators are encouraged to engage with the wider community to gather votes. On the 1st of July, the voting period will close, and the top 5 projects will each receive a $1k Stir Grant! </p>
                            </div>
                        </div>
				</div>
			</section>                    
			

			
            <section id="estadisticas" class="features features-13">
                <div class="container">
					<div class="row">
						<div class="col-md-12 col-sm-12 text-left">
							<h2>stir season one <span>Statistics</span></h4>
						</div>
                        <div class="col-sm-5 feature">
                        <h5 class="text-white">Stir Season One took place between the 27th of March and the 1st of June, 2015. During that time, it achieved the following reach and impact:</h5>
                        </div>
                        <div class="clearfix"></div>
						<div class="col-sm-4 feature">
							<h6>over</h6>
                            <h1>45</h1>
							<h4>UNIQUE VISITORS</h4>
							<p>were created during Season One, with another 13 left unpublished.</p>
							
						</div><div class="col-sm-4 feature">
							<h6>over</h6>
                            <h1>2700</h1>
							<h4>REGISTERED USERS</h4>
							<p> signed up to support projects casting over 2500 votes</p>
							
						</div>
					
						<div class="col-sm-4 feature">
							<h6>over</h6>
                            <h1>13000</h1>
							<h4>UNIQUE VISITORS</h4>
							<p>viewed the site 67,244 times during Season One.</p>
						</div>
					</div>
				</div>

			</section>
			
			
			
			<section id="testimonios" class="testimonials testimonials-5">
				<div class="container">
					<div class="row v-align-children">
						<div class="col-md-12 col-sm-12 text-left">
							<h4>Season One <span>Testimonials</span></h4>
                            
							<div id="slider-testimonios" class="flexslider">
								<ul class="slides">
									<li>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <p>"Being on Stir Gave me permission to pitch my ideas"</p>
                                                <span>Awais, 25 - Cleverbee</span>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/img-testimonio-1.jpg" alt="">
                                            </div>
                                        </div>
										
									</li>
                                    <li>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <p>"Stir helped me realise an idea is only as important as the team behind it"</p>
                                                <span>Shubham, 21 - Chippar</span>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/img-testimonio-2.jpg" alt="">
                                            </div>
                                        </div>
										
									</li>
                                    <li>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <p>"Stir encouraged me to concentrate on what I enjoy doing most"</p>
                                                <span>Sancho, 29 - LoBrow Gallery</span>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/img-testimonio-3.jpg" alt="">
                                            </div>
                                        </div>
										
									</li>
								</ul>
							</div>
                            
						</div>
                    </div>
				</div>
			</section>
			
			<section id="browse-projects" class="features features-2">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-sm-12 text-left">
							<h4>Browse Projects</h4>
						</div>
					</div>
				
					<div class="row">
                        <?php query_posts('showposts=4&post_type=project&orderby=rand');
if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="card">
                                <div class="card-image">
                                    <a href="<?php the_permalink(); ?>"><img src="<?php echo get('visuals_project_display');?>" alt=""></a>
                                </div>
                                <div class="card-content">
                                <span class="card-title"><?php the_title(); ?></span>
                                <p><?php echo get('describe_project');?> <?php echo get('describe');?></p>
                                </div>
                            </div>
                        </div>
                        
                    <?php endwhile; else: endif; wp_reset_query(); ?>
                        
					</div>
                    <div class="row">
						<div class="col-md-12 col-sm-12 text-left">
							<p class="link-browse">Or <a href="<?php echo esc_url( home_url() );?>/support/">Browse All</a></p>
						</div>
					</div>
				</div>
			</section>
			
			<section id="supported" class="header">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-left">
							<h4>Stir is Supported by</h4><br><br>
						</div>
                        <div class="clearfix"></div>
                        <div class="col-sm-12 text-left logos">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logos_01.jpg" alt="">
                        </div>
                        
					</div>
				</div>
			</section>
			
			<section id="whats-the-stir" class="cta cta-1">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h2>What's the stir you want to cause</h2>
							<a class="waves-effect waves-light btn" href="<?php echo esc_url( home_url() );?>/support/">Browse Projects</a>
                            <a class="waves-effect waves-light btn" href="<?php echo esc_url( home_url() );?>/welcome/">Create a Project</a>
                            <a class="waves-effect waves-light btn" target="_blank"  href="<?php echo esc_url( get_template_directory_uri() ); ?>/docs/StirInformationPack.pdf">Become a Sponsor</a>
						</div>
					</div>
				</div>
			</section>
		</div>

<div class="container-footer box fwidth fleft">
    <footer class="container">
        <div class="logo col-lg-3 col-md-3 col-sm-3 col-xs-3">
        </div>
        <div class="txt col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <p>Stir is an initiative of the CBR Innovation Network, Designed by IMPIMG, in collaboration with the SHIFT ONE Crew.</p>
        </div>
        <div class="menu col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <ul>
                <li><a href="<?php echo get_option('siteurl'); ?>/contact">Contact</a></li>
                <li><a href="<?php echo get_option('siteurl'); ?>/copyright">Copyright</a></li>
                <li><a href="<?php echo get_option('siteurl'); ?>/privacy">Privacy</a></li>
                <li><a href="<?php echo get_option('siteurl'); ?>/terms-and-conditions">Terms &amp; Conditions</a></li>
            </ul>
        </div>
        <div class="logo-in col-lg-4 col-md-4 col-sm-4 col-xs-4">
        </div>
        
    </footer>
</div>

				


      
      


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>var $base_url  = "<?php echo esc_url( home_url() );?>";</script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/bootstrap.min.js" type="text/javascript"></script>           
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.flexslider-min.js" type="text/javascript"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.clingify.js" type="text/javascript"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/bigSlide.min.js" type="text/javascript"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/materialize.js" type="text/javascript"></script>
                
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/scripts-maintenance.js" type="text/javascript"></script>     
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/app.js" type="text/javascript"></script>          
    
	<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-61388758-1', 'auto');
      ga('send', 'pageview');
    
    </script>
    
  </body>
</html>