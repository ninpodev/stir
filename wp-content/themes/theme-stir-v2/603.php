<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Edit title tag</title>
</head>

    <body>

				
		<div class="nav-container">
			
			
		
			<nav class="nav-1">
				<div class="navbar">
					<div class="container">
						<div class="row">
							<div class="col-md-3 col-sm-6 col-xs-4">
								<a href="#">
									<img class="logo" alt="Logo" src="img/logo-dark.png">
								</a>
							</div>
					
							<div class="col-md-3 text-right col-sm-6 col-md-push-6 col-xs-8">
								<a class="btn btn-filled" href="#">Join Now »</a>
								<div class="mobile-toggle">
									<div class="upper"></div>
									<div class="middle"></div>
									<div class="lower"></div>
								</div>
							</div>
					
							<div class="col-md-6 text-center col-md-pull-3 col-sm-12 col-xs-12">
								<ul class="menu">
									<li>
										<a href="#">
											Single
										</a>
									</li>
							
									<li class="has-dropdown">
										<a href="#">
											Dropdown
										</a>
										<ul class="subnav">
											<li>
												<a href="#">
													Single
												</a>
											</li>
									
											<li class="has-dropdown">
												<a href="#">
													Second Level
												</a>
												<ul class="subnav">
													<li>
														<a href="#">
															Single
														</a>
													</li>
												</ul>
											</li>
										</ul>
									</li>
								</ul>
							</div>
				
						</div>
					</div>
				</div>
			</nav>
		
		</div>
		
		<div class="main-container">
					
			<a id="determine-user-type" class="in-page-link"></a>
			
			<section class="cta cta-8 bg-secondary bg-dark">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h4>CROWD VOTED MICROGRANTS PLATFORM<br>Help creative youth enter the economy.</h4>
							<a class="btn inner-link" href="#determine-user-type" target="_blank">Browse Projects</a>
							<a class="btn btn-filled" href="#determine-user-type">Create Project</a>
						</div>
					</div>
				</div>
			</section>
			
			<section class="features features-13">
					<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1 col-sm-12 text-center">
							<h4 class="text-white">What is Stir?</h4>
						</div>
					</div>
				
					<div class="row">
						
					
						<div class="feature col-sm-4">
							
							<h5 class="text-white">LAUNCHED IN 2015, STIR PROVIDES CREATIVE YOUTH WITH A PLATFORM TO DEVELOP THEIR PASSIONS INTO PROJECT PROPOSALS BY PROVIDING EDUCATION, EXPOSURE AND THE POTENTIAL FOR FUNDING.
		</h5>
							
							
						</div>
					
						<div class="col-sm-4 feature">
							
							
							<p>
								Stir is a CBRIN initiative; a local crowd-voted grants platform that provides a simple process to help them structure, write and present their personal projects in a way that teaches them about grant applications. 
		
		 
							</p>
							
						</div>
					
					<div class="col-sm-4 feature">
							
							
							<p>
								Stir Season One ran between April and June, awarding 15 $1k grants. Two of these projects progressed to win a total of $20,000 in ANU’s startup program InnovationACT.
		
							</p>
						
							
						</div></div>
				</div>
			</section>
			
			<section class="header header-9">
					<div class="container">
						<div class="row">
							<div class="col-sm-12 text-center">
								<h2 class="text-white">How it works</h2>
							</div>
						</div>
					</div>
			</section>
			
			<section class="header header-11 overlay">
			
				<div class="background-image-holder">
					<img alt="Background Image" class="background-image" src="img/hero14.jpg">
				</div>
			
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h2 class="text-white">Process Diagram</h2>
						</div>
					</div>
				</div>
			</section>
			
			<section class="features features-13">
					<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1 col-sm-12 text-center">
							<h4 class="text-white"> </h4>
						</div>
					</div>
				
					<div class="row">
						<div class="col-sm-4 feature">
							<i class="icon-design-pencil-rule-streamline"></i>
							<h5 class="text-white">Intuitive Page Builder</h5>
							<p>
								Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi.
							</p>
						
							
						</div>
					
						<div class="feature col-sm-4">
							<i class="icon-envellope-mail-streamline"></i>
							<h5 class="text-white">Slick, Flexible Design</h5>
							<p>
								Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.
							</p>
							
						</div>
					
						<div class="col-sm-4 feature">
							<i class="icon-grid-lines-streamline"></i>
							<h5 class="text-white">Built on Bootstrap 3</h5>
							<p>
								It’s never been easier to create a unique and effective landing page.  Build, edit and customize your landing page intuitively in-browser with Variant. 
							</p>
							
						</div>
					
					</div>
				</div>
			</section>
			
			<section class="features features-13">
					<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1 col-sm-12 text-center">
							<h4 class="text-white">Season One Stats</h4>
						</div>
					<div class="col-sm-4 feature">
							
							<h5 class="text-white">BEHIND THESE NUMBERS ARE THE REAL STORIES OF 15 YOUNG CANBERRANS WHO RECEIVED $1000 TO PURSUE THEIR PASSIONS.</h5>
							
						
							
						</div></div>
				
					<div class="row">
						
					
						<div class="col-sm-4 feature">
							<i class="icon-envellope-mail-streamline"></i>
							<h5 class="text-white">CREATIVE PROJECTS</h5>
							<p>
								were created during Season One, with 
		another 13 left unpublished.
		
							</p>
							
						</div><div class="col-sm-4 feature">
							<i class="icon-envellope-mail-streamline"></i>
							<h5 class="text-white">REGISTERED USERS</h5>
							<p>registered to support projects, casting over 2500 votes.
		
							</p>
							
						</div>
					
						<div class="col-sm-4 feature">
							<i class="icon-grid-lines-streamline"></i>
							<h5 class="text-white">UNIQUE VISITORS</h5>
							<p>
								viewed the site 67,244 times during Season One.
		 
							</p>
							
						</div>
					
					</div>
				</div>
			</section>
			
			<section class="testimonials testimonials-5">
				<div class="container">
					<div class="row v-align-children">
						
					
						<div class="col-md-5 col-sm-6">
							<h4>Season One Testimonials</h4>
							<div class="slider">
								<ul class="slides">
									<li>
										<p>
											“Wow - what an amazing product. I’m so glad to have found Launchkit. Our landing page looks slick, stylish and we’re already seeing promising results.”
										</p>
										<p>
											<em>— Jesse Ware, Devotion</em>
										</p>
									</li>
							
									<li>
										<p>
											“Using Launchkit I was able to make our landing page faster than it would usually take to get a WordPress theme installed. The page builder seriously rocks.”
										</p>
										<p>
											<em>— Carl Craig, At Les</em>
										</p>
									</li>
							
									<li>
										<p>
											“Just a complete joy to work with. I am often skeptical about page builders but this one produced only clean, valid code. It's Variant or the highway from now on.”
										</p>
										<p>
											<em>— Guy Gerber, Moon Blur</em>
										</p>
									</li>
								</ul>
							</div>
						</div>
					<div class="col-md-7 text-center col-sm-6">
							<img alt="Testimonials" src="img/small9.jpg">
						</div></div>
				</div>
			</section>
			
			<section class="features features-2">
				<div class="container">
					<div class="row">
						<div class="col-md-10 col-md-offset-1 col-sm-12 text-center">
							<h4>Browse Projects</h4>
							<p>With multiple options for all sections - Launchkit has the right stuff for your next landing page.</p>
						</div>
					</div>
				
					<div class="row">
						<div class="col-sm-4 text-center feature">
							<img alt="Feature Image" src="img/small7.jpg">
							<h5>Unique, Engaging Style</h5>
							<p>
								At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores.
							</p>
						</div>
					
						<div class="col-sm-4 text-center feature">
							<img alt="Feature Image" src="img/small8.jpg">
							<h5>Built for mobile and up</h5>
							<p>
								At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores.
							</p>
						</div>
					
						<div class="col-sm-4 text-center feature">
							<img alt="Feature Image" src="img/small6.jpg">
							<h5>Variant Builder included</h5>
							<p>
								At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores.
							</p>
						</div>
					</div>
				</div>
			</section>
			
			<section class="header header-11 overlay">
			
				<div class="background-image-holder">
					<img alt="Background Image" class="background-image" src="img/hero14.jpg">
				</div>
			
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h2 class="text-white">Supported by</h2>
						</div>
					</div>
				</div>
			</section>
			
			<section class="cta cta-1">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h2>Get Involved</h2>
							<a class="btn btn-filled super-action" href="#purchase-template">Buy Launchkit</a>
							<p class="sub">By continuting you agree to our<br><a href="#">Terms of Use</a></p>
						</div>
					</div>
				</div>
			</section>
			<footer class="footer footer-3">
				<div class="container">
					<div class="row">
						<div class="col-md-2 col-md-offset-3 col-sm-4">
							<a href="#">
								<img alt="Logo" src="img/logo-dark.png">
							</a>	
						</div>
					
						<div class="col-md-2 col-sm-4">
							<ul class="menu">
								<li>
									<a href="#">
										News
									</a>
								</li>
						
								<li>
									<a href="#">
										FAQ
									</a>
								</li>
						
								<li>
									<a href="#">
										Privacy Policy
									</a>
								</li>
						
								<li>
									<a href="#">
										Terms of Use
									</a>
								</li>
							</ul>
						</div>
					
						<div class="col-md-2 col-sm-4">
							<ul class="menu">
								<li>
									<a href="#">
										Facebook
									</a>
								</li>
						
								<li>
									<a href="#">
										Twitter
									</a>
								</li>
						
								<li>
									<a href="#">
										Instagram
									</a>
								</li>
							</ul>
						</div>
					</div>
				
					<div class="col-md-6 col-md-offset-3">
						<div class="lower">
							<p class="sub">
								© Copyright 2015 Medium Rare - All Rights Reserved
							</p>	
						</div>
					</div>
				</div>
			</footer>
		</div>
		
				
		
    </body>
</html>

				