<?php get_header(); ?>    

        <div class="box fwidth fleft supertitulo">
            <div class="container ">
                <div class="col-lg-12 no-column">
                    <h1>Dev Blog</h1>
                </div>
            </div>
        </div>
      
		<div id="home" class="container-fluid no-column">
			<a id="determine-user-type" class="in-page-link"></a>
            <div id="content-tiles" class="container-fluid">
            <div class="col-lg-2 no-column">
                &nbsp;
            </div>
            <div class="clearfix"></div>


                <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?>

                <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' ); $url = $thumb['0']; ?>
               
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 no-column">
                    
                    
                    <div class="item box fleft fwidth">
                        <div class="meta">
                            <span class="box fright">
                                <?php the_time('d'); ?>.<?php the_time('m'); ?>.<?php the_time('Y'); ?>
                            </span>
                        </div>
                        <div class="clearfix"></div>
                        <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                        <div class="clearfix"></div>
                        <p class="excerpt"><?php echo excerpt(17); ?>
                            <br>
                            <a href="<?php the_permalink(); ?>">more</a></p>
                        <div class="clearfix"></div>
                    </div>
                    
                     <?php /*
                     
                    <?php  if ( has_term( 'podcast', 'type_of_content' )  ) { ?>
                    <?php } elseif ( has_term( 'journal', 'type_of_content' ) ) { ?>
                    <?php } elseif ( has_term( 'stir-story', 'type_of_content' ) ) { ?>
                    <?php } elseif ( has_term( 'skill-share', 'type_of_content' ) ) { ?>
                    <?php } elseif ( 'project' == get_post_type() ) { ?>
                    <?php } else {?>
                    <?php } ?>
                    
                    
                  <? */?>
                    
                    
                </div>
                
                <?php endwhile; ?>
                    <?php wp_pagenavi(); ?>
                <?php else : ?>
                <?php endif; ?> 
                
                

                
                
                
            </div>
            <div class="clearfix"></div>
            
            
            
			
		</div>
<?php get_footer(); ?>