<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>            
<section id="bbpress-container" class="container">
    <div class="col-lg-3">
      <?php dynamic_sidebar( 'forums_sidebar' ); ?>
    </div>
    <div class="col-lg-9">
        <?php the_content(''); ?>
    </div>
</section>
<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>
<?php get_footer(); ?>