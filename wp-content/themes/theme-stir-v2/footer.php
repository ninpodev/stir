<? if ( is_page( array(14,17,19,21,23,'dashboard','project') ) ) { ?>

<? } else { ?>

    <?php if (! is_user_logged_in() ) { ?>
        <? if ( is_page(array('about', 'welcome')) )  { ?>

        <? } else { ?>
            <div class="message-home fwidth fleft">
                <div class="container-fluid">
                    <div class="col-lg-9">
                        <h3>PUBLISH YOUR CREATIVE PROJECT, GATHER VOTES AND RECEIVE A $1000 GRANT TO PURSUE YOUR PASSIONS.</h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12 pl0">

                        <a href="http://causeastir.com.au/about/" class="small-cta">[learn more]</a>
                        <a href="http://causeastir.com.au/welcome/" class="small-cta">[Create a Project]</a>

                    </div>
                </div>
            </div>
            <div id="friends-footer" class="box fleft fwidth">
                <div class="container-fluid">
                    <div class="col-lg-12">
                        <h3>Friends</h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12 friends-logos">
                        <ul>
                            <li><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-ains.png" alt=""> </li>
                            <li><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/gamma.png" alt=""> </li>
                            <li><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/e29.png" alt=""> </li>
                            <li><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/impact.png" alt=""> </li>
                            <li><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/brindabella.png" alt=""> </li>
                            <li><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/winditup.png" alt=""> </li>
                            <li><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/youarehere.png" alt=""> </li>
                            <li><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/vacantspace.png" alt=""> </li>

                        </ul>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12">
                        <h3>Sponsors</h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12 sponsors-logos">
                        <div class="box fleft fwidth">
                            <div class="col-lg-3 text-left item-logo"><a href="#" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/cbrin-act.png" alt=""></a></div>
                            <div class="col-lg-3 text-left item-logo"><a href="#" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/bendigo.png" alt=""></a></div>
                            <div class="col-lg-3 text-left item-logo"><a href="#" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/au-insp.png" alt=""></a></div>
                            <div class="col-lg-3 text-left item-logo"><a href="#" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/citsa.png" alt=""></a></div>
                            <div class="clearfix"></div>
                            <div class="col-lg-3 text-left item-logo"><a href="#" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/uclogo.png" alt=""></a></div>
                            <div class="col-lg-3 text-left item-logo"><a href="#" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/AISACT-logo-white.png" alt=""></a></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
        <? } ?>
    <?php } else { ?>
            <div id="friends-footer" class="box fleft fwidth">
                <div class="container-fluid">
                    <div class="col-lg-12">
                        <h3>Friends</h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12 friends-logos">
                        <ul>
                            <li><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-ains.png" alt=""> </li>
                            <li><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/gamma.png" alt=""> </li>
                            <li><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/e29.png" alt=""> </li>
                            <li><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/impact.png" alt=""> </li>
                            <li><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/brindabella.png" alt=""> </li>
                            <li><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/winditup.png" alt=""> </li>
                            <li><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/youarehere.png" alt=""> </li>
                            <li><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/vacantspace.png" alt=""> </li>

                        </ul>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12">
                        <h3>Sponsors</h3>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12 sponsors-logos">
                        <div class="box fleft fwidth">
                            <div class="col-lg-3 text-left item-logo"><a href="#" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/cbrin-act.png" alt=""></a></div>
                            <div class="col-lg-3 text-left item-logo"><a href="#" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/bendigo.png" alt=""></a></div>
                            <div class="col-lg-3 text-left item-logo"><a href="#" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/au-insp.png" alt=""></a></div>
                            <div class="col-lg-3 text-left item-logo"><a href="#" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/citsa.png" alt=""></a></div>
                            <div class="clearfix"></div>
                            <div class="col-lg-3 text-left item-logo"><a href="#" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/uclogo.png" alt=""></a></div>
                            <div class="col-lg-3 text-left item-logo"><a href="#" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/AISACT-logo-white.png" alt=""></a></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
    <? } ?>

    <div class="container-footer box fwidth fleft">
        <footer class="container-fluid">
        <div class="logo col-lg-1 col-md-1 col-sm-6 col-xs-12">
            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-footer.svg" alt="">
            </div>
            
        <div class="txt col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <p>Stir is an initiative of the CBR Innovation Network, and was developed by creatives for creatives. If you would like to support Stir, please get in touch through <a href="mailto:crew@causeastir.com.au">crew@causeastir.com.au</a></p>
            <br><br>
        <p>Designed by Ninpo Design</p>
        </div>
        <div class="menu col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <ul>
                <li><a href="<?php echo get_option('siteurl'); ?>/about">What is Stir?</a></li>
                <li><a href="<?php echo get_option('siteurl'); ?>/contact">Contact</a></li>
                <li><a href="<?php echo get_option('siteurl'); ?>/copyright">Copyright</a></li>
                <li><a href="<?php echo get_option('siteurl'); ?>/privacy">Privacy</a></li>
                <li><a href="<?php echo get_option('siteurl'); ?>/terms-and-conditions">Terms &amp; Conditions</a></li>
                    
                <li><a>Our</a> <a href="https://www.facebook.com/causeastirCBR" target="_blank"><i class="fa fa-facebook-square fa-lg"></i></a> <a>,</a> <a href="https://twitter.com/StirCBR" target="_blank"><i class="fa fa-twitter fa-lg"></i></a> <a>&amp;</a>&nbsp;<a href="https://www.instagram.com/causeastircbr" target="_blank"><i class="fa fa-instagram fa-lg"></i></a></li>
            </ul>
        </div>
            <div id="newsletter_footer" class="col-lg-6">
                <h2>FORTNIGHTLY NEWSLETTER</h2>
                <p>Articles, toolkits, opinion pieces, upcoming events and grant rounds, platform updates and new projects. </p>
                <br>
                
             
                <!-- Begin MailChimp Signup Form -->
<div id="mc_embed_signup">
<form action="https://causeastir.us11.list-manage.com/subscribe/post?u=d8a353c369d09f86f438aaeed&amp;id=b5e35adbae" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate form-inline" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll" class="form-inline">
	
<div class="mc-field-group form-group">
	
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
    
    <input type="submit" value="[Sign me up]" name="subscribe" id="mc-embedded-subscribe" class="button cta-stir">
    
</div>
        
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_d8a353c369d09f86f438aaeed_b5e35adbae" tabindex="-1" value=""></div>
        
    <div class="clear">
        
        </div>
    </div>
</form>
</div>

<!--End mc_embed_signup-->
                
                
                
                
            </div>
    </footer>
    </div>

<? } ?>

   

    </div>
    <!--fullwrapper-->



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>var $base_url  = "<?php echo esc_url( home_url() );?>";</script>
    <?php wp_footer(); ?>
    
    
    
	<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-61388758-1', 'auto');
      ga('send', 'pageview');
    
    </script>    
    
  </body>
</html>