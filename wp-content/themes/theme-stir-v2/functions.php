<?php
//Options page ACF


if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
    acf_add_options_page('Home content');

}
add_filter('acf/options_page/settings', 'my_options_page_settings');
function my_options_page_settings( $options ){
    $options['title'] = ('Global');
    $options['pages'] = array(
        __('Home'),
        __('Footer'),
    );
    return $options;
}
/*
WooCommerce
*/
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

//ordenar los relationship de ACF por fecha 
function my_relationship_query( $args, $field, $post_id ) {
    // order by newest posts first
    $args['orderby'] = 'date';
    $args['order'] = 'DESC';
    // return
    return $args;
}
add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);

//remove p from images
function filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

add_filter('the_content', 'filter_ptags_on_images');

//para que funcionen los tags del custom post type con tag.php 
function wpse28145_add_custom_types( $query ) {
    if( is_tag() && $query->is_main_query() ) {

        // this gets all post types:
        $post_types = get_post_types();

        // alternately, you can add just specific post types using this line instead of the above:
        // $post_types = array( 'post', 'your_custom_type' );

        $query->set( 'post_type', $post_types );
    }
}
add_filter( 'pre_get_posts', 'wpse28145_add_custom_types' );

function twentythirteen_setup() {

    add_theme_support( 'automatic-feed-links' );

    add_theme_support( 'html5', array(
        'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
    ) );
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 604, 270, true );
    add_image_size('thumb-home-news', 800, 9999 );

    
    // This theme uses its own gallery styles.
    add_filter( 'use_default_gallery_style', '__return_false' );
}
add_action( 'after_setup_theme', 'twentythirteen_setup' );

function randomPassword() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function is_old_post($days = 5) {
    $days = (int) $days;
    $offset = $days*60*60*24;
    if ( get_post_time() < date('U') - $offset )
        return true;
    
    return false;
}

/*cambiando el slug de news*/
function add_custom_rewrite_rule() {
    if( ($current_rules = get_option('rewrite_rules')) ) {
        foreach($current_rules as $key => $val) {
            if(strpos($key, 'news') !== false) {
                add_rewrite_rule(str_ireplace('news', 'blog', $key), $val, 'top');             } // end if
        } // end foreach
    } // end if/else
    flush_rewrite_rules();
} // end add_custom_rewrite_rule
add_action('init', 'add_custom_rewrite_rule');


/*Inicia Widgets*/
/**
 * Register our sidebars and widgetized areas.
 *
 */
function widgets_zone() {
    register_sidebar( array(
        'name'          => 'Home right sidebar',
        'id'            => 'home_right_1',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name'          => 'Forums Sidebar',
        'id'            => 'forums_sidebar',
        'before_widget' => '<div class="box fleft fwidth modulo-widget-sidebar">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );
    register_sidebar( array(
        'name'          => 'Login Ajax Header',
        'id'            => 'login_ajax_header',
    ) );
}
add_action( 'widgets_init', 'widgets_zone' );
//excerpt
function excerpt($limit) {
      $excerpt = explode(' ', get_the_excerpt(), $limit);
      if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
      } else {
        $excerpt = implode(" ",$excerpt);
      } 
      $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
      return $excerpt;
    }
function content($limit) {
      $content = explode(' ', get_the_content(), $limit);
      if (count($content)>=$limit) {
        array_pop($content);
        $content = implode(" ",$content).'...';
      } else {
        $content = implode(" ",$content);
      } 
      $content = preg_replace('/\[.+\]/','', $content);
      $content = apply_filters('the_content', $content); 
      $content = str_replace(']]>', ']]&gt;', $content);
      return $content;
    }


function enqueue_teamvisuals() {
    if( is_page( array( 'team','visuals' )) )
    {
        
    wp_register_style('st-tagsinputcss', get_template_directory_uri() . '/css/bootstrap-tagsinput.css');
    wp_enqueue_style('st-tagsinputcss');

    wp_register_script( 'st-fileinputjs', get_template_directory_uri() . '/js/bootstrap-filestyle.min.js','','null',true );  
    wp_enqueue_script( 'st-fileinputjs');

    wp_register_script( 'st-duplicate', get_template_directory_uri() . '/js/jquery.duplicate.min.js','','null',true );  
    wp_enqueue_script( 'st-duplicate');

    wp_register_script( 'st-tagsinput', get_template_directory_uri() . '/js/bootstrap-tagsinput.js','','null',true );  
    wp_enqueue_script( 'st-tagsinput'); 

    }
}
add_action( 'wp_enqueue_scripts', 'enqueue_teamvisuals' );


//Scripts & styles 
function stir_scripts_styles() {
    //styles
    
    wp_register_style('st-flexslider', get_template_directory_uri() . '/css/flexslider.css');
    wp_enqueue_style('st-flexslider');
    
    wp_register_style('st-normalize', get_template_directory_uri() . '/css/normalize.css');
    wp_enqueue_style('st-normalize');

    wp_register_style('st-font-awesome', get_template_directory_uri() . '/css/font-awesome.css');
    wp_enqueue_style('st-font-awesome');
    
    wp_register_style('st-toggle-css', get_template_directory_uri() . '/css/bootstrap-toggle.min.css');
    wp_enqueue_style('st-toggle-css');
    
    wp_register_style('st-materializecss', get_template_directory_uri() . '/css/materialize.css');
    wp_enqueue_style('st-materializecss');
    
    wp_enqueue_style('st-bootstrap', get_template_directory_uri() . '/css/bootstrap.css');
    
    wp_register_style('st-bstsocial', get_template_directory_uri() . '/css/bootstrap-social.css');
    wp_enqueue_style('st-bstsocial');    
    
    
    
    //scripts
    wp_enqueue_script( 'st-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js','','null',true );
    
    wp_register_script( 'st-flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js','','null',true );
    wp_enqueue_script( 'st-flexslider');
    
    wp_enqueue_script( 'st-doubletaptogo', get_template_directory_uri() . '/js/doubletaptogo.js','','null',true );
    wp_enqueue_script( 'st-bigSlide', get_template_directory_uri() . '/js/bigSlide.min.js','','null',true );
    
    wp_register_script( 'st-toggle-js', get_template_directory_uri() . '/js/bootstrap-toggle.min.js','','null',true );  
    wp_enqueue_script( 'st-toggle-js'); 
    
    wp_register_script( 'st-unstickyjs', get_template_directory_uri() . '/js/jquery.sticky-kit.min.js','','null',true );  
    wp_enqueue_script( 'st-unstickyjs'); 
    
    wp_enqueue_script( 'st-materialize', get_template_directory_uri() . '/js/materialize.js','','null',true );

    
    wp_enqueue_script( 'st-validation', 'http://cdn.jsdelivr.net/jquery.validation/1.14.0/jquery.validate.js','','null',true );
    
    wp_register_script( 'st-aditionalmethods', 'http://cdn.jsdelivr.net/jquery.validation/1.14.0/additional-methods.js','','null',true );
    wp_enqueue_script( 'st-aditionalmethods');
    
    
    wp_register_script( 'st-allscripts', get_template_directory_uri() . '/js/scripts.js','','null',true );
    wp_enqueue_script( 'st-allscripts'); 
    
    wp_enqueue_script( 'st-app', get_template_directory_uri() . '/js/app.js','','1.1',true );
    
    wp_enqueue_script( 'st-comment', get_template_directory_uri() . '/js/comment.js','','null',true ); 
}
add_action( 'wp_enqueue_scripts', 'stir_scripts_styles' );

function enqueue_front() {
    if( is_front_page() )
    {
        wp_register_style('st-frontcss', get_template_directory_uri() . '/frontpage.css');
        wp_enqueue_style('st-frontcss');
    }
}
add_action( 'wp_enqueue_scripts', 'enqueue_front' );

function project_dequeue_login() {
    if( ! is_page( array( 'login', 'register','basics','details','visuals','team','plan' )) )
    {
        wp_dequeue_style( 'st-materializecss' );
        wp_deregister_style( 'st-materializecss' );
    }
}
add_action( 'wp_enqueue_scripts', 'project_dequeue_login' );


function login_redirect( $redirect_to, $request, $user ){ 
     return home_url('home'); 
 } 
add_filter( 'login_redirect', 'login_redirect', 10, 3 );

function project_dequeue_unnecessary_styles() {
    if( is_page( array( 'support' )) )
    {
        wp_dequeue_style( 'st-bxslider' );
        wp_deregister_style( 'st-bxslider' );
    }
}
add_action( 'wp_print_styles', 'project_dequeue_unnecessary_styles' );

function project_dequeue_unnecessary_scripts() {
    if( is_page( array( 'support' )) )
    {
        wp_dequeue_script( 'st-bxslider-js' );
        wp_deregister_script( 'st-bxslider-js' );
        wp_dequeue_script( 'st-pin' );
        wp_deregister_script( 'st-pin' );
        wp_dequeue_script( 'st-aditionalmethods' );
        wp_deregister_script( 'st-aditionalmethods');
    }
}
add_action( 'wp_print_scripts', 'project_dequeue_unnecessary_scripts' );

function enqueue_summer() {
    if( is_page('summer-of-stir') ) {
        wp_enqueue_style('st-summer', get_template_directory_uri() . '/summer-of-stir.css');
        wp_enqueue_script( 'st-paperfull', get_template_directory_uri() . '/js/libs/paper-full.min.js','','null',true );
        wp_enqueue_script( 'st-waves', get_template_directory_uri() . '/js/waves.js','','null',true );
        wp_enqueue_script( 'st-init-waves', get_template_directory_uri() . '/js/init-waves-summer.js','','null',true );
    }
}
add_action( 'wp_enqueue_scripts', 'enqueue_summer' );
 

function is_role($role="project"){
    $user = wp_get_current_user();
    if( in_array( $role, (array) $user->roles )) {
        //The user has the "author" role
        return true;
    }
    return false;
}
function is_acondition(){
    
    if(isset($_SESSION['project']['id'])){
        // si existe un projecto en curso & acepto terminos
        if(have_draft()){
                return true;
        }
        return false;
    } else{
        // si existe un projecto en curso & acepto terminos
        if(have_draft()){
            return true;
        }
        return false;
    }
    return false;
}

function have_draft(){
    $user = wp_get_current_user();
    $project = new WP_Query(array('post_type'=>'project','author'=>$user->ID,'post_status' => 'draft','post_per_page'=>1,'order'=>'DESC'));
    if($project->found_posts>0){
        if($project->have_posts()) { $project->the_post();
            $_SESSION['project']['id'] = get_the_ID();
            
            return true;
        }
    }
    @session_destroy(); 
    
    return false;
}
function publish_to_draft(){
    global $wpdb;
    if(isset(user_current_project()->ID)){
        if(!empty(user_current_project()->ID)){
            $wpdb->query( 
                $wpdb->prepare( 
                    "UPDATE $wpdb->posts SET post_status = 'draft' WHERE ID = %d", 
                    user_current_project()->ID
                )
            );
          }
     
    }
}


add_action('init', 'session_go');
function session_go() {
    if(!session_id()) {
        session_start();
    }
}

add_action('wp_ajax_project_save', 'project_save');
/*
    code 00: correcto
    code 01: error insert
    code 02: error funcion dinamica
    code 03: no acepto termino condiciones o no es un proyecto draft
 */
function project_save(){
    $_POST = clearHack($_POST);
    try {
        extract($_POST,EXTR_SKIP);
        call_user_func($step);
    } catch(Exception $e){
        die(json_encode(array('code'=>'02','message'=>$e->getMessage())));
    }
}
function elegibility(){
    if(!isset($_SESSION['project']['id'])){
        $_SESSION['project'] = array();
        $user = wp_get_current_user();
        if($post_id = wp_insert_post(array(
                'post_name' => sanitize_title("Temp Project {$user->ID}"),
                'post_title'=> "",
                'post_status'=>'draft',
                'post_type' => 'project',
                'post_author'=>$user->ID 
        ))){
            update_post_meta($post_id, 'conditions ','1');
            $id_item_id = get_mid_by_key($post_id,'conditions ');
            global $wpdb;
            $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
             VALUES ( $id_item_id, 'conditions' , 1,1 ,$post_id )");
            $_SESSION['project']['id'] = $post_id;
            die(json_encode(array('code'=>'00','url'=> get_permalink(14))));
        } else {
            die(json_encode(array('code'=>'01')));
        }
    }
    die(json_encode(array('code'=>'00')));
}

function basics(){
    if(have_draft()){
        try{

            $post_id = $_SESSION['project']['id'];

            $post_update = array(
                  'ID'           => $_SESSION['project']['id'],
                  'post_title' => $_POST['post_title'],
                  'post_name' => sanitize_title($_POST['post_title'])
              );
            wp_update_post($post_update);
            global $wpdb;
            update_post_meta($post_id, 'describe_project ',$_POST['describe']);
            $id_item_id = get_mid_by_key($post_id,'describe_project ');
            $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
            VALUES ( $id_item_id, 'describe_project' , 1,1 ,$post_id )");
            update_post_meta($post_id, 'type_project_product_or_service',0);
            update_post_meta($post_id, 'type_project_event_or_expo',0);
            update_post_meta($post_id, 'type_project_non_profit',0);
            if((int) $_POST['checkboxes']==1){
                update_post_meta($post_id, 'type_project_product_or_service',1);
                $id_item_id = get_mid_by_key($post_id,'type_project_product_or_service ');
                $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
                VALUES ( $id_item_id, 'type_project_product_or_service' , 1,1 ,$post_id )");
            } else if((int) $_POST['checkboxes']==2){
                update_post_meta($post_id, 'type_project_event_or_expo',1);
                $id_item_id = get_mid_by_key($post_id,'type_project_event_or_expo');
                $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
                VALUES ( $id_item_id, 'type_project_event_or_expo' , 1,1 ,$post_id )");
            } else if((int) $_POST['checkboxes']==3){
                update_post_meta($post_id, 'type_project_non_profit',1);
                $id_item_id = get_mid_by_key($post_id,'type_project_non_profit');
                $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
                VALUES ( $id_item_id, 'type_project_non_profit' , 1,1 ,$post_id )");
            }
            if(@is_array($_POST['file']['logo'])){
                if(isset($_POST['file']['logo'])){
                    update_post_meta($post_id, 'logo_project  ',end($_POST['file']['logo']));
                    $id_item_id = get_mid_by_key($post_id,'logo_project  ');
                    $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
                    VALUES ( $id_item_id, 'logo_project' , 1,1 ,$post_id )");
                }
            }

            if(@is_array($_POST['file']['visuals_project_display']) ){
                if(isset($_POST['file']['visuals_project_display'])){
                    delete_mf2($post_id, 'visuals_project_display');
                    add_post_meta($post_id, 'visuals_project_display',@end($_POST['file']['visuals_project_display']));
                    $id_item_id =  $wpdb->insert_id;
                    $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
                    VALUES ( $id_item_id, 'visuals_project_display' , 1,1 ,$post_id )");
                }
            }
            $contador = 0;
            $aditionalcontar = 1;
            if(@is_array($_POST['file']['visuals_project_aditional_photos'])){
                if(isset($_POST['file']['visuals_project_aditional_photos'])){
                    delete_mf2($post_id, 'visuals_project_aditional_photos');
                    foreach($_POST['file']['visuals_project_aditional_photos'] as $key => $value){
                            add_post_meta($post_id, 'visuals_project_aditional_photos',$_POST['file']['visuals_project_aditional_photos'][$key]);
                            $id_item_id =  $wpdb->insert_id;
                            $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
                            VALUES ( $id_item_id, 'visuals_project_aditional_photos' , $aditionalcontar,1 ,$post_id )");
                            $aditionalcontar++;
                    }
                }
            }

            if(isset($_POST['area'])){
                $areas = clearHack($_POST['area']);
                if(count($areas) != 0){
                    wp_delete_object_term_relationships( $post_id, 'eligible' );
                    wp_set_object_terms( $post_id, 'yes', 'eligible' );
                    wp_delete_object_term_relationships( $post_id, 'area' );
                    wp_set_post_terms( $post_id,  $areas, 'area' );
                } else {
                    wp_delete_object_term_relationships( $post_id, 'eligible' );
                    wp_set_object_terms( $post_id, 'no', 'eligible' );
                }
            } else {
                wp_delete_object_term_relationships( $post_id, 'area' );
                wp_delete_object_term_relationships( $post_id, 'eligible' );
                wp_set_object_terms( $post_id, 'no', 'eligible' );
            }

            delete_mf2($post_id,'choose_a_colour');
            insert_mf2($post_id,'choose_a_colour',$_POST['choose_a_colour']);
            
            $permalink = ($_POST['option']=="continue")? get_permalink(17):"";
            die(json_encode(array('code'=>'00','url'=>$permalink)));
         } catch(Exception $e){
            die(json_encode(array('code'=>'01','message'=>$e->getMessage())));
        }
    }
    die(json_encode(array('code'=>'03')));
}

function details(){
    if(have_draft()){
        try{
            global $wpdb;
            
            $post_id = (int) $_SESSION['project']['id'];    
            update_post_meta($post_id, 'detail_of_project_detailed',$_POST['detail_of_project_detailed']);
            $id_item_id = get_mid_by_key($post_id,'detail_of_project_detailed ');
            $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
            VALUES ( $id_item_id, 'detail_of_project_detailed' , 1,1 ,$post_id )");
            
            update_post_meta($post_id, 'detail_of_project_for',$_POST['detail_of_project_for']);
            $id_item_id = get_mid_by_key($post_id,'detail_of_project_for');
            $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
            VALUES ( $id_item_id, 'detail_of_project_for' , 1,1 ,$post_id )");
            
            update_post_meta($post_id, 'detail_of_project_for_description',$_POST['detail_of_project_for_description']);
            $id_item_id = get_mid_by_key($post_id,'detail_of_project_for_description');
            $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
            VALUES ( $id_item_id, 'detail_of_project_for_description' , 1,1 ,$post_id )");
            
            $serial = $_POST['detail_of_project_old_people'];
            update_post_meta($post_id, 'detail_of_project_old_people',$serial);
            $id_item_id = get_mid_by_key($post_id,'detail_of_project_old_people');
            $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
            VALUES ( $id_item_id, 'detail_of_project_old_people' , 1,1 ,$post_id )");
        
            if(@is_array($_POST['file'])){
                update_post_meta($post_id, 'logo_project  ',end($_POST['file']));
                $id_item_id = get_mid_by_key($post_id,'logo_project  ');
                $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
                VALUES ( $id_item_id, 'logo_project' , 1,1 ,$post_id )");
            }
            $permalink = ($_POST['option']=="continue")? get_permalink(19):"";
            die(json_encode(array('code'=>'00','url'=>$permalink)));
         } catch(Exception $e){
            die(json_encode(array('code'=>'01','message'=>$e->getMessage())));
        }
    }
    die(json_encode(array('code'=>'03')));
}

function visuals(){
    if(have_draft()){
        try{
            global $wpdb;
            $post_id = (int) $_SESSION['project']['id'];
            update_post_meta($post_id, 'visuals_project_url_video',$_POST['visuals_project_url_video']);
            $id_item_id = get_mid_by_key($post_id,'visuals_project_url_video ');
            $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
            VALUES ( $id_item_id, 'visuals_project_url_video' , 1,1 ,$post_id )");
            
            if(@is_array($_POST['visuals_project_find_project'])){
                $contador = 1;
                delete_mf2($post_id, 'visuals_project_find_project');
                foreach($_POST['visuals_project_find_project'] as $key => $value){
                    insert_mf2($post_id,'visuals_project_find_project',$_POST['visuals_project_find_project'][$key],$contador);
                    $contador++;
                }
                
            }
            $contador = 0;
            $aditionalcontar = 1;
            if(@is_array($_POST['file']['visuals_project_display'])  ){
                if(isset($_POST['file']['visuals_project_display'])){
                    delete_mf2($post_id, 'visuals_project_display');
                    add_post_meta($post_id, 'visuals_project_display',@end($_POST['file']['visuals_project_display']));
                    $id_item_id =  $wpdb->insert_id;
                    $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
                    VALUES ( $id_item_id, 'visuals_project_display' , 1,1 ,$post_id )");
                }
            }
           
            if(@is_array($_POST['file']['visuals_project_aditional_photos'])){
                if(isset($_POST['file']['visuals_project_aditional_photos'])){
                    delete_mf2($post_id, 'visuals_project_aditional_photos');
                    foreach($_POST['file']['visuals_project_aditional_photos'] as $key => $value){
                            add_post_meta($post_id, 'visuals_project_aditional_photos',$_POST['file']['visuals_project_aditional_photos'][$key]);
                            $id_item_id =  $wpdb->insert_id;
                            $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
                            VALUES ( $id_item_id, 'visuals_project_aditional_photos' , $aditionalcontar,1 ,$post_id )");
                            $aditionalcontar++;
                        
                    }
                }
            }

            if(@is_array($_POST['file']['logo'])){
                if( isset($_POST['file']['logo'] )){
                    update_post_meta($post_id, 'logo_project  ',end($_POST['file']['logo']));
                    $id_item_id = get_mid_by_key($post_id,'logo_project  ');
                    $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
                    VALUES ( $id_item_id, 'logo_project' , 1,1 ,$post_id )");
                }
            }

            $permalink = ($_POST['option']=="continue")? get_permalink(21):"";
            die(json_encode(array('code'=>'00','url'=>$permalink)));
        }catch(Exception $e){
            die(json_encode(array('code'=>'01','url'=>'','message'=>$e->getMessage())));
        }
    }
    die(json_encode(array('code'=>'03','url'=>'')));
}

function team (){
    if(have_draft()){
        try{
            global $wpdb;
            $post_id = (int) $_SESSION['project']['id'];

            delete_mf2($post_id,'team_project_old');
            insert_mf2($post_id,'team_project_old',$_POST['team_project_old']);

            delete_mf2($post_id,'team_project_location');
            insert_mf2($post_id,'team_project_location',$_POST['team_project_location']);

            delete_mf2($post_id,'team_project_sex');
            insert_mf2($post_id,'team_project_sex',$_POST['team_project_sex']);

            delete_mf2($post_id,'team_project_role');
            insert_mf2($post_id,'team_project_role',$_POST['team_project_role']);
            
            delete_mf2($post_id,'team_project_skills');
            insert_mf2($post_id,'team_project_skills',$_POST['team_project_skills']);
            
            delete_mf2($post_id,'team_project_team_collaborators');
            insert_mf2($post_id,'team_project_team_collaborators',$_POST['team_project_team_collaborators']);
            
            delete_mf2($post_id,'team_project_team_email');
            insert_mf2($post_id,'team_project_team_email',$_POST['team_project_team_email']);
            


            if(isset($_POST['file']['team_project_profile_picture'][0])){
                if(!empty($_POST['file']['team_project_profile_picture'][0])){
                    delete_mf2($post_id,'team_project_profile_picture');
                    insert_mf2($post_id,'team_project_profile_picture',@end($_POST['file']['team_project_profile_picture']));
                }   
            }

            if(isset($_POST['file']['team_project_proof_id'][0])){
                if(!empty($_POST['file']['team_project_proof_id'][0])){
                    delete_mf2($post_id,'team_project_proof_id');
                    insert_mf2($post_id,'team_project_proof_id',@end($_POST['file']['team_project_proof_id']));
                }   
            }
            if(isset($_POST['team_project_find_people'])){
                if (false === strpos($_POST['team_project_find_people'], '://')) {
                    $_POST['team_project_find_people'] = 'http://' . $_POST['team_project_find_people'];
                }
            }
            delete_mf2($post_id,'team_project_find_people');            
            insert_mf2($post_id,'team_project_find_people',$_POST['team_project_find_people']);

            delete_mf2($post_id,'member_project_other_people');
            insert_mf2($post_id,'member_project_other_people',$_POST['member_project_other_people']);
            if(!empty($_POST['member_project_project_password'])){
                delete_mf2($post_id,'member_project_project_password');
                insert_mf2($post_id,'member_project_project_password',$_POST['member_project_project_password']);
            }
            $contador = 1;
            

            delete_mf2($post_id,'other_member_project_role');
            delete_mf2($post_id,'other_member_project_age');
            delete_mf2($post_id,'other_member_project_gender');
            delete_mf2($post_id,'other_member_project_skills');         
            if(isset($_POST['other_member_project_role'])){
                foreach($_POST['other_member_project_role'] as $key => $value){
                    insert_mf2($post_id,'other_member_project_role',$_POST['other_member_project_role'][$key],1,$contador);
                    insert_mf2($post_id,'other_member_project_age',$_POST['other_member_project_age'][$key],1,$contador);
                    insert_mf2($post_id,'other_member_project_gender',$_POST['other_member_project_gender'][$key],1,$contador);
                    insert_mf2($post_id,'other_member_project_skills',$_POST['other_member_project_skills'][$key],1,$contador);
                    $contador++;
                }
            }
            $contador = 1;
            if(is_array($_POST['file']['other_member_project_picture'])){
                delete_mf2($post_id,'other_member_project_picture');
                foreach($_POST['file']['other_member_project_picture'] as $key => $value){
                    insert_mf2($post_id,'other_member_project_picture',$_POST['file']['other_member_project_picture'][$key],1,$contador);   
                    $contador++;
                }
            }
            $contador = 1;
            if(is_array($_POST['file']['other_member_project_proof_id'])){
                delete_mf2($post_id,'other_member_project_proof_id');
                foreach($_POST['file']['other_member_project_proof_id'] as $key => $value){
                    insert_mf2($post_id,'other_member_project_proof_id',$_POST['file']['other_member_project_proof_id'][$key],1,$contador); 
                    $contador++;
                }
            }
            
            $permalink = ($_POST['option']=="continue")? get_permalink(23):"";
            die(json_encode(array('code'=>'00','url'=>$permalink)));
        } catch(Exception $e){
            die(json_encode(array('code'=>'01','url'=>'','message'=>$e->getMessage())));
        }
    }
    die(json_encode(array('code'=>'03','url'=>'')));

}

function plan(){
    if(have_draft()){
        try{
            global $wpdb;
            $post_id = (int) $_SESSION['project']['id'];


            delete_mf2($post_id,'plan_project_stir_money');
            insert_mf2($post_id,'plan_project_stir_money',$_POST['plan_project_stir_money']);

            delete_mf2($post_id,'plan_project_support');
            insert_mf2($post_id,'plan_project_support',$_POST['plan_project_support']);

            delete_mf2($post_id,'plan_project_future');
            insert_mf2($post_id,'plan_project_future',$_POST['plan_project_future']);
            
            delete_mf2($post_id,'plan_project_current_status');
            insert_mf2($post_id,'plan_project_current_status',$_POST['plan_project_current_status']);
            
            
            $permalink = "";
            $permalink = ($_POST['option']=="continue")? get_permalink(41):$permalink;
            $permalink = ($_POST['option']=="publish")? get_permalink($post_id):$permalink;
            

            die(json_encode(array('code'=>'00','url'=>$permalink)));
        } catch(Exception $e){
            die(json_encode(array('code'=>'01','url'=>'','message'=>$e->getMessage())));
        }
    }
    die(json_encode(array('code'=>'03','url'=>'')));
}

function preview(){
    try{
        if(have_draft()){
            $post_id = (int) $_SESSION['project']['id'];
            $user = wp_get_current_user();
            $post_update = array(
                  'ID'           => $post_id,
                  'post_author'  => $user->ID 
              );
            $post_update['post_status'] = ($_POST['option'] =='publish')?'publish':'pending';
            wp_update_post($post_update);
            $permalink = "";
            if($_POST['option']=="publish"){
                $seasons_list = wp_get_post_terms($post_id, 'season', array("fields" => "all"));
                foreach( $seasons_list as $seasonmy){
                    if($seasonmy->slug != 's03' && $seasonmy->slug != 's02' && $seasonmy->slug != 's01'){
                        wp_delete_object_term_relationships( $post_id, 'season' );
                        wp_remove_object_terms( $post_id, 's04', 'season' );
                        wp_set_object_terms( $post_id, 's04', 'season' );
                        if( $areas = wp_get_post_terms($post_id, 'area', array("fields" => "all"))){
                            wp_delete_object_term_relationships($post_id, 'eligible');
                            wp_set_object_terms( $post_id, 'yes', 'eligible' );
                        }
                    }
                }
                die(json_encode(array('code'=>'00','url'=>get_permalink($post_id),'message'=>'Your project was published')));
            } else{
                die(json_encode(array('code'=>'00','url'=>$permalink,'message'=>'Your project was save')));
            }
        }
        
    } catch(Exception $e){
        die(json_encode(array('code'=>'01','url'=>'','message'=>$e->getMessage())));
    }
}

function get_project(){
    $post_id = (have_draft())? $_SESSION['project']['id'] : (int) $_GET['project_id'];
    $project = array('id'=>1,'post_title'=>'','logo'=>'','describe'=>'','detail_of_project_detailed'=>'','choose_a_colour'=>'',
        'detail_of_project_old_people'=>'','detail_of_project_for'=>'','detail_of_project_for_description'=>'','checkboxes'=>'','visuals_project_display'=>''
        ,'visuals_project_aditional_photos'=>"",'visuals_project_url_video'=>'','visuals_project_find_project'=>"",
        'team_project_old' => '','team_project_location'=>'', 'team_project_sex'=>'', 'team_project_role'=>'','team_project_skills'=>'',
        'team_project_profile_picture'=>'', 'team_project_find_people'=>'','member_project_other_people'=>'','member_project_project_password',
        'member_project_member_email'=>'','plan_project_stir_money'=>'','plan_project_support'=>'','plan_project_future'=>'','members'=>'',
        'team_project_proof_id'=>'','team_project_team_email'=>'','team_project_team_collaborators'=>'','plan_project_current_status'=>''
        );
    if($post_id != 0 && !empty($post_id)){
        $user = wp_get_current_user();
        
        wp_reset_query();
        wp_reset_postdata();
        $project = new WP_Query(array('post_type'=>'project','author'=>$user->ID,'p'=>$post_id,'post_status' => 'draft','post_per_page'=>1));
        if($project->found_posts > 0){
                if($project->have_posts()){ $project->the_post();
                    $checkboxes = 0;
                    if(get('type_project_non_profit')==1) $checkboxes = 3;
                    if(get('type_project_event_or_expo')==1) $checkboxes = 2;
                    if(get('type_project_product_or_service')==1) $checkboxes = 1;

                    $project = array(
                        'id'=>get_the_ID(),
                        'post_title'=>get_the_title(),
                        'logo' => get('logo_project'),
                        'describe'=> get('describe_project'),
                        'checkboxes' => $checkboxes,
                        'choose_a_colour'=>get('choose_a_colour'),
                        'detail_of_project_detailed'=> get('detail_of_project_detailed'),
                        'detail_of_project_old_people'=> get('detail_of_project_old_people'),
                        'detail_of_project_for'=> get('detail_of_project_for'),
                        'detail_of_project_for_description'=> get('detail_of_project_for_description'),
                        'visuals_project_display'=> get('visuals_project_display'),
                        'visuals_project_url_video'=>get('visuals_project_url_video'),
                        'team_project_old' => get('team_project_old'),
                        'team_project_location' => get('team_project_location'),
                        'team_project_sex'=> get('team_project_sex'),
                        'team_project_role'=> get('team_project_role'),
                        'team_project_skills'=> get('team_project_skills'),
                        'team_project_profile_picture'=> get('team_project_profile_picture'),
                        'team_project_find_people'=> get('team_project_find_people'),
                        'member_project_other_people'=> get('member_project_other_people'),
                        'member_project_project_password'=> get('member_project_project_password'),
                        'plan_project_stir_money'=> get('plan_project_stir_money'),
                        'plan_project_support'=>get('plan_project_support'),
                        'plan_project_future'=> get('plan_project_future'),
                        'team_project_proof_id'=>get('team_project_proof_id'),
                        'team_project_team_collaborators' => get('team_project_team_collaborators'),
                        'team_project_team_email' => get('team_project_team_email'),
                        'plan_project_current_status' => get('plan_project_current_status')
                    );
                    $visual_project_photo = get_order_field('visuals_project_aditional_photos');
                    foreach($visual_project_photo as $photo){
                        $project['visuals_project_aditional_photos'][] =get('visuals_project_aditional_photos',1,$photo);
                    }
                    $visuals_project_find_project = get_order_field('visuals_project_find_project');
                    foreach($visuals_project_find_project as $find){
                        $project['visuals_project_find_project'][] =get('visuals_project_find_project',1,$find);
                    }
                    $member_project_member_email = get_order_field('member_project_member_email');
                    foreach($member_project_member_email as $member){
                        $project['member_project_member_email'][] =get('member_project_member_email',1,$member);
                    } 
                    $contador = 0;
                    $other_member_project_role = get_order_group('other_member_project_role');
                    foreach($other_member_project_role as $member){
                        $project['members'][$contador]['other_member_project_role'] = get('other_member_project_role',$member);
                        $project['members'][$contador]['other_member_project_age'] = get('other_member_project_age',$member);
                        $project['members'][$contador] ['other_member_project_gender']= get('other_member_project_gender',$member);
                        $project['members'][$contador]['other_member_project_picture'] = get('other_member_project_picture',$member);
                        $project['members'][$contador]['other_member_project_skills'] = get('other_member_project_skills',$member);
                        $contador++;
                    }

                    if( $areas = wp_get_post_terms(get_the_ID(), 'area', array("fields" => "all"))){
                        foreach($areas as $area ){
                            $project['areas'][$area->term_id] = $area;
                        }
                    }


                }
            } 
    } 
    
    return $project;
    
}

add_action('wp_ajax_upload_files', 'upload_files');
function upload_files(){
    
    $imagesExts = array(
      'image/gif',
      'image/jpeg',
      'image/pjpeg',
      'image/png',
      'image/x-png'
    );
    $resp = array();

    foreach($_FILES as $key => $value){
        if(!empty($_FILES[$key]['tmp_name']) && !is_array($_FILES[$key]['tmp_name']) ){
             if(in_array($_FILES[$key]["type"],$imagesExts)){
                if($_FILES[$key]['size'] <4194304){
                     $special_chars = array(' ','`','"','\'','\\','/'," ","#","$","%","^","&","*","!","~","‘","\"","’","'","=","?","/","[","]","(",")","|","<",">",";","\\",",","+","-");
                      $filename = str_replace($special_chars,'',$_FILES[$key]['name']);
                      $filename = round(microtime(true) * 100) . $filename;
                      @move_uploaded_file( $_FILES[$key]['tmp_name'], MF_FILES_DIR . $filename );
                      @chmod(MF_FILES_DIR . $filename, 0644);
                      $info = pathinfo(MF_FILES_DIR . $filename);
                      $resp[] = array(
                        'error' => false, 
                        'name' => $filename,
                        'file_path' => MF_FILES_DIR . $filename,
                        'file_url' => MF_FILES_URL . $filename,
                        'key_file' => $key,
                        'index' => false,
                        'encode_file_url' => urlencode(MF_FILES_URL . $filename)
                      );
                } else{
                    unset($_FILES[$key]);
                }
            } else{
                unset($_FILES[$key]);
            }
        } else if(!empty($_FILES[$key]['tmp_name']) && is_array($_FILES[$key]['tmp_name']) ){
                foreach($_FILES[$key]['tmp_name'] as $keyc => $valuec){
                    if(in_array($_FILES[$key]["type"][$keyc],$imagesExts)){ 
                        if($_FILES[$key]['size'][$keyc] <4194304){
                          $special_chars = array(' ','`','"','\'','\\','/'," ","#","$","%","^","&","*","!","~","‘","\"","’","'","=","?","/","[","]","(",")","|","<",">",";","\\",",","+","-");
                          $filename = str_replace($special_chars,'',$_FILES[$key]['name'][$keyc]);
                          $filename = round(microtime(true) * 100) . $filename;
                          @move_uploaded_file( $_FILES[$key]['tmp_name'][$keyc], MF_FILES_DIR . $filename );
                          @chmod(MF_FILES_DIR . $filename, 0644);
                          $info = pathinfo(MF_FILES_DIR . $filename);
                          $resp[] = array(
                            'error' => false, 
                            'name' => $filename,
                            'file_path' => MF_FILES_DIR . $filename,
                            'file_url' => MF_FILES_URL . $filename,
                            'key_file' => $key,
                            'index' => $keyc,   
                            'encode_file_url' => urlencode(MF_FILES_URL . $filename)
                          );
                        }
                    }
                }
            
        }
         
    }
    die(json_encode($resp));
}
function get_mid_by_key( $post_id, $meta_key ) {
  global $wpdb;
  $mid = $wpdb->get_var( $wpdb->prepare(
        "SELECT meta_id FROM $wpdb->postmeta WHERE post_id = %d AND meta_key = %s", $post_id, $meta_key)
     );
  if( $mid != '' )
    return (int)$mid;
 
  return false;
}


function insert_mf2($post_id,$meta_key,$meta_value,$field_count=1,$group_count=1){
    global $wpdb;
    try{
        add_post_meta($post_id, $meta_key ,$meta_value);
        $id_item_id =  $wpdb->insert_id;
    
        $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
        VALUES ( $id_item_id, '$meta_key' , $field_count,$group_count ,$post_id )");
        return true;
    } catch(Exception $e){
        return false;
    }
    return false;
}

function delete_mf2($post_id, $meta_key){
    global $wpdb;
    try{
        $wpdb->query( "DELETE FROM ". MF_TABLE_POST_META ." WHERE post_id= {$post_id} 
                    and field_name='$meta_key' " );
        delete_post_meta($post_id, $meta_key);
        return true;
    } catch(Exception $e){
        return false;
    }
    return false;
}

add_filter('pre_get_posts', 'posts_for_current_author');
function posts_for_current_author($query) {
    global $post;
    if($post->post_type=='project'){
        if($query->is_admin && is_role('project')) {
            global $user_ID;
            $query->set('author',  $user_ID);
        } else{
            unset($query->author);
        }
    }
    return $query;
}

add_action('wp_insert_post_data', 'prevent_assign_author', '99', 2);
function prevent_assign_author($data,$postarr ){
    if($postarr['post_type']=='project' && is_admin()){
        $author_id = get_post_field( 'post_author', $postarr['ID'] );
        if($author_id != 0 && isset($author_id)){
            $data['post_author'] = $author_id;
        }
    }
    return $data;
}
function clearHack($_post){
    foreach ($_post as $key => $value){
        if(is_array($_post[$key])){
            $_post[$key]=clearHack($_post[$key]);
        }else{
            $_post[$key] = xss_clean($value);
        }
    }
    return $_post;
}
function xss_clean($data)
{
    // Fix &entity\n;
    $data = str_replace(array('&amp;','&lt;','&gt;'), array('&amp;amp;','&amp;lt;','&amp;gt;'), $data);
    $data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
    $data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
    $data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

    // Remove any attribute starting with "on" or xmlns
    $data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

    // Remove javascript: and vbscript: protocols
    $data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
    $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
    $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

    // Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
    $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
    $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
    $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

    // Remove namespaced elements (we do not need them)
    $data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

    do
    {
            // Remove really unwanted tags
            $old_data = $data;
            $data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml|script)[^>]*+>#i', '', $data);
    }
    while ($old_data !== $data);

    // we are done...
    return $data;
}
function strposa($haystack, $needles=array(), $offset=0) { 
    $chr = array(); 
    foreach($needles as $key => $needle) {
        $res = strpos($haystack, $needle); 
        if ($res !== false) $chr[] = $needle; 
    } 
    if(count($chr) > 0){
         return json_encode($chr);
     } else{ 
        return false;
     } 
}
/*
 *  code 00: support
 *  code 01: isset support 
 *  code 02: not support you project
 *  code 03: catch out
 */
add_action('wp_ajax_support', 'support');
function support(){
    global $wpdb;
    $user = wp_get_current_user();
    $post_id = (int) $_POST["post_id"];
    try{
        $support  = $wpdb->get_var( "SELECT COUNT(*) FROM wp_project_support 
            WHERE id_project=".$post_id." and id_user=".(int)$user->ID );
        $project = new WP_Query(array('post_type'=>'project','author'=>$user->ID,
            'post_status' => array('publish','draft'),'p'=>$post_id));
        
        if($project->found_posts==0){
            if($support==0){
                $wpdb->insert( 
                'wp_project_support', 
                    array( 
                        'id_user' => $user->ID, 
                        'id_project' => $post_id,
                        'age' => '',//$_POST['age'],
                        'cool' => '',//$_POST['cool'],
                        'clear' => '',//$_POST['clear'],
                        'capable'=> '',//$_POST['capable'],
                        'support'=> '',//$_POST['support'],
                        'comment' => $_POST['comment'],
                        'city'  => '',//$_POST['city'],
                        'date' => date("Y-m-d H:i:s")
                    ), 
                    array( 
                        '%d', 
                        '%d',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s'
                    ) 
                );
                update_post_meta( $post_id, '_count_support', support_count($post_id)); 
                die(json_encode(array('code'=>'00','message'=>'<span>Thanks for supporting this project</span>','state'=>'success')));
            }
            die(json_encode(array('code'=>'01','message'=>'<span>You have already supported this project</span>','state'=>'error')));
        } else{
            die(json_encode(array('code'=>'02','message'=>'<span>You can&#39;t support your own project</span>','state'=>'error')));
        }
    } catch(Exception $e){
        die(json_encode(array('code'=>'03','message'=>$e->getMessage(),'state'=>'error')));
    }
}
function user_publishproject(){
    $user = wp_get_current_user();
    $args = array(
        'author'      => $user->ID,
        'post_status' => array(
            'publish',
            'pending'
            
        ),
        'post_type'=>'project'
    );
    $posts = new WP_Query($args);
    
    $post_count = $posts->found_posts;
    if($post_count>0)
        return true;
    return false;
    
}
function support_count($id_project=0){
    global $wpdb;
    $count  = $wpdb->get_var( "SELECT COUNT(*) FROM wp_project_support WHERE id_project=".$id_project);
    return $count;
}
function support_project($id_project= 0){
    global $wpdb;
    $support =  $wpdb->get_results( "SELECT * FROM wp_project_support WHERE id_project=".$id_project, OBJECT);
    return $support;
}
function stat_project($id_project=0,$market_age=""){
    global $wpdb;
    $support =  $wpdb->get_results( "SELECT * FROM wp_project_support  ", OBJECT);
    $market_support = $market_age;
    $count = 0;
    $count_all = 0;
    $all_market_cool =0;
    $all_market_clear =0;
    $all_market_capable =0;
    $total_market_cool = 0;
    $total_market_clear = 0;
    $total_market_capable = 0;
    foreach($support as $supported){
        

        if(is_array($market_support)){
             if(in_array( "0-5 (TODDLERS)",$market_support) && ($supported->age >= 0 || $supported->age <=5) && $id_project== $supported->id_project ){
                $total_market_cool = $total_market_cool + $supported->cool;
                $total_market_clear = $total_market_clear + $supported->clear;
                $total_market_capable = $total_market_capable + $supported->capable; 
                $count++;
            }
            if( in_array("6-12 (CHILDREN)",$market_support) &&($supported->age >= 6 || $supported->age <=12) && $id_project== $supported->id_project){
                $total_market_cool = $total_market_cool + $supported->cool;
                $total_market_clear = $total_market_clear + $supported->clear;
                $total_market_capable = $total_market_capable + $supported->capable; 
                $count++;
            }
            if( in_array("13-17 (TEENAGERS)",$market_support) &&($supported->age >= 13 || $supported->age <=17) && $id_project== $supported->id_project){
                $total_market_cool = $total_market_cool + $supported->cool;
                $total_market_clear = $total_market_clear + $supported->clear;
                $total_market_capable = $total_market_capable + $supported->capable; 
                $count++;
            }
             if(in_array("18-25 (YOUNG ADULTS)", $market_support) && ($supported->age >= 18 || $supported->age <=25) && $id_project== $supported->id_project){
                $total_market_cool = $total_market_cool + $supported->cool;
                $total_market_clear = $total_market_clear + $supported->clear;
                $total_market_capable = $total_market_capable + $supported->capable; 
                $count++;
            }
            if( in_array("26-35 (ADULTS)",$market_support) &&($supported->age >= 26 || $supported->age <=35) && $id_project== $supported->id_project){
                $total_market_cool = $total_market_cool + $supported->cool;
                $total_market_clear = $total_market_clear + $supported->clear;
                $total_market_capable = $total_market_capable + $supported->capable; 
                $count++;
            }
             if(in_array("36-50 (MIDDLE-AGED)",$market_support) &&($supported->age >= 36 || $supported->age <=50) && $id_project== $supported->id_project){
                $total_market_cool = $total_market_cool + $supported->cool;
                $total_market_clear = $total_market_clear + $supported->clear;
                $total_market_capable = $total_market_capable + $supported->capable; 
                $count++;
            }
            if( in_array("50-65 (MATURE)",$market_support) &&($supported->age >= 50 || $supported->age <=65) && $id_project== $supported->id_project){
                $total_market_cool = $total_market_cool + $supported->cool;
                $total_market_clear = $total_market_clear + $supported->clear;
                $total_market_capable = $total_market_capable + $supported->capable; 
                $count++;
            }
            if( in_array("66+ (SENIOR)",$market_support) &&($supported->age >= 66) && $id_project== $supported->id_project){
                $total_market_cool = $total_market_cool + $supported->cool;
                $total_market_clear = $total_market_clear + $supported->clear;
                $total_market_capable = $total_market_capable + $supported->capable; 
                $count++;
            }
        } else{
            if($market_support == "0-5 (TODDLERS)" && ($supported->age >= 0 || $supported->age <=5) && $id_project== $supported->id_project ){
                $total_market_cool = $total_market_cool + $supported->cool;
                $total_market_clear = $total_market_clear + $supported->clear;
                $total_market_capable = $total_market_capable + $supported->capable; 
                $count++;
            }
            if( $market_support=="6-12 (CHILDREN)" &&($supported->age >= 6 || $supported->age <=12) && $id_project== $supported->id_project){
                $total_market_cool = $total_market_cool + $supported->cool;
                $total_market_clear = $total_market_clear + $supported->clear;
                $total_market_capable = $total_market_capable + $supported->capable; 
                $count++;
            }
            if( $market_support=="13-17 (TEENAGERS)" &&($supported->age >= 13 || $supported->age <=17) && $id_project== $supported->id_project){
                $total_market_cool = $total_market_cool + $supported->cool;
                $total_market_clear = $total_market_clear + $supported->clear;
                $total_market_capable = $total_market_capable + $supported->capable; 
                $count++;
            }
             if( $market_support=="18-25 (YOUNG ADULTS)" &&($supported->age >= 18 || $supported->age <=25) && $id_project== $supported->id_project){
                $total_market_cool = $total_market_cool + $supported->cool;
                $total_market_clear = $total_market_clear + $supported->clear;
                $total_market_capable = $total_market_capable + $supported->capable; 
                $count++;
            }
            if( $market_support=="26-35 (ADULTS)" &&($supported->age >= 26 || $supported->age <=35) && $id_project== $supported->id_project){
                $total_market_cool = $total_market_cool + $supported->cool;
                $total_market_clear = $total_market_clear + $supported->clear;
                $total_market_capable = $total_market_capable + $supported->capable; 
                $count++;
            }
             if( $market_support=="36-50 (MIDDLE-AGED)" &&($supported->age >= 36 || $supported->age <=50) && $id_project== $supported->id_project){
                $total_market_cool = $total_market_cool + $supported->cool;
                $total_market_clear = $total_market_clear + $supported->clear;
                $total_market_capable = $total_market_capable + $supported->capable; 
                $count++;
            }
            if( $market_support=="50-65 (MATURE)" &&($supported->age >= 50 || $supported->age <=65) && $id_project== $supported->id_project){
                $total_market_cool = $total_market_cool + $supported->cool;
                $total_market_clear = $total_market_clear + $supported->clear;
                $total_market_capable = $total_market_capable + $supported->capable; 
                $count++;
            }
            if( $market_support=="66+ (SENIOR)" &&($supported->age >= 66) && $id_project== $supported->id_project){
                $total_market_cool = $total_market_cool + $supported->cool;
                $total_market_clear = $total_market_clear + $supported->clear;
                $total_market_capable = $total_market_capable + $supported->capable; 
                $count++;
            }

        }
        $all_market_cool = $all_market_cool + $supported->cool;
        $all_market_clear = $all_market_clear + $supported->clear;
        $all_market_capable = $all_market_capable + $supported->capable;                                   
        $count_all++;


    }
    $stat['count_project'] = $count;
    $stat['count_all_project'] = $count_all;
    $stat['all_market_cool'] = $all_market_cool;
    $stat['all_market_clear'] = $all_market_clear;
    $stat['all_market_capable'] = $all_market_capable;
    $stat['total_market_cool'] = $total_market_cool;
    $stat['total_market_clear'] = $total_market_clear;
    $stat['total_market_capable'] = $total_market_capable;
    return $stat;

}
add_action( 'admin_menu', 'adjust_the_wp_menu', 999 );
function adjust_the_wp_menu() {
  if(is_role('project')){
    $page = remove_menu_page('edit.php?post_type=project' );
    $page = remove_submenu_page( 'edit.php?post_type=project', 'post-new.php?post_type=project' );
    $page = remove_submenu_page( 'edit.php?post_type=project', 'edit.php?post_type=project' );
    
  }
}
function hide_buttons()
{
    global $current_screen;
    if(is_role('project')){
        if($current_screen->id == 'page');
        {
            echo '<style>.add-new-h2{display: none;}.wrap .subsubsub{display:none;}#wp-admin-bar-new-content{display:none;}</style>';
        }
        show_admin_bar(false);
    }
// for posts the if statement would be:
// if($current_screen->id == 'edit-post' && !current_user_can('publish_posts'))
}
add_action('admin_head','hide_buttons');

add_action('after_setup_theme', 'remove_admin_bar');

// auto login
/*function auto_login_new_user( $user_id ) {
        wp_set_current_user($user_id);
        wp_set_auth_cookie($user_id);
            // You can change home_url() to the specific URL,such as 
        //wp_redirect( 'http://www.wpcoke.com' );
        wp_redirect( 'http://causeastir.com.au/#welcome' );
        exit;
    }
add_action( 'user_register', 'auto_login_new_user' );
*/  

add_filter( 'bbp_verify_nonce_request_url', 'my_bbp_verify_nonce_request_url', 999, 1 );
function my_bbp_verify_nonce_request_url( $requested_url )
{
    return 'http://localhost:8888' . $_SERVER['REQUEST_URI'];
}

function remove_admin_bar() {

    if (!current_user_can('administrator') && !is_admin()) {
      show_admin_bar(false);
    }
}

function user_current_project(){
    $user = wp_get_current_user();
    $project = new WP_Query(array('post_type'=>'project','post_status' => 'any','author'=>$user->ID,'post_per_page'=>1));
    return $project->post;
}

add_action( 'admin_menu', 'register_menu_my_project' );

function register_menu_my_project() {
    if(is_role('project')){
        add_menu_page( 'My Project', 'My Project', 'edit_projects','/post.php?post='.user_current_project()->ID.'&action=edit', '','dashicons-welcome-write-blog',10 );
    }
}

add_action('init', 'admin_project_access');
function admin_project_access() {
    if( defined('DOING_AJAX') && DOING_AJAX ) {
            //Allow ajax calls
            return;
     }  
    
    if(is_admin() ){
        
        if(is_role('project')){
            
            $current_user = user_current_project();
            wp_redirect(get_bloginfo('url'));
        }
    }
}
add_action( 'template_redirect', 'redirect_role_project' );
function redirect_role_project(){
    if(is_page('login') && is_user_logged_in() ){
        
        if(is_role('project')){
            
            $current_user = user_current_project();
            wp_redirect(get_bloginfo('url'));
        }
    }
}

function support_orderby_desc($orderby) {
    global $wpdb;
    $orderby = "wp_postmeta.meta_value+0 DESC";
    return $orderby;
}

if(is_role('administrator')){
      add_action( 'save_post', 'featured_project_save_post', 10, 2 );
      add_action( 'add_meta_boxes', 'featured_project_add_post_meta_boxes' );
}

function featured_project_add_post_meta_boxes() {
          add_meta_box(
            'super-feature-post',      // Unique ID
            esc_html__( 'Feature Post', 'Feature Post' ),    // Title
            'feature_post_fc',   // Callback function
            'project',         // Admin page (or post type)
            'side',         // Context
            'default'         // Priority
          );
}


function feature_post_fc( $object, $box ) { ?>

  <?php wp_nonce_field( basename( __FILE__ ), 'smashing_post_class_nonce' ); ?>

  <p><?php $featured =  get_post_meta( $object->ID, 'featured_project', true ) ;  ?>
    <label for="featured_project"><?php _e( "Featured Project", 'example' ); ?></label> &nbsp;&nbsp;<input type="checkbox"  name="featured_project" id="featured_project" value="1" <?php if($featured=="1") echo "checked";?>>
   
  </p>
<?php }

function featured_project_save_post( $post_id, $post ) {
  /* Verify the nonce before proceeding. */

  if ( !isset( $_POST['featured_project'] )  )
        $_POST['featured_project'] ='2';

  /* Get the post type object. */
  $post_type = get_post_type_object( $post->post_type );

  /* Check if the current user has permission to edit the post. */
  if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
    return $post_id;

  /* Get the posted data and sanitize it for use as an HTML class. */
  $new_meta_value = ( isset( $_POST['featured_project'] ) ? sanitize_html_class( $_POST['featured_project'] ) : '' );

  /* Get the meta key. */
  $meta_key = 'featured_project';

  /* Get the meta value of the custom field key. */
  $meta_value = get_post_meta( $post_id, $meta_key, true );

  /* If a new meta value was added and there was no previous value, add it. */
  if ( $new_meta_value && '' == $meta_value )
    add_post_meta( $post_id, $meta_key, $new_meta_value, true );

  /* If the new meta value does not match the old value, update it. */
  elseif ( $new_meta_value && $new_meta_value != $meta_value )
    update_post_meta( $post_id, $meta_key, $new_meta_value );

  /* If there is no new meta value but an old value exists, delete it. */
  elseif ( '' == $new_meta_value && $meta_value )
    delete_post_meta( $post_id, $meta_key, $meta_value );
}


function wpse18703_posts_where( $where, &$wp_query )
{
    global $wpdb;
    if ( $wpse18703_title = $wp_query->get( 'project_title' ) ) {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'' . esc_sql( $wpdb->esc_like( $wpse18703_title ) ) . '%\'';
    }
    return $where;
}
add_filter( 'posts_where', 'wpse18703_posts_where', 10, 2 );

function getYoutubeIdFromUrl($url) {
    $parts = parse_url($url);
    if(isset($parts['query'])){
        parse_str($parts['query'], $qs);
        if(isset($qs['v'])){
            return $qs['v'];
        }else if(isset($qs['vi'])){
            return $qs['vi'];
        }
    }
    if(isset($parts['path'])){
        $path = explode('/', trim($parts['path'], '/'));
        return $path[count($path)-1];
    }
    return false;
}
function get_vimeo_id($url) {
    return (int) substr(parse_url($url, PHP_URL_PATH), 1);
}

function getFrameVideo($url) {
    if(!empty($url)){
        if (strpos($url, 'youtube') > 0) {
            return '<iframe id="ytplayer"  width="100%" height="390" src="http://www.youtube.com/embed/'.getYoutubeIdFromUrl($url).'"></iframe>';
        } elseif (strpos($url, 'vimeo') > 0) {
            return '<iframe src="//player.vimeo.com/video/'.get_vimeo_id($url).'" width="100%" height="390" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
        } else {
            return '';
        }
    }
}

add_action('wp_logout','go_home');
function go_home(){
  wp_redirect( home_url() );
  exit();
}

add_action('admin_menu', 'menu_review');
function menu_review(){
    add_menu_page('Support Project', 'Support Project', 'edit_pages', 'review-support', 'review_support','dashicons-admin-page',64 );
    add_submenu_page('review-support', 'Review Support', 'Review Support', 'edit_pages', 'review-support','review_support' );
    add_submenu_page('review-support', 'Count Support', 'Count Support', 'edit_pages', 'count-support','count_support' );
    add_menu_page('Donate Project', 'Donate Project', 'edit_pages', 'donate-project', 'donate_project','dashicons-admin-page',65);
    add_submenu_page('Donate Project', 'Donate Project', 'Donate Project', 'edit_pages', 'donate-project','donate_project' );
}
function donate_project(){

global $wpdb;
    

    $paged = (isset($_GET['paged']) && $_GET['paged']>0) ? (int) $_GET['paged'] : 1;
    $termino = (isset($_GET['termino'])) ? @$_GET['termino'] : @$_POST['termino'];
    $post_per_page = 20; //6
    $offset = ($paged - 1)*$post_per_page;
    
    $query = "SELECT wpr.*, wpo.post_title, wpo.ID as id_project FROM  {$wpdb->prefix}project_donate as wpr
              LEFT JOIN {$wpdb->prefix}posts as wpo ON wpr.id_project = wpo.ID
              ORDER by wpr.id DESC
              ";
    
    $total_result = count($wpdb->get_results( $query, OBJECT)); 
    $max_num_pages = ceil($total_result / $post_per_page);  
    
    $query = $query . " LIMIT ".$offset.", ".$post_per_page."; ";
    
    $donates = $wpdb->get_results($query,OBJECT);
    
    ?>
    <style> 
        .button-primary.red,.button-primary.red:hover{ background:#FF424F;border-color:#AC0037;box-shadow:none;-webkit-box-shadow:none }
        .filtro{margin-top:10px; margin-bottom: 10px;}
    </style>
    <div class="wrap">
    <h2>Project Donate</h2><br/>
    <?php if(count($donates)>0){;?>
    <table class="wp-list-table widefat fixed posts">
        <thead>
            <tr>
                <th class="manage-column" width="50">#</th>
                
                <th class="manage-column">ID PayPal Donate</th>
                <th class="manage-column">Email Donate</th>
                <th class="manage-column">Name Donate</th>
                <th class="manage-column">Country Donate</th>
                <th class="manage-column">Zip Donate</th>
                <th class="manage-column">State Donate</th>
                <th class="manage-column">City Donate</th>
                <th class="manage-column">Street Donate</th>
                <th class="manage-column">Amount Donate</th>                
                <th class="manage-column">Amount Shipping</th>
                <th class="manage-column">Payment Amount</th>
                <th class="manage-column">Project</th>
                <th class="manage-column">Owner</th>
                <th class="manage-column">Date Operation</th>
                
            </tr>
        </thead>
        <tbody>
    <?php
        $contador=0;

        foreach($donates as $donate){
            //$user_info = get_userdata($donate->id_user);
            //$url_delete = admin_url("admin.php?page=review-support&paged={$paged}&delete={$review->id}");
    ?>
        <tr <?php if($contador%2):?>class="alternate"<?php endif;?>>
            <td><?php echo $donate->id;?></td>
            
            <td><?php echo $donate->payer_id;?></td>
            <td><?php echo $donate->payer_email;?></td>
            <td><?php echo $donate->payer_name;?></td>
            <td><?php echo $donate->address_country;?></td>
            <td><?php echo $donate->address_zip;?></td>
            <td><?php echo $donate->address_state;?></td>
            <td><?php echo $donate->address_city;?></td>
            <td><?php echo $donate->address_street;?></td>
            <td><?php echo @($donate->payment_amount - $donate->shipping_amount);?></td>    
            <td><?php echo $donate->shipping_amount;?></td> 
            <td><?php echo $donate->payment_amount;?></td>      
            <td><?php echo $donate->post_title;?></td>
            <td><a href="<?php bloginfo('url');?>/wp-admin/user-edit.php?user_id=<?php echo get_post($donate->id_project)->post_author;?>">
                    <?php echo the_author_meta('user_login',get_post($donate->id_project)->post_author);?>
                </a>
            </td>
            <td><?php echo $donate->date_operation;?></td>
            
        </tr>
    <?php
            $contador++;
        }
    ?>
        </tbody>
    </table>
    <?php }?>
    </div>
    <?php
    
    global  $wp_query, $max_page, $page;
    $_GET['paged'] > 1 ? $current = (int) $_GET['paged'] : $current = 1;

    $pagination = array(
        'base' => @add_query_arg('paged','%#%'),
        'format' => '',
        'total' => $max_num_pages,
        'current' => $current,
        'prev_text' => __('PREV'),
        'next_text' => __('NEXT'),
        'end_size' => 1,
        'mid_size' => 2,
        'show_all' => false,
        'type' => 'plain',

    );
    if(!empty($termino )) $pagination['add_args'] = array('termino'=>$termino);
?>
    <div class="tablenav-pages"><?php echo paginate_links( $pagination );?></div>
<?php
    
}

function hkdc_admin_styles() {
wp_enqueue_style( 'jquery-ui-datepicker-style' , '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css');
}
add_action('admin_print_styles', 'hkdc_admin_styles');
add_action( 'admin_enqueue_scripts', 'enqueue_date_picker' );

function enqueue_date_picker(){
                wp_enqueue_script(
            'field-date-js', 
            'Field_Date.js', 
            array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker'),
            time(),
            true
        );  

        wp_enqueue_style( 'jquery-ui-datepicker' );
}

add_action('init', function(){
    global $wpdb;
    if( isset($_GET['action']) ){
        if( $_GET['action'] == 'excel' ){
            $query = "SELECT wpr.*, wpo.post_title, wpo.ID as id_project FROM  {$wpdb->prefix}project_support as wpr
              LEFT JOIN {$wpdb->prefix}posts as wpo ON wpr.id_project = wpo.ID
              LEFT JOIN {$wpdb->prefix}users as wpu ON wpr.id_user = wpu.ID
              WHERE wpo.ID <> 0 ";
            $reviews = $wpdb->get_results($query,OBJECT);     
            set_time_limit(3000);
            ini_set('memory_limit', '-1');
            require 'phpexcel/PHPExcel.php';
            require 'phpexcel/PHPExcel/Writer/Excel2007.php';
            $cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
            $cacheSettings = array( 'memoryCacheSize' => '8MB');
            PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
            $objPHPExcel = new PHPExcel(); 
            $objPHPExcel->setActiveSheetIndex(0); 
            if( count($reviews) != 0 ) {
                $rowCount = 1;
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$rowCount, "ID"); 
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$rowCount, "User Support"); 
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$rowCount, "Email Support"); 
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$rowCount, "Comment"); 
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$rowCount, "Project"); 
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$rowCount, "Owner"); 
                $rowCount++;
                foreach($reviews as $review){
                    $user_info  = get_userdata($review->id_user);   
                    $author     = get_the_author_meta('user_login',get_post($review->id_project)->post_author);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$rowCount, $review->id); 
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$rowCount, $user_info->user_login); 
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$rowCount, $user_info->user_email); 
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$rowCount, $review->comment); 
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$rowCount, $review->post_title); 
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$rowCount, $author); 
                    $rowCount++;
                }
                
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007'); 
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="support-stir.xlsx"');
                header('Cache-Control: max-age=0');
                $objWriter->save('php://output');
                exit;
            }
        }
    }
});

function review_support(){
    global $wpdb;
    
    if(isset($_GET['delete']) && (int) @$_GET['delete']>0)  delete_review((int) $_GET['delete']);
    $paged = (isset($_GET['paged']) && $_GET['paged']>0) ? (int) $_GET['paged'] : 1;
    $termino = (isset($_GET['termino'])) ? @$_GET['termino'] : @$_POST['termino'];
    $title = (isset($_REQUEST['title'])) ? $_REQUEST['title'] : '';
    $post_per_page = 1000; //6
    $offset = ($paged - 1)*$post_per_page;
    
    $query = "SELECT wpr.*, wpo.post_title, wpo.ID as id_project FROM  {$wpdb->prefix}project_support as wpr
              LEFT JOIN {$wpdb->prefix}posts as wpo ON wpr.id_project = wpo.ID
              LEFT JOIN {$wpdb->prefix}users as wpu ON wpr.id_user = wpu.ID
              WHERE wpo.ID <> 0 ";
    $query = (!empty($termino)) ? $query ." AND wpu.user_email LIKE '%".$termino."%' ": $query ." ";
    $query = (!empty($title))? $query." AND wpo.post_title LIKE '%".$title."%' ": $query ." ";
    $total_result = count($wpdb->get_results( $query, OBJECT)); 
    $max_num_pages = ceil($total_result / $post_per_page);  
    
    $query = $query . " ORDER BY  wpr.id DESC LIMIT ".$offset.", ".$post_per_page."; ";
    
    if( isset($_POST['reviews'])){
    
        if(is_array($_POST['reviews'])){
            $reviews = array_map('sanitize_text_field',$_POST['reviews']);
            if(count($reviews) != 0){
                $wpdb->query("DELETE  FROM  {$wpdb->prefix}project_support WHERE id IN(".implode(',',$reviews).")");
            }
        }
    }
    
    $reviews = $wpdb->get_results($query,OBJECT);
    
    
    
    ?>
    <style> 
        .button-primary.red,.button-primary.red:hover{ background:#FF424F;border-color:#AC0037;box-shadow:none;-webkit-box-shadow:none }
        .filtro{margin-top:10px; margin-bottom: 10px; overflow:hidden; position:relative;}
        .filtro form {float:left; width:50%;}
        .filtro .export{float:right; width:40%;text-align:right;}
    </style>
    <script>
        jQuery(function($) {
            jQuery('input[type="checkbox"][name="all"]').change(function(){
                var checkboxes = jQuery(this).closest('form').find(':checkbox');
                $(this).is(":checked")?checkboxes.prop("checked",!0):checkboxes.prop("checked",!1);
            });
        });
    </script>
    <div class="wrap">
    <h2>Reviews Support</h2><br/>
    <div class="filtro"> 
        <form method="POST">
            <label>Search mail:</label> <input type="text" name="termino" value="<?php echo @$termino;?>">
            <label>Project title:</label> <input type="text" name="title" value="<?php echo @$title;?>">
            <button class="button-primary" type="submit">Search</button>    
        </form>
        <div class="export">
            <a href="<?php echo admin_url('admin.php?page=review-support&action=excel');?>" class="button-primary">Exportar a Excel</a>
        </div>
    </div>
    <?php if(count($reviews)>0){;?>
    <form method="POST">
        <table class="wp-list-table widefat fixed posts">
            <thead>
                <tr>
                    <th class="manage-column" width="30"><input type="checkbox" name="all" value="1"></th>
                    <th class="manage-column" width="50">#</th>
                    <th class="manage-column">User Support</th>
                    <th class="manage-column">Email Support</th>
                    
                    <th class="manage-column">Comment</th>
                    <th class="manage-column">Project</th>
                    <th class="manage-column">Owner</th>
                    <th class="manage-column" width="150">Action</th>
                </tr>
            </thead>
            <tbody>
        <?php
            $contador=0;
            foreach($reviews as $review){
                $user_info = get_userdata($review->id_user);
                $url_delete = admin_url("admin.php?page=review-support&paged={$paged}&delete={$review->id}");
        ?>
            <tr <?php if($contador%2):?>class="alternate"<?php endif;?>>
                <td><input type="checkbox" value="<?php echo $review->id;?>" name="reviews[]"></td>
                <td><?php echo $review->id;?></td>
                <td><a href="<?php bloginfo('url');?>/wp-admin/user-edit.php?user_id=<?php echo $review->id_user;?>"><?php echo $user_info->user_login;?></a></td>
                <td><?php echo $user_info->user_email;?></td>
                
                <td><?php echo $review->comment;?></td>
                <td><?php echo $review->post_title;?></td>
                <td><a href="<?php bloginfo('url');?>/wp-admin/user-edit.php?user_id=<?php echo get_post($review->id_project)->post_author;?>">
                        <?php echo the_author_meta('user_login',get_post($review->id_project)->post_author);?>
                    </a>
                </td>
                <td><a href="<?php echo $url_delete;?>" class="button-primary red">Delete</a></td>
            </tr>
        <?php
                $contador++;
            }
        ?>
            </tbody>
        </table>
        <p><button type="submit" class="button-primary red">Delete Selected</button></p>
    </form>
    
    <?php }?>
    </div>
    <?php
    
    global  $wp_query, $max_page, $page;
    $_GET['paged'] > 1 ? $current = (int) $_GET['paged'] : $current = 1;

    $pagination = array(
        'base' => @add_query_arg('paged','%#%'),
        'format' => '',
        'total' => $max_num_pages,
        'current' => $current,
        'prev_text' => __('PREV'),
        'next_text' => __('NEXT'),
        'end_size' => 1,
        'mid_size' => 2,
        'show_all' => false,
        'type' => 'plain',

    );
    if(!empty($termino )) $pagination['add_args'] = array('termino'=>$termino);
?>
    <div class="tablenav-pages"><?php echo paginate_links( $pagination );?></div>
<?php
    
}

function count_support(){
    global $wpdb;
    
    
    $paged = (isset($_GET['paged']) && $_GET['paged']>0) ? (int) $_GET['paged'] : 1;
    $termino = (isset($_GET['termino'])) ? @$_GET['termino'] : @$_POST['termino'];
    $title = (isset($_REQUEST['title'])) ? $_REQUEST['title'] : '';
    $post_per_page = 200; //6
    $offset = ($paged - 1)*$post_per_page;
    
    $query = "SELECT wpr.*, wpo.post_title, wpo.ID as id_project, COUNT( wpr.id_project) as support FROM  {$wpdb->prefix}project_support as wpr
              LEFT JOIN {$wpdb->prefix}posts as wpo ON wpr.id_project = wpo.ID
              WHERE wpo.ID <> 0 ";
    $datestart = (isset($_REQUEST['datestart']) ?((!empty($_REQUEST['datestart']))?$_REQUEST['datestart']:''):'');  
    $datend = (isset($_REQUEST['datend']) ?((!empty($_REQUEST['datend']))?$_REQUEST['datend']:''):'');
    if(!empty($datestart)){
        $query .= " AND wpr.date >= '{$datestart}' ";
    }
    if(!empty($datend)){
        $query .= " AND wpr.date <= '{$datend}' ";
    }
    
    $query  .= " GROUP BY wpr.id_project ORDER BY support DESC ";

    $total_result = count($wpdb->get_results( $query, OBJECT)); 
    $max_num_pages = ceil($total_result / $post_per_page);  
    
    $query = $query . " LIMIT ".$offset.", ".$post_per_page."; ";
    
    $reviews = $wpdb->get_results($query,OBJECT);
    
    ?>
    <style> 
        .button-primary.red,.button-primary.red:hover{ background:#FF424F;border-color:#AC0037;box-shadow:none;-webkit-box-shadow:none }
        .filtro{margin-top:10px; margin-bottom: 10px;}
    </style>
    <script>
    jQuery(document).ready(function(){
        jQuery('.example-datepicker').datepicker({  changeYear: true,dateFormat: 'yy-mm-dd'});
    }); 
    </script>
    <div class="wrap">
    <h2>Support by Project</h2><br/>
    <div class="filtro"> 
        <form method="POST" action="">
            <p>
                <label>Date Start 
                    <input type="input" name="datestart" value="" class="example-datepicker" />
                </label>
                <label>Date End
                    <input type="input" name="datend" value="" class="example-datepicker" />
                </label>
            </p>
            <button class="button-primary" type="submit">Search</button>
        </form>
    </div>
    <?php if(count($reviews)>0){;?>
    <table class="wp-list-table widefat fixed posts">
        <thead>
            <tr>
                <th class="manage-column" width="50">#</th>
                <th class="manage-column">Project</th>
                <th class="manage-column">Owner</th>
				<th class="manage-column">Season</th>
                <th class="manage-column">Count Support</th>
            </tr>
        </thead>
        <tbody>
    <?php
        $contador=0;
        foreach($reviews as $review){
            $user_info = get_userdata($review->id_user);
            $url_delete = admin_url("admin.php?page=review-support&paged={$paged}&delete={$review->id}");
			$terms = get_the_terms($review->id_project, 'season' );
			$term_project = '';
			if(is_array($terms)){
				$term = end($terms);
				if( isset($term->name) ) {
					$term_project = $term->name;
				}
			}
			
    ?>
        <tr <?php if($contador%2):?>class="alternate"<?php endif;?>> 
            <td><?php echo $review->id;?></td>
            <td><?php echo $review->post_title;?></td>
            <td><a href="<?php bloginfo('url');?>/wp-admin/user-edit.php?user_id=<?php echo get_post($review->id_project)->post_author;?>">
                    <?php echo the_author_meta('user_login',get_post($review->id_project)->post_author);?>
                </a>
            </td>
			<td><?php echo $term_project;?></td>
            <td><?php echo $review->support;?></td>
        </tr>
    <?php
            $contador++;
        }
    ?>
        </tbody>
    </table>
    <?php }?>
    </div>
    <?php
    
    global  $wp_query, $max_page, $page;
    $_GET['paged'] > 1 ? $current = (int) $_GET['paged'] : $current = 1;

    $pagination = array(
        'base' => @add_query_arg('paged','%#%'),
        'format' => '',
        'total' => $max_num_pages,
        'current' => $current,
        'prev_text' => __('PREV'),
        'next_text' => __('NEXT'),
        'end_size' => 1,
        'mid_size' => 2,
        'show_all' => false,
        'type' => 'plain',

    );
    if(!empty($termino )) $pagination['add_args'] = array('termino'=>$termino);
?>
    <div class="tablenav-pages"><?php echo paginate_links( $pagination );?></div>
<?php
    
}
function delete_review($id){
    $id = (int) $id;
    global $wpdb;
    $query = "DELETE FROM  {$wpdb->prefix}project_support WHERE id={$id}";
    if($wpdb->query($query)){
?>
    <div id="message" class="updated below-h2" style="margin:15px 15px 0px 0;"><p>Review was deleted :) </p></div>
<?php 
    }
}

add_filter('wp_dropdown_users', 'MySwitchUser');
function MySwitchUser($output)
{

    global $post;
    if($post->post_type=='project'){
        $user = get_user_by('id',$post->post_author);
        $output = $user->user_email;
    }
    return $output;

}

function get_donate_project($id) {
    global $wpdb;
    $id  = (int) $id;
    $query = "SELECT wpr.*, wpo.post_title, wpo.ID as id_project FROM  {$wpdb->prefix}project_donate as wpr
              LEFT JOIN {$wpdb->prefix}posts as wpo ON wpr.id_project = wpo.ID
              WHERE wpo.ID = {$id}
              ORDER by wpr.id DESC";

    $list_donate = null;
    $donates = $wpdb->get_results($query,OBJECT);

    
    foreach($donates as $donate){
        
        $list_donate = ($list_donate == null)? $donate->payer_name: $list_donate.','.$donate->payer_name;   
    }

    return $list_donate;
}

add_action('wp_ajax_create_project', 'create_project');
add_action('wp_ajax_nopriv_create_project', 'create_project');
function create_project(){
    global $wpdb;
    $post_title = xss_clean($_POST['post_title']);
    $areas  = clearHack($_POST['area']);

    if( ! is_user_logged_in() ) {
        $permalink  = get_permalink(4);
        $_SESSION['post_title'] = $post_title;
        $_SESSION['area'] = $areas;  
        $user_count = $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->prefix}posts WHERE post_title='{$post_title}'" );
        if ( $user_count!=0 ){
            die(json_encode(array('error'=>'true')));
        } else {
            die(json_encode(array('code'=>'00','url'=>$permalink)));
        }
    } else {
        $user_id = get_current_user_id(); 
        if($project_id = user_current_project()->ID) {
            $permalink  = get_permalink($project_id);
            die(json_encode(array('code'=>'00','url'=>$permalink)));
        } else {
            if( $project_id = wp_insert_post(array(
                    'post_name' => sanitize_title($post_title),
                    'post_title'=> $post_title,
                    'post_status'=>'draft',
                    'post_type' => 'project',
                    'post_author'=> $user_id 
            )) ) {
                wp_set_object_terms( $project_id, 'no', 'funded' );
                wp_set_object_terms( $project_id, 'no', 'eligible' );
                wp_set_object_terms( $project_id, 's04', 'season' );

                if( count($areas) != 0){
                    wp_set_post_terms( $project_id,  $areas, 'area' );
                }    
                
                update_post_meta($project_id, 'conditions ','1');
                $id_item_id = get_mid_by_key($post_id,'conditions ');
                
                $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
                VALUES ( $id_item_id, 'conditions' , 1,1 ,$project_id )");
            }
            $permalink  = get_bloginfo('url').'/basics';
            die(json_encode(array('code'=>'00','url'=>$permalink)));
        }
    }
}

add_action('wp_ajax_available', 'available');
add_action('wp_ajax_nopriv_available', 'available');

function available(){
    global $wpdb;
    $post_title = xss_clean($_POST['post_title']);
    $user_count = $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->prefix}posts WHERE post_title='{$post_title}'" );
    if ( $user_count!=0 ){
        die(json_encode(array('error'=>'true')));
    }else{
        die(json_encode(array('error'=>'false')));
    }
}

/*=== Login con API ===*/

add_action('wp_ajax_ajax_register_facebook', 'ajax_register_facebook', 10, 1);
add_action('wp_ajax_nopriv_ajax_register_facebook', 'ajax_register_facebook', 10, 1);

function ajax_register_facebook ( $RETURN = false ){
	
    $appid = '382399575495197';
	$appsecret = '0de704b7b6e059176a5e96e423854f81';
	if(get_bloginfo('url')=='http://jpcontreras.com/stir'){
		$appid = '1682881948707469';
		$appsecret = 'b23354288c765d696b7d07465ddfbfea';
	}
	
	include 'sdk/Facebook/autoload.php';
	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);
	$response = ['status'=>'error', 'msg'=> 'Internal error'];
	$fb = new Facebook\Facebook([
	  'app_id' => $appid,
	  'app_secret' => $appsecret,
	  'default_graph_version' => 'v2.2',
	]);
	
	$helper = $fb->getJavaScriptHelper();
	
	try {
	  $accessToken = $helper->getAccessToken();
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
	  $response = ['status' => 'error', 'msg' => 'Error with facebook app.'];
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
	  $response = ['status' => 'error', 'msg' => 'Error with facebook app.'];
	  // echo 'Facebook SDK returned an error: ' . $e->getMessage();
	}
	
	if ( !isset($accessToken) ) {
	  $response = ['status' => 'error', 'msg' => 'Enable javascript in your browser.'];
	}
	
	if( is_user_logged_in() ){
		$response = ['status' => 'success', 'msg' => 'Already logged in, redirecting to your project.', 'url' => get_bloginfo('url').'/basics/'];
	} else {
		$fb_user = json_decode(stripslashes($_REQUEST['user']));
        $guest = $_REQUEST['guest'];
        $url = $_REQUEST['url'];
        $url = preg_replace('/#.*/', '', $url);
		$_SESSION['fb_access_token'] = (string) $accessToken;
		if( isset($fb_user->id) ){
			// $url =  get_bloginfo('url').'/basics/';
			// if($guest === true || $guest == "true"){
			// 	$url =  get_bloginfo('url').'/support/';
            // }
            if( empty($url) ){
                $url = get_bloginfo('url');
            }
			if( $user = get_users(array('meta_key' => '_facebook_login_', 'meta_value' => $fb_user->id)) ){
				if( is_array($user) ){
					$user = end($user);
					update_user_meta($user->ID,'user_active','yes');
					wp_set_current_user($user->ID, $user->user_login);
					wp_set_auth_cookie($user->ID);
					do_action('wp_login', $user->user_login);
					$response = ['status' => 'success', 'msg' => 'Thank you and welcome!', 'url' => $url ];
				}
			} else {
				$user = [
					'first_name' => $fb_user->first_name,
					'last_name'  => $fb_user->last_name,
					'user_email' => $fb_user->email,
					// 'user_url' 	=> $fb_user->link,
					'user_pass' => wp_generate_password()
				];
				$user['user_login'] = generate_username($user);
				$user_response = wp_insert_user($user);
			
				if(!is_wp_error($user_response)){
					update_user_meta($user_response, '_facebook_login_', $fb_user->id );
                    register_project($user_response);
					wp_clear_auth_cookie();
					wp_set_current_user ( $user_response );
					wp_set_auth_cookie  ( $user_response );
					wp_facebook_user_notification_stir($user_response);
					$response = [ 'status' => 'success', 'msg' => 'Thank you and welcome!, redirecting.', 'url' => $url ];
				} else{
					$response = ['status' => 'error', 'msg' => 'Your email exists, recover you password.'];
				}
			}
		}
	}


	if( $RETURN === true) {
		return $response;
	}
    
	header('Content-Type: application/json');
	die( json_encode($response,true) );
    
}

add_action('wp_ajax_ajax_support_facebook', 'ajax_support_facebook', 20, 1);
add_action('wp_ajax_nopriv_ajax_support_facebook', 'ajax_support_facebook', 20, 1);

function ajax_support_facebook ( $RETURN = false ){
  
    if( $response =  ajax_register_facebook(true) ) {
        $support = json_decode(stripslashes($_REQUEST['support']), true);
        $project = (int) $support["post_id"];
        if($response['status'] == 'success'){
            $user_id = get_current_user_id();
            update_user_meta($user_id,'stir_support_cache',serialize($support));
            if( stir_support_release($project, $user_id) ) {
                $response = ['status' => 'success', 'url' => get_permalink($project), 
                    'msg' =>'<p>Thanks for supporting this project.</p>'  ];
            } else { 
                $response = ['status' => 'error', 'url' => get_permalink($project),
                 'msg' => '<p>You have already supported this project</p>' ];
            }
        } else if($response['status'] == 'error'){
			$response = ['status' => 'error', 'msg' => '<p>'.$response['msg'].'</p>' ,
			'url' => get_permalink($project)];
        } else {
			$response = ['status' => 'error',  'msg' =>'<p>Internal error, contact suport admin.</p>',
			'url' => get_permalink($project)];
		}
    }
    
    if( $RETURN === true) {
		return $response;
	}
    
	header('Content-Type: application/json');
	die( json_encode($response,true) );

}

function wp_facebook_user_notification_stir( $user_id ) {
    $headers = array('Content-Type: text/html; charset=UTF-8');
    if ( $deprecated !== null ) {
        _deprecated_argument( __FUNCTION__, '4.3.1' );
    }

    global $wpdb, $wp_hasher;
    $user = get_userdata( $user_id );
	
    $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
    
    $message  = "<html><head>\r\n";
	$message .= "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>\r\n";
	$message .= "</head><body>\r\n";
	$message .= "<table width='600' style='margin:0 auto 0 auto; border-collapse:collapse; background:#FFF;' align='center' border='0' cellspacing='0' cellpadding='0'>\r\n";
	$message .= "<tr><td width='600' height='75'>\r\n";
	$message .= "<img src='http://causeastir.com.au/mailing/general-welcoming/mail_01.jpg' alt='' width='600' height='75'>\r\n";
	$message .= "</td></tr>\r\n";
	$message .= "<tr><td width='600' height='401'>\r\n";
	$message .= "<img src='http://causeastir.com.au/mailing/general-welcoming/mail_02.jpg' alt='' width='600' height='401'>\r\n";
	$message .= "</td></tr>\r\n";
	$message .= "<tr><td width='600' height='44'><table width='600' height='44'><td width='192' height='44'>\r\n";
	$message .= "<img src='http://causeastir.com.au/mailing/general-welcoming/mail_03.jpg' alt='' width='192' height='44'>\r\n";
	$message .= "<td width='219' height='44'><img src='http://causeastir.com.au/mailing/general-welcoming/mail_05.jpg' alt='' width='211' height='44'></td></table>\r\n";
	$message .= "</td></tr>\r\n";
	$message .= "<tr><td width='600' height='86'><img src='http://causeastir.com.au/mailing/general-welcoming/mail_06.jpg' alt='' width='600' height='86'></td></tr>\r\n";
	$message .= "</table></body></html>\r\n";	
    wp_mail($user->user_email, sprintf(__('[%s] Welcome'), $blogname), $message,$headers);
}

function generate_username( $user ) {
	global $wpdb;
	if( !empty( $user['first_name'] ) && !empty( $user['last_name'] ) )
		$username = clean_username( trim( $user['first_name'] ) .'-'. trim( $user['last_name'] ) );
	if( ! validate_username( $username ) ) {
		$username = '';
		// use email
		$email    = explode( '@', $user['email'] );
		if( validate_username( $email[0] ) )
			$username = clean_username( $email[0] );
	}
	// User name can't be on the blacklist or empty
	$illegal_names = get_site_option( 'illegal_names' );
	if ( empty( $username ) || in_array( $username, (array) $illegal_names ) ) {
		// we used all our options to generate a nice username. Use id instead
		$username = 'fb_' . $user['id'];
	}
	// "generate" unique suffix
	$suffix = $wpdb->get_var( $wpdb->prepare(
		"SELECT 1 + SUBSTR(user_login, %d) FROM $wpdb->users WHERE user_login REGEXP %s ORDER BY 1 DESC LIMIT 1",
		strlen( $username ) + 2, '^' . $username . '(-[0-9]+)?$' ) );
	if( !empty( $suffix ) ) {
		$username .= "-{$suffix}";
	}
	return $username;
}

function clean_username( $username ) {
	return sanitize_title( str_replace('_','-', sanitize_user(  $username  ) ) );
}


function register_project($register_user, $elegibility = false){
	global $wpdb;
	$post_title = $_SESSION['post_title'];
	$areas = $_SESSION['area'];
	$_SESSION['project'] = array();
	if($post_id = wp_insert_post(array(
			'post_name' => sanitize_title($post_title),
			'post_title'=> $post_title,
			'post_status'=>'draft',
			'post_type' => 'project',
			'post_author'=> $register_user 
	))){
		update_post_meta($post_id, 'conditions ','1');
		$id_item_id = get_mid_by_key($post_id,'conditions ');
		
		$wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
		 VALUES ( $id_item_id, 'conditions' , 1,1 ,$post_id )");
		
		$_SESSION['project']['id'] = $post_id;
		
		$tax['funded'] = 'no';
		$tax['eligible'] = 'no';
		$tax['season'] = 's04';
		
		if( $elegibility  && count($areas) != 0 ) {
			$tax['funded'] = 'no';
			$tax['eligible'] = 'yes';
			$tax['season'] = 's04';	
		}

		if( count($areas) != 0 ){
			wp_set_post_terms( $post_id,  $areas, 'area' );
		}   

		wp_set_object_terms( $post_id, $tax['funded'], 'funded' );
		wp_set_object_terms( $post_id, $tax['eligible'], 'eligible' );
		wp_set_object_terms( $post_id, $tax['season'], 'season' ); 
		return true;
	}
	return false;
}


function wp_head_script_facebook() { 
	$appid = '382399575495197';
	if(get_bloginfo('url')=='http://jpcontreras.com/stir'){
		$appid = '1682881948707469';
	}
?>
	<script>
	  window.fbAsyncInit = function() {
		FB.init({
		  appId      : '<?php echo $appid;?>',
		  xfbml      : true,
		  cookie	 : true,
		  version    : 'v2.2'
		});
	  };

	  (function(d, s, id){
		 var js, fjs = d.getElementsByTagName(s)[0];
		 if (d.getElementById(id)) {return;}
		 js = d.createElement(s); js.id = id;
		 js.src = "//connect.facebook.net/en_US/sdk.js";
		 fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	   
	</script>
	<div id="fb-root"></div>
<?php }
add_action( 'wp_head', 'wp_head_script_facebook' );
/*=== Login con API ===*/

add_action('wp_ajax_ajax_register', 'ajax_register');
add_action('wp_ajax_nopriv_ajax_register', 'ajax_register');

function ajax_register(){
    if(get_bloginfo('url')=='http://jpcontreras.com/stir'){
        $captcha = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcTaSQUAAAAAP-s2SKqrnsEgWkq0o8uurNFl_XU&response=" . $_POST['response'] . "&remoteip=" . $_SERVER['REMOTE_ADDR']));
    } else{
        $captcha = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lf-WxoTAAAAANdkYRBl8hTcRebO5qevzkG_orsC&response=" . $_POST['response'] . "&remoteip=" . $_SERVER['REMOTE_ADDR']));
    }
    if($captcha->success == true){ 
        if(!is_user_logged_in()){
            $username = strip_tags(addslashes($_POST['user_login']));
            $email = strip_tags(addslashes($_POST['email']));
            $password = strip_tags(addslashes($_POST['pwd']));
            $password2 = strip_tags(addslashes($_POST['pwd2']));
            $first_name = strip_tags(addslashes($_POST['first_name']));
            $last_name = strip_tags(addslashes($_POST['last_name']));
            $located = strip_tags(addslashes($_POST['located']));
            $age = strip_tags(addslashes($_POST['age']));
            $guest = strip_tags(addslashes($_POST['guest']));
            $code =  strip_tags(addslashes($_POST['code']));
            $access_code = get_access_codes();
            $key = sha1( $register_user . time() );
            if( empty($code) || in_array($code, $access_code) ){ 
                $elegibility = false;
                if( in_array($code, $access_code) ){
                    $elegibility = true;

                }
            
                //$billing_address_1 = $_POST['billing_address_1'];
                $userdata = array(
                    'user_login' => esc_attr(sanitize_user($username)),
                    'user_email' => esc_attr($email),
                    'user_pass' => esc_attr($password),
                    'first_name' => esc_attr($first_name),
                    'last_name' => esc_attr($last_name)
                );
                
                $register_user = wp_insert_user($userdata);
                if (!is_wp_error($register_user)) {
                        update_user_meta($register_user, 'first_name', $first_name);
                        update_user_meta($register_user, 'age', $age);
                        update_user_meta($register_user, 'located', $located);
                        update_user_meta($register_user, 'elegibility',$elegibility);
                        update_user_meta($register_user, 'user_active','no');
                        update_user_meta($register_user, 'user_token', $key);
                        
                        $post_title = $_SESSION['post_title'];
                        $areas = $_SESSION['area'];
                        $_SESSION['project'] = array();
                        
                        if(register_project($register_user, $elegibility)){
                            wp_new_user_notification_stir($register_user, '', $key, $guest );
                            //$user_signon = wp_signon( $info, false );
                            if($guest =='true' || $guest== true){
                                die( json_encode( 
                                    array(  'clase'=>'card-panel green darken-1 text-center',
                                            'url' => get_bloginfo('url'),
                                            'mensaje'=>'Thank you and welcome! Activate your account from your email to finish registration.'
                                 
                                    ) ) );
                            }else{
                                die( json_encode( 
                                    array(  'clase'=>'card-panel green darken-1 text-center',
                                            'url' => get_bloginfo('url').'/basics',
                                            'mensaje'=>'To continue creating your project you must activate your account. Check your email.'
                                 
                                    ) ) );
                            }   
                        } else {
                            die(json_encode(array('code'=>'01')));
                        }
                } else {
                   die( json_encode ( 
                        array('clase'=>'card-panel deep-orange darken-1 text-center',
                            'mensaje'=>$register_user->get_error_message() 
                        ) ) );
                }
            } else {
                   die( json_encode ( 
                        array('clase'=>'card-panel deep-orange darken-1 text-center',
                            'mensaje'=> 'Code not valid' 
                        ) ) );
            }
        } else {
            die( json_encode(
                 array( 'clase'=>'card-panel deep-orange darken-1',
                    'mensaje'=>'Login now!') 
                 ) );
        }
    } else{
        die( json_encode(
            array( 'clase'=>'card-panel deep-orange darken-1',
            'mensaje'=>'Please click the reCAPTCHA checkbox below.') 
         ) );
    }
}


// Function to change sender name
function wpb_sender_name( $original_email_from ) {
    return 'Stir Crew';
}
// Hooking up our functions to WordPress filters 
add_filter( 'wp_mail_from_name', 'wpb_sender_name' );



//do_action( 'fbl/after_login', $user, $user_id);
            
add_action('wp_ajax_ajax_register_support', 'ajax_register_support');
add_action('wp_ajax_nopriv_ajax_register_support', 'ajax_register_support');

function ajax_register_support(){
    if(!is_user_logged_in()){
        $username = strip_tags(addslashes($_POST['user_login']));
        $email = strip_tags(addslashes($_POST['email']));
        $password = randomPassword();
        $key = sha1( $register_user . time() );
        $elegibility = false;
        //$billing_address_1 = $_POST['billing_address_1'];
        $userdata = array(
            'user_login' => esc_attr(sanitize_user($username)),
            'user_email' => esc_attr($email),
            'user_pass' => esc_attr($password)
        );
        $register_user = wp_insert_user($userdata);
        if (!is_wp_error($register_user)) {
                update_user_meta($register_user, 'elegibility',$elegibility);
                update_user_meta($register_user, 'user_active','no');
                update_user_meta($register_user, 'user_token', $key);
                $_SESSION['project'] = array();
                if($post_id = wp_insert_post(array(
                        'post_title'=> '',
                        'post_status'=>'draft',
                        'post_type' => 'project',
                        'post_author'=> $register_user 
                ))){
                    update_post_meta($post_id, 'conditions ','1');
                    $id_item_id = get_mid_by_key($post_id,'conditions ');
                    global $wpdb;
                    $wpdb->query("INSERT INTO wp_mf_post_meta ( meta_id, field_name, field_count, group_count, post_id )
                     VALUES ( $id_item_id, 'conditions' , 1,1 ,$post_id )");
                    $_SESSION['project']['id'] = $post_id;
                    
                    $info['user_login'] = addslashes(strip_tags($username));
                    $info['user_password'] = addslashes(strip_tags($password));
                    $info['remember'] = true;
                    //send mail stir
                    
                
                    
                    
                    //$user_signon = wp_signon( $info, false );
                    $post_id = (int) $_POST["post_id"];
                    $project = (int) $_POST["post_id"];
                    update_user_meta($register_user,'stir_support_cache',serialize($_POST));
                     
                    wp_new_user_support_notification_stir($register_user, $password, $key, $project);
                    die( json_encode( 
                        array(  'clase'=>'card-panel green darken-1 text-center',
                                'mensaje'=>'Thank you. Please check your email to activate your account and have your vote counted.'
                        ) ) );
                    
                } else {
                    die( json_encode(array( 'clase'=>'card-panel green darken-1 text-center',
                            'mensaje'=>'Thank you. Please check your email to activate your account and have your vote counted.'
                        ) ) );
                    die(json_encode(array('code'=>'01')));
                }
        } else {
           die( json_encode ( 
                array('clase'=>'card-panel deep-orange darken-1 text-center', 
                    'mensaje'=>$register_user->get_error_message() 
                ) ) );
        }
    } else {
        die( json_encode(
             array( 'clase'=>'card-panel deep-orange darken-1 text-center',
                'mensaje'=>'You can login now!') 
             ) );
    }
    
}


add_action('wp_ajax_project_comment', 'project_comment');

function project_comment($arg = array() ){
    $_POST = clearHack($_POST);
    try {
        extract($_POST,EXTR_SKIP);
        call_user_func($option);
    } catch(Exception $e){
        die(json_encode(array('code'=>'02','message'=>$e->getMessage())));
    }
}

function comment_project($arg = array()){
    $id_project = (int) $_POST['id_project'];
    $tipo = sanitize_title($_POST['type']);
    $comment = sanitize_text_field($_POST['comment']);
    $user = wp_get_current_user();
    $time = current_time( 'mysql' ); 
    $data = array(
        'comment_post_ID' => $id_project,
        'comment_author' => $user->display_name ,
        'comment_author_email' => $user->user_email,
        'comment_content' => $comment,
        'user_id' => $user->ID,
        'comment_author_IP' => $_SERVER['REMOTE_ADDR'],
        'comment_agent' =>  $_SERVER['HTTP_USER_AGENT'],
        'comment_date' => $time,
        'comment_approved' => 1
    );
    $author_post = get_post_field( 'post_author', $id_project); 
    if( $comment_id = wp_insert_comment($data) ) {
        add_comment_meta( $comment_id, '_type_', $tipo );
        add_user_meta( $author_post, '_reminder_comment_', $comment_id);
        $html  = "<li>";
            $html .= "<img src='http://causeastir.com.au/wp-content/themes/theme-stir-v2/img/img-prof-1.jpg' alt=''>";
            $html .= "<span class='nombre'>".$user->display_name." Says: </span>";
            $html .= "<div class='clearfix'></div>";
            $html .= "<span class='comentario'>".$comment."</span>";
            $html .= "<div class='clearfix'></div>";
            $html .= "<span clas='fecha'>".$time."</span>";
        $html .= "</li>";
        
        comment_mail( array(
            'html' =>locate_template("mail-comment-owner.php"),
            'replace' => array(
                '#user#' => $user->display_name,
                '#permalink#' => get_permalink($id_project)
            ),
            'to' => get_userdata($author_post)->user_email,
            'subject' => "The user ".$user->display_name." commented your project.",
            
        ) );
        
        die(json_encode(array('code'=>'00','message'=>'Thanks for commenting on this project','state'=>'success',"html"=>$html)));
    } else{
        die(json_encode(array('code'=>'01','message'=> 'Problems commenting on this project, try again later' ,'state'=>'error')));
    }

    
}

function comment_project_reply() {
    $id_project = (int) $_POST['id_project'];
    $id_comment = (int) $_POST['id_comment'];
    $comment = sanitize_text_field($_POST['comment']);
    $tipo = sanitize_title($_POST['type']);
    $user = wp_get_current_user();
    $time = current_time( 'mysql' ); 
    $data = array(
        'comment_post_ID' => $id_project,
        'comment_author' => $user->display_name ,
        'comment_author_email' => $user->user_email,
        'comment_content' => $comment,
        'user_id' => $user->ID,
        'comment_author_IP' => $_SERVER['REMOTE_ADDR'],
        'comment_agent' =>  $_SERVER['HTTP_USER_AGENT'],
        'comment_date' => $time,
        'comment_approved' => 1,
        'comment_parent'=> $id_comment
    );
    if( $comment_id = wp_insert_comment($data) ) {
        add_comment_meta( $comment_id, '_type_', $tipo  );
        $author_post = get_post_field( 'post_author', $id_project); 
        $user_mail_id = null;
        if($user->ID != $author_post){
            $user_mail_id = $author_post;
            comment_mail( array(
                'html' =>locate_template("mail-comment-owner.php"),
                'replace' => array(
                    '#user#' => $user->display_name,
                    '#permalink#' => get_permalink($id_project)
                ),
                'to' => get_userdata($user_mail_id)->user_email,
                'subject' => $user->display_name." commented on your project",
                
            ) );
        
            add_user_meta( $author_post, '_reminder_comment_', $id_comment);
        } else{
            $author_comment = get_comment( $id_comment);
            $user_mail_id = $author_comment->user_id;   
            
            comment_mail( array(
                'html' => locate_template("mail-comment-user.php"),
                'replace' => array(
                    '#user#' => $user->display_name,
                    '#permalink#' => get_permalink($id_project)
                ), 
                'to' => get_userdata($user_mail_id)->user_email,
                'subject' => $user->display_name." has replied to your comment",
                 
            ) );
        
            add_user_meta( $author_comment->user_id, '_reminder_comment_', $id_comment);
        }
        $html  = "<li>";
            $html .= "<img src='http://causeastir.com.au/wp-content/themes/theme-stir-v2/img/img-prof-1.jpg' alt=''>";
            $html .= "<span class='nombre'>".$user->display_name." Says: </span>";
            $html .= "<div class='clearfix'></div>";
            $html .= "<span class='comentario'>".$comment."</span>";
            $html .= "<div class='clearfix'></div>";
            $html .= "<span clas='fecha'>".$time."</span>";
        $html .= "</li>";
        die(json_encode(array('code'=>'00','message'=>'Thanks for commenting on this project','state'=>'success', "html"=>$html)));
    } else{
        die(json_encode(array('code'=>'01','message'=> 'Problems commenting on this project, try again later.' ,'state'=>'error')));
    }
}

function comment_mail ($arg= array()){
    
        $arg['header'] = array('Content-Type: text/html; charset=UTF-8');
        $arg['html'] = file_get_contents($arg['html']);
        foreach($arg['replace'] as $key =>$value){
            $arg['html'] = str_replace($key, $value, $arg['html']);
        } 
        wp_mail($arg['to'],$arg['subject'] , $arg['html'] ,$arg['header']); 
}
function comment_resolve(){
    $id_project = (int) $_POST['id_project'];
    $id_comment = (int) $_POST['id_comment'];
    
    $user = wp_get_current_user();
    $author_project = get_post_field( 'post_author', $id_project );
    if( $author_project == $user->ID){
        if(update_comment_meta($id_comment, '_resolve_',true)){
            $author_comment =  get_comment($id_comment)->user_id;
            $user_comment = get_userdata($author_comment);
            //$user_comment->user_email
            //$headers = array('Content-Type: text/html; charset=UTF-8');
            //wp_mail("sns.gfuentes@gmail.com", "Your comment was resolved", "Thank you for helping in the project, the thread was resolved. :)" ,$headers);
            die(json_encode(array('code'=>'00','message'=>'Thread resolved','state'=>'success')));
        }else{
            die(json_encode(array('code'=>'01','message'=>'Problems for resolve this comment','state'=>'error')));
        }
    } else{
        die(json_encode(array('code'=>'01','message'=>'Problems for resolve this comment','state'=>'error')));
    }
}

function get_resolve($id_comment = 0){
    return get_comment_meta($id_comment,'_resolve_',true);
    
}
function comment_helpful(){
    $id_comment = (int) $_POST['id_comment'];
    $user_id_comment = get_comment($id_comment)->user_id;
    $karma = get_user_meta($user_id_comment,'_karma_',true);
    update_user_meta($user_id_comment,'_karma_', (1+$karma) );
    comment_closed($id_comment);

    die(json_encode(array('code'=>'00','message'=>'Thread has been rated','state'=>'success','url'=> true)));
}

function comment_not_helpful(){
    $id_comment = (int) $_POST['id_comment'];
    $user_id_comment = get_comment($id_comment)->user_id;
    $karma = get_user_meta($user_id_comment,'_karma_',true);
    update_user_meta($user_id_comment,'_karma_',($karma-1));
    comment_closed($id_comment);
    die(json_encode(array('code'=>'00','message'=>'Thread has been rated','state'=>'success','url'=> true)));
}

function comment_closed($id_comment = 0){
    update_comment_meta($id_comment, '_closed_',true);
}


function get_user_karma ($id_user){
    return get_user_meta($id_user,'_karma_',true);
}


function get_project_comments ( $id_post, $type,  $id_author = null, $id_parent = null ){
    $arg = array(   'meta_key' => '_type_',
                    'meta_value'=> $type,
                    'post_id' => $id_post
                 ); 
    if(!empty($id_author) || !empty($id_parent)){
        if( (int) $id_author != 0 ){
            $arg['author__in'] =  array( (int) $id_author) ;
        }
        if( (int) $id_parent != 0 ){
            $arg['parent'] =  (int) $id_parent;
        }
    }
    
    $comments = get_comments( $arg );
    foreach($comments as $comment){
?>
    <ul class="lista-comentarios hay-respuesta">
        <li>
            <img src="http://causeastir.com.au/wp-content/themes/theme-stir-v2/img/img-prof-1.jpg" alt="">
            <span class="nombre"><?php echo $comment->comment_author;?> Says: </span>
            <div class="clearfix"></div>
            <span class="comentario"><?php echo $comment->comment_content;?></span>
        </li>
<?php 
    $coments_child = get_comments(get_the_ID(),'project_support',null, $comment->comment_ID);
    foreach($coments_child as $child){  
?>
        <li>
            <img src="http://causeastir.com.au/wp-content/themes/theme-stir-v2/img/img-prof-1.jpg" alt="">
            <span class="nombre"><?php echo $child->comment_author;?> Says: </span>
            <div class="clearfix"></div>
            <span class="comentario"><?php echo $child->comment_content;?></span>
        </li>
<?php   } ?>
    </ul>
<?php 
    }
}



function wp_new_user_notification_stir( $user_id,  $password,  $key, $guest = false) {
    $headers = array('Content-Type: text/html; charset=UTF-8');
    if ( $deprecated !== null ) {
        _deprecated_argument( __FUNCTION__, '4.3.1' );
    }
   
    global $wpdb, $wp_hasher;
    $user = get_userdata( $user_id );

    // The blogname option is escaped with esc_html on the way into the database in sanitize_option
    // we want to reverse this for the plain text arena of emails.
    $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
    $redirect = get_bloginfo('url')."/basics/?token=".$key."&action=stir_active&user=".$user->user_login;
    if($guest){
        $redirect = get_bloginfo('url')."/support/?token=".$key."&action=stir_active&user=".$user->user_login."&guest=true";
    }
    
    $message  = "<html><head>\r\n";
    $message .= "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>\r\n";
    $message .= "</head><body>\r\n";
    $message .= "<table width='600' style='margin:0 auto 0 auto; border-collapse:collapse; background:#FFF;' align='center' border='0' cellspacing='0' cellpadding='0'>\r\n";
    $message .= "<tr><td width='600' height='75'>\r\n";
    $message .= "<img src='http://causeastir.com.au/mailing/general-welcoming/mail_01.jpg' alt='' width='600' height='75'>\r\n";
    $message .= "</td></tr>\r\n";
    $message .= "<tr><td width='600' height='401'>\r\n";
    $message .= "<img src='http://causeastir.com.au/mailing/general-welcoming/mail_02.jpg' alt='' width='600' height='401'>\r\n";
    $message .= "</td></tr>\r\n";
    $message .= "<tr><td width='600' height='44'><table width='600' height='44'><td width='192' height='44'>\r\n";
    $message .= "<img src='http://causeastir.com.au/mailing/general-welcoming/mail_03.jpg' alt='' width='192' height='44'>\r\n";
    $message .= "</td><td width='189' height='44'><a href='".$redirect."' target='_blank'><img src='http://causeastir.com.au/mailing/general-welcoming/mail_04.jpg' alt='' width='189' height='44'></a></td>\r\n";
    $message .= "<td width='219' height='44'><img src='http://causeastir.com.au/mailing/general-welcoming/mail_05.jpg' alt='' width='211' height='44'></td></table>\r\n";
    $message .= "</td></tr>\r\n";
    $message .= "<tr><td width='600' height='86'><img src='http://causeastir.com.au/mailing/general-welcoming/mail_06.jpg' alt='' width='600' height='86'></td></tr>\r\n";
    $message .= "</table></body></html>\r\n";
        
/*  
$message  = "<html>\r\n";
$message .= "<head>\r\n";
$message .= "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>\r\n";
$message .= "</head>\r\n";
$message .= "<body>\r\n";
$message .= "<table width='700' style='margin:0 auto 0 auto; border-collapse:collapse; background:#FFF;' align='center' border='0' cellspacing='0' cellpadding='0'>\r\n";
$message .= "<tr>\r\n";
$message .= "<td width='700' height='329'>\r\n";
$message .= "<img src='".get_bloginfo('url')."/mails/mail-bienvenida/welcome-mail_01.jpg' width='700' height='329'>\r\n";
$message .= "</td>\r\n";
$message .= "</tr>\r\n";    
$message .= "<tr>\r\n";
$message .= "<td width='700' height='78' style='font-family:Verdana; font-size:22px; color:#1D1250 ;text-align:center;' valign='middle' >\r\n";
$message .= "<p>Hello {$user->first_name}</p>\r\n";
$message .= "</td>\r\n";
$message .= "</tr>\r\n";
$message .= "<tr>\r\n";
$message .= "<td width='700' height='93'>\r\n";
$message .= "<img src='".get_bloginfo('url')."/mails/mail-bienvenida/welcome-mail_03.jpg' width='700' height='93'>\r\n";
$message .= "</td>\r\n";
$message .= "</tr>\r\n";    
$message .= "<tr>\r\n";
$message .= "<td width='700' height='108'>\r\n";
$message .= " <a href='".get_bloginfo('url')."/login' target='_blank'><img src='".get_bloginfo('url')."/mails/mail-bienvenida/welcome-mail_04.jpg' width='700' height='108'></a>\r\n";
$message .= "</td>\r\n";
$message .= "</tr>\r\n";    
$message .= "<tr>\r\n";
$message .= "<td width='700' height='108' align='center'>\r\n";
$message .= " <a href='".get_bloginfo('url')."/?token=".$key."&action=stir_active&user=".$user->user_login."' target='_blank'>Activate your account</a>\r\n";
$message .= "</td>\r\n";
$message .= "</tr>\r\n";
$message .= "</table>\r\n";
$message .= "</body>\r\n";
$message .= "</html>\r\n";
*/
    
    
    
    
    
    
    
    wp_mail($user->user_email, sprintf(__('[%s] Welcome'), $blogname), $message,$headers);
}
//mail new user fast
function wp_new_user_support_notification_stir( $user_id, $password, $key,$project ) {
    $headers = array('Content-Type: text/html; charset=UTF-8');
    if ( $deprecated !== null ) {
        _deprecated_argument( __FUNCTION__, '4.3.1' );
    }

    global $wpdb, $wp_hasher;
    $user = get_userdata( $user_id );

    // The blogname option is escaped with esc_html on the way into the database in sanitize_option
    // we want to reverse this for the plain text arena of emails.
    $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

    
    /*
    $message  = "<html>\r\n";
    $message .= "<head>\r\n";
    $message .= "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>\r\n";
    $message .= "</head>\r\n";
    $message .= "<body>\r\n";
    $message .= "<table width='700' style='margin:0 auto 0 auto; border-collapse:collapse; background:#FFF;' align='center' border='0' cellspacing='0' cellpadding='0'>\r\n";
        $message .= "<tr>\r\n";
            $message .= "<td width='700' height='329'>\r\n";
                $message .= "<img src='".get_bloginfo('url')."/mails/mail-bienvenida/welcome-mail_01.jpg' width='700' height='329'>\r\n";
            $message .= "</td>\r\n";
        $message .= "</tr>\r\n";    
        $message .= "<tr>\r\n";
            $message .= "<td width='700' height='78' style='font-family:Verdana; font-size:22px; color:#1D1250 ;text-align:center;' valign='middle' >\r\n";
                $message .= "<p>Hello {$user->user_login}</p>\r\n";
                $message .= "<p>Your details to access your account to keep voting are:  </p>\r\n";
                $message .= "<p>Username:{$user->user_login} <br/>
                                Password:{$password} <br/>
                            </p>\r\n";
            $message .= "</td>\r\n";
        $message .= "</tr>\r\n";
        $message .= "<tr>\r\n";
            $message .= "<td width='700' height='93'>\r\n";
                $message .= "<img src='".get_bloginfo('url')."/mails/mail-bienvenida/welcome-mail_03.jpg' width='700' height='93'>\r\n";
            $message .= "</td>\r\n";
        $message .= "</tr>\r\n";    
    $message .= "<tr>\r\n";
        $message .= "<td width='700' height='108'>\r\n";
            $message .= " <a href='".get_bloginfo('url')."/login' target='_blank'><img src='".get_bloginfo('url')."/mails/mail-bienvenida/welcome-mail_04.jpg' width='700' height='108'></a>\r\n";
        $message .= "</td>\r\n";
    $message .= "</tr>\r\n";    
    $message .= "<tr>\r\n";
        $message .= "<td width='700' height='108' align='center'>\r\n";
            $message .= " <a href='".get_bloginfo('url')."/?token=".$key."&action=stir_active&user=".$user->user_login."&project=".$project."' target='_blank'>Activate your account and Vote</a>\r\n";
        $message .= "</td>\r\n";
    $message .= "</tr>\r\n";
    $message .= "</table>\r\n";
    $message .= "</body>\r\n";
    $message .= "</html>\r\n";
    */
    
    
$message  = "<html><head>\r\n";
$message .= "<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>\r\n";
$message .= "</head><body>\r\n";
$message .= "<table width='600' style='margin:0 auto 0 auto; border-collapse:collapse; background:#FFF;' align='center' border='0' cellspacing='0' cellpadding='0'>\r\n";
$message .= "<tr><td width='600' height='75'>\r\n";
$message .= "<img src='http://causeastir.com.au/mailing/general-welcoming/mail_01.jpg' alt='' width='600' height='75'>\r\n";
$message .= "</td></tr>\r\n";
$message .= "<tr><td width='600' height='401'>\r\n";
$message .= "<img src='http://causeastir.com.au/mailing/general-welcoming/mail_02.jpg' alt='' width='600' height='401'>\r\n";
$message .= "</td></tr>\r\n";
$message .= "<tr><td width='600' height='100'>\r\n";
$message .= "<table width='600' height='100'>\r\n";
$message .= "<td width='214' height='100'>&nbsp;</td>\r\n";
$message .= "<td width='189' height='44' style='font-family:Arial, Helvetica, sans-serif; color:#3c3c3e; text-transform:uppercase; vertical-align:middle; mso-line-height-rule:exactly; font-size:12px; line-height:13px; font-weight:bold; text-align:left;'><p>Your login details</p><p>Username: {$user->user_login} </p><p>Password: {$password} </p>\r\n";
$message .= "</td><td width='197' height='100'>&nbsp;</td>\r\n";
$message .= "</table></td></tr><tr><td width='600' height='44'><table width='600' height='44'><td width='192' height='44'>\r\n";
$message .= "<img src='http://causeastir.com.au/mailing/general-welcoming/mail_03.jpg' alt='' width='192' height='44'>\r\n";
$message .= "</td><td width='189' height='44'><a href='".get_bloginfo('url')."/?token=".$key."&action=stir_active&user=".$user->user_login."&project=".$project."' target='_blank'><img src='http://causeastir.com.au/mailing/general-welcoming/mail_04.jpg' alt='' width='189' height='44'></a></td>\r\n";
$message .= "<td width='219' height='44'><img src='http://causeastir.com.au/mailing/general-welcoming/mail_05.jpg' alt='' width='211' height='44'></td></table>\r\n";
$message .= "</td></tr>\r\n";
$message .= "<tr><td width='600' height='86'><img src='http://causeastir.com.au/mailing/general-welcoming/mail_06.jpg' alt='' width='600' height='86'></td></tr>\r\n";
$message .= "</table></body></html>\r\n";    
    
    
    
    
    wp_mail($user->user_email, sprintf(__('[%s] Welcome'), $blogname), $message,$headers);
}
/* procesa comentarios reminder y elimina una vez llamado */
function comment_reminder( $comments ){
    $current_user = wp_get_current_user();
    $reminder = "_false_";
    if(is_array($comments)){
        foreach($comments as $comment){
            if(in_array($comment->comment_ID, get_user_meta( $current_user->ID,'_reminder_comment_'))){
                $reminder = "_true_";
                delete_user_meta($current_user->ID, '_reminder_comment_', $comment->comment_ID);
            }
        }
        return $reminder;
    } else if(in_array($comments,get_user_meta( $current_user->ID,'_reminder_comment_'))){
        $reminder = "_true_";
        delete_user_meta($current_user->ID, '_reminder_comment_', $comments);
        
        return $reminder;
    }
    return $reminder;
}
 

function reverse_wpautop($s){
    $s = str_replace("\n", "", $s);
    $s = str_replace("<p>", "", $s);
    $s = str_replace(array("<br />", "<br>", "<br/>"), "\n", $s);
    $s = str_replace("</p>", "\n\n", $s);       
    return $s;      
}


function wp_redirect_register(){
    if(is_user_logged_in()){
        wp_redirect(get_bloginfo('url'));
    }else{
        if(isset($_GET['guest'])){
            $guest = true;
            return $guest;
        } else{
            if(!isset($_SESSION['post_title'])){
                 wp_redirect(get_bloginfo('url').'/welcome');
            }else{
                if(empty($_SESSION['post_title'])){
                    wp_redirect(get_bloginfo('url').'/welcome');
                }
            }
        }
    }
    return false;
}



add_action('admin_menu', 'menu_remove_user');
function menu_remove_user(){
  add_menu_page('Remove Users', 'Remove Users', 'edit_pages', 'remove-users-invalid', 'remove_user_invalid','dashicons-editor-removeformatting',72);
  add_submenu_page('Remove Users', 'Remove Users', 'Remove Users', 'edit_pages', 'remove-users-invalid','remove_user_invalid' );
} 
function remove_user_invalid(){
    $error = null;
    if ( isset($_POST["submit"]) ) {
       if ( isset($_FILES["file"])) {
            if ($_FILES["file"]["error"] > 0) {
                $error = "Return Code: " . $_FILES["file"]["error"] . "<br />";
            }
            else {
                $storagename = "users-invalid.csv";
                $upload_dir = wp_upload_dir();
                move_uploaded_file($_FILES["file"]["tmp_name"], trailingslashit($upload_dir['basedir']) . $storagename);
                $filename = trailingslashit($upload_dir['baseurl']) . $storagename;
                global $wpdb;
                $fila = 1;
                $html = null;
                if (($gestor = fopen(trailingslashit($upload_dir['basedir']) . $storagename, "r")) !== FALSE) {
                    $count = 0;
                    while (($datos = fgetcsv($gestor, 1000, ",")) !== FALSE) {
                        //$userID =  $wpdb->get_var('SELECT ID FROM  wp_users WHERE user_email = "'.$datos[0].'"');
                        $datos[0] = trim($datos[0]);
                        if(!empty($datos[0])){
                            
                            $project_id =  $wpdb->get_var('SELECT  wp_project_support.id_project FROM wp_users  INNER JOIN wp_project_support  
                            WHERE wp_project_support.id_user = wp_users.ID and wp_users.user_email = "'.$datos[0].'"');
                            
                            
                            $wpdb->query('DELETE wp_users , wp_project_support  FROM wp_users  INNER JOIN wp_project_support  
                            WHERE wp_project_support.id_user = wp_users.ID and wp_users.user_email = "'.$datos[0].'"');
                            update_post_meta( $project_id, '_count_support', support_count($project_id));
                            $html .= '<p>Eliminando voto y usuario con email '.$datos[0].'</p>';        
                            $count++;
                        }
                    }
                    
                }
            } 
        } else {
            $error = "File don't load";
        }
?>
    <div id="message" class="<?php if(!empty($error)){?>error<?php } ?> notice notice-success is-dismissible">
        <?php if(!empty($error)){?>
        <p><?php  echo $error;?></p>
        <?php } else{?>
        <p>Archivo cargado en : <?php echo $filename;?></p>
        <p></p>
        <p>Usuarios Eliminados <?php echo $count;?></p>
        <?php echo $html;?>
        <?php } ?>
    </div>
<?php
    }
?>
    <div class="wrap" >
        <h2>Remove Invalid Users</h2><br/>
        <div id="poststuff">
            <div class="postbox">
                <form method="POST" name="form-invalid-user" action="" enctype="multipart/form-data">
                    <h2 class="hndle ui-sortable-handle"> <label>File User at Remove</label></h2>
                    <p class="inside"> <input type="file" name="file" ></p>
                    <p class="inside"> <button type="submit" class="button button-primary button-large" name="submit">Enviar</button> </p>
                    <p class="inside"><small style="color:red;">Note: The first field in csv is email</small></p>
                </form>
            </div>
        </div>
    </div>
<?php
}
/*Summer of Stir Codes*/
function get_access_codes(){
    return array('AB151','E2141','P123','OQA31');
}

add_action('wp_ajax_ajax_code_verify', 'ajax_code_verify');
//add_action('wp_ajax_nopriv_ajax_register_support', 'ajax_code_verify');

function ajax_code_verify() {
    $code = $_REQUEST['code'];
    $access_code = get_access_codes();
    if( in_array($code, $access_code) ){ 
        $user_id = get_current_user_id(); 
        $project_id = user_current_project()->ID;
        update_user_meta($user_id, 'elegibility',true);
        wp_set_object_terms( $project_id, 'yes', 'eligible' );
        wp_remove_object_terms( $project_id, 's04', 'season' );
        wp_set_object_terms( $project_id, 's04', 'season' );
        

        die( json_encode ( 
        array('clase'=>'success',
            'mensaje'=> 'Code Accepted!' 
        ) ) );
    } else {
        die( json_encode ( 
        array('clase'=>'error',
            'mensaje'=> 'Code not Accepted!' 
        ) ) );
    }
}

function user_reminder_code() {
    $user_id = get_current_user_id(); 
    $reminder = false;
    if ( is_user_logged_in() ) {
        if ( get_user_meta($user_id, 'elegibility',true) != 1 || get_user_meta($user_id, 'elegibility',true) != "1" )
            $reminder = true;
    }
    return $reminder;
}


add_filter('wp_authenticate_user', 'stir_valid_auth_authentication',10,2);
function stir_valid_auth_authentication ($user, $password) {
     $user_active =  get_user_meta($user->ID,'user_active',true);
     if($user_active == 'no' ){
         return new WP_Error( 'broke', __( "You must activate your account from the email sent at the time of registration", "my_textdomain" ) );
     }
     return $user;
}

add_action('parse_request', 'stir_request_url');
function stir_request_url() {
    if( isset($_REQUEST['action'])){
        if( isset($_REQUEST['user']) && $_REQUEST['action']== 'stir_active'){
            $user_login = sanitize_text_field($_REQUEST['user']);
            $user = get_user_by( 'login', $user_login);
            if(isset($user->ID)){   
                $token = get_user_meta($user->ID, 'user_token',true);
                if($token == @$_REQUEST['token']) {
                    update_user_meta($user->ID,'user_active','yes');
                    wp_set_current_user($user->ID, $user_login);
                    wp_set_auth_cookie($user->ID);
                    do_action('wp_login', $user_login);
                    if(isset($_REQUEST['project'])){
                        if(!empty($_REQUEST['project'])){
                            $project = (int) $_REQUEST['project'];
                            stir_support_release($project, $user->ID);
                            $_SESSION['vote'] = 'yes';
                            wp_redirect(get_permalink($project));
                            exit;
                        }
                    }
                    if(isset($_REQUEST['guest'])){
                        wp_redirect(get_bloginfo('url').'/support/?action=stir_active');
                        exit;
                    }
                    wp_redirect(get_bloginfo('url').'/basics/?action=stir_active');
                    exit;
                }
            }   
        } else if( isset($_REQUEST['user']) && $_REQUEST['action']== 'auto_login'){
                $user_login = sanitize_text_field($_REQUEST['user']);
                $user = get_user_by( 'login', $user_login);
                if( isset($user->ID) ){   
                    $token = get_user_meta($user->ID, 'user_token',true);
                    if($token == @$_REQUEST['token']) {
                        wp_set_current_user($user->ID, $user_login);
                        wp_set_auth_cookie($user->ID);
                        do_action('wp_login', $user_login); 
                        wp_redirect(get_bloginfo('url').'/basics/');
                    }
                }
        }
    }

}

function stir_support_release($project_id = 0,$user_id=0){
    global $wpdb;
    $project_id = (isset($_REQUEST['post_id']))? (int) $_REQUEST['post_id']: $project_id;
    $user_id = ($user_id == 0)?get_current_user_id():$user_id;
    $request = get_user_meta($user_id,'stir_support_cache',true);
    $request = unserialize($request);
    $support  = $wpdb->get_var( "SELECT COUNT(*) FROM wp_project_support 
            WHERE id_project=".$project_id." and id_user=".(int)$user_id );
    
    if($project_id != 0 ){
        if($support == 0){ 
            $wpdb->insert( 
            'wp_project_support', 
                array( 
                    'id_user' => $user_id, 
                    'id_project' => $project_id,
                    'age' => '',//$_POST['age'],
                    'cool' => '',
                    'clear' => '',
                    'capable'=> '' ,
                    'support'=> '',//$_POST['support'],
                    'comment' => $request['comment'],
                    'city'  => '',//$_POST['city'],
                    'date' =>  date("Y-m-d H:i:s") 
                ), 
                array( 
                    '%d', '%d','%s','%s','%s','%s','%s','%s','%s','%s'
                ) 
            );
            update_post_meta( $project_id, '_count_support', support_count($project_id));
            return true;     
        }
        update_post_meta( $project_id, '_count_support', support_count($project_id));
        return false;    
    }
    return false;
}

add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) { 
    $user_active = get_user_meta($user->ID,'user_active',true);
    $user_active = ($user_active != "no")? 'yes':'no';
?>
<table class="form-table">
    <tr class="user-nickname-wrap">
        <th><label for="user_active">User Active</label></th>
        <td><input type="checkbox" style="width:15px" name="user_active" id="user_active" value="yes" <?php if($user_active=='yes'):?>checked='checked'<?php endif;?> class="regular-text"></td>
    </tr>
</table>
<?php }

add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );

function save_extra_user_profile_fields( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) ) { return false; }
    $user_active = (@$_POST['user_active'] != 'yes')?'no':'yes';
    update_user_meta( $user_id, 'user_active', $user_active );

}

function notice_season_change($season = ''){
    $current_project = user_current_project();
    if(isset($current_project->ID)){
        //delete_post_meta($current_project->ID,$season);
        $available = get_post_meta($current_project->ID, $season, true);
        if( empty($available) ) {
            return $current_project;
        }
    } 

    return false;
}
add_action('wp_loaded', 'time_zone_function');

function time_zone_function(){
    date_default_timezone_set('Australia/Sydney');
}
 
function available_support_date(){
    $paymentDate = strtotime(date("Y-m-d H:i:s"));
    $contractDateBegin = strtotime("2017-02-06 08:00:00");
    $contractDateEnd = strtotime("2017-02-10 17:00:00");
    if($paymentDate > $contractDateBegin && $paymentDate < $contractDateEnd) {
        return true;
    }
    return false;
}
add_action('wp_ajax_ajax_season_change', 'ajax_season_change');

function ajax_season_change(){
    if(isset($_REQUEST['season'])){
        if(!empty($_REQUEST['season'])){
            $_REQUEST['season'] = sanitize_text_field($_REQUEST['season']);
            if($current_project = notice_season_change($_REQUEST['season'])){
                if(isset($current_project->ID)){
                    $seasons = wp_get_post_terms($current_project->ID,'season', array("fields" => "all"));
                
                    $seasons_matriz = array($_REQUEST['season']);
                    foreach($seasons as $season){
                        $seasons_matriz[] = $season->slug;
                    }
                    
                    wp_delete_object_term_relationships($current_project->ID, 'eligible');
                    //wp_delete_object_term_relationships($current_project->ID, 'funded');
                    //wp_set_object_terms( $post_id, 'no', 'funded' );
                    wp_set_object_terms( $current_project->ID, 'yes', 'eligible' );
                    wp_set_object_terms( $current_project->ID, $seasons_matriz, 'season' ); 
                    update_post_meta($current_project->ID, $_REQUEST['season'], 'yes');
                    die( json_encode ( 
                        array('clase'=>'success',
                            'mensaje'=> 'Your project is now eligible for a grant and open to receive votes' 
                        ) ) );
            
                }
            }
        } 
        die( json_encode ( 
        array('clase'=>'error',
            'mensaje'=> 'Something went wrong, try again' 
        ) ) );
        update_post_meta($current_project->ID, $_REQUEST['season'], 'no');
    }
    
}

add_action('parse_request', 'request_send_draft_project');

function request_send_draft_project(){
    if(isset($_REQUEST['send'])){
        if($_REQUEST['send']=='draft'){
            get_draft_project();
        }
    }
}
function get_draft_project(){
    $drafter = new WP_Query(array('post_type'=>'project','post_status'=>'draft','posts_per_page'=>-1));
    while($drafter->have_posts()): $drafter->the_post();
        if(get_the_title() != ""){
           if($_SERVER['SERVER_NAME'] != 'jpcontreras.com') {
               mail_send_draft( get_the_author_meta('ID') );
              // mail_send_draft( get_the_author_meta('ID'), 'tarkonnen@gmail.com' );
              // return false;
           } 
        }
    endwhile;
    return true;
}

function mail_send_draft($user_id, $destino = ''){
    $headers = array('Content-Type: text/html; charset=UTF-8');
    global $wpdb, $wp_hasher;
    $user = get_userdata( $user_id );

    $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);    
    $token    = sha1( $user_id . time() );

    update_user_meta($user_id, 'user_token', $token);
    
    $autologin= get_bloginfo('url').'/?action=auto_login&user='.$user->user_login.'&token='.$token;
    $message  = file_get_contents( locate_template("mail-reminder-draft.php") );
    $message  = str_replace('#link_project#',$autologin, $message);
    //$user->user_email
    $destion = (empty($destino) ? $user->user_email : $destino);
    wp_mail( $destino, sprintf(__('[%s] Your project is waiting!'), $blogname), $message,$headers);
    
}

function add_new_intervals($schedules) {
    $schedules['weekly'] = array(
        'interval' => 604800,
        'display' => __('Once Weekly')
    );
    $schedules['monthly'] = array(
        'interval' => 2635200,
        'display' => __('Once a month')
    );

    return $schedules;
}
add_filter( 'cron_schedules', 'add_new_intervals');


add_action('new_send_draft_event', 'weekly_event');

function send_draft_event_activation(){
    if ( !wp_next_scheduled( 'new_send_draft_event' ) ) {
        wp_schedule_event( current_time( 'timestamp' ), 'weekly', 'new_send_draft_event');
    }
}
add_action('wp', 'send_draft_event_activation');

function weekly_event(){
     get_draft_project();
}

function user_login_support_this_project($post_id){
    global $wpdb;
    $user = wp_get_current_user();
    if(isset($user->ID)){
        if($user->ID != 0){
            $support  = $wpdb->get_var( "SELECT COUNT(*) FROM wp_project_support 
            WHERE id_project=".$post_id." and id_user=".(int)$user->ID );
            if($support == 0){
                return false;
            }
        }
    }
    return true;
}

add_action( 'wp_ajax_app_login_ajax', 'app_login_ajax');
add_action( 'wp_ajax_nopriv_app_login_ajax','app_login_ajax');

function app_login_ajax(){
    $creds = [];
    $request = $_REQUEST;
    if(isset($request['email']) && isset($request['pwd']) ){
        $creds['user_login'] =  $request['email'];
        $creds['user_password'] = $request['pwd'];
        $creds['remember'] = true;
        $url = (!empty($request['url']))? $request['url'] : get_bloginfo('url') ;
        $user = wp_signon( $creds, false );
        $user_pass = wp_user_onlogin( $creds['user_login'] );
        if(!username_exists($creds['user_login']) && !email_exists($creds['user_login'])){
            die( json_encode ( 
             array('clase'=>'alert alert-warning ',
                'mensaje'=> 'Unregistered email. <a href="'.get_option('siteurl').'/register/?guest" target="_blank"> Register on Stir?</a>',
                'status' => 'error' 
            ) ) );
        } else if(!wp_check_password( $creds['user_password'], $user_pass->data->user_pass,
         $user_pass->ID)){
            die( json_encode ( 
            array('clase'=>'alert alert-warning ',
                'mensaje'=> 'User / Password combination not working.
                <a href="#registration-form">Send magic link?</a>',
                'status' => 'error' 
            ) ) );
        } else if ( is_wp_error($user) ){
            die( json_encode ( 
             array('clase'=>'alert alert-warning ',
                'mensaje'=> $user->get_error_message(),
                'status' => 'error' 
            ) ) );
        } else {
            wp_set_current_user($user->ID);
            $parse = parse_url($url);
            if($parse['host'] == $_SERVER['SERVER_NAME']){
                die( json_encode ( 
                array( 'url'=> $url,
                    'status' => 'success' 
                ) ) );
            }
            if ( isset( $user->roles ) && is_array( $user->roles ) ) {
                if ( in_array( 'committeemember', $user->roles ) || 
                     in_array( 'administrator', $user->roles ) ) {
                    die( json_encode ( 
                     array( 'url'=> $url,
                        'status' => 'success' 
                    ) ) );
                } 
            }
            
            if( $team_id = $this->iact_user_in_team() ){
                die( json_encode ( 
                 array( 'url'=> get_permalink($team_id),
                    'status' => 'success' 
                ) ) );
            }

            die( json_encode ( 
             array( 'url'=> get_bloginfo('url').'/edit-your-profile/',
                'status' => 'success' 
            ) ) );
        }
    } 
    die( json_encode ( 
        array('clase'=>'alert alert-warning ',
        'mensaje'=> 'You must complete all fields of the form login are required',
        'status' => 'error' 
    ) ) );
   
}


add_action( 'wp_ajax_app_send_magic_link', 'app_send_magic_link');
add_action( 'wp_ajax_nopriv_app_send_magic_link','app_send_magic_link');

function app_send_magic_link(){
    $email = sanitize_text_field($_REQUEST['email']);
    if($user = wp_user_onlogin($email)){
        $url = esc_url($_REQUEST['url']);
        $headers = array('Content-Type: text/html; charset=UTF-8');
        if ( $deprecated !== null ) {
            _deprecated_argument( __FUNCTION__, '4.3.1' );
        }
    
        global $wpdb, $wp_hasher;
        $magic_token = md5(uniqid().$user->ID);
        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
     
        $message  = file_get_contents(__DIR__.'/mail-magic-link.html');
        $message  = str_replace('#permalink#', get_bloginfo('url')."?magic_token=".$magic_token."&user_login=".$user->user_email.'&url='.$url, $message);
    
        
        wp_mail($user->user_email, sprintf(__('[%s] Magic Link'), $blogname), $message,$headers);
        update_user_meta( $user->ID, '_magic_link_', array('magic_token'=> $magic_token, 'time' => strtotime("now")));
        die( json_encode ( 
            array('clase'=>'alert alert-success ',
            'mensaje'=> 'Link sent successfully, check your email (just in case, check your spam too).',
            'status' => 'success' 
        ) ) );
    } else {
        die( json_encode ( 
            array('clase'=>'alert alert-warning ',
            'mensaje'=> 'That email is not registered. Would you like to <a href="'.get_option('siteurl').'/register/?guest" target="_blank">register on Stir?</a> ',
            'status' => 'error' 
        ) ) );
    }
}

add_action('init', function(){
    if( isset($_REQUEST['magic_token']) && isset($_REQUEST['user_login']) ){
        $magic_token = sanitize_text_field($_REQUEST['magic_token']);
        $user_login = sanitize_text_field($_REQUEST['user_login']);
        $url = esc_url($_REQUEST['url']);
        if(  $user = wp_user_onlogin($user_login) ){
            $magic_link =  get_user_meta($user->ID, '_magic_link_', true);
            if(is_array($magic_link)){
                if( $magic_link['time'] >=  strtotime('-2 hours') && $magic_token == $magic_link['magic_token']){
                        wp_clear_auth_cookie();
                        wp_set_current_user ( $user->ID );
                        wp_set_auth_cookie  ( $user->ID );
                        update_user_caches($user);
                        if( strpos($url, get_bloginfo('url')) !== false ){
                            wp_redirect($url);
                            exit;
                        }
                        wp_redirect(get_bloginfo('url'));
                        exit;
                } else {
                    wp_redirect(get_bloginfo('url').'?action=magic_link');
                    exit;
                }
            }
        }
    }
});

function wp_user_onlogin($user_login){
    if( $user_pass = get_user_by( 'login', $user_login  )){
        return $user_pass;
    }
    return get_user_by( 'email', $user_login );
}

add_action( 'delete_user', function($user_id){
    global $wpdb;
    $wpdb->delete( 'wp_project_support', array( 'id_user' => $user_id ) );
    $project = new WP_Query(array('post_type'=>'project','post_per_page'=>1,'order'=>'DESC'));
    while($project->have_posts()):   $project->the_post();
        $project_id = get_the_ID();
        update_post_meta( $project_id, '_count_support', support_count($project_id));
    endwhile;
   
} );