<?php if ( is_page(array( 749, 751 )) ) { ?>
             
<? } else { ?>
<div class="placeholder-header box fleft fwidth"></div>
             
<header id="head-main" class="box fleft fwidth">
<div id="publishit" class="saving ocultar" rel="alert-slide">
      <div class="table">
          <div class="table-cell">
              <p data-cache="Saving">Saving and Publishing</p><div class="spinner"></div>
          </div>
      </div>
      <span class="closem"><i class="fa fa-times fa-2x" aria-hidden="true"></i></span>
  </div>  
    
    
    <div class="container-fluid">
    <div id="logo" class="col-lg-1 col-md-1 col-sm-4 col-xs-4">
        <a href="<?php echo get_option('siteurl'); ?>">&nbsp;</a>
    </div>
    <div id="top-menu" class="col-lg-6 col-md-6 hidden-sm hidden-xs no-column">
        <ul>
            <?php if ( is_user_logged_in() ) { ?>
                &nbsp;
                <? } else { ?>
                <?php /* <li><a href="<?php echo get_option('siteurl'); ?>/welcome/">Create a Project</a></li> */ ?>
                <? }?>
                <li><a href="<?php echo get_option('siteurl'); ?>/support/" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>Support a Project</a></li>
            <li><a>/</a></li>
            <li><a href="<?php echo get_option('siteurl'); ?>/about/">What is Stir?</a></li>
            <li><a>/</a></li>
            <li><a href="<?php echo get_option('siteurl'); ?>/blog/">Blog</a></li>
            <li><a>/</a></li>
            <li class="social">
        <a href="https://www.facebook.com/causeastirCBR" target="_blank"><i class="fa fa-facebook-square fa-lg"></i></a>&nbsp;&nbsp;&nbsp;<a href="https://twitter.com/StirCBR" target="_blank"><i class="fa fa-twitter fa-lg"></i></a>&nbsp;&nbsp;&nbsp;<a href="https://www.instagram.com/causeastircbr" target="_blank"><i class="fa fa-instagram fa-lg"></i></a>
    </li>
            
            
        </ul>
    </div>
    <div id="top-meta" class="col-lg-5 col-md-5 hidden-sm hidden-xs text-right no-column">
        <?php if ( is_user_logged_in() ) { ?>
        <?php } else { ?>
        <? } ?>
        
        
        <?php if ( is_user_logged_in() ) { ?>
      <?php   $current_user = wp_get_current_user();?>
            <p>Hello <?php echo $current_user->user_login;?></p>
        
            <?php if(have_draft()){?>
            <a href="<?php echo esc_url( home_url() );?>/basics/" class="btn-gris-small button-loggedin" data-block="yes">Complete project <i class="fa fa-pencil fa-lg"></i></a>   
        
            <?php } else if(user_publishproject()){ ?>
            <a href="<?php echo esc_url( home_url() );?>/basics/?draft" class="btn-gris-small button-loggedin" data-block="yes">Edit my project <i class="fa fa-pencil fa-lg"></i></a>   
        
            <?php  } else{?>
            <a href="<?php echo esc_url( home_url() );?>/welcome/" class="btn-gris-small button-loggedin" data-block="yes">Create project <i class="fa fa-plus fa-lg"></i></a> 
            <?php } ?>
            <p><a href="<?php echo wp_logout_url( get_permalink() ); ?>">Log Out -></a></p>
        <?php ; } else{?>
            
        
            <a href="<?php echo get_option('siteurl'); ?>/welcome/" class="btn-gris-small" data-block="yes">Start a Project &nbsp;<i class="fa fa-plus fa-lg"></i></a>
        
        
        <p>
            <a href="<?php echo get_option('siteurl'); ?>/register/?guest">Register </a> /
        </p>
        <?php dynamic_sidebar( 'login_ajax_header' ); ?>
        
        <? /*
        <p>
            <a href="<?php echo get_option('siteurl'); ?>/register/?guest">Register </a> / <a href="<?php echo get_option('siteurl'); ?>/login/">Log in</a></p>
            */?>
        
        <? } ?>        

        
        
        
        
        
    </div>
    
    <div id="hamburger-mob-icon" class="hidden-lg hidden-md col-sm-8 col-xs-8 text-right no-column">
        <a href="#menu" class="menu-link">
            <div id="nav-icon1">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </a>
    </div>
        
  <?php if(isset($_SESSION['vote'])){?>
    <?php if($_SESSION['vote']=='yes'){?>
  <div id="mensaje" rel="alert-slide" class="vote-message" >
      <div class="table">
          <div class="table-cell">
            <?php if( isset($_SESSION['vote']['msg']) ) {?>
             <p data-cache="Saving"><?php echo $_SESSION['vote']['msg'];?></p>
            <?php } else {?>
              <p data-cache="Saving">Thanks for activating your account, your vote has been submitted</p>
            <?php } ?>
          </div>
      </div>
        <span class="closem"><i class="fa fa-times fa-2x" aria-hidden="true"></i></span>
  </div>  
  <?php unset($_SESSION['vote']);?>
    <?php } ?>
  <?php } ?>
    
  <div id="mensaje" class="saving" rel="alert-slide" style="display:none;">
      <div class="table">
          <div class="table-cell">
              <p data-cache="Saving">Saving</p><div class="spinner"></div>
          </div>
      </div>
      <span class="closem"><i class="fa fa-times fa-2x" aria-hidden="true"></i></span>
  </div>
  <div id="warning" class="saving" rel="alert-slide" style="display:none;">
      <div class="table">
          <div class="table-cell">
              <p data-cache="Saving">Saving</p><div class="spinner"></div>
          </div>
      </div>
      <span class="closem"><i class="fa fa-times fa-2x" aria-hidden="true"></i></span>
  </div>  
  <?php if(isset($_REQUEST['action'])){?>
  <?php if($_REQUEST['action'] == 'stir_active'){?>
  
   <div id="stir-active" class="saving" rel="alert-slide">
      <div class="table">
          <div class="table-cell">
              <p data-cache="Saving">Your account is active</p><div class="spinner"></div>
          </div>
      </div>
      <span class="closem"><i class="fa fa-times fa-2x" aria-hidden="true"></i></span>
  </div>  
  <?php } ?>
  <?php } ?>
    <?php /*
   <?php if(  user_reminder_code() ) {?>
   <div id="watch-code-reminder" style="background:#fff;">
    <form name="watch-code" method="post">
      <input name="code" type="text">
      <button rel="button" data-page="whatchcode" data-option="watch-code">Enviar</button>
      <div class="watch-code-message" style="display:none;">
      </div>
    </form>
   </div>
   <?php } ?>
   */ ?>
    </div>
</header>
      
<nav id="menu" class="panel" role="navigation">

    <ul class="list-group">
    
<?php if ( is_user_logged_in() ) { ?>
    
    <?php   $current_user = wp_get_current_user();?>
        <li ><p>Hello <?php echo $current_user->user_login;?></p></li>
        
    <?php if(have_draft()){?>
        <li ><a href="<?php echo esc_url( home_url() );?>/basics/" class="btn-gris-small" data-block="yes">Complete project</a></li>
        
    <?php } else if(user_publishproject()){ ?>
        <li ><a href="<?php echo esc_url( home_url() );?>/basics/?draft" class="btn-gris-small" data-block="yes">Edit my project</a></li>
        
    <?php  } else{?>
        <li ><a href="<?php echo esc_url( home_url() );?>/welcome/" class="btn-gris-small" data-block="yes">Create project </a></li>
        
    <?php } ?>
        
    <?php ; } else{?>
        <li >
          <a href="<?php echo get_option('siteurl'); ?>/welcome/" class="btn-gris-small" data-block="yes">Start a project</a>
        </li>

        <li >or <a href="<?php echo get_option('siteurl'); ?>/login/">Log in</a> here</li>
 
<? } ?>         

    <li >
        <a href="<?php echo get_option('siteurl'); ?>/support/" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>Support a Project</a>
    </li>
    <li >
        <a href="<?php echo get_option('siteurl'); ?>/About" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>What is Stir?</a>
    </li>
        
        <li >
            <a href="<?php echo get_option('siteurl'); ?>/blog" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>Blog</a>
        </li>
    <li class="list-group-item social">
        <a href="https://www.facebook.com/causeastirCBR" target="_blank"><i class="fa fa-facebook-square fa-2x"></i></a>&nbsp;&nbsp;&nbsp;<a href="https://twitter.com/StirCBR" target="_blank"><i class="fa fa-twitter fa-2x"></i></a>&nbsp;&nbsp;&nbsp;<a href="https://www.instagram.com/causeastircbr" target="_blank"><i class="fa fa-instagram fa-2x"></i></a>
    </li>
    
    <?php if ( is_user_logged_in() ) { ?>
    <li ><p class="logout-link-mob"><a href="<?php echo wp_logout_url( get_permalink() ); ?>">Log Out -></a></p></li>
    <?php ; } else{?>
    <?php } ?>
    
</ul>

    
    
    
    
</nav>      

<? }?>  