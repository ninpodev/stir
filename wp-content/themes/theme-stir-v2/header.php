<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="theme-color" content="#121B24" />
    <title><?php if(@$is_search !== true){ bloginfo('name'); ?>&nbsp;<?php if ( is_home() ) { ?><? } else { ?> / <?php
echo empty( $post->post_parent ) ? get_the_title( $post->ID ) : get_the_title( $post->post_parent );
?><? }  }else { ?>Search Results<?php  }?></title>
      
      
<? if ( is_singular('project') ) : ?>
      
      <!--project-->
        <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>        
        <?php 
        $aditional = get_order_field('visuals_project_aditional_photos');
        $imagen = get('visuals_project_display');
        ?>

        <!--meta regular-->
        <meta name="title" content="<?php the_title(); ?>"/>
        <meta name="description" content="<?php echo get('describe_project');?> <?php echo get('describe');?>"/>
        <!--open graph facebook-->
        <meta property="og:url" content="<?php the_permalink(); ?>" />
        <meta property="og:image" content="<?php echo $imagen;?>" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="<?php the_title(); ?>" />
        <meta property="og:description" content="<?php echo get('describe_project');?> <?php echo get('describe');?>" />
        <meta property="og:site_name" content="Stir / Crowd-voted Grants for Creative Projects"/>
        <meta property="og:description" content="<?php echo get('describe_project');?> <?php echo get('describe');?>"/>
        <meta property="fb:app_id" content="1560221257581148"/>



        <?php endwhile; ?>
        <?php else : ?>
        <?php endif; ?>
        <?php wp_reset_query(); ?>

<?php elseif( is_singular('news') ): ?>
      
      <!--news-->
        <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>        
        

        <!--meta regular-->
        <meta name="title" content="<?php the_title(); ?>"/>
        <meta name="description" content="<?php echo(get_the_excerpt()); ?>"/>
        <!--open graph facebook-->
        <meta property="og:url" content="<?php the_permalink(); ?>" />
        <meta property="og:image" content="<?php the_post_thumbnail_url(); ?>" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="<?php the_title(); ?>" />
        <meta property="og:description" content="<?php echo(get_the_excerpt()); ?>" />
        <meta property="og:site_name" content="Stir / Crowd-voted Grants for Creative Projects"/>
        <meta property="og:description" content="<?php echo(get_the_excerpt()); ?>"/>
        <meta property="fb:app_id" content="1560221257581148"/>



        <?php endwhile; ?>
        <?php else : ?>
        <?php endif; ?>
        <?php wp_reset_query(); ?>  
      
<? else : ?>
      
    <!--else header-->

      
        <meta property="og:url" content="https://causeastir.com.au" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Stir" />
        <meta property="og:description" content="Stir is a CBRIN initiative; a local crowd-voted grants platform that provides a simple process to help them structure, write and present their personal projects in a way that teaches them about grant applications" />
        <meta property="og:image" content="https://causeastir.com.au/mailing/fb-sharer-image-2018" />       
  
<?php endif; ?>

    <?php wp_head(); ?>
    <link href="<?php bloginfo('stylesheet_url'); ?>?v=<?php echo rand(0,1000);?>" rel="stylesheet">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/custom.css" rel="stylesheet"> 

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>    
      <link href="https://fonts.googleapis.com/css?family=Karla:400,700" rel="stylesheet">
      
    <script src="https://www.google.com/recaptcha/api.js"></script>
      
      
            <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '1533988453300322'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=1533988453300322&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
      <meta property="fb:pages" content="1549508148635205" />
          <? if ( is_singular('project') ) : ?>
<style>
    button#un-button.un-bottom {
        display:none;
        visibility:hidden;
        opacity:0;
    }
          </style>
          
<? else : ?>

<?php endif; ?>      

      
      
  </head>
    
    
    
    
  <body <?php if ( is_front_page() ) { body_class( 'desktop-body' ); } else { body_class(); } ?>>
      
<script>

  jQuery(document).ready(function($){
    var fb = {
      scope: 'me?fields=id,name,email,first_name,last_name,age_range',
      set : function(e){
        var $last_name = $("input[name=last_name]");
        var $first_name = $("input[name=first_name]")
        var $email = $("input[name=email]")
        var $email_confirm = $("input[name=email_confirm]")
        $last_name.val(e.last_name).focus();
        $first_name.val(e.first_name).focus();
        $email.val(e.email).focus();
        $email_confirm.val(e.email).focus();
      }, 
      preloading: function($option,$text ){ 

        var $mensaje = $("#mensaje");
        var $cache = (typeof $text !== "undefined")? true: false;
        var $text = (typeof $text !== "undefined")?  $text : $mensaje.find('p').data('cache');
        if($.trim($text) != ""){
          $mensaje.find('p').html($text);
        }
        if($option=='show'){
          if($cache == false)
            $mensaje.stop(true,true).slideDown();
          else
            $mensaje.stop(true,true).slideDown().delay(2000).slideUp();
          return true;
        }
        $mensaje.delay(500).stop(true,true).slideUp()
        return true;
      },
      getdata: function(){
        FB.getLoginStatus(function(response) {
          if (response.status === 'connected') {
            fb.preloading('show','Loading your data from facebook.');
            FB.api(fb.scope, function(response) {
              if(typeof response != "undefined") {
                fb.set(response);
              }
            });
          } else {
           FB.login(function(response) {
              if (response.authResponse.accessToken) {    
                fb.preloading('show','Loading your data from facebook.'); 
                FB.api(fb.scope, function(response) {
                  if(typeof response != "undefined") {
                    fb.set(response);
                  }
                }); 
              } 
            }, {scope:'email'});
          }
        });
      }
    }
    $('[rel=prefill-fb]').click(function(e){
        fb.getdata();
    });
  });
   
</script>

<!-- Modal -->
<div class="modal fade" id="modal-login" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content bg-bunker">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span class="glyphicon glyphicon-remove"></span>
                </button>
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#login-form" class="font-standard text-white"> Login </a></li>
                    <li><a class="separator font-standard text-white" >//</a></li>
                    <li><a data-toggle="tab" href="#registration-form" class="font-standard text-white"> I forgot my password</a></li>
                </ul>
            </div>
            <div class="modal-body">
                <div class="tab-content">
                    <div id="login-form" class="tab-pane fade in active"  userapp>
                        <div class="row">
                            <div class="col-lg-12">
                            <form action="/">
                                <div class="form-group">
                                    <!--<label for="email">Email:</label>-->
                                    <input type="email" class="form-control border-0 border-bottom border-white no-radius bg-none" id="email" placeholder="Username or Email Address" name="email">
                                </div>
                                <div class="form-group">
                                    <!--<label for="pwd">Password:</label>-->
                                    <input type="password" class="form-control border-0 border-bottom border-white no-radius bg-none" id="pwd" placeholder="Password" name="pwd">
                                </div>
                                <div class="col-lg-6 no-column">
                                    <div class="checkbox">
                                    <label class="text-white"><input type="checkbox" name="remember"> Remember me</label>
                                    </div>
                                </div>
                                <div class="col-lg-6 no-column text-right">
                                    <div class="checkbox">
                                    <a href="<?php get_option('siteurl'); ?>/register/?guest" class="text-white"><i class="fa fa-user" aria-hidden="true"></i>&nbsp; Create an Account</a>
                                    </div>
                                    
                                </div>
                                <div class="col-lg-12 no-column">
                                <?php
                                    global $wp;
                                    $current_url =  home_url( $wp->request ); 
                                ?>
                                <input type="hidden" name="url"  value="<?php echo $current_url;?>">
                                <button type="submit" class="btn btn-default fwidth bg-orange text-white border-0 text-uppercase font-700" rel="userapp" data-action="login">Login</button>
                                </div>
                            </form>

                        </div>
                            <div class="col-lg-12 or-separator text-center text-white">
                                <span class="bg-bunker">or</span>
                                <span class="line"></span>
                            </div>
                            
                            <div class="col-lg-12 text-center">
                                <p class="text-white">Use this to register to Stir using your Facebook profile.</p>
                                <div id="fb-root"></div> 
                                <a class="btn btn-block btn-social btn-facebook text-center text-uppercase font-700" rel="social" data-red="facebook" data-action="login" href="#"><span class="fa fa-facebook"></span> Login With Facebook</a>
                                <p class="text-white" >The information provided from Facebook will never be shared.</p>
                                
                               
                            </div>
                        </div>
                        <div class="modal-footer border-0">
                        <div class="alert alert-warning text-left" role="alert" style="display:none;"></div>
                            <!-- <div class="alert alert-warning text-left" role="alert">Email o Password incorrectos. <a href="#registration-form">Enviar link mágico?</a> </div>
                            <div class="alert alert-warning text-left" role="alert">Email inexistente <a href="<?php echo get_option('siteurl'); ?>/register/?guest" target="_blank">Registrarse en Stir?</a>  </div> -->
                        </div>
                    </div>
                    <div id="registration-form" class="tab-pane fade" userapp>
                        <form action="/">
                            <p class="text-white">Enter your registered email and we'll send you a link that will log you in without needing a password.</p>
                            <div class="form-group">
                                <?php
                                    global $wp;
                                    $current_url =  home_url( $wp->request ); 
                                ?>
                                <input type="hidden" name="url"  value="<?php echo $current_url;?>">
                                <input type="email" class="form-control border-0 border-bottom border-white no-radius bg-none" id="newemail" placeholder="Enter email" name="email">
                            </div>
                            <button type="submit" class="btn btn-default fwidth bg-orange text-white border-0 text-uppercase font-700" rel="userapp" data-action="magiclink">Send magic Link</button>
                        </form>
                        
                        <div class="modal-footer border-0">
                           <div class="alert text-left" role="alert" style="display:none"></div>    
                        </div>
                        
                    </div>

                </div>
            </div>
            
        </div>
    </div>
</div>
      

                
        <div class="fullwrapper box fleft fwidth">
<div class="placeholder-header box fleft fwidth"></div>
             
<header id="head-main" class="box fleft fwidth">
<div id="publishit" class="saving ocultar" rel="alert-slide">
      <div class="table">
          <div class="table-cell">
              <p data-cache="Saving">Saving and Publishing</p><div class="spinner"></div>
          </div>
      </div>
      <span class="closem"><i class="fa fa-times fa-2x" aria-hidden="true"></i></span>
  </div>  
    
    
    <div class="container-fluid">
    <div id="logo" class="col-lg-1 col-md-1 col-sm-4 col-xs-4">
        <a href="<?php echo get_option('siteurl'); ?>">&nbsp;</a>
    </div>
    <div id="top-menu" class="col-lg-6 col-md-6 hidden-sm hidden-xs no-column">
        <ul>
            <?php if ( is_user_logged_in() ) { ?>
                &nbsp;
                <? } else { ?>
                <?php /* <li><a href="<?php echo get_option('siteurl'); ?>/welcome/">Create a Project</a></li> */ ?>
                <? }?>
                <li><a href="<?php echo get_option('siteurl'); ?>/support/" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>Support a Project</a></li>
            <li><a>/</a></li>
            <li><a href="<?php echo get_option('siteurl'); ?>/about/">What is Stir?</a></li>
            <li><a>/</a></li>
            <li><a href="<?php echo get_option('siteurl'); ?>/blog/">Blog</a></li>
            <li><a>/</a></li>
            <li class="social">
        <a href="https://www.facebook.com/causeastirCBR" target="_blank"><i class="fa fa-facebook-square fa-lg"></i></a>&nbsp;&nbsp;&nbsp;<a href="https://twitter.com/StirCBR" target="_blank"><i class="fa fa-twitter fa-lg"></i></a>&nbsp;&nbsp;&nbsp;<a href="https://www.instagram.com/causeastircbr" target="_blank"><i class="fa fa-instagram fa-lg"></i></a>
    </li>
            
            
        </ul>
    </div>
    <div id="top-meta" class="col-lg-5 col-md-5 hidden-sm hidden-xs text-right no-column">
        <?php if ( is_user_logged_in() ) { ?>
        <?php } else { ?>
        <? } ?>
        
        
        <?php if ( is_user_logged_in() ) { ?>
      <?php   $current_user = wp_get_current_user();?>
            <p>Hello <?php echo $current_user->user_login;?></p>
        
            <?php if(have_draft()){?>
            <a href="<?php echo esc_url( home_url() );?>/basics/" class="btn-gris-small button-loggedin" data-block="yes">Complete project <i class="fa fa-pencil fa-lg"></i></a>   
        
            <?php } else if(user_publishproject()){ ?>
            <a href="<?php echo esc_url( home_url() );?>/basics/?draft" class="btn-gris-small button-loggedin" data-block="yes">Edit my project <i class="fa fa-pencil fa-lg"></i></a>   
        
            <?php  } else{?>
            <a href="<?php echo esc_url( home_url() );?>/welcome/" class="btn-gris-small button-loggedin" data-block="yes">Create project <i class="fa fa-plus fa-lg"></i></a> 
            <?php } ?>
            <p><a href="<?php echo wp_logout_url( get_permalink() ); ?>">Log Out -></a></p>
        <?php ; } else{?>
        <!--si no está logueado-->
            
        
            <a href="<?php echo get_option('siteurl'); ?>/welcome/" class="btn-gris-small" data-block="yes">Start a Project &nbsp;<i class="fa fa-plus fa-lg"></i></a>
        
        
        <p>
            <a href="<?php echo get_option('siteurl'); ?>/register/?guest">Register </a> /
        
        <? /* <?php dynamic_sidebar( 'login_ajax_header' ); ?>*/?>
        <a href="#" data-toggle="modal" data-target="#modal-login">Log in</a>
            
        <!--<a href="<?php echo get_option('siteurl'); ?>/login/">Log in</a>-->
        </p>
            
        
        <? } ?>        

        
        
        
        
        
    </div>
    
    <div id="hamburger-mob-icon" class="hidden-lg hidden-md col-sm-8 col-xs-8 text-right no-column">
        <a href="#menu" class="menu-link">
            <div id="nav-icon1">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </a>
    </div>
        
  <?php if(isset($_SESSION['vote'])){?>
    <?php if($_SESSION['vote']=='yes'){?>
  <div id="mensaje" rel="alert-slide" class="vote-message" >
      <div class="table">
          <div class="table-cell">
            <?php if( isset($_SESSION['vote']['msg']) ) {?>
             <p data-cache="Saving"><?php echo $_SESSION['vote']['msg'];?></p>
            <?php } else {?>
              <p data-cache="Saving">Thanks for activating your account, your vote has been submitted</p>
            <?php } ?>
          </div>
      </div>
        <span class="closem"><i class="fa fa-times fa-2x" aria-hidden="true"></i></span>
  </div>  
  <?php unset($_SESSION['vote']);?>
    <?php } ?>
  <?php } ?>
    
  <div id="mensaje" class="saving" rel="alert-slide" style="display:none;">
      <div class="table">
          <div class="table-cell">
              <p data-cache="Saving">Saving</p><div class="spinner"></div>
          </div>
      </div>
      <span class="closem"><i class="fa fa-times fa-2x" aria-hidden="true"></i></span>
  </div>
  <div id="warning" class="saving" rel="alert-slide" style="display:none;">
      <div class="table">
          <div class="table-cell">
              <p data-cache="Saving">Saving</p><div class="spinner"></div>
          </div>
      </div>
      <span class="closem"><i class="fa fa-times fa-2x" aria-hidden="true"></i></span>
  </div>  
  <?php if(isset($_REQUEST['action'])){?>
    <?php if($_REQUEST['action'] == 'stir_active'){?>
  
    <div id="stir-active" class="saving" rel="alert-slide">
      <div class="table">
          <div class="table-cell">
              <p data-cache="Saving">Your account is active</p><div class="spinner"></div>
          </div>
      </div>
      <span class="closem"><i class="fa fa-times fa-2x" aria-hidden="true"></i></span>
    </div>  
    <?php } else if($_REQUEST['action']== 'magic_link') {?>
    <div id="stir-active" class="saving" rel="alert-slide">
        <div class="table">
            <div class="table-cell">
                <p data-cache="Saving">Your Magic Link expired</p><div class="spinner"></div>
            </div>
        </div>
        <span class="closem"><i class="fa fa-times fa-2x" aria-hidden="true"></i></span>
    </div>  
    <?php } ?>
  <?php } ?>

  
    <?php /*
   <?php if(  user_reminder_code() ) {?>
   <div id="watch-code-reminder" style="background:#fff;">
    <form name="watch-code" method="post">
      <input name="code" type="text">
      <button rel="button" data-page="whatchcode" data-option="watch-code">Enviar</button>
      <div class="watch-code-message" style="display:none;">
      </div>
    </form>
   </div>
   <?php } ?>
   */ ?>
    </div>
</header>
      
<nav id="menu" class="panel" role="navigation">

    <ul class="list-group">
    
<?php if ( is_user_logged_in() ) { ?>
    
    <?php   $current_user = wp_get_current_user();?>
        <li ><p>Hello <?php echo $current_user->user_login;?></p></li>
        
    <?php if(have_draft()){?>
        <li ><a href="<?php echo esc_url( home_url() );?>/basics/" class="btn-gris-small" data-block="yes">Complete project</a></li>
        
    <?php } else if(user_publishproject()){ ?>
        <li ><a href="<?php echo esc_url( home_url() );?>/basics/?draft" class="btn-gris-small" data-block="yes">Edit my project</a></li>
        
    <?php  } else{?>
        <li ><a href="<?php echo esc_url( home_url() );?>/welcome/" class="btn-gris-small" data-block="yes">Create project </a></li>
        
    <?php } ?>
        
    <?php ; } else{?>
        <li >
          <a href="<?php echo get_option('siteurl'); ?>/welcome/" class="btn-gris-small" data-block="yes">Start a project</a>
        </li>
        <li>
            <a href="#" data-toggle="modal" data-target="#modal-login">Log in</a>
        </li>
        <!--<li >or  here</li>-->
 
<? } ?>         

    <li >
        <a href="<?php echo get_option('siteurl'); ?>/support/" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>Support a Project</a>
    </li>
    <li >
        <a href="<?php echo get_option('siteurl'); ?>/About" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>What is Stir?</a>
    </li>
        
        <li >
            <a href="<?php echo get_option('siteurl'); ?>/blog" <? if ( is_page(array('14','17','12','23','41','21','19')  ) ) { ?>target="_blank"<? } else { ?><? } ?>>Blog</a>
        </li>
    <li class="list-group-item social">
        <a href="https://www.facebook.com/causeastirCBR" target="_blank"><i class="fa fa-facebook-square fa-2x"></i></a>&nbsp;&nbsp;&nbsp;<a href="https://twitter.com/StirCBR" target="_blank"><i class="fa fa-twitter fa-2x"></i></a>&nbsp;&nbsp;&nbsp;<a href="https://www.instagram.com/causeastircbr" target="_blank"><i class="fa fa-instagram fa-2x"></i></a>
    </li>
    
    <?php if ( is_user_logged_in() ) { ?>
    <li ><p class="logout-link-mob"><a href="<?php echo wp_logout_url( get_permalink() ); ?>">Log Out -></a></p></li>
    <?php ; } else{?>
    <?php } ?>
    
</ul>

    
    
    
    
</nav>      
          
          
      
    
          
          <? /*
    <?php if ( is_user_logged_in() ) { ?>
        <?php if(have_draft()){?>
            <!--si tiene borrador sin publicar-->
            <!--<div id="pause-fest-warn" class="box fleft fwidth text-center">
                <div class="container">
                    <div class="col-lg-10 col-lg-offset-1">
                        <form>
                            <h2>Unpublished draft. Lorem Ipsum Dolor Sit Amet?</h2>
                            <button type="submit" class="btn-gris-small">Yes</button>
                            <button type="submit" class="btn-gris-small">No</button>

                        </form> 
                        <p>Read the bases at the <a href="#">blog</a></p>
                    </div>
                </div>
                
            </div> -->         

        <?php } else if(user_publishproject()){ ?>
    
      <!--  nunca eligio -->
      <?php if(notice_season_change('pausefest')){?>
            <!-- si su proyecto ya está publicado-->
            <div id="pause-fest-warn" class="box fleft fwidth text-center">
                <div class="container">
                    <div class="col-lg-10 col-lg-offset-1"> 
                        <form action="season">
                            <h2>Lorem Ipsum Dolor Sit Amet?</h2>
              <fieldset>
                <button type="submit" class="btn btn-gris-small" rel="button" data-page="season" data-option="pausefest">Yes</button>
                <button type="submit" class="btn btn-gris-small" rel="button" data-page="season" data-option="">No</button>
                
              </fieldset>
                        </form> 
                        
                    </div>
                </div>
                
            </div> 
      <?php } ?>
        <?php  } else{?>
            <!--si no tiene ni proyecto publicado ni borrador-->
        <?php } ?>
    <?php ; } else{?>
    <!--si no está logueado-->
    <? } ?>           
          */?>
          
          <!--<div id="pause-fest-warn" class="box fleft fwidth text-center" style="background-color:#eb715f;">
                <div class="container">
                    <div class="col-lg-10 col-lg-offset-1"> 
                        <p ><a style="font-size:18px!important; font-weight:700; color:#FFF;" href="https://causeastir.com.au/support/?season=54">Vote for your favourite #PauseFest project here</a></p>
                    </div>
                </div>
                
            </div>
-->
