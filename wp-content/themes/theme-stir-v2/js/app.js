$(document).ready(function() {
	$("button[rel=button], input[rel=button]").on('click',function(e){
	
		e.preventDefault();	
		try{
			
			$project.getMetodo($(this));
		} catch(e){
			console.log('problema metodo :( ');		
			console.log(e);
		}
		return false;
	});
	
	
	
	$project = new Object();
	$project.getMetodo = function($this){
			//console.log("$project." + $this.data('page') + "('"+$this.data('option')+"')");
			//console.log( $this.data('page') +'#'+$this.data('option'));
			eval("$project." + $this.data('page') + "('"+$this.data('option')+"')");
	}
	
	// Metodo elegibility
	$project.elegibility = function($option){
		var checkboxes = $("input[name='checkboxes']");
		if(checkboxes.is(':checked')){
			$project.loading('show');	
			$.ajax({
			  method: "POST",
			  url:  $base_url+"/wp-admin/admin-ajax.php",
			  data: { action: "project_save", step: "elegibility"}
			}) .done(function( json ) {
				var code = jQuery.parseJSON(json);
				$project.loading('hidden');
		    	if(code.code=="01"){
		    		console.log('BD ==> 01');
		    		return false;
		    	}
		    	window.location.href= code.url;
		    	
		  	});
	  	} else{
	  		checkboxes.parent().addClass('error-caja');
	  	}
	};
	
	// Metodo Basic
	
	$project.basics = function($option){
		//var checkboxes = $("input[name='checkboxes']");
		//if(!checkboxes.is(':checked')) checkboxes.parent().addClass("error-caja"); 
		//&& checkboxes.is(':checked')
		if(!$project.validate($('form[name="basics"]')) ) {
			var formObj = $('form[name="basics"]');
		  	var $data = $('form[name="basics"]').serialize();	
			var $datafile = "";
			$project.loading('show');	
			if(window.FormData !== undefined)  // for HTML5 browsers
			{
				var formData = new FormData(formObj[0]);
				
				$files = $.ajax({
		        	url:  $base_url+"/wp-admin/admin-ajax.php?action=upload_files",
					type: "POST",
					data:  formData,
					mimeType:"multipart/form-data",
					contentType: false,
		    	    cache: false,
					processData:false
			   }).done(function(json){
					var $datafile = "";
					var file = jQuery.parseJSON(json);
					$counthidden = $('.cache-image input[type="hidden"]').length;
			   		$.each(file,function(i,item){
						if(item.key_file=="visuals_project_display") {
							$('.cache-display').html('<img src="'+$base_url+'/wp-content/files_mf/'+item.name+'" width="200">');
						}  else if(item.key_file=="logo") {
							$('.cache-display').html('<img src="'+$base_url+'/wp-content/files_mf/'+item.name+'" width="200">');
						} else{
							$('.cache-image:eq('+$counthidden+')').html('<img src="'+$base_url+'/wp-content/files_mf/'+item.name+'" width="200">');
							$counthidden++;
						}
						//console.log(item);
						$datafile = $datafile+"&file["+item.key_file+"][]="+item.name;
			   		})
			   		setsend( $datafile );
			   });
		   } else {
		   		setsend( $datafile );
		   }
		  
		   setsend = function( $datafile ){ 
			   $data = $data+$datafile+"&action=project_save&step=basics&option="+$option;
				$.ajax({
				  method: "POST",
				  url:  $base_url+"/wp-admin/admin-ajax.php",
				  data: $data
				}) .done(function( json ) {
					var code = jQuery.parseJSON(json);
					$project.loading('hidden');	
			    	if(code.code=="01"){
			    		console.log('BD ==> 01');
			    		return false;
			    	} else if(code.url!=""){
			    			window.location.href= code.url;
			    	} 
			  	});
		  	}
		}
	};
	//Metodo Details
	$project.details=function($option){
		
		var project_old_people = $("input[name='detail_of_project_old_people[]']");
		var project_for = $("input[name='detail_of_project_for']");
		//if(!project_old_people.is(':checked')) project_old_people.parent().addClass("error-caja");  
		if(!project_for.is(':checked')) project_for.parent().addClass("error-caja"); 
		//tinyMCE.get('detail_of_project_detailed').save();
		// && project_old_people.is(':checked')
		if(!$project.validate($('form[name="details"]')) ) {
			var $data = $('form[name="details"]').serialize()+'&action=project_save&step=details&option='+$option;
			$project.loading('show');	
			$.ajax({
				  method: "POST",
				  url:  $base_url+"/wp-admin/admin-ajax.php",
				  data: $data
			}) .done(function( json ) {
				var code = jQuery.parseJSON(json);
				$project.loading('hidden');	
		    	if(code.code=="01"){
		    		console.log('BD ==> 01');
		    		return false;
		    	} else if(code.url!=""){ 
		    		window.location.href= code.url;
		  		}
		  	});
		};
	};
	//metodo visual
	$project.visuals =function($option){
		
		if(!$project.validate($('form[name="visuals"]'))){
			
			var formObj = $('form[name="visuals"]');
		  	var $data = $('form[name="visuals"]').serialize();	
			var $datafile = "";
			$project.loading('show');	
			if(window.FormData !== undefined)  // for HTML5 browsers
			{
				var formData = new FormData(formObj[0]);
				
				$files = $.ajax({
		        	url:  $base_url+"/wp-admin/admin-ajax.php?action=upload_files",
					type: "POST",
					data:  formData,
					mimeType:"multipart/form-data",
					contentType: false,
		    	    cache: false,
					processData:false
			   }).done(function(json){
					var $datafile = "";
					var file = jQuery.parseJSON(json);
					$counthidden = $('.cache-image input[type="hidden"]').length;
					// console.log($counthidden);
			   		$.each(file,function(i,item){
			   			if(item.key_file=="visuals_project_display") {
			   				$('.cache-display').html('<img src="'+$base_url+'/wp-content/files_mf/'+item.name+'" width="200">');
			   			} else if(item.key_file=="logo") {
							$('.cache-display').html('<img src="'+$base_url+'/wp-content/files_mf/'+item.name+'" width="200">');
						} else{
			   				$('.cache-image:eq('+$counthidden+')').html('<img src="'+$base_url+'/wp-content/files_mf/'+item.name+'" width="200">');
			   				$counthidden++;
			   			}
			   			//console.log(item);
			   			$datafile = $datafile+"&file["+item.key_file+"][]="+item.name;
			   		});
			   		
			   		setsend( $datafile );
			   });
		   } else {
		   		setsend( $datafile );
		   }
		   
		   setsend = function( $datafile ){ 
			   $data = $data+$datafile+"&action=project_save&step=visuals&option="+$option;
				$.ajax({
				  method: "POST",
				  url:  $base_url+"/wp-admin/admin-ajax.php",
				  data: $data
				}) .done(function( json ) {
					var code = jQuery.parseJSON(json);
					$project.loading('hidden');	
			    	if(code.code=="01"){
			    		console.log('BD ==> 01');
			    		return false;
			    	} else if(code.url!=""){
			    			window.location.href= code.url;
			    	} 
			  	});
		  	}
		}
	};
	// Metodo team 
	$project.team = function($option){
		var team_project_sex = $("input[name='team_project_sex']");
		if(!team_project_sex.is(':checked')) team_project_sex.parent().addClass("error-caja");  
		if(!$project.validate($('form[name="team"]')) && team_project_sex.is(':checked')) {
			var formObj = $('form[name="team"]');
			var $datafile = "";
			//team_project_sex
			$project.loading('show');	
			if(window.FormData !== undefined)  // for HTML5 browsers
			{
				var formData = new FormData(formObj[0]);
				//nota guardar index de cada elemento y luego modificar functions para traer
				$files = $.ajax({
		        	url:  $base_url+"/wp-admin/admin-ajax.php?action=upload_files",
					type: "POST",
					data:  formData,
					mimeType:"multipart/form-data",
					contentType: false,
		    	    cache: false,
					processData:false
			   }).done(function(json){
					var $datafile = "";
					var file = jQuery.parseJSON(json);
					var other_member_project_picture = [];
					$.each(formObj.find('input[name="other_member_project_picture[]"]'), function(i, item){
						if($(this).val() != ""){
							other_member_project_picture.push(i);
						}	
					});
					
			   		$.each(file,function(i,item){
						if(item.key_file == 'other_member_project_picture') {
							$('input[name="file['+item.key_file+'][]"]').eq(item.index).val(item.name);
							$('.cache[rel='+item.key_file+']').eq(item.index).html('<img src="'+$base_url+'/wp-content/files_mf/'+item.name+'" width="200">');
							
						} else if(item.key_file != 'other_member_project_picture' ) {
							$('.cache[rel='+item.key_file+']').html('<img src="'+$base_url+'/wp-content/files_mf/'+item.name+'" width="200">');
							$datafile = $datafile+"&file["+item.key_file+"][]="+item.name;
						}
			   		});
					
					setsend( $datafile );
			   });
		   } else {
		   		setsend( $datafile );
		   }
		   
		   setsend = function( $datafile ){ 
			   var $data = $('form[name="team"]').serialize();
			   $data = $data+$datafile+"&action=project_save&step=team&option="+$option;
				$.ajax({
				  method: "POST",
				  url:  $base_url+"/wp-admin/admin-ajax.php",
				  data: $data
				}) .done(function( json ) {
					var code = jQuery.parseJSON(json);
					$project.loading('hidden');	
			    	if(code.code=="01"){
			    		console.log('BD ==> 01');
			    		return false;
			    	} else if(code.url!=""){
			    			window.location.href= code.url;
			    	} 
			  	});
		  	}
		}
	}
	//Metodo plan
	$project.plan = function($option){
		//tinyMCE.get('plan_project_support').save();
		//tinyMCE.get('plan_project_stir_money').save();
		//tinyMCE.get('plan_project_future').save();
		if(!$project.validate($('form[name="plan"]'))) {
			$project.loading('show');	
			var formObj = $('form[name="plan"]');
		  	var $data = $('form[name="plan"]').serialize();	
		   $data = $data+"&action=project_save&step=plan&option="+$option;
			$.ajax({
			  method: "POST",
			  url:  $base_url+"/wp-admin/admin-ajax.php",
			  data: $data
			}) .done(function( json ) {
				var code = jQuery.parseJSON(json);
				$project.loading('hidden');	
		    	if(code.code=="01"){
		    		console.log('BD ==> 01');
		    		return false;
		    	} else if(code.url!=""){
		    			window.location.href= code.url;
		    	} 
		  	});
		  	
		}
	}
	$project.support = function($option){
		if(!$project.validate($('form[name="support"]'))) {
			var $data = $('form[name="support"]').serialize()+'&action=support';
			var $mensaje = $('form[name="support"]').find('div[rel=message]');
			$.ajax({
				  method: "POST",
				  url:  $base_url+"/wp-admin/admin-ajax.php",
				  data: $data
			}) .done(function( json ) {
				var code = jQuery.parseJSON(json);
				$mensaje.removeClass('error');
				$mensaje.removeClass('success');
		    	$mensaje.addClass(code.state).html(code.message);
		    	$('form[name="support"]')[0].reset();
		    	if(code.state=="success"){
		    		setTimeout(function() {
		    			window.location.href = window.location.href+"#support";
		    			window.location.reload(true);
		    			//$mensaje.removeClass('success');
					    //$('#survey').removeClass('aparecido');
						//$('#survey').addClass('desaparecido');
					  }, 1000);
				}
		  	});
		};
	}
	$project.preview = function($option){
		$data = 'action=project_save&step=preview&option='+$option;
		$.ajax({
			  method: "POST",
			  url:  $base_url+"/wp-admin/admin-ajax.php",
			  data: $data
		}) .done(function( json ) {
			var code = jQuery.parseJSON(json);
	    	if(code.code=="01"){
	    		console.log('BD ==> 01');
	    		return false;
	    	} else if(code.url!=""){
	    		if($option=='publish')
					$(".preview-mensaje").html("<p>Your project is being published</p>");				
	    		setTimeout(function() {
	    			$project.loading('hidden');	
	    			window.location.href= code.url;
	    		},5000)
	    		
	    	} 
	  	});
	}
	$project.validate= function($element){
		var $error = false;
		$element.find('*[rel=requerido]').each(function(){
			if($.trim($(this).val())==""){
				if($(this).hasClass('tinymce') )
					$(this).prev().addClass('error');
				else if($(this).attr('type')=='file' )
					$(this).closest('.input-field').addClass('error');
				else
					$(this).addClass('error');
				
				$project.loading('show', 'Fields left to complete, review the project');	
				$error = true;
				return false;
				
			}
		});

		return $error;
	}
	$project.loading = function($option,$text ){ 
		var $mensaje = $("#mensaje");
		var $cache = (typeof $text !== "undefined")? true: false;
		var $text = (typeof $text !== "undefined")?  $text : $mensaje.find('p').data('cache');
		if($.trim($text) != ""){
			$mensaje.find('p').html($text);
		}
		if($option=='show'){
			if($cache == false)
				$mensaje.stop(true,true).slideDown();
			else
				$mensaje.stop(true,true).slideDown().delay(15000).slideUp();
			return true;
		}
		$mensaje.delay(15000).stop(true,true).slideUp()
		return true;
	}
	
	$project.create = function($option){
		
		var $data = $('form[name="create"]').serialize()+'&action=create_project';
		var $mensaje = $('form[name="create"]').find('div[rel=message]');
		$.ajax({
			  method: "POST",
			  url:  $base_url+"/wp-admin/admin-ajax.php",
			  data: $data
		}) .done(function( json ) {
			var code = jQuery.parseJSON(json);
	    	if(code.error=="true" || code.error == true){
	    		$('.available').stop(true,true).fadeOut('normal',function(){
					$('.not-available').stop(true,true).fadeIn();
				});
	    		return false;
	    	} else if(code.url!=""){
				//console.log(code.url);
	    		window.location.href= code.url;	    		
	    	} 
	  	});
		return false;
	}
	$project.available = function($post_title){
		
		var $data = "post_title="+$post_title+'&action=available'; 
	
		$.ajax({
			  method: "POST",
			  url:  $base_url+"/wp-admin/admin-ajax.php",
			  data: $data
		}) .done(function( json ) {
			var code = jQuery.parseJSON(json);
			
	    	if(code.error == "false" || code.error ==false){
				//console.log(code);
				$('.not-available').fadeOut('normal',function(){
					$('.available').fadeIn();
				});
			} else{
				
				$('.available').fadeOut('normal',function(){
					$('.not-available').fadeIn();
				});
			}
	  	});
	}
	$project.register = function($option){
		var $data = $('form[name="register"]').serialize()+'&action=ajax_register&response='+grecaptcha.getResponse();
		var $mensaje = $('form[name="register"]').find('div[rel=message]').html('').attr('class','');
		
		$.ajax({
			  method: "POST",
			  url:  $base_url+"/wp-admin/admin-ajax.php",
			  data: $data,
			  beforeSend: function(){
				$("form[name='register']").find('fieldset').attr('disabled','disabled');
			  }
		}) .done(function( json ) {
			var code = jQuery.parseJSON(json);
	    	$mensaje.addClass(code.clase).html(code.mensaje).fadeIn();
			if(typeof code.url !== "undefined" ){
				if (code.url != ""){
					//console.log(code.url);
					$("form[name='register']")[0].reset();
					setTimeout(function() {
						//window.location.href= code.url;
					},2000);
				}
			} else{
				grecaptcha.reset();
			}
	  	}).always(function(){
			$("form[name='register']").find('fieldset').removeAttr('disabled');
		});
	}
	
	$project.registersupport = function($form){
		var $data = $('form[name="'+$form+'"]').serialize()+'&action=ajax_register_support';
		var $mensaje = $('form[name="'+$form+'"]').find('div[rel=message]').html('').attr('class','');
		
		if(!$project.validate($('form[name="'+$form+'"]'))) {
			$.ajax({
				  method: "POST",
				  url:  $base_url+"/wp-admin/admin-ajax.php",
				  data: $data,
				  beforeSend: function(){
					$("form[name='"+$form+"']").find('fieldset').attr('disabled','disabled');
				  }
			}) .done(function( json ) {
				var code = jQuery.parseJSON(json);
				$mensaje.removeClass('error success');
				$mensaje.addClass(code.clase).html(code.mensaje).fadeIn();
				$("form[name='"+$form+"']")[0].reset();
				if(code.clase == 'success' || code.code == "01"){
					setTimeout(function() {
						//location.reload();
					},2000);
					
				}
				
			}).always(function(){
				$("form[name='"+$form+"']").find('fieldset').removeAttr('disabled');
			});
		}
	}
	
	$project.whatchcode = function($form){
		var $data = $('form[name="'+$form+'"]').serialize()+'&action=ajax_code_verify';
		var $mensaje =  $('form[name="'+$form+'"]').find(".class=watch-code-message");
		if(!$project.validate($('form[name="'+$form+'"]'))) {
				$.ajax({
				  method: "POST",
				  url:  $base_url+"/wp-admin/admin-ajax.php",
				  data: $data,
				  beforeSend: function(){
					$("form[name='"+$form+"']").find('fieldset').attr('disabled','disabled');
				  }
			}) .done(function( json ) {
				var code = jQuery.parseJSON(json);
				$mensaje.attr('class','watch-code-message');
				$mensaje.addClass(code.clase).html(code.mensaje).fadeIn();
				if(code.clase == 'success' || code.code == "01"){
					$("form[name='"+$form+"']")[0].reset();
				};
			}).always(function(){
				$("form[name='"+$form+"']").find('fieldset').removeAttr('disabled');
			});;
		}
	}
	
	$project.season = function($season){
		var $data = 'action=ajax_season_change&season='+$season
		$.ajax({
			  method: "POST",
			  url:  $base_url+"/wp-admin/admin-ajax.php",
			  data: $data,
			  beforeSend: function(){
				$("form[name='season']").find('fieldset').attr('disabled','disabled');
			  }
		}) .done(function( json ) {
			console.log(json);
			var code = jQuery.parseJSON(json);
			$project.loading ('show',code.mensaje);
		}).always(function(){
			$("#pause-fest-warn").slideUp();
		});
	}
	
	$userapp = new Object();
	$userapp.login = function($this){
		$form = $this;
		$data = $this.serializeArray();
		$content = $this.closest('[userapp]');
		$data.push({name: 'action', value: 'app_login_ajax'});
		$content.css({ opacity: 0.3 });
		$.post( $base_url+"/wp-admin/admin-ajax.php", $data, function(response){
			if(typeof response.status != "undefined"){
				if(response.status == 'success' ){
					if(typeof response.url != "undefined"){
						$form[0].reset();
						window.top.location = response.url;
					}
				} else {
					$content.find('[role=alert]')
					.attr('class','alert text-left')
					.html(response.mensaje)
					.addClass(response.clase).fadeIn(500).delay(15000).fadeOut();
				}
			}
			$content.css({ opacity: 1 });
		 }, 'json')
	}

	$userapp.magiclink = function($this){
		$form = $this;
		$data = $this.serializeArray();
		$content = $this.closest('[userapp]');
		$data.push({name: 'action', value: 'app_send_magic_link'});
		$content.css({ opacity: 0.3 });
		$.post( $base_url+"/wp-admin/admin-ajax.php", $data, function(response){
			if(typeof response.status != "undefined"){
				$form[0].reset();
				$content.find('[role=alert]')
				.attr('class','alert text-left')
				.html(response.mensaje)
				.addClass(response.clase).fadeIn(500).delay(15000).fadeOut();
			}
			$content.css({ opacity: 1 });
		 }, 'json')
	}

	$('button[rel="userapp"], a[rel="userapp"]').on("click",function(e){
		e.preventDefault();
		var action = $(this).data('action');
		var form = $(this).closest('form')
		$userapp[action](form);
	})

	$(document).on("click","[href='#registration-form']",function(e){
		e.preventDefault();
		$('.nav-tabs a[href="#registration-form"]').tab('show');
	})
	/*
	setTimeout(function() {
        if (location.href.indexOf("#welcome") != -1) {
        	$project.loading('show','Welcome to Stir. Please go and check your e-mail to confirm your account.');
        	setTimeout(function() {
				$project.loading('hide');	
			},10000);
        } else if (location.href.indexOf("#support") != -1) {
        	$project.loading('show','Thanks for your support!.');
        	setTimeout(function() {
				$project.loading('hide');	
			},10000);
        }
    }, 1);
	*/
	$("button[rel=social], a[rel=social]").on('click',function(e){
		if($(this).attr('rel') != ""){
			$redes.api($(this));
		}
	});
	//observar que esten todas las api cargadas antes
	$redes = new Object();
	
	$redes.api = function($this){
		$action = $this.data('action');
		$red  = $this.data('red');
		try{
			$redes[$red]($action);
		} catch(err) {
			console.log('red social no permitida :( !');
		}
	};
	
	$redes.facebook = function($action){
		if( ( $action == 'login' || $action=='support' ) && typeof FB.login != 'undefined'){
			FB.login(function (response) {
				$project.loading('show','Connected to Facebook, wait a moment.')
				if (response.authResponse) {
					$redes.facebook.get($action);
				} else {
					console.log('not login');
				}
			}, {scope: 'email'});
		}
	}
	$redes.facebook.get = function($action){
		FB.api('/me', {
			locale: 'en_US', 
			fields: 'id,first_name,last_name,email,link,gender,locale,picture'
		}, function(response){
			if( typeof $action == 'undefined' || $action == 'login' ){
				$redes.facebook.set(response);
			} else if($action == 'support'){
				$redes.facebook.support(response);
			}
			
		});
	}
	$redes.facebook.set = function(response){
		var guest = window.location.search.indexOf('guest');
		var url = window.location.href;
		guest = (guest > -1)? true : false;
		$.post( $base_url+"/wp-admin/admin-ajax.php", {	
			action: 'ajax_register_facebook',
			oauth_provider:'facebook',
			dataType: 'json',
			user: JSON.stringify(response),
			guest: guest,
			url : url
		}, function(response){ 
			$project.loading('show', response.msg);
			if(response.status == 'success'){
				if( typeof response.url !=  'undefined'){
					top.location.href = response.url;
				}
			}

		});
	}

	$redes.facebook.support = function(response){
		var form = $('form[name="support"]');
		var msg = form.find('div[rel=message]');
		var support = $project.arrtoject( form.serializeArray() );
		$.post( $base_url+"/wp-admin/admin-ajax.php", {	
			action: 'ajax_support_facebook',
			oauth_provider:'facebook',
			dataType: 'json',
			user: JSON.stringify(response),
			support: JSON.stringify(support)
		}, function(response){ 
			// msg.removeClass('success error').addClass(response.status).html(response.msg);
			$project.loading('show', response.msg);
			if(response.status == 'success'){
				setTimeout(function() {
	    			window.location.href = response.url;
				}, 1000);
				form[0].reset();
			}
		});
	}

	$project.arrtoject = function(array){
		var data = {};
		$( array ).each(function(index, obj){
		    data[obj.name] = obj.value;
		});
		return data;
	}
});
function getDoc(frame) {
     var doc = null;
     
     // IE8 cascading access check
     try {
         if (frame.contentWindow) {
             doc = frame.contentWindow.document;
         }
     } catch(err) {
     }
     if (doc) { // successful getting content
         return doc;
     }
     try { // simply checking may throw in ie8 under ssl or mismatched protocol
         doc = frame.contentDocument ? frame.contentDocument : frame.document;
     } catch(err) {
         // last attempt
         doc = frame.document;
     }
     return doc;
 }
$("#multiform").submit(function(e)
{
	var formObj = $(this);
	var formURL = formObj.attr("action");
	if(window.FormData !== undefined)  // for HTML5 browsers
	{
	
		var formData = new FormData(this);
		$.ajax({
        	url: formURL,
			type: "POST",
			data:  formData,
			mimeType:"multipart/form-data",
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data, textStatus, jqXHR)
		    {
		    },
		  	error: function(jqXHR, textStatus, errorThrown) 
	    	{
	    	} 	        
	   });
        e.preventDefault();
   }
   else  //for olden browsers
	{
		//generate a random id
		var  iframeId = "unique" + (new Date().getTime());
		//create an empty iframe
		var iframe = $('<iframe src="javascript:false;" name="'+iframeId+'" />');
		//hide it
		iframe.hide();
		//set form target to iframe
		formObj.attr("target",iframeId);
		//Add iframe to body
		iframe.appendTo("body");
		iframe.load(function(e)
		{
			var doc = getDoc(iframe[0]);
			var docRoot = doc.body ? doc.body : doc.documentElement;
			var data = docRoot.innerHTML;
			//data return from server.
			
		});
	
	}
});
$("#multiform").submit();

/* KeyUp from available project */

var timer;
$( "input[name=post_title]" ).keyup(function( event ) {
	var $post_title = $(this).val();
	clearTimeout(timer);
    var ms = 250; // milliseconds
    var val = this.value;
    timer = setTimeout(function() {
		$project.available($post_title);
    }, ms);
});


// valid para class de form
$('select[name=located]').change(function(){
		var age = $('input[name=age]').val();
		var located = $(this).find('option:selected').val();
		if(age != "" && typeof age != undefined && located != "" ){
			if( located =='ACT' && ( parseInt(age)>= 15 && parseInt(age) <= 30 ) ){
				$('.card-panel.no-elegible').hide();
				$('.card-panel.elegible').show();
				
			} else {
				$('.card-panel.elegible').hide();
				$('.card-panel.no-elegible').show();
				
			}
		}
})
$('input[name=age]').keyup(function(){
	clearTimeout(timer);
	var age = $('input[name=age]').val();
	var located = $('select[name=located]').find('option:selected').val();	
    var ms = 150; // milliseconds
    var val = this.value;
	if(age != "" && typeof age != undefined && located != "" ){
		timer = setTimeout(function() {
			
			if( located =='ACT' && ( parseInt(age)>= 15 && parseInt(age) <= 30 ) ){
				$('.card-panel.no-elegible').hide();
				$('.card-panel.elegible').show();
				
			} else {
				$('.card-panel.elegible').hide();
				$('.card-panel.no-elegible').show();
				
			}
		}, ms);
	}
})
