$(document).ready(function() {
	$("button[rel=comment]").on("click",function(e){
		e.preventDefault();
		$this = $comment.comment($(this));
	});
	$comment = new Object();
	
	$comment.comment = function($this){
		$formObj = $this.parent();
		$box = $this.parent().parent();
		$option = $this.data('option');
		
		
		if(!$comment.validate( $formObj )){
		  	
			var $data = 'action=project_comment&type='+$formObj.attr('name')+'&option='+$this.data('option')+'&'+$formObj.find('input, textarea').serialize();
			
			$.ajax({
				  method: "POST",
				  url:  $base_url+"/wp-admin/admin-ajax.php",
				  data: $data,
				  beforeSend: function(){
					$formObj.css("opacity","0.5");
					$formObj.find('textarea').attr('disabled','disabled');
					
				  }
			}) .done(function( json ) {
				try{
					var code = jQuery.parseJSON(json);
					$formObj.css("opacity","1");
					$comment.loading('show', code.message);	
					if(code.state !='error' && $option=='comment_resolve'){
						$formObj.fadeOut(function(){
							$box.find('.helpful').fadeIn();
						})
					}
					setTimeout(function() {
						$comment.loading('hidden');
					},5000);
					
					if(typeof code.url !== "undefined")
						location.reload();
					if(typeof code.html !== "undefined")
						$formObj.parent().find('.lista-comentarios').append(code.html);
					
					$formObj.find('textarea').removeAttr('disabled');
					$formObj[0].reset();
				} catch(err) {
					console.log(err);
				}					
			});
		}
	}
	
	$comment.validate= function($element){
		var $error = false;
		$element.find('*[rel=requerido]').each(function(){
			
			if($.trim($(this).val())==""){
				if($(this).hasClass('tinymce'))
					$(this).prev().addClass('error');
				else
					$(this).addClass('error');
				$error = true;
			}
		});
		return $error;
	}
	$comment.loading = function($option,$text){ 
		var $mensaje = $("#mensaje");
		if($.trim($text) != ""){
			$mensaje.find('p').html($text);
		}
		if($option=='show'){
			$mensaje.slideDown();
			return true;
		}
		$mensaje.delay(500).slideUp()
		return true;
	}
});