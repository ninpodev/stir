typer('#typercon',40)
.run(function() { $("#logo-curtain").addClass('fadein');})
.pause(500)
  .line('PUBLISH YOUR CREATIVE PROJECT, GATHER VOTES AND RECEIVE A $500 GRANT TO PURSUE YOUR PASSIONS.')
.pause(500)
.run(function() { $("#startproject").addClass('fadein');})
.pause(500)
.run(function() { $("#footer-curtain").addClass('fadein');})
.pause(500)
.run(function() { $("#keep-scrolling").addClass('fadein');})
.end();


$(function(){
   $('.curtains').curtain({
       scrollSpeed: 300,
       nextSlide: function(){
            $("#header-curtain").addClass('fadein');
        },
       prevSlide: function(){
            $("#header-curtain").removeClass('fadein');
        }
   });
    
});