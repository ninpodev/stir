var viewWidth, viewHeight;

var container = document.getElementById("wave-container");
if(container) {
	container.addEventListener("click", function(e) {
		if(container.className === 'triggered') {
			container.className = '';
		}
		else {
			container.className = 'triggered';
		}
	});
    //F7931E amarillo
    //FF592A naranjo
    //EF4136 rojo

	// view infos
	viewWidth = container.offsetWidth;
	viewHeight = container.offsetHeight;

	var wavesArray = [];
	
	wavesArray[0] = new Wave(
		"bottomPaper",
		"waves-bottom",
		0.5, // opacity
		//['#ef3124','#823088',],
        ['#F7931E', '#FF592A',  '#F7931E' ],
		9,
		viewWidth / 2 , 
		40,
		60,
		0.5,
		-1,
		1,
		"wave-container"
	);
	
	 wavesArray[1] = new Wave(
		"midBottomPaper",
		"waves-middle-bottom",
		0.8,
		['#F7931E',  '#EF4136','#F7931E' ],
		9,
		0, 
		30,
		75,
		1,
		-1.5,
		1.2,
		"wave-container"
	);
	
	
	//F7931E amarillo
    //FF592A naranjo
    //EF4136 rojo 
    
	wavesArray[3] = new Wave(
		"midTopPaper",
		"waves-middle-top",
		0.9,
        ['#F7931E', '#FF592A',  '#F7931E' ],
		//'#823088',
		6,
		0,
		50,
		80,
		0.7,
		-3,
		0.7,
		"wave-container"
	);

	
	
	wavesArray[4] = new Wave(
		"topPaper",
		"waves-top",
		0.8,
		['#F7931E','#FF592A','#F7931E','#FF592A'],
		4,
		0,
		40,
		120,
		0.5,
		1.5,
		0.4,
		"wave-container"
	);

	/* wavesArray[0] = new Wave(
		"bottomPaper",
		"waves-bottom",
		0.7,
		['#ef3124', '#f7931e'],
		14,
		viewWidth / 5,
		40,
		30,
		1.7,
		-1,
		1,
		"wave-container"
	);
	
	wavesArray[4] = new Wave(
		"bottomPaper2",
		"waves-bottom-2",
		1,
		['#f7931e', '#ef3124'],
		14,
		viewWidth / 5,
		viewHeight + 140,
		25,
		1.7,
		-1,
		1,
		"wave-container"
	);

	wavesArray[1] = new Wave(
		"midBottomPaper",
		"waves-middle-bottom",
		0.7,
		['#f7931e', '#ef3124'],
		14,
		0,
		40,
		45,
		1.3,
		1,
		1,
		"wave-container"
	);
	
 	wavesArray[5] = new Wave(
		"midBottomPaper2",
		"waves-middle-bottom-2",
		1,
		'#ef3124',
		14,
		0,
		viewHeight + 140,
		40,
		1.3,
		1,
		1,
		"wave-container"
	);

	wavesArray[2] = new Wave(
		"midTopPaper",
		"waves-middle-top",
		0.7,
		['#ef3124', '#f7931e'],
		14,
		0,
		40,
		50,
		1.7,
		-1,
		0.75,
		"wave-container"
	);
	
	wavesArray[6] = new Wave(
		"midTopPaper2",
		"waves-middle-top-2",
		1,
		'#ef3124',
		14,
		0,
		viewHeight + 140,
		45,
		1.7,
		-1,
		0.75,
		"wave-container"
	);

	wavesArray[3] = new Wave(
		"topPaper",
		"waves-top",
		0.7,
		['#f7931e', '#ef3124'],
		14,
		0,
		40,
		60,
		1.3,
		1,
		0.5,
		"wave-container"
	);
	
 	wavesArray[7] = new Wave(
		"topPaper2",
		"waves-top-2",
		1,
		'#ef3124',
		14,
		0,
		viewHeight + 140,
		55,
		1.3,
		1,
		0.5,
		"wave-container"
	); */
}

var footerWaveContainer = document.getElementById("footer-wave");
if(footerWaveContainer) {
	// view infos
	footerViewHeight = footerWaveContainer.offsetHeight;
	//footerTopViewHeight = document.getElementById("footer-white-wave").offsetHeight;
	
	var yStartCoord = Math.floor(Math.random() * (50 - 10 + 1) + 10);
	var penteCoeff = Math.floor(Math.random() * (40 - 30 + 1) + 30);
	var upOrDown = Math.floor(Math.random() * (2 - 1 + 1) + 1);
	var wavesOffset = (footerViewHeight * 16) / 100;
	//upOrDown = 1;
	if(upOrDown === 2) {
		penteCoeff = -1* penteCoeff;
		wavesOffset = wavesOffset * 3;
	}
	else {
		penteCoeff = 1.3 * penteCoeff;
		yStartCoord = yStartCoord / 3;
		//console.log(penteCoeff, yStartCoord, penteCoeff/1.3, wavesOffset, yStartCoord + wavesOffset);
	}
	
	footerWave = new Wave(
		"footerWave",
		"footer-wave",
		0.7,
		['#ffffff', '#ef3124'],
		10,
		0,
		yStartCoord,
		penteCoeff,
		1.7,
		1,
		1,
		["content-overlay", "footer"]
	);
	
	footerTopWave = new Wave(
		"footerWhiteWave",
		"footer-white-wave",
		1,
		'#ffffff',
		10,
		0,
		yStartCoord + wavesOffset,
		penteCoeff/1.3,
		1.7,
		1,
		1,
		["content-overlay", "footer"]
	);
	
	/* footerWave = new Wave(
		"footerWave",
		"footer-wave",
		0.8,
		['#ffffff', '#ef3124'],
		14,
		0,
		yStartCoord,
		penteCoeff,
		1.7,
		-1,
		1,
		"footer-canvas"
	); */
	
	/* footerTopWave = new Wave(
		"footerWhiteWave",
		"footer-white-wave",
		1,
		'#ffffff',
		14,
		0,
		yStartCoord + 150,
		penteCoeff/1.3,
		1.7,
		-1,
		1,
		"footer-canvas"
	); */
}