(function($) {
	$('.out').attr('target','_blank');
	
	// home header background
	if($('body').hasClass('home')) {
		function triggerHeader() {
			var heightToTrigger = $('#home-festival').offset().top - 100
			
			if($(window).scrollTop() >= heightToTrigger) {
				$('#header').addClass('red-header');
			}
			else {
				$('#header').removeClass('red-header');
			}
		}
		
		triggerHeader();
		
		$(window).scroll(function() {
			triggerHeader();
		});
	}
	
	// main menu
	$('#main-menu-link').click(function() {
		$(this).toggleClass('toggled');
		$('#header').toggleClass('toggled');
		return false;
	});
	
	// searchform
	$('#searchsubmit').click(function() {
		if(!$('#searchform #s').val()) {
			$('#searchform-wrapper').toggleClass('toggled');
			return false;
		}
	});
	
	// animation between pages
	$('#main-menu-wrapper a').click(function() {
		$('#main-menu-link, #header').removeClass('toggled');
	});
	
	$('a').click(function(e) {
		var link = $(this).attr('href');
		if(!$(this).hasClass('out') && link.indexOf('#') === -1 && link.indexOf('mailto') === -1 && !e.ctrlKey && e.which !== 2) {
			$('#inner-content').stop().animate({
				'opacity': 0
			});
			document.location.href = link;
			e.preventDefault();
		}
	});
	
	// footer back to top
	var container;
	if($('body').hasClass('mobile-body')) {
		container = $('body');
	}
	else {
		container = $('html, body');
	}
	$('#footer .back-to-top').click(function() {
		if($('body').hasClass('mobile-body')) {
			$('html, body').css('height', 'auto');
		}
		container.stop().animate({
			scrollTop: 0
		},600);
		return false;
	});
	
	// fade in au chargement
	$('#inner-content').css({
		'opacity': 0,
		'visibility': 'visible'
	}).stop().animate({
		'opacity': 1
	});
	
	
	// programme chronologique
	var progInitialSlide, mocfInitialSlide = 0;
	if($('body').hasClass('date-03-03-2016')) { // Jeu. 03
		progInitialSlide = 1;
		mocfInitialSlide = 0;
	}
	else if($('body').hasClass('date-04-03-2016')) { // Ven. 04
		progInitialSlide = 5;
		mocfInitialSlide = 0;
	}
	else if($('body').hasClass('date-05-03-2016')) { // Sam. 05
		progInitialSlide = 10;
		mocfInitialSlide = 0;
	}
	else if($('body').hasClass('date-06-03-2016')) { // Dim. 06
		progInitialSlide = 15;
		mocfInitialSlide = 0;
	}
	
	$('#programme-chronologique').slick({
		infinite: false,
		variableWidth: true,
		initialSlide: progInitialSlide,
	//	slidesToShow: 2,
		appendArrows: $('#programme-chronologique-nav')
	});
	
	// programme chronologique mocf
	$('#programme-chronologique-mocf').slick({
		infinite: false,
		variableWidth: true,
		initialSlide: mocfInitialSlide,
		//slidesToShow: 1,
		appendArrows: $('#programme-chronologique-mocf-nav')
	});
	
	if($('#programme-chronologique-nav').length > 0) {
		adjustTimelineNav('#home-programme', '#programme-chronologique-nav');
		
		$(window).resize(function() {
			adjustTimelineNav('#home-programme', '#programme-chronologique-nav');
		});
	}
	if($('#programme-chronologique-mocf-nav').length > 0) {
		adjustTimelineNav('#home-mocf', '#programme-chronologique-mocf-nav');
		
		$(window).resize(function() {
			adjustTimelineNav('#home-mocf', '#programme-chronologique-mocf-nav');
		});
	}
	
	function adjustTimelineNav(timelineWrapper, timelineNav) {
		if($(timelineWrapper).height() >= ($(window).height() - $('#header').height())) {
			$(timelineNav).addClass('small-screen');
		}
		else {
			$(timelineNav).removeClass('small-screen');
		}
	}
	
	//programme switcher
	$('.programme-switcher').click(function() {
		if(!$(this).hasClass('active')) {
			$('.programme-switcher').toggleClass('active');
			$('.programme-template').toggleClass('programme-template-hidden');
		}
		return false;
	});
	
	if($('body').hasClass('post-type-archive-programme')) {
		var hashContent;
		if(window.location.hash) {
			hashContent = window.location.hash.substring(1);
			// custom program select
			tamingselect(hashContent);
			changeProgram(hashContent);
		}
		else {
			tamingselect();
			changeProgram('all');
		}
		
		$(window).bind('hashchange', function() {
			hashContent = window.location.hash.substring(1);
			changeProgram(hashContent);
		});
		
		$(window).resize(function() {
			changeProgram(hashContent || 'all');
		});
	}
	else if($('body').hasClass('page-template-billeterie')) {
		changeProgram('all');
		$(window).resize(function() {
			changeProgram('all');
		});
	}
	
	function changeProgram(programFilter) {
		var nbSortedElement = 0;
		var nbElementPerColumn = 4;
		
		if(window.matchMedia("(max-width: 780px)").matches) {
			nbElementPerColumn = 2;
		}
		else if(window.matchMedia("(max-width: 1080px)").matches) {
			nbElementPerColumn = 3;
		}
		
		$('#programme-images .quarter-col').each(function() {
			$(this).removeClass('sorted-element last clear');
			if($(this).hasClass(programFilter)) {
				$(this).addClass('sorted-element');
				$(this).show();
				nbSortedElement++;
				if(nbSortedElement % nbElementPerColumn === 0) {
					$(this).addClass('last');
				}
				if(nbSortedElement % nbElementPerColumn === 1) {
					$(this).addClass('clear');
				}
			}
			else {
				$(this).hide();
			}
		});
	}
	
	
	// tracking liens billetterie
	$('#global').on('click', '.billetterie-outside-link', function() {
		ga('send', 'event', 'Clic lien billetterie', 'Click');
	});
	
	// player radio
	/* var isRadioPlaying = false;
	$('.jp-play').click(function() {
		$(this).toggleClass('jp-playing');
		isRadioPlaying = !isRadioPlaying;
		if(isRadioPlaying) {
		//	ga('send', 'event', 'Ecoute radio', 'Click');
		}
		return false;
	}); */
	
})(jQuery);

// trigger onload event on back/forth navigation
window.onunload = function(){};