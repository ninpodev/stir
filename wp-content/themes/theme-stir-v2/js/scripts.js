//Flexslider
$(document).ready(function(){
    $('#main-slide-home').flexslider({
        animation: "fade",
        controlNav: true,
        directionNav:true,
        slideshowSpeed: 15000,      
        animationSpeed: 1000,
    });
    $('#support-slide').flexslider({
        animation: "fade",
        controlNav: true,
        directionNav:false,
        slideshowSpeed: 15000,      
        animationSpeed: 1000,
    });
    $('#slide-single').flexslider({
        animation: "fade",
        controlNav:false,
        directionNav:true,
        slideshowSpeed: 10000,      
        animationSpeed: 500,
        prevText:"&nbsp;",
        nextText:"&nbsp;",
        start: function(){
         $('#contenedor-slide').removeClass('bggif'); 
        },
    });
    $('#slider-testimonios').flexslider({
        animation: "fade",
        controlNav: true,
        directionNav:false,
        slideshowSpeed: 12000,      
        animationSpeed: 500,
    });
    $('.slider-inblog').flexslider({
        animation: "fade",
        controlNav: true,
        directionNav:true,
        slideshowSpeed: 12000,      
        animationSpeed: 500,
    });
});

$('.menu-link').click(function() {
    $('#menu').toggleClass('opened');
});

//filestyle 
/*$(":file").filestyle({
    icon: false,
    size:"xs"
});
*/

//Ancla animada 
$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
//cambiar estado pestaña feedback 
$(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    if (scroll >= 1000) {
        $("#un-button").addClass("hscrolled");
    } else {
        $("#un-button").removeClass("hscrolled");
    }
});
/*#un-button.hscrolled*/

//inline title 
$.fn.mathSpace = function() {
  return $(this).each(function(){
    $(this).children('span').each(function() {
      var el = $(this);
      var text = el.text();
      el.text(
        text.split(' ').join('\u205f')
      ); 
    });
  });
}

$('.title-slider').mathSpace();

//Scroll home
$(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    
    if (scroll >= 100) {
    $(".img-scroll").removeClass("ocultar").addClass("animated  slideInDown");
    }
    
});
var $w = $(window), $display = $(".pixeles");
$w.on("scroll", function(e){
  $display.text($w.scrollTop())
});
/*
$("#abrecierraguidance").click(function() {
$('.guidance').toggleClass("abierto");
$('.contenidoguidance').toggleClass("contenidooculto");
});
*/
//Guidance Switch
$('#guidanceswitch').change(function() {
    //$('.guidance').toggleClass("abierto");
    //$('.contenidoguidance').toggleClass("contenidooculto");
});


var parallaxElements = $('.parallax'),
    parallaxQuantity = parallaxElements.length;

$(window).on('scroll', function () {
  window.requestAnimationFrame(function () {
    for (var i = 0; i < parallaxQuantity; i++) {
      var currentElement =  parallaxElements.eq(i);
      var scrolled = $(window).scrollTop();
        currentElement.css({
          'transform': 'translate3d(0,' + scrolled * 0.5 + 'px, 0)'
        });
    }
  });
});



//maxlength de la edad
function maxLengthCheck(object) {
    if (object.value.length > object.max.length)
      object.value = object.value.slice(0, object.max.length)
  }
    
  function isNumeric (evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode (key);
    var regex = /[0-9]|\./;
    if ( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }

//disabled register
 $('input#bigsubmit').on('click',function(){
    var $thisbutton = $(this).addClass('disabled');
    window.setTimeout(function(){
        $thisbutton.removeClass('disabled');
    }, 5000); //<-- Delay in milliseconds
 });

//publishing message
$('button.save-button').on('click',function(){
    $('#publishit.saving').removeClass('ocultar');
    $('#publishit.saving').addClass('mostrar');
 });
//cerrar message 
$('span.closem').on('click',function(){
    $('#mensaje.vote-message').addClass('ocultar');
    $('#mensaje.saving').addClass('ocultar');
    $('#warning.saving').addClass('ocultar');
    $('#publishit.saving').addClass('ocultar');
	$('#stir-active.saving').addClass('ocultar');
 });



//modal login
$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').focus()
});
$('.modal-trigger').leanModal();

// $('#modal-login').modal('show');

 
//Material
$('select.material').material_select();
$('input.contado').characterCounter();

$('input[type="submit"]').click(function() {
    var $thissubmit = $(this).addClass('disabled');
    window.setTimeout(function(){
        $thissubmit.removeClass('disabled');
    }, 5000); //<-- Delay in milliseconds
});
$('*[data-block="yes"]').click(function() {
    var $thisbut = $(this).addClass('disabled');
    window.setTimeout(function(){
        $thisbut.removeClass('disabled');
    }, 5000); //<-- Delay in milliseconds
});

$("button.support-button").click(function(){
	$('body').addClass('overflow-hidden');
	if($('#survey').hasClass('desaparecido'))
	{
		$('#survey').removeClass('desaparecido');
		$('#survey').addClass('aparecido');
	}
	/*$('.donation_box').addClass('animated');
	$('.donation_box').addClass('bounceInDown');
    */
});
$("button.cerrar").click(function(){
	$('body').removeClass('overflow-hidden');
	if($('#survey').hasClass('aparecido'))
	{
		$('#survey').removeClass('aparecido');
		$('#survey').addClass('desaparecido');
	}
	/*$('.donation_box').removeClass('animated');
	$('.donation_box').removeClass('bounceInDown');
    */
});



/*imagenes*/
$("#insert-more").click(function () {
	$("#mytable").each(function () {
		 var tds = '<tr class="nueva">';
		 jQuery.each($('tr:last td', this), function () {
		 	 $this = $(this).clone();
		 	 $this.find('img').remove();
		 	 $this.find('input[type="hidden"]').remove()
			 tds += '<td>' + $this.html() + '</td>';
		 });
		 tds += '</tr>';
		 if ($('tbody', this).length > 0) {
			 $('tbody', this).append(tds);
		 } else {
			 $(this).append(tds);
		 }
	});
	$("#grouptable").each(function(){
		var $this = $(this);
		$clone = $this.find('tbody:first tr').clone();
		$clone.find('input').removeAttr('value');
		$clone.find('option').removeAttr('selected');
		$this.find('tbody:first').append('<tr class="nueva">'+$clone.html()+'</tr>');

	});
});
$( "#remove-more").click(function() {
    if($('.cache-image').size() == 1){
        $('.cache-image').empty();
        $('.cache-image').closest('td').find('input').val('');
    }
    $(".nueva:last-child").remove();
});


$(".check-caja").hover(function(){
    $('p.error-caja').removeClass('error-caja');
});
$("input#age").hover(function(){
    $(this).removeClass('error');
});
$("input#cityfrom").hover(function(){
    $(this).removeClass('error');
});



/*links*/
$("#insert-more-links").click(function () {
	
	$("#mytable-links").each(function () {
		 var time = (new Date()).getTime();
		
		 var tds = '<tr class="nueva-link">';
		 jQuery.each($('tr:last td', this), function () {
		 	$this = $(this).clone();
			$this.find('.fa-select.material').html(
				$('<select/>',{
					class:'clone-material',
				}).html('<option >Facebook</option>'+
						'<option >Linked-In</option>'+
						'<option >Twitter</option>'+
						'<option >Google+</option>'+
						'<option >YouTube</option>'+
						'<option >Pinterest</option>'+
						'<option >Souncloud</option>'+
						'<option >Instagram</option>'+
						'<option >Behance</option>'+
						'<option >Kickstarter</option>'+
						'<option >Other</option>')
			).removeClass('initialized').removeClass('select-wrapper');
			tds += '<td>' + $this.html() + '</td>';
		 });
		 tds += '</tr>';
		 if ($('tbody', this).length > 0) {
			 $('tbody', this).append(tds);
		 } else {
			 $(this).append(tds);
		 }
	});
	$('select.clone-material').material_select();
});
$( "#remove-more-links" ).click(function() {
  $(".nueva-link:last-child").remove();
});

/*Remover las clases de error*/
$(".input-field input, .input-field textarea").click(function() {
  $(this).removeClass('error');
});

/*Video*/
$('#video-si').hide();

if($("#video_1").is(':checked')==false){
	$('#video-si').hide();
} else {
	$('#video-si').show();
}
$('#video_1').click(function() {
	$('#video-si').show();
});
$('#video_0').click(function() { 
	$('#video-si').hide().find('input').val('');
});

if($('#yes-team').is(':checked')==false){
	$('.team-members').hide();
}
$('#no-team').click(function() {
	$('.team-members').hide();
});
$('#yes-team').click(function() {
	$('.team-members').show();
});



/*Responsive menu*/
$('.menu-link').bigSlide();


//header
$("#head-main").stick_in_parent({});
//filtro de busqueda + barra support proyecto
$("#filter-bar, #share-support").stick_in_parent({ 
    offset_top: 50 
});
$(".sidebar-filtro").stick_in_parent({
    parent: "#contenedor-support",
    offset_top: 70 
});
$('.sidebar-filtro')
.on('sticky_kit:bottom', function(e) {
    $(this).parent().css('position', 'static');
})
.on('sticky_kit:unbottom', function(e) {
    $(this).parent().css('position', 'relative');
});

$(document).ready(function(){


    var window_width = $( window ).width();

    if (window_width < 768) {
      $(".sidebar-filtro").trigger("sticky_kit:detach");
    } else {
      make_sticky();
    }

    $( window ).resize(function() {

      window_width = $( window ).width();

      if (window_width < 768) {
        $(".sidebar-filtro").trigger("sticky_kit:detach");
      } else {
        make_sticky();
      }

    });

    function make_sticky() {
      $(".sidebar-filtro").stick_in_parent({
        parent: "#contenedor-support",
        offset_top: 70 
        });
    }

});







/*Abre menu*/
$('#nav-icon1').click(function(){
    $(this).toggleClass('open');
});
/*probando pipeline*/


/*Comentarios*/
$('.modulo-comentarios').hide();
/*Botón para cerrar todo el módulo comentarios*/
$("span.cerrar-modulo").click(function(){
  $(this).parent().fadeToggle(400);
});
$('.modulo-contenido span.icos-comentarios').click(function() {
    //$(this).closest('.comentarios').find('.modulo-comentarios').fadeToggle(400);
	console.log($(this).parent().parent().find('.comentarios'));
    $(this).parent().parent().find('.modulo-comentarios').fadeToggle(400);
}); 


/*Boton para abrir comentarios
$('span.agregar-nuevo-comentario').on('click', function(event) {        
    $('#agregar-nuevo-comentario').fadeToggle(400);
});
/*Boton para abrir comentario sin reespuesta visto desde el creador del comentario 
$('span.comentario-sin-respuesta').on('click', function(event) {        
    $('#comentario-sin-respuesta').fadeToggle(400);
});
/*Boton para abrir comentario sin reespuesta visto desde el creador del comentario 
$('span.comentario-con-respuesta').on('click', function(event) {        
    $('#comentario-con-respuesta').fadeToggle(400);
});
/*Boton para abrir comentario para moderar visto desde el autor del proyecto 
$('span.comentario-para-moderar').on('click', function(event) {        
    $('#comentario-para-moderar').fadeToggle(400);
});
*/

/*Apertura y cierre general de modulo de comentarios*/
/*Boton para abrir comentario para moderar visto desde el autor del proyecto */ 
/*$('span.icos-comentarios').on('click', function(event) {        
    $('.comentarios .modulo-comentarios').fadeToggle(400);
    $(this).find('.comentarios .modulo-comentarios').fadeToggle(400);
});
*/



//$("span.cerrar-modulo").click(function(){
  //$(this).parent().fadeToggle(400);
//});

/*Remover las clases de error*/
$(".contenedor-textarea").mouseover(function(event){
	if($("#mceu_15").hasClass('error'))
	{
		$("#mceu_15").removeClass('error');
	}
});
$(".contenedor-textarea").mouseover(function(event){
	if($("#mceu_43").hasClass('error'))
	{
		$("#mceu_43").removeClass('error');
	}
});
$(".contenedor-textarea").mouseover(function(event){
	if($("#mceu_71").hasClass('error'))
	{
		$("#mceu_71").removeClass('error');
	}
});
$(".check-inicio").mouseover(function(event){
	if($("label").hasClass('error-caja'))
	{
		$("label").removeClass('error-caja');
	}
});


 $('#single .contenedor-comentarios button[type="submit"]').on('click',function(){
    var $this = $(this).addClass('disabled');
    window.setTimeout(function(){
        $this.removeClass('disabled');
    }, 5000); //<-- Delay in milliseconds
 });
 

//validacion mail
$("#user-register").on('submit', function(e) {
    var email = $("#user_email").val(),
        confirm = $("#user_email_confirm").val();

    if (email!=confirm) {
       alert('emails are not the same!');
       e.preventDefault();
    }
});


var ww = document.body.clientWidth;
$(document).ready(function() {
  $(".nav li a").each(function() {
    if ($(this).next().length > 0) {
        $(this).addClass("parent");
        };
    })

    $(".toggleMenu").click(function(e) {
        e.preventDefault();
        $(this).toggleClass("active");
        $(".nav").toggle();
    });
    adjustMenu();
})

$(window).bind('resize orientationchange', function() {
    ww = document.body.clientWidth;
    adjustMenu();
});

var adjustMenu = function() {
    if (ww < 991) {
    // if "more" link not in DOM, add it
    if (!$(".more")[0]) {
    $('<div class="more">&nbsp;</div>').insertBefore($('.parent')); 
    }
        $(".toggleMenu").css("display", "inline-block");
        if (!$(".toggleMenu").hasClass("active")) {
            $(".nav").hide();
        } else {
            $(".nav").show();
        }
        $(".nav li").unbind('mouseenter mouseleave');
        $(".nav li a.parent").unbind('click');
    $(".nav li .more").unbind('click').bind('click', function() {

            $(this).parent("li").toggleClass("hover");
        });
    } 
    else if (ww >= 991) {
    // remove .more link in desktop view
    $('.more').remove(); 
        $(".toggleMenu").css("display", "none");
        $(".nav").show();
        $(".nav li").removeClass("hover");
        $(".nav li a").unbind('click');
        $(".nav li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
            // must be attached to li so that mouseleave is not triggered when hover over submenu
            $(this).toggleClass('hover');
        });
    }
}	


/*
tinymce.init({
	theme: "modern",
    skin: 'lightgray',
	 menubar : false,
	selector:'textarea.details',
	mode : "exact",
	toolbar_items_size: 'small',
	plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
});
*/
/*var s = $("#sticker");
var pos = s.position();                    
$(window).scroll(function() {
    var windowpos = $(window).scrollTop();
    if (windowpos >= pos.top) {
        s.addClass("stick");
    } else {
        s.removeClass("stick"); 
    }
});
*/
//donaciones
/*
$(".donation-button").click(function(){
	var i = $(this).data('id');
	var u = $(this).data('donate');
	var p = $(this).data('item');
	var d = "https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=leftbrain@implementimagination.com.au&item_name=Project "+p+
	"&item_number="+i;
	var s = 0;
	var m = 10;
	s = (u=="50auds"? 12:s); 
	s = (u=="20auds"? 12:s);
	s = (u=='10auds'? 2:s); 
	m = (u=="50auds"|| u=="50aud" ?50:m);
	m = (u=="20auds"|| u=="20aud" ?20:m);
	m = (u=="10auds"|| u=="10aud" ?10:m);
	d = d+"&amount="+m+"&currency_code=AUD&custom="+i;
	d = (s!==0? d+"&return=http://causeastir.com.au/thank-you/&shipping="+s:d+"&address_override=1&country=AU&address1=CBRIN%2C+Level+5%2C+1+Moore+Street%2C+Canberra%2C+ACT+2601&city=Canberra&zip=2601");
	
	window.top.location=d;
});
*/

/*$('.mySelect').selectpicker();*/

/*function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function (e) {
			$('.blah').attr('src', e.target.result);
		}	
		reader.readAsDataURL(input.files[0]);
	}
}

$(".imgInp").change(function(){
	readURL(this);
});
*/

/*
$("#color_me").change(function(){
    var color = $("option:selected", this).attr("class");
    $("#color_me").attr("class", color);
});
*/
//#filter-bar input
$('button[name=filter]').click(function(e){
	
	e.preventDefault();
	var o = {};
	var i = {};
	var a = $("#filter-bar :input").serializeArray();
	var url = '';
	$.each(a,function(index, item){
		var key = item.name.replace(/[^A-Za-z0-9]/g, '');
		if( typeof o[key] != 'undefined') {
			 if (!o[key].push) {
                o[key] = [o[key]];
            }
			o[key].push(item.value);
		}
		if( typeof o[key] == 'undefined'){
			o[key] = item.value;
		} 
	});
	
	$.each(o, function(key, data){
			if( !data.push ){
				if( data != "" ) {
					url =  key+'='+data +"&"+url;
				}
			} else{
				var datalenght = parseInt(data.length);
				var urlaux = '';
				if(datalenght > 0){
					for (var i=0;  i < datalenght; i++){
						urlaux =data[i]+","+urlaux;
					}
					urlaux = urlaux.replace(/,+$/, '');
					url = key+"="+urlaux+"&"+url
				}
			}
			
	});
	
	url = url.replace(/&+$/, '');
	
	window.top.location.href = '?'+encodeURI(url);
	
});

$('button[data-button="search"]').click(function(e){
    
    e.preventDefault();

    var o = {};
    var i = {};
    var f = $(this).closest('form');
    var a = f.find(":input").serializeArray();
    var url = '';
    var href = f.attr('action');
    $.each(a,function(index, item){
        var key = item.name.replace(/[^A-Za-z0-9]/g, '');
        if( typeof o[key] != 'undefined') {
             if (!o[key].push) {
                o[key] = [o[key]];
            }
            o[key].push(item.value);
        }
        if( typeof o[key] == 'undefined'){
            o[key] = item.value;
        } 
    });
    
    $.each(o, function(key, data){
            if( !data.push ){
                if( data != "" ) {
                    url =  key+'='+data +"&"+url;
                }
            } else{
                var datalenght = parseInt(data.length);
                var urlaux = '';
                if(datalenght > 0){
                    for (var i=0;  i < datalenght; i++){
                        urlaux =data[i]+","+urlaux;
                    }
                    urlaux = urlaux.replace(/,+$/, '');
                    url = key+"="+urlaux+"&"+url
                }
            }
            
    });
    
    url = url.replace(/&+$/, '');
    window.top.location.href = href+'?'+encodeURI(url);
    
});

