function Wave(id, canvasName, opacity, color, amount, xStart, yStart, step, speed, cycle, animationCoef, mouseMoveContainer) {

	this.id = id;
	this.canvasName = canvasName;
	this.opacity = opacity;
	this.color = color;
	this.xStart = xStart;
	this.yStart = yStart;
	this.step = step;
	this.speed = speed;
	this.cycle = cycle;
	this.animationCoef = animationCoef;
	
	this.viewWidth = document.getElementById(this.canvasName).offsetWidth;
	this.viewHeight = document.getElementById(this.canvasName).offsetHeight;

	// The amount of segment points we want to create:
	this.amount = amount;

	var height = [];

	this.path;
	
	this.interactionThreshold = 15;
		
	this.init = function() {
	
		var actualScope = this.scope;
		actualScope.activate();

		this.viewWidth = document.getElementById(this.canvasName).offsetWidth;
		this.viewHeight = document.getElementById(this.canvasName).offsetHeight;
		
		this.stepCoef;
		if(this.step >= 0) {
			this.stepCoef = this.viewHeight;
		}
		else {
			this.stepCoef = 0;
		}
		
		if(this.path)
			this.path.remove();
			
		actualScope.project.clear();
		
		// change amount of points based on window width
		if(window.matchMedia("(max-width: 780px)").matches) {
			this.amount = Math.floor(this.amount / 2);
		}
		else if(window.matchMedia("(max-width: 1080px)").matches) {
			this.amount = Math.floor(this.amount / 1.5);
		}
		else {
			this.amount = amount;
		}
		
		if(this.amount < 4) {
			this.amount = 4;
		}
		
		
		// Create a new path and style it:
		this.path = new actualScope.Path({
			opacity: this.opacity
		});
		
		if(this.color.constructor === Array) {
			this.path.fillColor = {
				gradient: {
					stops: this.color
				},
				origin: new actualScope.Point(0, this.viewHeight),
				destination: new actualScope.Point(this.viewWidth, 0)
			}
		}
		else {
			this.path.fillColor = this.color;
		}

		//this.path.add(new actualScope.Point((this.xStart) - 2*(this.viewWidth / this.amount), this.viewHeight + this.viewHeight / this.amount));
		//this.path.add(new actualScope.Point(this.xStart - (this.viewWidth / this.amount), this.yStart));
		
		this.path.add(new actualScope.Point(this.xStart - 2*(this.viewWidth / this.amount), this.viewHeight + this.viewHeight / this.amount));
		this.path.add(new actualScope.Point(this.xStart - (this.viewWidth / this.amount), this.yStart));
		
		//this.path.add(new actualScope.Point(this.xStart, this.yStart));
		
		
		height[0] = this.viewHeight / (Math.floor(Math.random() * (20 - 10)) + 10);
		height[1] = this.viewHeight / (Math.floor(Math.random() * (20 - 10)) + 10);

		// Add segment points to the path spread out
		// over the width of the view:		
		for (var i = 0; i <= this.amount; i++) {

			this.path.add(new actualScope.Point(((i / this.amount) * this.viewWidth + this.xStart), (this.stepCoef - i * this.step) + this.yStart));

			height[i+2] = this.viewHeight / (Math.floor(Math.random() * (20 - 10)) + 10);
		}

	//	this.path.add(new actualScope.Point(this.viewWidth + this.viewWidth / this.amount, this.viewHeight + this.viewHeight / this.amount));
		this.path.add(new actualScope.Point(this.viewWidth, this.viewHeight + this.viewHeight / this.amount));
		height.push(this.viewHeight / (Math.floor(Math.random() * (20 - 10)) + 10));
		
		this.path.segments[0].point.fixed = true;
		this.path.segments[1].point.fixed = true;
		this.path.segments[this.path.segments.length - 1].point.fixed = true;
		
		// correct first point segment out
		
		// Select the path, so we can see how it is constructed:
		//this.path.selected = true;
		//this.path.fullySelected = true;
	}
	
	var previousRepulsion = new Array();
	var previousEvent = new Array();
	
	this.animateWave = function(event) {

		
		// Loop through the segments of the path:
		for (var i = 1; i <= this.amount + 2; i++) {
			var segment = this.path.segments[i];
			
			// if we have a mouse interaction
			var repulsion = 0;
			if(this.interaction && this.interaction.event) {
			
				/**
					We will use the gravitational force from vectorial equation :
					-1 * gravitation * ((mouseWeight * pointWeight) / (distance * distance)) * vectorFromMouseToPoint
				*/
			
				var eventPoint = this.interaction.event;
				var actualPoint = new this.scope.Point(segment.point);
				
				var vectorFromMouseToPoint = {};
				
				vectorFromMouseToPoint.x = eventPoint.x - actualPoint.x;
				vectorFromMouseToPoint.y = eventPoint.y - actualPoint.y;
				
				vectorFromMouseToPoint = new this.scope.Point(vectorFromMouseToPoint);
				
				var distance = vectorFromMouseToPoint.length;

				var gravitation = 1;
				var mouseWeight = 35000;
				var pointWeight = 1;
				
				// this is for attraction (still buggy)
				/* repulsion = 1 * gravitation * ((mouseWeight * pointWeight) / (distance * distance)) * vectorFromMouseToPoint.y;
				
				if((repulsion > 0 && repulsion > 1 * vectorFromMouseToPoint.y) || (repulsion < 0 && repulsion < 1 * vectorFromMouseToPoint.y)) {
					repulsion = 0.95 * vectorFromMouseToPoint.y;
				} */
				
				// this is for repulsion
				repulsion = -1 * gravitation * ((mouseWeight * pointWeight) / (distance * distance)) * vectorFromMouseToPoint.y;
				
				// interaction is too light
				/* if(Math.abs(repulsion) < this.interactionThreshold) {
					repulsion = 0;
				} */
				
				// interaction is too powerful
				if((repulsion > 0 && repulsion > -1 * vectorFromMouseToPoint.y) || (repulsion < 0 && repulsion < -1 * vectorFromMouseToPoint.y)) {	
					repulsion = -0.75 * vectorFromMouseToPoint.y;
					
					// interaction is too close from previous interaction
					/* if(Math.abs(repulsion - previousRepulsion[i]) < 2 * this.interactionThreshold) {
						//console.log('restrain interaction ', repulsion, previousRepulsion[i]);
						repulsion = previousRepulsion[i];
					} */
				}
				

				// interaction is too close from previous interaction
				//if(Math.abs(repulsion - previousRepulsion[i]) < this.interactionThreshold && previousEvent[i] !== this.interaction.event) {
				if(Math.abs(repulsion - previousRepulsion[i]) < this.interactionThreshold) {
					repulsion = previousRepulsion[i];
				}
				
				previousEvent[i] = this.interaction.event;
			}
			
			previousRepulsion[i] = repulsion;
			
			// A cylic value between -1 and 1
			var sinus = Math.sin(event.time * this.speed - (i - 1));
			
			// Change the y position of the segment point:
			segment.point.y = this.cycle * (sinus * height[i]) + (this.stepCoef * this.animationCoef - i * this.step + this.yStart) + repulsion;
			
		}
		// to smooth the path:
		 this.path.smooth();
	}
	
	var bodyClasses = document.getElementsByTagName('body')[0].className;
	var isDesktop = false;
	if(bodyClasses.indexOf('desktop-body') !== -1) {
		isDesktop = true;
	}
	
	if(isDesktop) {		
		
		this.handleInteraction = function(e, mouseContainer) {
			
			var bodyBoundaries = document.body.getBoundingClientRect();
			//var boundaries = mouseContainer.getBoundingClientRect();
			var boundaries = document.getElementById(this.canvasName).getBoundingClientRect();
			
			// get relative coords
			var event = new this.scope.Point((e.pageX - (boundaries.left - bodyBoundaries.left)), (e.pageY - (boundaries.top - bodyBoundaries.top)));

			var location = this.path.getNearestLocation(event);
			var segment = location.segment;
			var point = segment.point;

			/* if(this.interaction && this.interaction.event) {
				this.interaction = {
					'event': event
				}
			}
			else {
				this.interaction = {
					'event': event
				}
			} */
			
			this.interaction = {
				'event': event
			};
			
		}
		
		
	}
	
	// one scope per wave
	this.scope = new paper.PaperScope();
	this.canvasElement = document.getElementById(this.canvasName);
	
	this.scope.setup(this.canvasElement);
	
	var me = this;
	
	me.init();
	
	me.scope.view.onResize = function() {
		me.init();
	}
	
	me.scope.view.onFrame = function(event) {
		me.animateWave(event);
	}
	
	var lastMousePosition = {};
	
	if(isDesktop) {
	
		this.mouseMoveContainer = mouseMoveContainer;
		var mouseMoveContainerRef;
		
		if(this.mouseMoveContainer.constructor === Array) {
			mouseMoveContainerRef = document.body;
		}
		else {
			mouseMoveContainerRef = document.getElementById(this.mouseMoveContainer);
		}
		
		var me = this;
		
		mouseMoveContainerRef.addEventListener("mousemove", function(e) {
			
			me.handleInteraction(e, mouseMoveContainerRef);
		});
	}
}