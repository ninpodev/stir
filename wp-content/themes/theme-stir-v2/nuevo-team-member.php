<section class="box fleft fwidth">
                        <a class="btn btn-md btn-primary atm" data-duplicate-add="teammember">Add a Team Member</a>
                        
                        <div class="clearfix"></div>
                            
<div class="col-md-4 col-sm-4 col-xs-12 card-team-member" data-duplicate="teammember" data-duplicate-min="0">
    <a class="btn btn-sm btn-danger" data-duplicate-remove="teammember"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
    
    <div class="card">
        <div class="card-content">
            
            <div class="form-group profile">
                <label>Profile Picture</label>
                <input type="file" style="margin-bottom:40px;" name="other_member_project_picture[]" type="file" class="filestyle" data-icon="false" data-size="xs" data-placeholder="jpg, png of gif" >
            </div>
            
            <div class="form-group">
                <label>Role</label>
                <input type="text" class="form-control" placeholder="Role of the team member">
            </div>
            <div class="form-group">    
                <label>Age</label>
                <input type="number" class="form-control" placeholder="#" maxlength="2" min="1" max="99" length="2" oninput="maxLengthCheck(this)">
            </div>

            <div class="form-group">
                <label>Gender</label>
                <select name="other_member_project_gender[]">
                <option value="Female" <?php if ($member['other_member_project_gender']=='Female')echo "selected";?>>Female</option>
                    <option value="Male"  <?php if ($member['other_member_project_gender']=='Male')echo "selected";?>>Male</option>
                </select>
            </div>
            <div class="form-group">
                <label>Skills [Separated by enter]</label>
                <div class="clearfix"></div>
                <input type="text" class="form-control" data-role="tagsinput">
                <div class="clearfix"></div>
            </div>

        </div>
    </div>
</div>
                            
                        </section>