<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>            
<div class="box fwidth fleft supertitulo">
    <div class="container">
        <div class="col-lg-12 no-column">
            <h1>Create. Local. Support.</h1>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<section id="page" class="about container-fluid no-column">

    <div class="box fleft fwidth main-img">
        <div class="title">
            <h2>Stir provides a starting point for creatives to transform their talents into venture concepts, helping them engage with the economy in a meaningful and rewarding way.</h2>
        </div>
    </div>
    
    
    <div class="clearfix"></div>
    <div class="container">
        
        <div class="col-lg-3 col-lg-offset-1">
            <h4>Online</h4>
            <p>You can use the Stir platform to develop the skills to structure and present a project proposal.</p>

            <p>Our project creation process draws from applications for grants and other forms of support, and is aimed at helping you develop an entrepreneurial mindset.</p>

            <p>Once your project is published, it will be able to receive comments from the community. Use these comments to help you improve.</p>
        </div>
        <div class="col-lg-3">
            <h4>Offline</h4>
            <p>Stir also hosts events. These events are  aimed at helping you to get started on your projects, receiving support from the Stir Crew and other young creatives...</p>

            <p>These events are a great opportunity to connect with the community, share experiences and learn from each other.</p>

            <p>Keep track of our events by following our <a href="https://www.facebook.com/causeastirCBR" target="_blank">facebook</a> / <a href="https://twitter.com/StirCBR" target="_blank">twitter</a></p>
        </div>
        <div class="col-lg-4 col-lg-offset-1 img-pcp"> 
            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/img-pcp.jpg" alt="">
        </div>
    </div>
    <section id="sessions-grants" class="box fleft fwidth">
        <div id="grants" class="box fleft fwidth">
            <h2>Each year, Stir holds a number of Grant Seasons. During these seasons, project creators can gather votes from their supporters. At the end of a season, the projects with the most votes each receive a grant to help them grow!</h2>
            
            <div class="col-lg-6 col-lg-offset-6 text-right">
                <figure>
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/img-grant-about-page.jpg" alt="">
                    <figcaption>Photography by Josh Sellick </figcaption>
                </figure>
                
            </div>
        </div>
        <div id="sessions" class="container">
            <div class="col-lg-4 col-sm-12">
                <div class="box fleft img">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-s3.png" alt="">
                </div>
                <div class="clearfix"></div>
                <p>Our main season of grants runs between April and June. Each year, we hand out 10 grants of $1000 each to the projects that receive the most votes during that period.</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="box fleft img">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-summer-of-stir.png" alt="">
                </div>
                <div class="clearfix"></div>
                <p>Between November and February, Stir provides a $500 grant to support a specific community of creatives. These grants are exclusive to that community, with projects remaining eligible for the main season of grants.</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="box fleft img">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-stir-on-schools.png" alt="">
                </div>
                <div class="clearfix"></div>
                <p>Currently under development in collaboration with AISACT, this program aims to encourage highschool students and teachers to engage with entrepreneurship. The pilot will launch in Term 1, 2017.</p>
            </div>
            
        </div>
    </section>
    <section id="process" class="box fleft fwidth">
        <h3>The Stir Process</h3>
        <div class="container">
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <p>The Stir platform allows young creatives to upload a project by completing five simple stages designed to help them structure a project proposal.</p>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <p>Once a project is published, project creators can share it with potential supporters. Supporters can provide feedback to help the project improve, and have the option to vote for the projects they believe should receive a grant.</p>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <p>At the end of each season, the ten projects with the most votes receive $1000 to help them get off the ground.</p>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-12">
                <p>Project creators are then encouraged to share their experience with the community, helping the next generation of creative entrepreneurs!</p>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <section id="steps" class="box fleft fwidth text-center">
        <div class="container">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <h6>Step 01</h6>
                <p>Use the stir process to create your project</p>
                <span class="flecha"></span>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <h6>Step 02</h6>
                <p>Publish the project to receive feedback and improve</p>
                <span class="flecha hidden-sm hidden-xs"></span>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <h6>Step 03</h6>
                <p>Promote it to gather votes from the community.</p>
                <span class="flecha"></span>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <h6>Step 04</h6>
                <p>Share your experience with the next generation.</p>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    
    <div id="testimonios-about" class="container">
        <div class="col-lg-12">
        <div id="slider-testimonios" class="flexslider">
        <ul class="slides">
            <li>
                <div class="container-fluid">
                    <div class="col-lg-6 col-xs-12">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/img-vivarium.jpg">
                    </div>
                    <div class="col-lg-6 col-xs-12 text-right">
                        <h2>“Being on Stir provided exposure, validated my idea and showed that people could get behind my cause”</h2>
                        <!--<span class="link">Read the <a href="#">project -></a></span>-->
                        <div class="clearfix"></div>
                        <h6>Vivarium</h6>
                        <p>project from season one</p>
                    </div>
                    
                </div>
                
            </li>
            <li>
                <div class="container-fluid">
                    <div class="col-lg-6 col-xs-12">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/persian-rug-store-test.png">
                    </div>
                    <div class="col-lg-6 col-xs-12 text-right">
                        <h2>"Stir helped us to make that crucial first step into the real world. I’ve now received another grant in collaboration with people I met through the platform"</h2>
                        <!--<span class="link">Read <a href="#">the project -></a></span>-->
                        <div class="clearfix"></div>
                        <h6>Persian Rug Store</h6>
                        <p>project from season one</p>
                    </div>
                    
                </div>
                
            </li>
            <li>
                <div class="container-fluid">
                    <div class="col-lg-6 col-xs-12">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/paragon-test.png">
                    </div>
                    <div class="col-lg-6 col-xs-12 text-right">
                        <h2>"Stir pushed us to turn our idea into a real product. It helped us to dream bigger and make Paragon Games what it is today.”</h2>
                        <span class="link">Read the <a href="http://causeastir.com.au/project/paragon-games/" target="_blank">project -></a></span>
                        <div class="clearfix"></div>
                        <h6>Paragon Games</h6>
                        <p>project from season two</p>
                    </div>
                    
                </div>
                
            </li>
            
            
            
        </ul>
    </div>
        </div>
    </div>
    <div class="clearfix"></div>
    
    
    <section id="numbers" class="box fleft fwidth">
        <h3>Stir In Numbers</h3>
        <div class="container">
            <div class="col-lg-7">
                <div class="container-fluid no-column">
                    <div class="col-lg-6">
                        <p>Stir development began on November 22nd 2014, with the platform launching on March 27th, 2015.</p>
                    </div>
                    <div class="col-lg-6">
                        <p>Between 2015 and 2016, two major voting seasons have taken place, during which we have delivered 25 $1000 grants.</p>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12">
                        <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/img/img-stats-1.png" alt="">
                    </div>
                </div>
            </div>
            
            <div class="col-lg-5">
                <p>Since its launch, the platform has drawn in a large community of supporters who have viewed the site over 100,000 times.</p>
                                        <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/img/img-stats-2.png" alt="">

            </div>
            <div class="clearfix"></div>
            <div class="col-lg-12 timeline">
                <a href="<?php echo esc_url( get_template_directory_uri() ) ?>/img/img-stats-3.png" target="_blank">
                <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/img/img-stats-3.png" alt="">
                </a>
            </div>
        </div>
    </section>
    <div class="clearfix"></div>
    <section id="logos" class="box fleft fwidth">
        <h3>Friends and Supporters</h3>
        <div class="container">
            <div class="col-lg-3 col-sm-6 col-xs-6 logo text-center">
                <div class="table">
                    <div class="table-cell">
                        <a href="http://youareherecanberra.com.au/" target="_blank"> <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/img/logo-fs-01.jpg" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-6 logo text-center">
                <div class="table">
                    <div class="table-cell">
                        <a href="http://entry29.org.au/" target="_blank">
                        <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/img/logo-fs-02.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-6 logo text-center">
                <div class="table">
                    <div class="table-cell">
                        <a href="https://impactcomics.com.au/cbr/" target="_blank">
                        <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/img/logo-fs-03.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-6 logo text-center">
                <div class="table">
                    <div class="table-cell">
                        <a href="https://sanchosdirtylaundry.com/" target="_blank">
                            <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/img/logo-fs-04.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-lg-3 col-sm-6 col-xs-6 logo text-center">
                <div class="table">
                    <div class="table-cell">
                        <a href="http://thecreativeelement.com.au/" target="_blank">
                        <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/img/logo-fs-05.jpg" alt="">
                        </a>
                    </div>
                </div> 
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-6 logo text-center">
                <div class="table">
                    <div class="table-cell">
                        <a href="https://www.facebook.com/GAMMACON/" target="_blank">
                        <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/img/logo-fs-06.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 col-xs-6 logo text-center">
                <div class="table">
                    <div class="table-cell">
                        <a href="http://ucgd-piecebypiece.com/" target="_blank">
                        <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/img/logo-fs-08.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div> 
            <div class="col-lg-3 col-sm-6 col-xs-6 logo text-center">
                <div class="table">
                    <div class="table-cell">
                        <a href="http://reloadesports.com/" target="_blank">
                        <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/img/logo-fs-09.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-lg-3 hidden-sm hidden-xs logo text-center">
                <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/img/logo-fs-placeholder.jpg" alt="">
            </div>
            
            <div class="col-lg-3 col-sm-6 col-xs-6 logo text-center">
                <div class="table">
                    <div class="table-cell">
                        <a href="https://www.facebook.com/lobrowgallery/" target="_blank">
                        <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/img/logo-fs-010.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 hidden-sm hidden-xs logo text-center">
                <img src="<?php echo esc_url( get_template_directory_uri() ) ?>/img/logo-fs-placeholder.jpg" alt="">
            </div>
        </div>
        <div class="clearfix"></div>
        
        <h3>Sponsors</h3>
        <div class="container">
            <div class="col-lg-4 logo-sp text-center">
                <div class="table">
                    <div class="table-cell">
                        <a href="http://www.bendigobank.com.au/public/community/community-promise" target="_blank">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-sp-01.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 logo-sp text-center">
                <div class="table">
                    <div class="table-cell">
                        <a href="http://inspiringaustralia.net.au/" target="_blank">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-sp-02.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 logo-sp text-center">
                <div class="table">
                    <div class="table-cell">
                        <a href="http://ais.act.edu.au/" target="_blank">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-sp-03.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="call" class="box fleft fwidth">
        <div class="container">
            <div class="col-lg-10 col-lg-offset-1 text-center">
                <h2>Would you like to be part of a movement that empowers young creatives to transform their talent into entrepreneurial careers?</h2>
            </div>
        </div>
    </section>
    
    <section id="ctas" class="box fleft fwidth">
        <div class="container">
            <div class="col-lg-4 text-center">
                <h3>Create a Project</h3>
                <div class="box fleft fwidth text">
                    <p>Transforming your passions into a project proposal is our main purpose. Use our platform, share it with others and give us feedback. We look forward to seeing your work.</p>
                </div>
                <a href="<?php echo get_option('siteurl'); ?>/welcome" target="_blank" class="small-cta">Get Started Here</a>
            </div>
            <div class="col-lg-4 text-center">
                <h3>Support a Project</h3>
                <div class="box fleft fwidth text">
                    <p>Check out the amazing projects that have been uploaded to our platform. You can help them improve by commenting on them, and once the season begins, help them win a grant by voting.</p>
                </div>
                <a href="<?php echo get_option('siteurl'); ?>/support" target="_blank" class="small-cta">Start Browsing</a>
            </div>
            <div class="col-lg-4 text-center">
                <h3>Become a Sponsor</h3>
                <div class="box fleft fwidth text">
                    <p>Your support directly contributes to the size and number of Stir Grants we can deliver each year. It also helps us to develop new programs and initiatives, extending our reach and impact.</p>
                </div>
                <a href="<?php the_field('prospectus_file', 'option'); ?>" target="_blank" class="small-cta">Read The Document</a>
            </div>
        </div>
    </section>
    <section id="conversation" class="box fleft fwidth">
        <h3>Join The Conversation</h3>
        <div class="clearfix"></div>
        <div class="container">
            <div class="col-lg-6 col-sm-12 col-xs-12 text-center sm">
                <div class="box fcenter">
                <a class="twitter-timeline" data-width="400" data-height="480" href="https://twitter.com/StirCBR">Tweets by StirCBR</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
            </div>
            <div class="col-lg-6 col-sm-12 col-xs-12 sm">
                <div class="box fcenter">
                <div class="fb-page" data-href="https://www.facebook.com/causeastirCBR/" data-tabs="timeline" data-width="480" data-height="470" data-small-header="true" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/causeastirCBR/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/causeastirCBR/">Stir</a></blockquote></div>
                
            </div>
                
            </div>
        </div>
        
    </section>
</section>


<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>  

<?php get_footer(); ?>