<?php if(isset($_GET['draft'])) publish_to_draft();?>
<?php if(!is_role('project') && !is_role('administrator')) wp_redirect(get_permalink(4));?>
<?php if(!have_draft()) wp_redirect(get_permalink(12)); ?>
<?php get_header(); ?>
<div class="box fwidth fleft supertitulo">
    <div class="container">
        <div class="col-lg-12">
            <h1>Start With The Basics</h1>
        </div>
    </div>
</div>

<div id="modal-guidance" class="modal fade guidance-module" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

        <div id="guidance" class="container-fluid">
            
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <h3>Guidance Mode</h3>
                    <?php the_field('basics_intro', '14'); ?>
                    <br>
                    <h4>Why is this important?</h4>
                    <?php the_field('basics-why-important','14'); ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <?php the_field('basics_final_words', '14'); ?>

                    <div class="toolshed-box box fwidth fleft">
                        <?php $posts = get_field('choose_tools_basics','14'); if( $posts ): ?>
                        <?php foreach( $posts as $post): ?>
                        <?php setup_postdata($post); ?>
                        <div class="toolbox" style="background-color:<?php the_field('color_toolshed'); ?>;">
                        <a href="<?php the_field('pdf_link_toolshed'); ?>" target="_blank"><?php the_title(); ?></a>
                        </div>
                        <?php endforeach; ?>
                        <?php wp_reset_postdata(); ?>
                        <?php endif; ?>
                    </div>
                </div>
            
        </div>
        
    </div>
  </div>
</div>


<div id="container-create" class="box fwidth fleft">
    
    <section id="create" class="container">
    <div class="form-create col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
        
    	<form id="pctform" name="basics" enctype="multipart/form-data">
        <?php $project= get_project();?>
            
            <div class="modulo box fwidth fleft">
                <div class="container-fluid">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <h3>What is the name of your project?</h3>
                        <div class="input-field">
                            <input id="name_project" type="text" value="<?php echo $project['post_title'];?>" name="post_title" maxlength="30" length="30" rel="requerido" >
                            <label for="name_project">What is the name of your project?</label>
                        </div>
                        <div class="aso-tool box fleft fwidth text-left">
                            <p><i class="fa fa-wrench"></i> Use the <a href="<?php echo get_option('siteurl'); ?>/wp-content/uploads/2015/03/stirtools_basics_lvl1.pdf" target="_blank">Project Naming Tool</a> if you need guidance.</p>
                        </div>
                        
                    </div>
                    <div class="text-side col-lg-3 col-md-3 col-sm-3 col-xs-12 text-right">
                        <?php the_field('basics_name_project', '14'); ?>
                    </div>
                </div>
            </div>
            <!--modulo-->
            
            <div class="modulo box fwidth fleft">
                <div class="container-fluid">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <h3>Describe your project in one sentence <span>*</span></h3>
                        <div class="input-field">
                            <textarea name="describe" maxlength="140" class="materialize-textarea" rel="requerido"><?php echo reverse_wpautop($project['describe']);?></textarea>
                            <label for="textarea1">This sentence should briefly cover what your project is</label>
                        </div>
                        <div class="aso-tool box fleft fwidth text-left">
                            <p><i class="fa fa-wrench"></i> Use the <a href="<?php echo get_option('siteurl'); ?>/wp-content/uploads/2015/03/stirtools_basics_lvl2.pdf" target="_blank">Project Description Tool</a> for help.</p>
                        </div>
                    </div>
                    <div class="text-side col-lg-3 col-md-3 col-sm-3 col-xs-12 text-right">
                        <?php the_field('basics_describe', '14'); ?>
                    </div>
                </div>
            </div>
            <!--modulo-->
            <!--testeando scala-->
            
            <div class="modulo box fwidth fleft">
                <div class="container-fluid">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <h3>Upload the main image for your project <span>*</span></h3>
                        
                        <div class="file-field input-field">
                            <div class="btn">
                                <span>Upload</span>
                                <input type="file" value="examinar" <?php if(empty($project['visuals_project_display'])) { ?> rel="requerido" <?php }?>  name="visuals_project_display">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>
                        <div class="aso-tool box fleft fwidth text-left">
                            <p><i class="fa fa-wrench"></i> Make sure to use the <a href="<?php echo get_option('siteurl'); ?>/wp-content/uploads/2015/03/stirtools_visuals_lvl1.pdf" target="_blank">Project Images Tool</a> to help you make the best impression.</p>
                        </div>
                        
                        <br>
                        <div id="display-photo">
                            <?php if(!empty($project['visuals_project_display'])){?>
                            <div class="cache-display">
                                <img src="<?php echo $project['visuals_project_display'];?>" width="200">  
                                <input type="hidden" value="<?php echo basename($project['visuals_project_display']);?>"  name="file[visuals_project_display]"> 
                            </div>
                            <?php }else{
                            ?>
                            <div class="cache-display"></div>
                            <?php                    
                            } ?>
                        </div><br><br>
			             <p class="title-create">Upload additional photos of your project:</p>            
                            <table style="width:100%;" id="mytable">
                            <tbody>
                                <?php 
                                if(is_array($project['visuals_project_aditional_photos'])){
                                foreach($project['visuals_project_aditional_photos'] as $key =>$value){?>
                                <tr <?php if($key!=0) echo "class='nueva'";?>>
                                    <td>
                                        <div class="file-field input-field">
                                          <div class="btn">
                                            <span>Upload</span>
                                            <input type="file" value="Browse" name="visuals_project_aditional_photos[]">
                                          </div>
                                          <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text">
                                          </div>
                                        </div>


                                        <div class="cache-image">
                                            <img src="<?php echo $project['visuals_project_aditional_photos'][$key];?>" width="200">
                                            <input type="hidden" value="<?php  echo basename($project['visuals_project_aditional_photos'][$key]);?>" name="file[visuals_project_aditional_photos][]" >
                                            
                                        </div>

                                    </td>
                                </tr>
                                <?php } 
                                } else{
                                ?>
                                <tr>
                                    <td>
                                        <div class="file-field input-field">
                                          <div class="btn">
                                            <span>Upload</span>
                                            <input type="file" value="Browse" name="visuals_project_aditional_photos[]">
                                          </div>
                                          <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text">
                                          </div>
                                        </div>

                                        <div class="cache-image"></div>
                                    </td>
                                </tr>
                                <?php 
                                }
                                ?>

                            </tbody>
                        </table>
                        <div class="clearfix"></div><br>
                        <div class="col-lg-2 col-xs-6 pl0">
                            <button type="button" id="remove-more" class="waves-effect waves-light btn quita">Remove last</button>
                        </div>
                        <div class="col-lg-2 col-xs-6">
                            <button type="button" id="insert-more" class="waves-effect waves-light btn agrega">Add another</button>
                        </div>
                        <div class="clearfix"></div>
                        <br>
                        <p style="text-transform:uppercase;"><small>Tip: Save the project to preview the photos</small></p>
                        
                        
                        
                    </div>
                    <!--/lado a-->
                    <div class="text-side col-lg-3 col-md-3 col-sm-3 col-xs-12 text-right">
                        <?php the_field('visuals_main_image', '19'); ?>
                    </div>
                    <!--/lado b-->
                </div>
            </div>
            <!--modulo-->
            
            <div class="modulo box fwidth fleft">
                <div class="container-fluid">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 check-caja">
                        <h3>Choose the area that best represents your project</h3>
                        <?php $terms = get_terms( 'area', array( 'hide_empty' => false) );?>
                        <?php foreach($terms as $term){?>
                        <p>
                            <input <?php if(isset($project['areas'][$term->term_id])){?>checked<?php } ?> type="checkbox" class="filled-in" name="area[]" value="<?php echo $term->term_id;?>" id="<?php echo $term->slug;?>">
                            <label for="<?php echo $term->slug;?>"><?php echo $term->name;?></label>
                        </p>
                        <?php } ?>
                        
                        
                    </div>
                    <div class="text-side col-lg-3 col-md-3 col-sm-3 col-xs-12 text-right">
                        <?php the_field('basics_choose_area', '14'); ?>
                    </div>
                </div>
            </div>
            <!--modulo-->
            
            
            
            
            
    
            
            
            <?php /*
            
            <p class="title-create">Choose a highlight colour for your project. *</p>
            <p>This colour will display on your project’s page once it is published</p>
            <br><br>
            
            <select id="color_me" name="choose_a_colour" rel="requerido">
                <option value=" ">Choose...</option>
                <option class="purple" value="Purple" <?php if($project['choose_a_colour']=='Purple') echo "selected";?>>Purple</option>
                <option class="lilac" value="Lilac"<?php if($project['choose_a_colour']=='Lilac') echo "selected";?>>Lilac</option>
                <option class="pink" value="Pink" <?php if($project['choose_a_colour']=='Pink') echo "selected";?>>Pink</option>
                <option class="red" value="Red" <?php if($project['choose_a_colour']=='Red') echo "selected";?>>Red</option>
                <option class="orange" value="Orange" <?php if($project['choose_a_colour']=='Orange') echo "selected";?>>Orange</option>
                <option class="yellow" value="Yellow" <?php if($project['choose_a_colour']=='Yellow') echo "selected";?>>Yellow</option>                
            </select>

            */ ?>
            
            
        </form>    
    </div>
  </section>

    
    <?php include 'save-bar.php' ?>
    <!--/ save bar-->

    
</div>
<?php get_footer(); ?>