<?php if(isset($_GET['draft'])) publish_to_draft();?>
<?php if(!is_role('project') && !is_role('administrator')) wp_redirect(get_permalink(4));?>
<?php if(!have_draft()) wp_redirect(get_permalink(12)); ?>
<?php get_header(); ?>

<div class="box fwidth fleft supertitulo">
    <div class="container">
        <div class="col-lg-12">
            <h1>Fill In The Details</h1>
        </div>
    </div>
</div>

<div id="modal-guidance" class="modal fade guidance-module" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

        <div id="guidance" class="container-fluid">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h3>Guidance Mode</h3>
                <?php the_field('details_intro', '17'); ?>
                <br>
                <h4>Why is this important?</h4>
                <?php the_field('details_important', '17'); ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <?php the_field('details_final_words', '17'); ?>

                <div class="toolshed-box box fwidth fleft">
                    <?php $posts = get_field('choose_tools_details','17'); if( $posts ): ?>                                           <?php foreach( $posts as $post): ?>
                    <?php setup_postdata($post); ?>
                        <div class="toolbox" style="background-color:<?php the_field('color_toolshed'); ?>;">
                        <a href="<?php the_field('pdf_link_toolshed'); ?>" target="_blank"><?php the_title(); ?></a>
                        </div>
                        <?php endforeach; ?>
                        <?php wp_reset_postdata();?>
                        <?php endif; ?>
                </div>
            </div>            
        </div>
        
    </div>
  </div>
</div>


<div id="container-create" class="box fwidth fleft">
    
    <!--<input type="submit" class="link" value="save" rel="button" data-page="details" data-option="save">-->

    <section id="create" class="container">

    <div class="form-create col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
        <form id="pctform" name="details">
            <?php $project = get_project();?>

            <div class="modulo box fwidth fleft">
                <div class="container-fluid">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <h3>Provide a detailed explanation of the project <span>*</span></h3>
                        <div class="input-field">
                            <textarea name="detail_of_project_detailed" id="detail_of_project_detailed" class="materialize-textarea" rel="requerido"><?php echo reverse_wpautop($project['detail_of_project_detailed']);?></textarea>
                            <label for="detail_of_project_detailed">Tell us, how cool your project is</label>
                        </div>
                        <div class="aso-tool box fleft fwidth text-left">
                            <p><i class="fa fa-wrench"></i> Check out the <a href="<?php echo get_option('siteurl'); ?>/wp-content/uploads/2015/03/stirtools_details_lvl1.pdf" target="_blank">Project Explanation Tool</a> for some simple but effective guidelines.</p>
                        </div>
                    </div>
                    <div class="text-side col-lg-3 col-md-3 col-sm-3 col-xs-12 text-right">
                        <?php the_field('details_explanation', '17'); ?>
                    </div>
                </div>
            </div>
            <!--modulo-->

            <?php /*?><p>Add Tags to your project to make it easier to find. *</p><br />
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-column">
                <input type="text" />
            </div>
            <p>Review the detailed explanation you have written and find the key words. Add those here, separating each with a comma.</p>
            <div class="clearfix"></div><br><br><br><?php */?>


            <div class="modulo box fwidth fleft">
                <div class="container-fluid">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <h3>Who is the project for?</h3>
                        <div class="container-fluid no-column">

                            <?php /*
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 check-caja text-center no-column">
                                <p>
                                <input type="radio" name="detail_of_project_for" id="checkboxes-1" value="MALE" <?php if($project['detail_of_project_for']=="MALE") echo "checked";?>>
                                <label for="checkboxes-1">Male</label>
                                </p>              
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 check-caja text-center no-column">
                                <p>
                                <input type="radio" name="detail_of_project_for" id="checkboxes-2" value="FEMALE"  <?php if($project['detail_of_project_for']=="FEMALE") echo "checked";?>>
                                <label for="checkboxes-2">Female</label>
                                </p>              
                            </div>
                            <div class="clearfix"></div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 check-caja text-center no-column">
                                <p>
                                <input type="radio" name="detail_of_project_for" id="checkboxes-3" value="BOTH" <?php if($project['detail_of_project_for']=="BOTH") echo "checked";?>>
                                <label for="checkboxes-3">Both</label>
                                </p>              
                            </div>
                            */?>

                            <div class="clearfix"></div>
                            <br>
                            <!--nuevo-->
                            <div class="input-field">
                            <textarea name="detail_of_project_for_description" id="detail_of_project_for_description" class="materialize-textarea" ><?php echo reverse_wpautop($project['detail_of_project_for_description']);?></textarea>
                            <label for="detail_of_project_detailed">Describe the kinds of people who you think will enjoy your project.</label>
                        </div>
                            <br>


                            <!--<div class="aso-tool box fleft fwidth text-left">
                                <p><i class="fa fa-wrench"></i>&nbsp;Use the <a href="#" target="_blank">Target Audience Tool</a> for some tips from marketing people. </p>
                        </div>-->
                        </div>
                    </div>
                    <div class="text-side col-lg-3 col-md-3 col-sm-3 col-xs-12 text-right">
                        <?php the_field('details_age', '17'); ?>
                    </div>
                </div>
            </div>
            <!--modulo-->            





            <?php /*
            <p class="title-create">How old are the people who it’s for?*</p>
            <?php the_field('details_age', '17'); ?><br>

            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 check-caja text-center">
                <input type="checkbox" name="detail_of_project_old_people[]" id="checkboxes-1" value="0-5 (TODDLERS)"  <?php if(is_array($project['detail_of_project_old_people'])) { if(in_array("0-5 (TODDLERS)",$project['detail_of_project_old_people'])) echo "checked";}?>>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                <p style="text-transform:uppercase;">0-5 (TODDLERS)</p>
            </div>            
            <div class="clearfix"></div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 check-caja text-center">
                <input type="checkbox" name="detail_of_project_old_people[]" id="checkboxes-2" value="6-12 (CHILDREN)"  <?php if(is_array($project['detail_of_project_old_people'])) {if(in_array("6-12 (CHILDREN)",$project['detail_of_project_old_people'])) echo "checked";}?>>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                <p style="text-transform:uppercase;">6-12 (CHILDREN)</p>
            </div>            
            <div class="clearfix"></div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 check-caja text-center">
                <input type="checkbox" name="detail_of_project_old_people[]" id="checkboxes-3" value="13-17 (TEENAGERS)" <?php if(is_array($project['detail_of_project_old_people'])) {if(in_array("13-17 (TEENAGERS)",$project['detail_of_project_old_people'])) echo "checked";}?>>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                <p style="text-transform:uppercase;">13-17 (TEENAGERS)</p>
            </div>            
            <div class="clearfix"></div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 check-caja text-center">
                <input type="checkbox" name="detail_of_project_old_people[]" id="checkboxes-3" value="18-25 (YOUNG ADULTS)" <?php if(is_array($project['detail_of_project_old_people'])) {if(in_array("18-25 (YOUNG ADULTS)",$project['detail_of_project_old_people'])) echo "checked";}?>>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                <p style="text-transform:uppercase;">18-25 (YOUNG ADULTS)</p>
            </div>            
            <div class="clearfix"></div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 check-caja text-center">
                <input type="checkbox" name="detail_of_project_old_people[]" id="checkboxes-3" value="26-35 (ADULTS)"  <?php if(is_array($project['detail_of_project_old_people'])) {if(in_array("26-35 (ADULTS)",$project['detail_of_project_old_people'])) echo "checked";}?>>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                <p style="text-transform:uppercase;">26-35 (ADULTS)</p>
            </div>            
            <div class="clearfix"></div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 check-caja text-center">
                <input type="checkbox" name="detail_of_project_old_people[]" id="checkboxes-3" value="36-50 (MIDDLE-AGED)"   <?php if(is_array($project['detail_of_project_old_people'])) {if(in_array("36-50 (MIDDLE-AGED)",$project['detail_of_project_old_people'])) echo "checked";}?>>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                <p style="text-transform:uppercase;">36-50 (MIDDLE-AGED)</p>
            </div>            
            <div class="clearfix"></div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2  check-caja text-center">
                <input type="checkbox" name="detail_of_project_old_people[]" id="checkboxes-3" value="50-65 (MATURE)"  <?php if(is_array($project['detail_of_project_old_people'])) {if(in_array("50-65 (MATURE)",$project['detail_of_project_old_people'])) echo "checked";}?>>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                <p style="text-transform:uppercase;">50-65 (MATURE)</p>
            </div>            
            <div class="clearfix"></div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 check-caja text-center">
                <input type="checkbox" name="detail_of_project_old_people[]" id="checkboxes-3" value="66+ (SENIOR)" <?php if(is_array($project['detail_of_project_old_people'])) {if(in_array("66+ (SENIOR)",$project['detail_of_project_old_people'])) echo "checked";}?> >
            </div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                <p style="text-transform:uppercase;">66+ (SENIOR)</p>
            </div>
            */?>


        </form>    
    </div>
    </section>

    <?php include 'save-bar.php' ?>
    <!--/ save bar-->

    
    
  
</div>
<?php get_footer(); ?>