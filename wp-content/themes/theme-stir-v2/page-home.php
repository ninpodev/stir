<?php get_header(); ?>    

    <div id="home" class="container-fluid no-column">
        <a id="determine-user-type" class="in-page-link"></a>
        <div class="clearfix"></div>
        <div id="main-slide-home" class="flexslider">
            
            
            <ul class="slides">
                
                <?/*
                <?php $query = new WP_Query( array(
    'post_type' => 'news',
    'tax_query' => array(
    array(
        'taxonomy' => 'featured',  
        'field' => 'yes',
        'terms' => 70, 
    )
    )
    ) );

    while ( $query->have_posts() ) : $query->the_post(); ?>
    <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' ); $url = $thumb['0']; ?>
                */?>
                
                
                <?php 

$posts = get_field('slider_content', 'option');

if( $posts ): ?>
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' ); $url = $thumb['0']; ?>
                
        <li style="background-image:url(<?=$url?>);">
                    <a class="read-more" href="<?php the_permalink(); ?>">[Read More]</a>
                    <div class="wrapper box fleft fwidth">
                        <div class="container-fluid">
                        <div class="col-lg-6 col-lg-offset-1 col-md-6 col-md-offset-1 col-sm-8 col-sm-offset-1 col-xs-10 col-xs-offset-1 content">
                            <div class="box fleft fwidth meta">
                            <span class="cate">
                                <?php echo get_post_type( $post_id ); ?>
                                
                                <?php $terms = get_the_terms( $global_post_id, 'type_of_content' );?><?php if($terms){ ?><?php $term = array_pop($terms);?>/ <?php echo $term->slug;?><?php } ?></span>
                            <?php if(get_the_tag_list()) { echo get_the_tag_list('<span>','</span><span>','</span></span>'); } ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="fleft fwidth titulo">
                                <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                            </div>
                            <div class="clearfix"></div>
                            <p class="excerpt hidden-xs"><span><?php echo excerpt(17); ?><a href="<?php the_permalink(); ?>">[...]</a></span></p>
                        </div>
                    </div>
                    </div>
                </li>
                
    <?php endforeach; ?>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>
                
                

                <? /*
                
            <?php endwhile; ?>
            <?php wp_reset_query(); ?> */?>
                
            </ul>
            
            
            
        </div>
        <div class="clearfix"></div>
        <div id="project-tiles" class="container">
            
            <div class="col-lg-5 no-column nsos">
                <h3>New Season 04 projects</h3>
            </div>
            <div class="col-lg-7 text-right ofv">
                <p class="tag">open for voting</p>
            </div>
            <div class="clearfix"></div>

            <?php $args=array( //Loop 2
                //'post_type' => array ('news','project'), 
                'post_type' => 'project', 
                'posts_per_page' => 4,
                'orderby' => 'rand',
                'taxonomy' => 'season',
                'term' => 's04',
                'orderby' => 'rand',
            );
            $myloop = new WP_Query($args);
            if($myloop->have_posts()) : while($myloop->have_posts()) :
            $myloop->the_post();
            ?>
            <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' ); $url = $thumb['0']; ?>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 no-column">
                <div class="item box fleft fwidth">
                    <div class="meta">
                        <span class="box fleft">
                            &nbsp;
                        </span>
                        <span class="box fright season">
                        <?php $terms = get_the_terms( $global_post_id, 'season' );?><?php if($terms){ ?><?php $term = array_shift($terms);?><span class="season <?php echo $term->slug;?>"> <?php echo $term->name;?> </span><?php } ?>
                        </span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="photo box fleft fwidth" style="background-image:url(<?php echo get('visuals_project_display');?>);">
                        <?php if( has_term('yes','funded', $global_post_id) ) {?><span class="funded">GRANT RECIPIENT</span><?php } else {?><? } ?>

                        <a href="<?php the_permalink(); ?>">&nbsp;</a>
                    </div>

                    <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                    <div class="clearfix"></div>


                        <div class="excerpt">
                            <?php echo get('describe_project');?>
                            <a href="<?php the_permalink(); ?>">more</a>
                        </div>
                    <!--comentar votos-->
                    <?/* <span class="supporters"><?php echo support_count(get_the_ID());?> Votes</span> */?>
                    <div class="clearfix"></div>

                    <?php if(get_the_tag_list()) { echo get_the_tag_list('<ul class="post-tags-front"><li>','</li><li>','</li></ul>'); } ?>
                </div>
            </div>

            <?php endwhile; endif; ?>
            <?php wp_reset_query(); ?>
            
            <div class="box fleft fwidth caja-boton text-center">
                <a href="<?php echo get_option('siteurl'); ?>/support/?season=22" class="small-cta">[more projects]</a>
            </div>

        </div>               
        <div class="clearfix"></div>
        
        <div id="project-tiles" class="container">
            <div class="col-lg-2 no-column">
                <h3>Support a Project</h3>
            </div>
            <div class="col-lg-10">
                &nbsp;
            </div>
            <div class="clearfix"></div>

            <?php $args=array( //Loop 2
                //'post_type' => array ('news','project'), 
                'post_type' => 'project', 
                'posts_per_page' => 8,
                'orderby' => 'rand',
                //'taxonomy' => array ('journal','podcast'/*'tools-skill','stir-story'*/),
                //'term' => 'yes',
                //'orderby' => 'rand',
            );
            $myloop = new WP_Query($args);
            if($myloop->have_posts()) : while($myloop->have_posts()) :
            $myloop->the_post();
            ?>
            <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' ); $url = $thumb['0']; ?>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 no-column">
                <div class="item box fleft fwidth">
                    <div class="meta">
                        <span class="box fleft">
                            &nbsp;
                        </span>
                        <span class="box fright season">
                        <?php $terms = get_the_terms( $global_post_id, 'season' );?><?php if($terms){ ?><?php $term = array_shift($terms);?><span class="season <?php echo $term->slug;?>"> <?php echo $term->name;?> </span><?php } ?>
                        </span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="photo box fleft fwidth" style="background-image:url(<?php echo get('visuals_project_display');?>);">
                        <?php if( has_term('yes','funded', $global_post_id) ) {?><span class="funded">GRANT RECIPIENT</span><?php } else {?><? } ?>

                        <a href="<?php the_permalink(); ?>">&nbsp;</a>
                    </div>

                    <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                    <div class="clearfix"></div>


                        <div class="excerpt">
                            <?php echo get('describe_project');?>
                            <a href="<?php the_permalink(); ?>">more</a>
                        </div>
                    <!--comentar votos-->
                    <?/* <span class="supporters"><?php echo support_count(get_the_ID());?> Votes</span> */?>
                    <div class="clearfix"></div>

                    <?php if(get_the_tag_list()) { echo get_the_tag_list('<ul class="post-tags-front"><li>','</li><li>','</li></ul>'); } ?>
                </div>
            </div>

            <?php endwhile; endif; ?>
            <?php wp_reset_query(); ?>
            
            <div class="box fleft fwidth caja-boton text-center">
                <a href="<?php echo get_option('siteurl'); ?>/support" class="small-cta">[more projects]</a>
            </div>

        </div>               
        <div class="clearfix"></div>
        
        <div id="content-tiles" class="container">
            <div class="col-lg-2 no-column">
                <h3>From <br>the blog</h3>
            </div>
            <div class="col-lg-10 filter text-left">
                <p>Filter Topics</p>
                <ul>
                    <?
                    // your taxonomy name
                    $tax = 'type_of_content';
                    // get the terms of taxonomy
                    $terms = get_terms( $tax, [
                    'hide_empty' => true, // do not hide empty terms
                    ]);
                    // loop through all terms
                    foreach( $terms as $term ) {
                    // if no entries attached to the term
                    if( 0 == $term->count )
                    // display only the term name
                    echo '<li>' . $term->name . '</li>';
                    // if term has more than 0 entries
                    elseif( $term->count > 0 )
                    // display link to the term archive
                    echo '<li><a href="'. get_term_link( $term ) .'">'. $term->name .'</a></li>';
                    }?>
                </ul>
            </div>
            <div class="clearfix"></div>
                

            <?php $args=array( //Loop 2
                        //'post_type' => array ('news','project'), 
                        'post_type' => 'news', 
                        'posts_per_page' => 4,
                        'taxonomy' => array ('journal','podcast'/*'tools-skill','stir-story'*/),
                        //'term' => 'yes',
                        //'orderby' => 'rand',
                    );
                    $myloop = new WP_Query($args);
                    if($myloop->have_posts()) : while($myloop->have_posts()) :
                    $myloop->the_post();
                ?>
            <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumb-home-news' ); $url = $thumb['0']; ?>


            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 no-column">
                <div class="item box fleft fwidth">
                    <div class="meta">
                        <span class="box fleft">
                            <?php   // Get terms for post
                            $terms = get_the_terms( $post->ID , 'type_of_content' );
                            // Loop over each item since it's an array
                            if ( $terms != null ){
                            foreach( $terms as $term ) {
                            // Print the name method from $term which is an OBJECT
                            print $term->slug ;
                            // Get rid of the other data stored in the object, since it's not needed
                            unset($term);
                            } } ?>
                        </span>
                        <span class="box fright">
                            <?php the_time('d'); ?>.<?php the_time('m'); ?>.<?php the_time('Y'); ?>
                        </span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="photo box fleft fwidth" style="background-image:url(<?=$url?>);">
                        <a href="<?php the_permalink(); ?>">&nbsp;</a>
                    </div>

                    <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                    <div class="clearfix"></div>
                    <p class="excerpt"><?php echo excerpt(17); ?><a href="<?php the_permalink(); ?>">more</a></p>
                    <div class="clearfix"></div>

                    <?php
    if(get_the_tag_list()) {
    echo get_the_tag_list('<ul class="post-tags-front"><li>','</li><li>','</li></ul>');
    }
    ?>        
                </div>
            </div>

            <?php endwhile; endif; ?>
            <?php wp_reset_query(); ?>
            
            
            <div class="box fleft fwidth caja-boton text-center">
                <a href="<?php echo get_option('siteurl'); ?>/news" class="small-cta">[more articles]</a>
            </div>

        </div>
    </div>
<?php get_footer(); ?>