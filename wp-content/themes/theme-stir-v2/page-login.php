<?php 
	if( is_user_logged_in() ){ 	
		
		wp_redirect(get_permalink(get_echo esc_url( home_url() ))); 
	}
?>
<?php get_header(); ?>

<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>            
<div class="box fwidth fleft supertitulo">
    <div class="container no-column">
        <div class="col-lg-12 ">
            <h1>Login to continue</h1>
        </div>
    </div>
</div>
<div class="clearfix"></div>
  <section id="register" class="container login">
  	<div class="col-lg-12">
		<div id="tab-1" class="tab-content current">
			<?php /*<?php echo do_shortcode("[pie_register_login]"); ?> */?>
            <?php wp_login_form(); ?>
            <div class="clearfix"></div>
            <br><br>
			<div class="text-left">
				<?php do_action('facebook_login_button');?>
			</div>
            <p><a href="<?php echo get_option('siteurl'); ?>/register/?guest">Register</a> / <a href="<?php echo wp_lostpassword_url(); ?>"> Lost Your Password?</a> </p>
		</div>
	</div>
</section>
<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>
  

<?php get_footer(); ?>