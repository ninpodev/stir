<?php get_header(); ?>    
<div class="box fwidth fleft supertitulo">
    <div class="container-fluid">
        <div class="col-lg-12 no-column">
            <h1>Blog</h1>
        </div>
    </div>
</div>

		<div id="archive" class="container-fluid no-column">
            
			<a id="determine-user-type" class="in-page-link"></a>
            <div class="clearfix"></div>
            
            <div id="content-tiles" class="container-fluid">
                <div class="col-lg-2 no-column">
                &nbsp;
            </div>
            <div class="col-lg-12 filter text-left">
                <p>Filter Topics</p>
                <ul>
                    <?
                    // your taxonomy name
                    $tax = 'type_of_content';
                    // get the terms of taxonomy
                    $terms = get_terms( $tax, [
                    'hide_empty' => true, // do not hide empty terms
                    ]);
                    // loop through all terms
                    foreach( $terms as $term ) {
                    // if no entries attached to the term
                    if( 0 == $term->count )
                    // display only the term name
                    echo '<li>' . $term->name . '</li>';
                    // if term has more than 0 entries
                    elseif( $term->count > 0 )
                    // display link to the term archive
                    echo '<li><a href="'. get_term_link( $term ) .'">'. $term->name .'</a></li>';
                    }?>
                </ul>
            </div>
            <div class="clearfix"></div>
                <?php 
                $temp = $wp_query; 
                $wp_query = null; 
                $wp_query = new WP_Query(); 
                $wp_query->query('showposts=12&post_type=news'.'&paged='.$paged); 

                while ($wp_query->have_posts()) : $wp_query->the_post(); 
                ?>
                            <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumb-home-news' ); $url = $thumb['0']; ?>
               
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 no-column">
                    
                    
                    <div class="item box fleft fwidth">
                        <div class="meta">
                            <span class="box fleft">
                                <?php   // Get terms for post
                                $terms = get_the_terms( $post->ID , 'type_of_content' );
                                // Loop over each item since it's an array
                                if ( $terms != null ){
                                foreach( $terms as $term ) {
                                // Print the name method from $term which is an OBJECT
                                print $term->slug ;
                                // Get rid of the other data stored in the object, since it's not needed
                                unset($term);
                                } } ?>
                            </span>
                            <span class="box fright">
                                <?php the_time('d'); ?>.<?php the_time('m'); ?>.<?php the_time('Y'); ?>
                            </span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="photo box fleft fwidth" style="background-image:url(<?=$url?>);">
                            <a href="<?php the_permalink(); ?>">&nbsp;</a>
                        </div>
                        
                        <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                        <div class="clearfix"></div>
                        <p class="excerpt"><?php echo excerpt(17); ?><a href="<?php the_permalink(); ?>">more</a></p>
                        <div class="clearfix"></div>
                        
                        <?php
if(get_the_tag_list()) {
    echo get_the_tag_list('<ul class="post-tags"><li>','</li><li>','</li></ul>');
}
?>        
                    </div>
                </div>

                <?php endwhile; ?>
                <?php wp_pagenavi(); ?>
                <?php 
                $wp_query = null; 
                $wp_query = $temp;  // Reset
                ?>          
            </div>
            <div class="clearfix"></div>
            
            
            
			
		</div>
<?php get_footer(); ?>