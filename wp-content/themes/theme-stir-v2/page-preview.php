<?php if(!is_role('project') && !is_role('administrator')) wp_redirect(get_permalink(4));?>
<?php if(!have_draft()) wp_redirect(get_permalink(12)); ?>
<?php get_header(); ?>
 <div id="mensaje" class="preview-mensaje">
    <p>This is only a preview of your project. You can go <a href="<?php echo get_permalink(23);?>" class="boton-gris">Back</a> or &nbsp; </p>
	<form class="form-preview">
        <input type="submit" class="save-and-continue-button waves-effect waves-light btn" value="PUBLISH IT!" data-option="publish" data-page="preview" rel="button" style="color:#FFF;">
    </form>
  </div>
<?php $project = get_project();?> 
<?php $project = new WP_Query(array('post_type'=>'project','p'=>(int) $project['id']));?>
<?php if($project->have_posts()): $project->the_post();

       $color = @strtolower(get('choose_a_colour'));  
?> 







<section id="single" class="box fleft fwidth">

    <div class="contenedor-slide box fwidth fleft">
        <?php 
         $aditional = get_order_field('visuals_project_aditional_photos');
         $imagen = get('visuals_project_display');
        ?>        
        <div id="slide-single" class="flexslider">
            <ul class="slides">
                <?php if(!empty($imagen)){ ?>
                <li style="background-image:url(<?php echo $imagen;?>);">&nbsp;</li>            
                <?php } ?>
                <?php
                foreach($aditional as $key){ ?>
                <li style="background-image:url(<?php echo get('visuals_project_aditional_photos',1,$key);?>);">&nbsp;</li>
                <?php   } ?>
            </ul>
        </div>
        
        
    </div>
    <div class="clearfix"></div>
    
    
    <div id="excerpt" class="container">
        <!--si el logo existe-->
        <?php $logo = get('logo_project'); if(!empty($logo)) {?>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 logo-project">        
            <img src="<?php echo get('logo_project');?>" alt="">
        </div>
        <?php } else { ?>
        &nbsp;
        <?php } ?>   

        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
            <div class="row">
                <div class="col-lg-12">
                    <h1><?php the_title();?></h1>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <?php echo get('describe_project');?> <?php echo get('describe');?>
                </div>
            </div>
        </div>


    </div>    
    
    
    
    <div class="clearfix"></div>
	
    <!--contenido del proyecto-->
    <div class="container">
        <?php
			$comment_type = 'project_support';

			if( is_user_logged_in() ) {	
				global $post;
				$post_id = $post->ID;
				$author_post = get_post_field( 'post_author', $post_id );
				$current_user = wp_get_current_user();
				
				if($current_user->ID == $author_post){
					$coments = get_comments( array( 'meta_query' => array(
						array( 'key' => '_type_', 'value'=> $comment_type,'compare'=>'='),
						array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
					),'order'=>'ASC', 'parent'=>0, 'post_id' => get_the_ID()) );
					$reminder = comment_reminder($coments);
					if(isset(end($coments)->comment_ID)){
						$coments_child = get_comments(array('parent'=> $coments->comment_ID,'order'=>'ASC' ));
					}
				} else{ 
					
					$coments = get_comments(array( 'meta_query' => array(
						array( 'key' => '_type_', 'value'=> $comment_type,'compare'=>'='),
						array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
					),'parent'=>0,'order'=>'ASC', 'post_id' => get_the_ID(),'author__in'=> array($current_user->ID)));
					
					$reminder = comment_reminder($coments);	
					if(isset(end($coments)->comment_ID)){
						$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
					}

				}
				
			}
		?>
        
        
        
        
        <div class="modulo-contenido box fwidth fleft detail-project">
            <div class="txt col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <?php echo get('detail_of_project_detailed');?>
            </div>
        </div>
        
        <div class="modulo-contenido box fwidth fleft">
            <div class="video col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2">
                <?php echo getFrameVideo(get('visuals_project_url_video'));?>
            </div>
        </div>
        
        
        
        <div class="modulo-contenido box fwidth fleft">
            <div class="txt col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<?php if( is_user_logged_in() ){?>
                <?php if ( ( count($coments) != 0 && $current_user->ID == $author_post )   ) { ?>
					<span class="icos-comentarios <?php if($reminder == "_true_"){ ?>comentario-con-respuesta<?php }else{ ?>sin-comentarios agregar-nuevo-comentario<?php } ?>" title="Add new comment">&nbsp;</span>
                <?php } else if($current_user->ID != $author_post && count($coments)!=0){ ?>
					<span class="icos-comentarios <?php if(isset($coments_child) && $reminder == "_true_"){ ?>comentario-con-respuesta<?php }else{ ?>comentario-sin-respuesta<?php } ?> " title="Add new comment">&nbsp;</span>
				<?php } else if($current_user->ID != $author_post && count($coments)==0) { ?>
					<span class="icos-comentarios agregar-nuevo-comentario" title="Add new comment">&nbsp;</span>
				<?php } ?>
			<?php } ?>
                <h3>Why you should support <br><?php the_title(); ?>?</h3>
                <?php echo get('plan_project_support');?>
                <ul class="fa-ul edit">
                    <!--<li><i class="fa-li fa fa-pencil"></i><span>Edit</span></li>-->
                </ul>
            </div>
            <div class="comentarios col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <!--cuando no hay comentarios-->
                <div id="comentario-para-moderar" class="modulo-comentarios fwidth fleft">
                    <span class="cerrar-modulo text-center"><i class="fa fa-times fa-lg"></i></span>
                <?php if(is_user_logged_in()){?>	    
                   	<?php if ( ( count($coments) != 0 && $current_user->ID == $author_post )   ) { ?>
                   		<?php if( $reminder == "_true_" ){ ?>
                   		<h6>There is a reply to your comment</h6>
                   		<?php } else{?>
                   		<h6>Review and reply your comment</h6>
                   		<?php } ?>
                   	<?php } else if($current_user->ID != $author_post && count($coments)!=0){ ?>
                   	 <?php if( $reminder == "_true_" ){ ?>
                   		<h6>There is a reply to your comment</h6>
                   	 <?php } else{ ?>
                   	 	<h6>No reply to your comment</h6>
                   	<?php  } ?>
                   	<?php } else if($current_user->ID != $author_post && count($coments)==0) { ?>
                    	<h6>Help the author by leaving a comment or question</h6>
                    <?php } ?>
						
					
						<?php if($current_user->ID != $author_post){?>
						<div class="contenedor-comentarios fleft fwidth">
							<!-- user visit -->
								
							<ul class="lista-comentarios hay-respuesta">
							<?php foreach($coments as $coment){ ?>
								<li>
									<?php echo get_avatar( $coment->user_id, 32 ); ?>
									<span class="nombre"><?php echo $coment->comment_author;?> Says: </span><div class="clearfix"></div>
									<span class="comentario"><?php echo $coment->comment_content;?></span>
									<div class="clearfix"></div>
									<span class="fecha"> <?php echo $coment->comment_date;?></span>
								</li>
								<?php 
									$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
									foreach($coments_child as $child){	
								?>
								<li>
									<?php echo get_avatar( $child->user_id, 32 ); ?>
									<span class="nombre"><?php echo $child->comment_author;?> Says: </span>
                                    <div class="clearfix"></div>
									<span class="comentario"><?php echo $child->comment_content;?></span>
									<div class="clearfix"></div>
									<span class="fecha"> <?php echo $child->comment_date;?></span>
								</li>
								<?php } ?>
							<?php } ?>
							</ul>
							
							<?php if (count($coments)!=0 && count($coments_child) != 0 && !get_resolve($coment->comment_ID)){?>
							<form class="para-respuesta" name="project_support">
								<div class="input-field">
									<input type="hidden" name="id_project" value="<?php the_ID();?>">
									<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
									<textarea id="textarea2" class="materialize-textarea" name="comment"></textarea>
									<label for="textarea2">Reply</label>
								</div>
								<button class="btn waves-effect waves-light fright" type="submit" rel="comment" data-option="comment_project_reply">Reply <i class="fa fa-comments" aria-hidden="true"></i></button>
							</form>
							<?php } ?>
							
							<?php if(count($coments)==0 && !get_resolve($coment->comment_ID)){?>
							<form class="para-respuesta" name="project_support" >	
								<div class="input-field">
									<textarea id="textarea1" name="comment" class="materialize-textarea"></textarea>
									<input type="hidden" name="id_project" value="<?php the_ID();?>">
									<label for="textarea1">Leave your comment here</label>
								</div>
								
								<button class="btn waves-effect waves-light fleft" type="reset" name="action">Reset</button>
								<button class="btn waves-effect waves-light fright" type="submit" rel="comment"  data-option="comment_project">Submit <i class="fa fa-comments-o" aria-hidden="true"></i></button>
							</form>	
							<?php } ?>
						</div>
						<?php } ?>
					
						<!-- author del proyecto -->
						<?php if($current_user->ID == $author_post){ ?>
							<?php $coments = get_comments( array( 'meta_query' => array(
									array( 'key' => '_type_', 'value'=>'project_support','compare'=>'='),
									array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
									),'order'=>'ASC', 'parent'=>0, 'post_id' => get_the_ID()) );?>
							
							<?php foreach($coments as $coment){ ?>
							
							<div class="contenedor-comentarios fleft fwidth">
								<ul class="lista-comentarios hay-respuesta">
									<li>
										<?php echo get_avatar( $coment->user_id, 32 ); ?>
										<span class="nombre"><?php echo $coment->comment_author;?> Says: </span><div class="clearfix"></div>
										<span class="comentario"><?php echo $coment->comment_content;?></span>
										<div class="clearfix"></div>
										<span class="fecha"> <?php echo $coment->comment_date;?></span>
									</li>
									<?php 
										$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
										foreach($coments_child as $child){	
									?>
									<li>
										<?php echo get_avatar( $child->user_id, 32 ); ?>
										<span class="nombre"><?php echo $child->comment_author;?> Says: </span>
                                        <div class="clearfix"></div>
										<span class="comentario"><?php echo $child->comment_content;?></span>
										<div class="clearfix"></div>
										<span class="fecha"> <?php echo $child->comment_date;?></span>
									</li>
									<?php } ?>
								</ul>	
								
								<?php $resolved = get_resolve($coment->comment_ID);
									if($resolved != true){?>
								<form class="para-respuesta" name="project_support">
									<div class="input-field">
										<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
										<input type="hidden" name="id_project" value="<?php the_ID();?>">
										<textarea class="materialize-textarea" name="comment"></textarea>
										<label>Reply</label>
									</div>
									
									<button class="btn waves-effect waves-light fleft resolver" type="reset"  rel="comment" data-option="comment_resolve">Resolve this thread <i class="fa fa-check" aria-hidden="true"></i></button>
									<button class="btn waves-effect waves-light fright" type="submit"   rel="comment" data-option="comment_project_reply">Reply <i class="fa fa-comments" aria-hidden="true"></i></button>
								</form>
								<?php } ?>
								<div class="helpful box fwidth fleft text-center" <?php if($resolved != true){?>style="display:none"<?php } ?>>
									<p>Was this person helpful for your project?</p>
									<div class="clearfix"></div>
									<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
									<button class="btn waves-effect waves-light helpful-button" rel="comment"  data-option="comment_helpful">Helpful</button>
									<button class="btn waves-effect waves-light not-helpful-button" rel="comment" data-option="comment_not_helpful">Not Helpful</button>
								</div>
							
							</div>
							<?php } ?>
						<!-- owner project -->
						<?php } ?>
				<?php } ?>	
                </div>
                <!--cuando no hay comentarios-->
            </div>
        </div>
        
        
        
        <?php if ( is_old_post(160) ) { ?>
        <?php } else { ?>
        
        <?php
			$comment_type = 'project_current_status';

			if( is_user_logged_in() ) {	
				global $post;
				$post_id = $post->ID;
				$author_post = get_post_field( 'post_author', $post_id );
				$current_user = wp_get_current_user();
				
				if($current_user->ID == $author_post){
					$coments = get_comments( array( 'meta_query' => array(
						array( 'key' => '_type_', 'value'=> $comment_type,'compare'=>'='),
						array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
					),'order'=>'ASC', 'parent'=>0, 'post_id' => get_the_ID()) );
					$reminder = comment_reminder($coments);
					if(isset(end($coments)->comment_ID)){
						$coments_child = get_comments(array('parent'=> $coments->comment_ID,'order'=>'ASC' ));
					}
				} else{ 
					
					$coments = get_comments(array( 'meta_query' => array(
						array( 'key' => '_type_', 'value'=> $comment_type,'compare'=>'='),
						array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
					),'parent'=>0,'order'=>'ASC', 'post_id' => get_the_ID(),'author__in'=> array($current_user->ID)));
					
					$reminder = comment_reminder($coments);	
					if(isset(end($coments)->comment_ID)){
						$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
					}

				}
				
			}
		?>
        <div class="modulo-contenido box fwidth fleft">
            <div class="txt col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <?php if( is_user_logged_in() ){?>
                <?php if ( ( count($coments) != 0 && $current_user->ID == $author_post )   ) { ?>
					<span class="icos-comentarios <?php if($reminder == "_true_"){ ?>comentario-con-respuesta<?php }else{ ?>sin-comentarios agregar-nuevo-comentario<?php } ?>" title="Add new comment">&nbsp;</span>
                <?php } else if($current_user->ID != $author_post && count($coments)!=0){ ?>
					<span class="icos-comentarios <?php if(isset($coments_child) && $reminder == "_true_"){ ?>comentario-con-respuesta<?php }else{ ?>comentario-sin-respuesta<?php } ?> " title="Add new comment">&nbsp;</span>
				<?php } else if($current_user->ID != $author_post && count($coments)==0) { ?>
					<span class="icos-comentarios agregar-nuevo-comentario" title="Add new comment">&nbsp;</span>
				<?php } ?>
			<?php } ?>
                <h3>What is the project’s current state?</h3>
                <?php echo get('plan_project_current_status');?>
                
                <ul class="fa-ul edit">
                    <!--<li><i class="fa-li fa fa-pencil"></i><span>Edit</span></li>-->
                </ul>
            </div>
            <div class="comentarios col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <!--cuando no hay comentarios-->
                <div id="comentario-para-moderar" class="modulo-comentarios fwidth fleft">
                    <span class="cerrar-modulo text-center"><i class="fa fa-times fa-lg"></i></span>
                <?php if(is_user_logged_in()){?>	    
                   	<?php if ( ( count($coments) != 0 && $current_user->ID == $author_post )   ) { ?>
                   		<?php if( $reminder == "_true_" ){ ?>
                   		<h6>There is a reply to your comment</h6>
                   		<?php } else{?>
                   		<h6>Review and reply your comment</h6>
                   		<?php } ?>
                   	<?php } else if($current_user->ID != $author_post && count($coments)!=0){ ?>
                   	 <?php if( $reminder == "_true_" ){ ?>
                   		<h6>There is a reply to your comment</h6>
                   	 <?php } else{ ?>
                   	 	<h6>No reply to your comment</h6>
                   	<?php  } ?>
                   	<?php } else if($current_user->ID != $author_post && count($coments)==0) { ?>
                    	<h6>Help the author by leaving a comment or question</h6>
                    <?php } ?>
						
					
						<?php if($current_user->ID != $author_post){?>
						<div class="contenedor-comentarios fleft fwidth">
							<!-- user visit -->
								
							<ul class="lista-comentarios hay-respuesta">
							<?php foreach($coments as $coment){ ?>
								<li>
									<?php echo get_avatar( $coment->user_id, 32 ); ?>
									<span class="nombre"><?php echo $coment->comment_author;?> Says: </span>
                                    <div class="clearfix"></div>
									<span class="comentario"><?php echo $coment->comment_content;?></span>
									<div class="clearfix"></div>
									<span class="fecha"> <?php echo $coment->comment_date;?></span>
								</li>
								<?php 
									$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
									foreach($coments_child as $child){	
								?>
								<li>
									<?php echo get_avatar( $child->user_id, 32 ); ?>
									<span class="nombre"><?php echo $child->comment_author;?> Says: </span>
                                    <div class="clearfix"></div>
									<span class="comentario"><?php echo $child->comment_content;?></span>
									<div class="clearfix"></div>
									<span class="fecha"> <?php echo $child->comment_date;?></span>
								</li>
								<?php } ?>
							<?php } ?>
							</ul>
							
							<?php if (count($coments)!=0 && count($coments_child) != 0 && !get_resolve($coment->comment_ID)){?>
							<form class="para-respuesta" name="<?php echo $comment_type;?>">
								<div class="input-field">
									<input type="hidden" name="id_project" value="<?php the_ID();?>">
									<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
									<textarea id="textarea2" class="materialize-textarea" name="comment"></textarea>
									<label for="textarea2">Reply</label>
								</div>
								<button class="btn waves-effect waves-light fright" type="submit" rel="comment" data-option="comment_project_reply">Reply <i class="fa fa-comments" aria-hidden="true"></i></button>
							</form>
							<?php } ?>
							
							<?php if(count($coments)==0 && !get_resolve($coment->comment_ID)){?>
							<form class="para-respuesta" name="<?php echo $comment_type;?>" >	
								<div class="input-field">
									<textarea id="textarea1" name="comment" class="materialize-textarea"></textarea>
									<input type="hidden" name="id_project" value="<?php the_ID();?>">
									<label for="textarea1">Leave your comment here</label>
								</div>
								
								<button class="btn waves-effect waves-light fleft" type="reset" name="action">Reset</button>
								<button class="btn waves-effect waves-light fright" type="submit" rel="comment"  data-option="comment_project">Submit <i class="fa fa-comments-o" aria-hidden="true"></i></button>
							</form>	
							<?php } ?>
						</div>
						<?php } ?>
					
						<!-- author del proyecto -->
						<?php if($current_user->ID == $author_post){ ?>
							<?php $coments = get_comments( array( 'meta_query' => array(
									array( 'key' => '_type_', 'value'=>$comment_type,'compare'=>'='),
									array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
									),'order'=>'ASC', 'parent'=>0, 'post_id' => get_the_ID()) );?>
							
							<?php foreach($coments as $coment){ ?>
							
							<div class="contenedor-comentarios fleft fwidth">
								<ul class="lista-comentarios hay-respuesta">
									<li>
										<?php echo get_avatar( $coment->user_id, 32 ); ?>
										<span class="nombre"><?php echo $coment->comment_author;?> Says: </span><div class="clearfix"></div>
										<span class="comentario"><?php echo $coment->comment_content;?></span>
										<div class="clearfix"></div>
										<span class="fecha"> <?php echo $coment->comment_date;?></span>
									</li>
									<?php 
										$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
										foreach($coments_child as $child){	
									?>
									<li>
										<?php echo get_avatar( $child->user_id, 32 ); ?>
										<span class="nombre"><?php echo $child->comment_author;?> Says: </span>
                                        <div class="clearfix"></div>
										<span class="comentario"><?php echo $child->comment_content;?></span>
										<div class="clearfix"></div>
										<span class="fecha"> <?php echo $child->comment_date;?></span>
									</li>
									<?php } ?>
								</ul>	
								
								<?php $resolved = get_resolve($coment->comment_ID);
									if($resolved != true){?>
								<form class="para-respuesta" name="<?php echo $comment_type;?>">
									<div class="input-field">
										<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
										<input type="hidden" name="id_project" value="<?php the_ID();?>">
										<textarea class="materialize-textarea" name="comment"></textarea>
										<label>Reply</label>
									</div>
									
									<button class="btn waves-effect waves-light fleft resolver" type="reset"  rel="comment" data-option="comment_resolve">Resolve this thread <i class="fa fa-check" aria-hidden="true"></i></button>
									<button class="btn waves-effect waves-light fright" type="submit"   rel="comment" data-option="comment_project_reply">Reply <i class="fa fa-comments" aria-hidden="true"></i></button>
								</form>
								<?php } ?>
								<div class="helpful box fwidth fleft text-center" <?php if($resolved != true){?>style="display:none"<?php } ?>>
									<p>Was this person helpful for your project?</p>
									<div class="clearfix"></div>
									<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
									<button class="btn waves-effect waves-light helpful-button" rel="comment"  data-option="comment_helpful">Helpful</button>
									<button class="btn waves-effect waves-light not-helpful-button" rel="comment" data-option="comment_not_helpful">Not Helpful</button>
								</div>
							
							</div>
							<?php } ?>
						<!-- owner project -->
						<?php } ?>
				<?php } ?>	
                </div>
                <!--cuando no hay comentarios-->
            </div>
        </div>
        
        
        <?php } ?>
        
        
        
        
         <?php
			$comment_type = 'project_stir_money';

			if( is_user_logged_in() ) {	
				global $post;
				$post_id = $post->ID;
				$author_post = get_post_field( 'post_author', $post_id );
				$current_user = wp_get_current_user();
				
				if($current_user->ID == $author_post){
					$coments = get_comments( array( 'meta_query' => array(
						array( 'key' => '_type_', 'value'=> $comment_type,'compare'=>'='),
						array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
					),'order'=>'ASC', 'parent'=>0, 'post_id' => get_the_ID()) );
					$reminder = comment_reminder($coments);
					if(isset(end($coments)->comment_ID)){
						$coments_child = get_comments(array('parent'=> $coments->comment_ID,'order'=>'ASC' ));
					}
				} else{ 
					
					$coments = get_comments(array( 'meta_query' => array(
						array( 'key' => '_type_', 'value'=> $comment_type,'compare'=>'='),
						array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
					),'parent'=>0,'order'=>'ASC', 'post_id' => get_the_ID(),'author__in'=> array($current_user->ID)));
					
					$reminder = comment_reminder($coments);	
					if(isset(end($coments)->comment_ID)){
						$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
					}

				}
				
			}
		?>
        
        <div class="modulo-contenido box fwidth fleft">
            <div class="txt col-lg-8 col-md-8 col-sm-8 col-xs-12">
             <?php if( is_user_logged_in() ){?>
                <?php if ( ( count($coments) != 0 && $current_user->ID == $author_post )   ) { ?>
					<span class="icos-comentarios <?php if($reminder == "_true_"){ ?>comentario-con-respuesta<?php }else{ ?>sin-comentarios agregar-nuevo-comentario<?php } ?>" title="Add new comment">&nbsp;</span>
                <?php } else if($current_user->ID != $author_post && count($coments)!=0){ ?>
					<span class="icos-comentarios <?php if(isset($coments_child) && $reminder == "_true_"){ ?>comentario-con-respuesta<?php }else{ ?>comentario-sin-respuesta<?php } ?> " title="Add new comment">&nbsp;</span>
				<?php } else if($current_user->ID != $author_post && count($coments)==0) { ?>
					<span class="icos-comentarios agregar-nuevo-comentario" title="Add new comment">&nbsp;</span>
				<?php } ?>
			<?php } ?>
                <h3>How will the money be used?</h3>
                <?php echo get('plan_project_stir_money');?>
                <ul class="fa-ul edit">
                    <!--<li><i class="fa-li fa fa-pencil"></i><span>Edit</span></li>-->
                </ul>
            </div>
            <div class="comentarios col-lg-4 col-md-4 col-sm-4 col-xs-12">
                
               <div id="comentario-para-moderar" class="modulo-comentarios fwidth fleft">
                    <span class="cerrar-modulo text-center"><i class="fa fa-times fa-lg"></i></span>
                <?php if(is_user_logged_in()){?>	    
                   	<?php if ( ( count($coments) != 0 && $current_user->ID == $author_post )   ) { ?>
                   		<?php if( $reminder == "_true_" ){ ?>
                   		<h6>There is a reply to your comment</h6>
                   		<?php } else{?>
                   		<h6>Review and reply your comment</h6>
                   		<?php } ?>
                   	<?php } else if($current_user->ID != $author_post && count($coments)!=0){ ?>
                   	 <?php if( $reminder == "_true_" ){ ?>
                   		<h6>There is a reply to your comment</h6>
                   	 <?php } else{ ?>
                   	 	<h6>No reply to your comment</h6>
                   	<?php  } ?>
                   	<?php } else if($current_user->ID != $author_post && count($coments)==0) { ?>
                    	<h6>Help the author by leaving a comment or question</h6>
                    <?php } ?>
						
					
						<?php if($current_user->ID != $author_post){?>
						<div class="contenedor-comentarios fleft fwidth">
							<!-- user visit -->
								
							<ul class="lista-comentarios hay-respuesta">
							<?php foreach($coments as $coment){ ?>
								<li>
									<?php echo get_avatar( $coment->user_id, 32 ); ?>
									<span class="nombre"><?php echo $coment->comment_author;?> Says: </span>
                                    <div class="clearfix"></div>
									<span class="comentario"><?php echo $coment->comment_content;?></span>
									<div class="clearfix"></div>
									<span class="fecha"> <?php echo $coment->comment_date;?></span>
								</li>
								<?php 
									$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
									foreach($coments_child as $child){	
								?>
								<li>
									<?php echo get_avatar( $child->user_id, 32 ); ?>
									<span class="nombre"><?php echo $child->comment_author;?> Says: </span>
                                    <div class="clearfix"></div>
									<span class="comentario"><?php echo $child->comment_content;?></span>
									<div class="clearfix"></div>
									<span class="fecha"> <?php echo $child->comment_date;?></span>
								</li>
								<?php } ?>
							<?php } ?>
							</ul>
							
							<?php if (count($coments)!=0 && count($coments_child) != 0 && !get_resolve($coment->comment_ID)){?>
							<form class="para-respuesta" name="<?php echo $comment_type;?>">
								<div class="input-field">
									<input type="hidden" name="id_project" value="<?php the_ID();?>">
									<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
									<textarea id="textarea2" class="materialize-textarea" name="comment"></textarea>
									<label for="textarea2">Reply</label>
								</div>
								<button class="btn waves-effect waves-light fright" type="submit" rel="comment" data-option="comment_project_reply">Reply <i class="fa fa-comments" aria-hidden="true"></i></button>
							</form>
							<?php } ?>
							
							<?php if(count($coments)==0 && !get_resolve($coment->comment_ID)){?>
							<form class="para-respuesta" name="<?php echo $comment_type;?>" >	
								<div class="input-field">
									<textarea id="textarea1" name="comment" class="materialize-textarea"></textarea>
									<input type="hidden" name="id_project" value="<?php the_ID();?>">
									<label for="textarea1">Leave your comment here</label>
								</div>
								
								<button class="btn waves-effect waves-light fleft" type="reset" name="action">Reset</button>
								<button class="btn waves-effect waves-light fright" type="submit" rel="comment"  data-option="comment_project">Submit <i class="fa fa-comments-o" aria-hidden="true"></i></button>
							</form>	
							<?php } ?>
						</div>
						<?php } ?>
					
						<!-- author del proyecto -->
						<?php if($current_user->ID == $author_post){ ?>
							<?php $coments = get_comments( array( 'meta_query' => array(
									array( 'key' => '_type_', 'value'=>$comment_type,'compare'=>'='),
									array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
									),'order'=>'ASC', 'parent'=>0, 'post_id' => get_the_ID()) );?>
							
							<?php foreach($coments as $coment){ ?>
							
							<div class="contenedor-comentarios fleft fwidth">
								<ul class="lista-comentarios hay-respuesta">
									<li>
										<?php echo get_avatar( $coment->user_id, 32 ); ?>
										<span class="nombre"><?php echo $coment->comment_author;?> Says: </span><div class="clearfix"></div>
										<span class="comentario"><?php echo $coment->comment_content;?></span>
										<div class="clearfix"></div>
										<span class="fecha"> <?php echo $coment->comment_date;?></span>
									</li>
									<?php 
										$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
										foreach($coments_child as $child){	
									?>
									<li>
										<?php echo get_avatar( $child->user_id, 32 ); ?>
										<span class="nombre"><?php echo $child->comment_author;?> Says: </span>
                                        <div class="clearfix"></div>
										<span class="comentario"><?php echo $child->comment_content;?></span>
										<div class="clearfix"></div>
										<span class="fecha"> <?php echo $child->comment_date;?></span>
									</li>
									<?php } ?>
								</ul>	
								
								<?php $resolved = get_resolve($coment->comment_ID);
									if($resolved != true){?>
								<form class="para-respuesta" name="<?php echo $comment_type;?>">
									<div class="input-field">
										<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
										<input type="hidden" name="id_project" value="<?php the_ID();?>">
										<textarea class="materialize-textarea" name="comment"></textarea>
										<label>Reply</label>
									</div>
									
									<button class="btn waves-effect waves-light fleft resolver" type="reset"  rel="comment" data-option="comment_resolve">Resolve this thread <i class="fa fa-check" aria-hidden="true"></i></button>
									<button class="btn waves-effect waves-light fright" type="submit"   rel="comment" data-option="comment_project_reply">Reply <i class="fa fa-comments" aria-hidden="true"></i></button>
								</form>
								<?php } ?>
								<div class="helpful box fwidth fleft text-center" <?php if($resolved != true){?>style="display:none"<?php } ?>>
									<p>Was this person helpful for your project?</p>
									<div class="clearfix"></div>
									<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
									<button class="btn waves-effect waves-light helpful-button" rel="comment"  data-option="comment_helpful">Helpful</button>
									<button class="btn waves-effect waves-light not-helpful-button" rel="comment" data-option="comment_not_helpful">Not Helpful</button>
								</div>
							
							</div>
							<?php } ?>
						<!-- owner project -->
						<?php } ?>
				<?php } ?>	
                </div>
                <!--cuando no hay comentarios-->
                
                
            </div>
        </div>
        <?php
			$comment_type = 'project_future';

			if( is_user_logged_in() ) {	
				global $post;
				$post_id = $post->ID;
				$author_post = get_post_field( 'post_author', $post_id );
				$current_user = wp_get_current_user();
				
				if($current_user->ID == $author_post){
					$coments = get_comments( array( 'meta_query' => array(
						array( 'key' => '_type_', 'value'=> $comment_type,'compare'=>'='),
						array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
					),'order'=>'ASC', 'parent'=>0, 'post_id' => get_the_ID()) );
					$reminder = comment_reminder($coments);
					if(isset(end($coments)->comment_ID)){
						$coments_child = get_comments(array('parent'=> $coments->comment_ID,'order'=>'ASC' ));
					}
				} else{ 
					
					$coments = get_comments(array( 'meta_query' => array(
						array( 'key' => '_type_', 'value'=> $comment_type,'compare'=>'='),
						array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
					),'parent'=>0,'order'=>'ASC', 'post_id' => get_the_ID(),'author__in'=> array($current_user->ID)));
					
					$reminder = comment_reminder($coments);	
					if(isset(end($coments)->comment_ID)){
						$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
					}

				}
				
			}
		?>
        
        <div class="modulo-contenido box fwidth fleft">
            <div class="txt col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <?php if( is_user_logged_in() ){?>
            <?php if ( ( count($coments) != 0 && $current_user->ID == $author_post )   ) { ?>
				<span class="icos-comentarios <?php if($reminder == "_true_"){ ?>comentario-con-respuesta<?php }else{ ?>sin-comentarios agregar-nuevo-comentario<?php } ?>" title="Add new comment">&nbsp;</span>
            <?php } else if($current_user->ID != $author_post && count($coments)!=0){ ?>
				<span class="icos-comentarios <?php if(isset($coments_child) && $reminder == "_true_"){ ?>comentario-con-respuesta<?php }else{ ?>comentario-sin-respuesta<?php } ?> " title="Add new comment">&nbsp;</span>
			<?php } else if($current_user->ID != $author_post && count($coments)==0) { ?>
				<span class="icos-comentarios agregar-nuevo-comentario" title="Add new comment">&nbsp;</span>
			<?php } ?>
			<?php } ?>
                <h3>Where will <?php the_title(); ?> <br>go in the future?</h3>
                <?php echo get('plan_project_future');?>
                <ul class="fa-ul edit">
                    <!--<li><i class="fa-li fa fa-pencil"></i><span>Edit</span></li>-->
                </ul>
            </div>
            <div class="comentarios col-lg-4 col-md-4 col-sm-4 col-xs-12">
                
                <!--cuando hay comentarios visto desde el autor del proyecto-->
                <div id="comentario-para-moderar" class="modulo-comentarios fwidth fleft">
                    <span class="cerrar-modulo text-center"><i class="fa fa-times fa-lg"></i></span>
                <?php if(is_user_logged_in()){?>	    
                   	<?php if ( ( count($coments) != 0 && $current_user->ID == $author_post )   ) { ?>
                   		<?php if( $reminder == "_true_" ){ ?>
                   		<h6>There is a reply to your comment</h6>
                   		<?php } else{?>
                   		<h6>Review and reply your comment</h6>
                   		<?php } ?>
                   	<?php } else if($current_user->ID != $author_post && count($coments)!=0){ ?>
                   	 <?php if( $reminder == "_true_" ){ ?>
                   		<h6>There is a reply to your comment</h6>
                   	 <?php } else{ ?>
                   	 	<h6>No reply to your comment</h6>
                   	<?php  } ?>
                   	<?php } else if($current_user->ID != $author_post && count($coments)==0) { ?>
                    	<h6>Help the author by leaving a comment or question</h6>
                    <?php } ?>
						
					
						<?php if($current_user->ID != $author_post){?>
						<div class="contenedor-comentarios fleft fwidth">
							<!-- user visit -->
								
							<ul class="lista-comentarios hay-respuesta">
							<?php foreach($coments as $coment){ ?>
								<li>
									<?php echo get_avatar( $coment->user_id, 32 ); ?>
									<span class="nombre"><?php echo $coment->comment_author;?> Says: </span><div class="clearfix"></div>
									<span class="comentario"><?php echo $coment->comment_content;?></span>
									<div class="clearfix"></div>
									<span class="fecha"> <?php echo $coment->comment_date;?></span>
								</li>
								<?php 
									$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
									foreach($coments_child as $child){	
								?>
								<li>
									<?php echo get_avatar( $child->user_id, 32 ); ?>
									<span class="nombre"><?php echo $child->comment_author;?> Says: </span>
                                    <div class="clearfix"></div>
									<span class="comentario"><?php echo $child->comment_content;?></span>
									<div class="clearfix"></div>
									<span class="fecha"> <?php echo $child->comment_date;?></span>
								</li>
								<?php } ?>
							<?php } ?>
							</ul>
							
							<?php if (count($coments)!=0 && count($coments_child) != 0 && !get_resolve($coment->comment_ID)){?>
							<form class="para-respuesta" name="<?php echo $comment_type;?>">
								<div class="input-field">
									<input type="hidden" name="id_project" value="<?php the_ID();?>">
									<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
									<textarea id="textarea2" class="materialize-textarea" name="comment"></textarea>
									<label for="textarea2">Reply</label>
								</div>
								<button class="btn waves-effect waves-light fright" type="submit" rel="comment" data-option="comment_project_reply">Reply <i class="fa fa-comments" aria-hidden="true"></i></button>
							</form>
							<?php } ?>
							
							<?php if(count($coments)==0 && !get_resolve($coment->comment_ID)){?>
							<form class="para-respuesta" name="<?php echo $comment_type;?>" >	
								<div class="input-field">
									<textarea id="textarea1" name="comment" class="materialize-textarea"></textarea>
									<input type="hidden" name="id_project" value="<?php the_ID();?>">
									<label for="textarea1">Leave your comment here</label>
								</div>
								
								<button class="btn waves-effect waves-light fleft" type="reset" name="action">Reset</button>
								<button class="btn waves-effect waves-light fright" type="submit" rel="comment"  data-option="comment_project">Submit <i class="fa fa-comments-o" aria-hidden="true"></i></button>
							</form>	
							<?php } ?>
						</div>
						<?php } ?>
					
						<!-- author del proyecto -->
						<?php if($current_user->ID == $author_post){ ?>
							<?php $coments = get_comments( array( 'meta_query' => array(
									array( 'key' => '_type_', 'value'=>$comment_type,'compare'=>'='),
									array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
									),'order'=>'ASC', 'parent'=>0, 'post_id' => get_the_ID()) );?>
							
							<?php foreach($coments as $coment){ ?>
							
							<div class="contenedor-comentarios fleft fwidth">
								<ul class="lista-comentarios hay-respuesta">
									<li>
										<?php echo get_avatar( $coment->user_id, 32 ); ?>
										<span class="nombre"><?php echo $coment->comment_author;?> Says: </span><div class="clearfix"></div>
										<span class="comentario"><?php echo $coment->comment_content;?></span>
										<div class="clearfix"></div>
										<span class="fecha"> <?php echo $coment->comment_date;?></span>
									</li>
									<?php 
										$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
										foreach($coments_child as $child){	
									?>
									<li>
										<?php echo get_avatar( $child->user_id, 32 ); ?>
										<span class="nombre"><?php echo $child->comment_author;?> Says: </span>
                                        <div class="clearfix"></div>
										<span class="comentario"><?php echo $child->comment_content;?></span>
										<div class="clearfix"></div>
										<span class="fecha"> <?php echo $child->comment_date;?></span>
									</li>
									<?php } ?>
								</ul>	
								
								<?php $resolved = get_resolve($coment->comment_ID);
									if($resolved != true){?>
								<form class="para-respuesta" name="<?php echo $comment_type;?>">
									<div class="input-field">
										<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
										<input type="hidden" name="id_project" value="<?php the_ID();?>">
										<textarea class="materialize-textarea" name="comment"></textarea>
										<label>Reply</label>
									</div>
									
									<button class="btn waves-effect waves-light fleft resolver" type="reset"  rel="comment" data-option="comment_resolve">Resolve this thread <i class="fa fa-check" aria-hidden="true"></i></button>
									<button class="btn waves-effect waves-light fright" type="submit"   rel="comment" data-option="comment_project_reply">Reply <i class="fa fa-comments" aria-hidden="true"></i></button>
								</form>
								<?php } ?>
								<div class="helpful box fwidth fleft text-center" <?php if($resolved != true){?>style="display:none"<?php } ?>>
									<p>Was this person helpful for your project?</p>
									<div class="clearfix"></div>
									<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
									<button class="btn waves-effect waves-light helpful-button" rel="comment"  data-option="comment_helpful">Helpful</button>
									<button class="btn waves-effect waves-light not-helpful-button" rel="comment" data-option="comment_not_helpful">Not Helpful</button>
								</div>
							
							</div>
							<?php } ?>
						<!-- owner project -->
						<?php } ?>
				<?php } ?>	
                </div>
               
                <!--cuando hay comentarios sin respuesta visto desd el creador del comentario-->
                
            </div>
        </div>
        
        
        
        <div class="modulo-contenido box fwidth fleft collaborators">
            
                <?php 
                    $collab = get('team_project_team_collaborators');
                ?>
            
                <?php if(empty($collab)){?> 
                <?php } else { ?>
                        <div class="txt col-lg-3 col-md-3 col-sm-3 col-xs-12"> 
                            <h3><?php the_title(); ?> is looking for collaborators</h3>
                             
                            <?php if ( is_user_logged_in() ) { ?>
                <p>Required Skills: <?php echo get('team_project_team_collaborators');?></p>
                             <p>Contact at <a href="mailto:<?php echo get('team_project_team_email');?>"> <?php echo get('team_project_team_email');?></a></p>
                <div class="card warn">
    <div class="card-block">
        <p class="card-text">This content will be shown only to registered users.</p>
    </div>
</div>
                            
                <? } else { ?>
                <p><small><a data-toggle="modal" data-target="#myModal">Log-in</a> or <a href="<?php echo get_option('siteurl'); ?>/register/?guest" target="_blank">Register <i class="fa fa-external-link fa-lg" aria-hidden="true"></i></a> to check contact details. </small></p>
                <? }?>
                            
                            
                            
                            
                            
                            

                         </div>
                <?php } ?>
            
            
            <div class="txt col-lg-3 col-md-3 col-sm-3 col-xs-12">
                
                <h3>Find out more about <?php the_title(); ?></h3>
                <?php 
                    $find = get_order_field('visuals_project_find_project');
                    foreach($find as $key ){ 
                    $icons = array('facebook','linkedin','twitter','google','youtube','pinterest','soundcloud','instagram','behance','wordpress');
                    $uri = get('visuals_project_find_project',1,$key);
                    $url = parse_url($uri);
                    $domain = preg_replace('/(www(\.)|\.(\w|\d)*)/','',$url["host"]);
                    if($domain=='google') $domain="google-plus";
                    if(!empty($domain)){
                ?> 
                <ul class="redes">
                    <li>
                        <a href="<?php echo  get('visuals_project_find_project',1,$key);?>" target="_blank">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-square fa-stack-2x"></i>
                                <i class="fa fa-<?php echo $domain;?> fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>                    
                    </li>
                </ul>    
                <?php   } ?>
                <?php } ?>
                <div class="clearfix"></div>
                
                <ul class="fa-ul edit">
                    <!--<li><i class="fa-li fa fa-pencil"></i><span>Edit</span></li>-->
                </ul>
            </div>
            
            
            
            <div class="txt col-lg-6 col-md-6 col-sm-6 col-xs-12">
                
                <h3>Who is behind <?php the_title(); ?>?</h3>
                
                
                
                <?php /* <?php
                    $teams = get_order_group('other_member_project_role');
                    foreach($teams as $team){
                    $role = get('other_member_project_role',$team);
                    $age  = get('other_member_project_age',$team);
                    if(!empty($role) && !empty($age)){
                ?>
                <div class="item-proj" style="background-image:url(<?php echo get('other_member_project_picture',$team);?>);">
                    <div class="info bg-<?php echo $color;?>"  <?php if(empty($color)){?> style="background-color:#ef4136;"<?php } ?>>
                    <aside class="txt">
                        <p><?php echo get('other_member_project_role',$team);?></p>
                        <span>Age: <?php echo  get('other_member_project_age',$team);?>, Gender: <?php echo  get('other_member_project_gender',$team);?></span>
                        <span><?php echo get('other_member_project_skills',$team);?>; </span>
                    </aside>
                    </div>
                </div>
                <?php } } ?>
                */ ?>
                
        <div class="clearfix"></div>
                
             
                <?php
                    $teams = get_order_group('team_project_old');
                    foreach($teams as $team){
                    $linkperso = get('team_project_find_people',$team);
                ?>
                 
                <div class="col-lg-5 col-md-4 col-sm-6 col-xs-12">
            <div class="card">
                <div class="card-image">
                  <img src="<?php echo get('team_project_profile_picture',$team);?>">
                </div>
                <div class="card-content">
                    <span class="card-title"><?php echo get('team_project_role',$team);?></span>
                    <p><?php echo  get('team_project_sex',$team);?>, <?php echo  get('team_project_old',$team);?></p>
                    <br>
                    <p>My Skills: <?php echo get('team_project_skills',$team);?></p>
                </div>

            <?php if(empty($linkperso)){?> 
            <?php } else { ?>
            <div id="finsingle" class="card-action">
                  <a href="<?php echo get('team_project_find_people',$team);?>" target="_blank">Find out more about me</a>
                </div>
            <?php } ?>
              </div>
            </div>
                 
                <?php } ?>
                 
                 <?php                
                $teams = get_order_group('other_member_project_role');
                foreach($teams as $team){
                $role = get('other_member_project_role',$team);
                $age  = get('other_member_project_age',$team);
                if(!empty($role) && !empty($age)){
                ?>
                 
                 

                 <div class="col-lg-5 col-md-4 col-sm-6 col-xs-12">
                 <div class="card">
                <div class="card-image">
                  <img src="<?php echo get('other_member_project_picture',$team);?>" alt="">
                </div>
                <div class="card-content">
                    <span class="card-title"><?php echo get('other_member_project_role',$team);?></span>
                    <p><?php echo  get('other_member_project_gender',$team);?>, <?php echo  get('other_member_project_age',$team);?></p>
                    <br>
                    <p>My Skills: <?php echo get('other_member_project_skills',$team);?></p>
                </div>
              </div> 
            </div>
                 
                 
                
               <?php } } ?> 
                 
                 
                 
            
        
                
                       
                
                
                <div class="clearfix"></div>
                <ul class="fa-ul edit">
                    <!--<li><i class="fa-li fa fa-pencil"></i><span>Edit</span></li>-->
                </ul>
            </div>
            
        </div>
        
    </div>
    <!--/contenido del proyecto-->
    <div id="mobile-share" class="box fleft fwidth hidden-lg hidden-md hidden-sm">
        <?php echo do_shortcode('[ssbp]'); ?>
    </div>
    
    <div id="similar-projects" class="box fleft fwidth">
        <div class="container">
            <div id="project-tiles" class="container">
                <div class="co-lg-12 col-md-12 col-sm-12 filter">
                    <h3>Check Related Projects</h3>
                </div>
                
                <?php $args=array( //Loop 2
                    //'post_type' => array ('news','project'), 
                    'post_type' => 'project', 
                    'posts_per_page' => 4,
                    'orderby' => 'rand',
                    //'taxonomy' => array ('journal','podcast'/*'tools-skill','stir-story'*/),
                    //'term' => 'yes',
                    //'orderby' => 'rand',
                );
                $myloop = new WP_Query($args);
                if($myloop->have_posts()) : while($myloop->have_posts()) :
                $myloop->the_post();
                ?>
                <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' ); $url = $thumb['0']; ?>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 no-column">
                    <div class="item box fleft fwidth">
                        <div class="meta">
                            <span class="box fleft">
                                &nbsp;
                            </span>
                            <span class="box fright season">
                            <?php if( has_term('season-01','season', $global_post_id) ) {?>S01 <?php } else {?>S02<? } ?>
                            </span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="photo box fleft fwidth" style="background-image:url(<?php echo get('visuals_project_display');?>);">
                            <?php if( has_term('yes','funded', $global_post_id) ) {?><span class="funded">GRANT RECIPIENT</span><?php } else {?><? } ?>
                            
                            <a href="<?php the_permalink(); ?>">&nbsp;</a>
                        </div>
                        
                        <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                        <div class="clearfix"></div>
                        
                        
                            <div class="excerpt">
                                <?php echo get('describe_project');?>
                                <a href="<?php the_permalink(); ?>">more</a>
                            </div>
                        <span class="supporters"><?php echo support_count(get_the_ID());?> Votes</span>
                        <div class="clearfix"></div>
                        
                        <?php if(get_the_tag_list()) { echo get_the_tag_list('<ul class="post-tags"><li>','</li><li>','</li></ul>'); } ?>
                    </div>
                </div>

                <?php endwhile; endif; ?>
                <?php wp_reset_query(); ?>
                <a href="<?php echo get_option('siteurl'); ?>/support" class="sublink">Browse More Projects on the<br class="hidden-lg hidden-md hidden-sm"> <span>Support Page</span></a>
                
            </div>
            
            
        </div>
    </div>
    
</section>




<?php /*
<section id="single-project" class="container">
  
  
<div class="clearfix"></div>  
  
  
    <div class="project col-lg-9 col-md-9 col-sm-9 col-xs-12">
        <div class="title-project col-lg-9 col-md-9 col-sm-9 col-xs-9">
            <span class="small">Project <?php echo get_the_ID();?></span>
            <h1><?php the_title();?></h1>
            <?php echo get('describe_project');?>
        </div>
        
        <div class="project-logo  col-lg-3 col-md-2 col-sm-3 col-xs-3">
            <?php 
            $logo = get('logo_project');
            if(!empty($logo)) {?>
            <img src="<?php echo get('logo_project');?>" alt="" width="400">
            <?php } else { ?>
            <img src="<?php echo esc_url( get_template_directory_uri() );?>/img/project-logo.jpg" alt="">
            <?php } ?>
        </div>
        <div class="video col-lg-9 col-md-9 col-sm-9 col-xs-9">
               <?php echo getFrameVideo(get('visuals_project_url_video'));?>      
        </div>
        <div class="clearfix"></div>
        
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-column">
            <?php 
             $aditional = get_order_field('visuals_project_aditional_photos');
             $imagen = get('visuals_project_display');
            ?>
            <div id="slider-featured" class="flexslider" style="margin-bottom:30px; margin-top:20px;">
                <ul class="slides">
                <?php if(!empty($imagen)){ ?>
                    <li><img src="<?php echo $imagen;?>" width="450"></li>
                <?php } ?>
                <?php
                    foreach($aditional as $key){ ?>
                    <li><img src="<?php echo get('visuals_project_aditional_photos',1,$key);?>" width="450"></li>
                <?php   } ?>
                </ul>
            </div>
           
        
        </div>
        <div class="clearfix"></div>
        
        <div class="descripcion col-lg-12 col-md-12 col-sm-12 col-xs-12 no-column">
            <div class="txt col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <?php echo get('detail_of_project_detailed');?><br>
            <div class="title-section box fleft">
                <h3>why you should support this project?</h3>
            </div>
                <?php echo get('plan_project_support');?>
            <div class="title-section box fleft">
                <h3>how will money be used?</h3>
            </div>
                 <?php echo get('plan_project_stir_money');?>
            <div class="title-section box fleft">
                <h3>where will the project go in the future?</h3>
            </div>
                 <?php echo get('plan_project_future');?>
            </div>
            <div class="info-sideabr col-lg-3 col-md-3 col-sm-12 col-xs-12 no-column">
                <span class="small">What people are saying?</span>
                <blockquote  class="bg-<?php echo $color;?>"  style="padding:10px; color:#121c26; font-style:italic;">This column will display comments and feedback from your supporters.</blockquote>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            
            <div class="profiles col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="title-section box fleft">
                    <h3>who is behind this project?</h3>
                </div>
                
                <?php
                
                    $teams = get_order_group('team_project_old');
                   
                    foreach($teams as $team){
                    
                ?>
                <div class="item-proj" style="background-image:url(<?php echo get('team_project_profile_picture',$team);?>);">
                    <div class="info bg-<?php echo $color;?>"  <?php if(empty($color)){?> style="background-color:#131C26;"<?php } ?>>
                        <aside class="txt">
                            <p>Team member role <?php echo get('team_project_role',$team);?></p>
                            <span>Age: <?php echo  get('team_project_old',$team);?>, Gender: <?php echo  get('team_project_sex',$team);?></span>
                            <span><?php echo get('team_project_skills',$team);?>; </span>
                        </aside>
                    </div>
                </div>
               <?php } ?>
                <?php
                
                    $teams = get_order_group('other_member_project_role');
                 
                   
                    foreach($teams as $team){
                      $role = get('other_member_project_role',$team);
                      $age  = get('other_member_project_age',$team);
                      if(!empty($role) && !empty($age)){
                ?>
                <div class="item-proj" style="background-image:url(<?php echo get('other_member_project_picture',$team);?>);">
                    <div class="info bg-<?php echo $color;?>"  <?php if(empty($color)){?> style="background-color:#131C26;"<?php } ?>>
                        <aside class="txt">
                            <p>Team member role <?php echo get('other_member_project_role',$team);?></p>
                            <span>Age: <?php echo  get('other_member_project_age',$team);?>, Gender: <?php echo  get('other_member_project_gender',$team);?></span>
                            <span><?php echo get('other_member_project_skills',$team);?>; </span>
                        </aside>
                    </div>
                </div>
                   <?php 
                       } 
                    } ?>
            </div>
            <div class="clearfix"></div>
            <div class="social-network col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <ul>
                    <li><p>Find out more:</p></li>
                    <?php 
                    $find = get_order_field('visuals_project_find_project');
                    foreach($find as $key ){ 
                            $icons = array('facebook','linkedin','twitter','google'
                                ,'youtube','pinterest','soundcloud','instagram','behance');
                            $uri = get('visuals_project_find_project',1,$key);
                            $url = parse_url($uri);
                            $domain = preg_replace('/(www(\.)|\.(\w|\d)*)/','',$url["host"]);
                            if($domain=='google') $domain="google-plus";
                      if(!empty($domain)){
                        ?> 
                    <li>
                        <a href="<?php echo  get('visuals_project_find_project',1,$key);?>">
                            <span class="fa-stack fa-lg">
                              <i class="fa fa-square fa-stack-2x"></i>
                              <i class="fa fa-<?php echo $domain;?> fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>                    
                    </li>
                    <?php   } ?>
                    <?php } ?>
                    
                    
                </ul>
            </div>
        </div>
        <?php $supportcount = support_count(get_the_ID());?>
        <div id="support" class="box fwidth fleft hidden-lg hidden-md hidden-sm">
            <!--button class="support">Support this project</button-->
            <ul>
                <li>Supporters: <?php echo $supportcount;?></li>
                <li>Share:</li>
            </ul>
        </div>
        
    </div>
    <!--/project-->
    
    
    <?php /*?><div id="related-projects" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="title-section box fleft">
            <h3>Similar projects</h3>
        </div>
        <?php
            wp_reset_postdata();
            wp_reset_query();
            $project = new WP_Query(array('post_type'=>'project','post_status'=>'publish','posts_per_page'=>4,'orderby'=>'rand'));
            if($project->have_posts()) while($project->have_posts()): $project->the_post();
            $color = @strtolower(get('choose_a_colour'));
        ?>
        <div class="item-proj" style="background-image:url(<?php echo get('visuals_project_display');?>);">
            <div class="info bg-<?php echo $color;?>"  <?php if(empty($color)){?> style="background-color:#131C26;"<?php } ?>>
                <span>Project <?php the_ID();?></span>
                <h4><a href="<?php echo get_permalink();?>"><?php the_title();?></a></h4>

                <aside class="txt">
                   <?php echo get('describe_project');?>
                    <span><?php echo support_count(get_the_ID());?> Suporters</span>
                    <a href="<?php echo get_permalink();?>" class="boton-gris">view</a>
                </aside>
            </div>
        </div>
      <?php endwhile;?>
        
    </div>
    <!--related projects-->
    
    
    
  </section>
*/ ?>

<?php endif;?>
<?php get_footer(); ?>