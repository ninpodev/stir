<?php get_header(); ?>
<div class="box fwidth fleft supertitulo">
    <div class="container">
        <div class="col-lg-12 no-column">
            <h1>Privacy</h1>
        </div>
    </div>
</div>

<div class="page-wrapper box fleft fwidth">
<section id="page" class="container">
        <div id="terms" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>            
			<div class="clearfix"></div>
            <div id="termsandcond" class="item-term box fwidth fleft">
            	<div class="txt col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php the_content(''); ?>
                </div>
            </div>
            <!--item term-->
            <?php endwhile; ?>
            <?php else : ?>
            <?php endif; ?>
        </div>
</section>
</div>
  

<?php get_footer(); ?>