<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>            
<div class="box fwidth fleft supertitulo">
    <div class="container no-column">
        <div class="col-lg-12 no-column">
            <h1>Recover Password</h1>
        </div>
    </div>
</div>

  <section id="register" class="container login">
  	<div class="col-lg-12">
		<div id="tab-1" class="tab-content current">
            <br><br>
			<?php echo do_shortcode("[pie_register_forgot_password]"); ?>
            <div class="clearfix"></div>
		</div>
	</div>
</section>
<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>




<?php get_footer(); ?>