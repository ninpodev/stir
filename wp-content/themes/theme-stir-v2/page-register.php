<?php $guest = wp_redirect_register();?>
<?php get_header(); ?>
<div class="box fwidth fleft supertitulo">
    <div class="container no-column">
        <div class="col-lg-12 no-column">
        <?php if($guest ==true || $guest =='true'){?>
        <h1>Welcome!</h1>
        <?php }else{?>
        <h1>Create an account to continue</h1>
        <?php } ?>    
        </div>
    </div>
</div>

<div class="clearfix"></div>
<section id="register" class="box fleft fwidth">
    <div class="container">
        <div class="col-lg-6">
            <p class="title-section">
                Login With Facebook
            </p>
            
            <div class="container-fluid slogin">
                
                <!--
                <div class="col-lg-12 slbtn">
					<div id="fb-root"></div> 
                    <a class="btn btn-block btn-social btn-facebook" rel="prefill-fb" href="#">
                        <span class="fa fa-facebook"></span> Prefill with Facebook
                    </a>
                </div>
                -->
                <!--probando pipelines de nuevo-->
                
				<div class="col-lg-12 slbtn text-left">
                    <p>Use this to register to Stir using your Facebook profile.</p>
                    <br>
                    
					<div id="fb-root"></div> 
                    <a class="btn btn-block btn-social btn-facebook text-center" rel="social" data-red="facebook" data-action="login" href="#">
                        <span class="fa fa-facebook"></span> One click access with Facebook
                    </a><br>
                    <p>The information provided from Facebook will never be shared.</p>
                </div>
                <!--<div class="col-lg-6 slbtn">
                    <a class="btn btn-block btn-social btn-twitter">
                        <span class="fa fa-twitter"></span> Sign in with Twitter
                    </a>
                </div>
                <div class="col-lg-6 slbtn">
                    <a class="btn btn-block btn-social btn-google">
                        <span class="fa fa-google"></span> Sign in with Google
                    </a>
                </div>-->
            </div>            
        </div>
        <div class="col-lg-6">
            <p class="title-section">
                Register form    
            </p>
            
            <form id="user-register" name="register">
                <fieldset>
					<div class="form-section section-1 box fleft fwidth">
						<div class="input-field">
							<input id="user_login" name="user_login" type="text" class="validate"  required="" aria-required="true">
							<label for="user_login">User Name</label>
						</div>
						<div class="input-field">
							<input id="first_name" name="first_name" type="text" class="validate"  required="" aria-required="true">
							<label for="first_name">First Name</label>
						</div>
						<div class="input-field">
							<input id="last_name" name="last_name" type="text" class="validate"  required="" aria-required="true">
							<label for="last_name">Last Name</label>
						</div>
						<div class="clearfix"></div>
                        
                        <?php if($guest ==true || $guest =='true'){?>
						<div class="col-lg-6 col-md-6 col-xs-12 pl0">
							<div class="input-field">
								<select class="material validate" name="located"  required="" aria-required="true">
									<option value="" disabled selected>Where are you Located?</option>
									<option value="ACT">ACT</option>
									<option value="NWS">NSW</option>
									<option value="NT">NT</option>
									<option value="QLD">QLD</option>
                                    <option value="SA">SA</option>
									<option value="TAS">TAS</option>
									<option value="VIC">VIC</option>
									<option value="WA">WA</option>
                                    
								</select>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-12 pr0">
							<div class="input-field">
								<input id="birthdate" name="age" type="number"  required="" aria-required="true">
								<label for="birthdate">What's your age?</label>
							</div>
						</div>
						<?php }else{?>
						<?php } ?>
                        
						
						<div class="clearfix"></div>
                        
                        <?php if($guest ==true || $guest =='true'){?>
                        
                        <?/*
                        <div class="summer-box box fleft fwidth">
                        <div class="box fleft fwidth summer-head">
                            <div class="col-lg-6 col-sm-8 col-xs-12">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-summer.png" alt="">
                            </div>
                            <div class="col-lg-6 col-sm-4 col-xs-12">
                                <p>If you are taking part in a Summer of Stir activity, enter your code here:</p>
                            </div>

                        </div>
                            
                        <div class="input-field summer-code box fleft fwidth">
                            <input id="user_code" type="text" class="validate" name="code"  required="" aria-required="true">
                            <label for="user_code">Input your Summer of Stir code here</label>
                        </div>
                        
                        </div>
                        */?>
                        
                        <?php }else{?>
                        <?/*
                        <div class="summer-box box fleft fwidth">
                        <div class="box fleft fwidth summer-head">
                            <div class="col-lg-6 col-sm-8 col-xs-12">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-summer.png" alt="">
                            </div>
                            <div class="col-lg-6 col-sm-4 col-xs-12">
                                <p>If you are taking part in a Summer of Stir activity, enter your code here:</p>
                            </div>

                        </div>
                        <div class="input-field summer-code box fleft fwidth">
                            <input id="user_code" type="text" class="validate" name="code"  required="" aria-required="true">
                            <label for="user_code">Input your Summer of Stir code here</label>
                        </div>
                        </div>
                        
                        */?>
                        <?php } ?>            
                        
                        
                        
						<!--div class="clearfix"></div>
							<div class="card-panel mensaje-bienvenida text-center elegible" style="display:none;">
								<span class="text " > You can now create a project and apply for a Stir Microgrant.<br/>Welcome to Stir!</span>
								
							</div>
							<div class="card-panel mensaje-bienvenida text-center no-elegible" style="display:none;">
								<span class="text no-elegible" >You can now create a project.<br/>Welcome to Stir!</span>
							</div>
						-->
						<div class="clearfix"></div>
						<br><br>
						<div class="input-field">
							<input id="user_email" type="email" class="validate" name="email"  required="" aria-required="true">
							<label for="user_email">Email</label>
						</div>
						<div class="input-field">
							<input id="user_email_confirm" type="email" class="validate" name="email_confirm"  required="" aria-required="true">
							<label for="user_email_confirm">Confirm Email</label>
						</div>
						<br><br>
						<div class="input-field">
							<input id="user_password" type="password" name="pwd" class="validate"  required="" aria-required="true">
							<label for="user_password">Password</label>
						</div>
						<div class="input-field">
							<input id="user_password_confirm" type="password" name="pwd2" class="validate"  required="" aria-required="true">
							<label for="user_password_confirm">Confirm Password</label> 
						</div>
						<br><br>
                        
						<div class="card-panel">
						<?php if(get_bloginfo('url')=='http://jpcontreras.com/stir'){?>
						  <div class="g-recaptcha" data-sitekey="6LcTaSQUAAAAANsFHyZFVxtMLfpxlpIbiR3XVyTA"></div>
						<?php } else{?>
							<div class="g-recaptcha" data-sitekey="6Lf-WxoTAAAAAKGtgWVe8dzs_xLb49HadV751LY_"></div>
						<?php } ?>
						</div>
						<div rel="message" style="display:none;"> </div
						<br><br>
						<?php if($guest ==true || $guest =='true'){?>
						<div class="input-field">
							<input id="guest" type="hidden" name="guest" value="true">
						</div>
						<?php } ?>
					</div>
				
					
					<div class="form-section section-3 box fleft fwidth text-center">
						<p class="ack">BY CONTINUING I HAVE READ AND ACCEPT <a href="<?php echo get_option('siteurl'); ?>/terms-and-conditions" target="_blank">T&Cs</a></p>
						<?php if($guest ==true || $guest =='true'){?>
						<input class="big-submit" type="submit" value="Send Activation Email"  rel="button" data-page="register" data-option="register">
						<?php }else{?>
						<input class="big-submit" type="submit" value="Send Activation Email"  rel="button" data-page="register" data-option="register">
						<?php } ?>
					</div>
					<br><br>
				</fieldset>
            </form>
        </div>
        <div class="clearfix"></div>
            <!--
        <div class="col-lg-6 col-md-6 col-sm-10 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-1">
            
            
            <div class="box fleft fwidth text-center bread">
                <br><br>
                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/ico-process-account.png" alt="Welcome">
            </div>
            
        </div>-->
            
    </div>
</section>
<?php get_footer(); ?>