<?php get_header(); ?>
  <section id="scene-report" class="container-fluid">
      
    <div class="title-page box fleft fwidth text-center">
        <h1>Scene Report</h1>
    </div>
    <div class="clearfix"></div>
      
	<div class="container-offset col-lg-10 col-md-10 col-sm-10 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-12">
      
    <?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>            
    <?php the_content(); ?>
    <?php endwhile; ?>
    <?php else : ?>
    <?php endif; ?>
        
      
      
      </div>
    <!--/project-->
    
  </section>

<?php get_footer(); ?>