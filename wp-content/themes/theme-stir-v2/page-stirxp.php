<?php
/**
 * Template Name: Stir XP
 *
 */
?>
<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>            
<section id="stirxp" class="box fleft fwidth">
  <div class="container">
      <div class="col-lg-12 text-left logo">
          <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/stirxp_logo.svg" alt="">
      </div>
  	<div class="col-lg-12 form text-left">
        <?php the_content(); ?>
	</div>
      <!-- hola-->
</div>
</section>
<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>

<?php get_footer(); ?>