<?php get_header(); ?>    
		<div id="summer-of-stir" class="box fleft fwidth">
            <section id="wave-container">
                <canvas id="waves-middle-bottom" class="wave-canvas" data-paper-resize="true"></canvas>
                <canvas id="waves-middle-top" class="wave-canvas" data-paper-resize="true"></canvas>
                <canvas id="waves-top" class="wave-canvas" data-paper-resize="true"></canvas>
                <canvas id="waves-bottom" class="wave-canvas" data-paper-resize="true"></canvas>
            </section> 
		</div>

<?php get_footer(); ?>