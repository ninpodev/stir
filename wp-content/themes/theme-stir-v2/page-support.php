
<?php get_header(); ?>

<div class="clearfix"></div>
<div id="support-slide" class="flexslider">
    <ul class="slides">
        
        <?php $args=array( //Loop 2
                //'post_type' => array ('news','project'), 
                'post_type' => 'project', 
                'posts_per_page' => 3,
                'orderby' => 'rand',
                'tax_query' => array(
                    array( 
                        'taxonomy' => 'season',
                        'field'    => 'slug',
                        'terms'    => 's04',
                    ),
                ), 
                'orderby' => 'rand',
            );
            $myloop = new WP_Query($args);
            $exclude = [];
            if($myloop->have_posts()) : while($myloop->have_posts()) :
            $myloop->the_post();
                $exclude[] = get_the_ID();
            ?>
        
        
        <li style="background-image:url(<?php echo get('visuals_project_display');?>);">
            
            <div class="container-fluid">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8">
                    <div class="meta">
                        <span class="cate">Recent Project</span>
                    </div>
                    <div class="clearfix"></div>
                    <h1 class="title"><span><?php the_author(); ?> is creating <br> <?php the_title(); ?> </span></h1>
                </div>
                <div class="col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-4">
                    <a class="small-cta fright text-center" href="<?php the_permalink(); ?>">[View Project]</a>
                </div>
            </div>
        </li>
        

            <?php endwhile; endif; ?>
            <?php wp_reset_query(); ?>
    </ul>
</div>
<div class="clearfix"></div>
<?php
        $is_search = false;
        extract($_GET,EXTR_SKIP);
        $search_title = [];
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $arg = array('post_type'=>'project',
            'post_status'=>'publish',
            'posts_per_page'=>18,
            'paged'=>$paged, 
            'orderby'=> 'meta_value_num', //ordenar por votos
            'order'=>'DESC',
            'meta_query' => array(
                'relation' => 'OR', 
                array(
                    'key' => '_count_support', 
                    'compare' => 'NOT EXISTS'
                ), array( //check to see if date has been filled out
                    'key' => '_count_support',
                    'compare' => '>=',
                    'value' => '0'
                )
            )
        ); 
        if( isset($season) ) {
            if( !is_array($season) ) {
                $season = explode(',', $season);    
            
                if( !empty($season)  ) {
                    $is_search = true;
                    $arg['tax_query'][] =  array(
                            'taxonomy' => 'season',
                            'field'    => 'term_id',
                            'terms'    => $season
                        );
                     $terms = get_terms( 'season', array(
                        'parent'=> 0,
                        'hide_empty' => 0,
                        'include' => $season
                     ) );
                     foreach($terms as $term){
                        $search_title[] = $term->name;
                     }
                }
            }
        }
        if(isset($voting)) {
            if(!empty($voting)){
                $is_search = true;
                unset($arg['tax_query']);
                $arg['tax_query'][] =  array(
                    'taxonomy' => 'season',
                    'field'    => 'slug',
                    'terms'    => $voting
                );
                $search_title[] = 'Only open for voting projects';
            }
        }
        if( isset($date) ) {
            if( !is_array($date) ) {
                $date = explode(',', $date);    
                if( !empty($date)  ) {
                     $is_search = true;
                    $arg['date_query']['relation'] = 'OR';
                    foreach($date as $year){
                        $arg['date_query'][] = array('year' => $year);
                        $search_title[] = $year;
                    }
                }
            }
        }
       

        if( !empty($search) ) {
            $is_search = true;
            $arg['s'] = $search;
            $search_title[] = $search;
        }
        set_query_var( 'is_search',$is_search );
        $projects = new WP_Query($arg); 

?>
<div class="container-fluid">
    <div class="col-lg-12 main-title">
        <?php $count_project = wp_count_posts('project'); ?>
        <h1>Explore <span><?php echo $count_project->publish;?> projects on Stir</span></h1>
    </div>
</div>

<div class="clearfix"></div>
<div id="contenedor-support" class="container-fluid">
    <div class="spacer-filtro col-lg-3 col-md-3 col-sm-4 col-xs-12">
    <div class="sidebar-filtro fwidth no-column">
        <div class="filtro-buscar">
            <?php global $post;?>
            <form method="GET" name="filter" action="<?php echo get_permalink($post->ID);?>">
                <div class="form-group">
                    <label class="title">By name</label>
                    <input type="search" class="form-control" placeholder="Enter Name" name="search" value="<?php echo @strip_tags(htmlentities($search));?>">
                </div>
                <div class="form-check">
                    <label class="title">Open for voting</label>
                    <div class="checkbox">
                        <label><input type="checkbox" value="s04" name="voting" <?php if(isset($voting)){?>checked<?php } ?>>Show only open for voting projects</label>
                    </div>
                </div>
                <div class="form-check">
                    <label class="title">Season</label>
                    <?php
                    $terms = get_terms( 'season', array(
                        'parent'=> 0,
                        'hide_empty' => 1
                    ) );
                    foreach ( $terms as $term ) {
                    ?>
                    <div class="checkbox">
                        <label><input name="season[]" type="checkbox" id="test-<?php echo $term->term_id;?>" value="<?php echo $term->term_id;?>" <?php if( @in_array($term->term_id, $season)){?>checked<?php }?> ><?php echo $term->name ;?></label>
                    </div>
                    <!--p>
                    <input name="season[]" type="checkbox" id="test-<?php echo $term->term_id;?>" value="<?php echo $term->term_id;?>" <?php if( @in_array($term->term_id, $season)){?>checked<?php }?> />
                    <label for="test-<?php echo $term->term_id;?>"><?php echo $term->name ;?></label>
                    </p-->
                    <?php } ?>
                    
                </div>

                <div class="form-check">
                    <label class="title">Year</label>
                    <?php for($i=2015; $i<= date('Y'); $i++){?>
                    <div class="checkbox">
                        <label><input type="checkbox" value="<?php echo $i;?>" name="date[]" <?php if( @in_array($i, $date)){?>checked<?php }?>><?php echo $i;?></label>
                    </div>
                    <?php } ?>
                    
                </div>


                <button style="text-transform:uppercase;" type="submit" class="btn btn-primary" data-button="search">[Show results]</button>
            </form>
        </div>
    </div>
    </div>

    
    <div id="support-tiles" class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
        <div class="wrapper box fleft fwidth">
    
        
        <?php if($is_search == false){?>
        <div id="project-tiles" class="container-fluid no-column">
            <h1>New Season 04 projects</h1>
            <?php $args=array( //Loop 2
                //'post_type' => array ('news','project'), 
                'post_type' => 'project', 
                'posts_per_page' => 3,
                'orderby' => 'rand',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'season',
                        'field'    => 'slug',
                        'terms'    => 's04',
                    ),
                ),
                'post_status' => 'publish',
                'post__not_in' => $exclude
            ); 
            $myloop = new WP_Query($args);
           
            if($myloop->have_posts()) : while($myloop->have_posts()) :
            $myloop->the_post();
            ?>
            <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' ); $url = $thumb['0']; ?>

            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 no-column">
                <div class="item box fleft fwidth">
                    <div class="meta">
                        <span class="box fleft">
                            <span class="supporters"><?php echo support_count(get_the_ID());?> Votes</span>
                        </span>
                        <?php $global_post_id = get_the_ID();?>
                        <span class="box fright season">
                        <?php $terms = get_the_terms( $global_post_id, 'season' );?><?php if($terms){ ?><?php $term = array_shift($terms);?><span class="season <?php echo $term->slug;?>"> <?php echo $term->name;?> </span><?php } ?>
                        </span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="photo box fleft fwidth" style="background-image:url(<?php echo get('visuals_project_display');?>);">
                        <?php if( has_term('yes','funded', $global_post_id) ) {?><span class="funded">GRANT RECIPIENT</span><?php } else {?><? } ?>

                        <a href="<?php the_permalink(); ?>">&nbsp;</a>
                    </div>

                    <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                    <div class="clearfix"></div>


                        <div class="excerpt">
                            <?php echo get('describe_project');?>
                            <a href="<?php the_permalink(); ?>">more</a>
                        </div>
                    <!--comentar votos-->
                    <?/* <span class="supporters"><?php echo support_count(get_the_ID());?> Votes</span> */?>
                    <div class="clearfix"></div>

                    <?php if(get_the_tag_list()) { echo get_the_tag_list('<ul class="post-tags-front"><li>','</li><li>','</li></ul>'); } ?>
                </div>
            </div>

            
            <?php endwhile; endif; ?>
            <?php wp_reset_query(); ?>
            
                          
        <div class="clearfix"></div>
        </div>
        <?php } ?>
        <div id="project-tiles" class="container-fluid no-column ">
            <?php if($is_search == true){?>
                <?php if($projects->found_posts != 0){?>
            <h1>THERE ARE <?php echo $projects->found_posts;?> RESULTS FOR: <?php echo implode(', ', $search_title);?></h1>
                <?php } else { ?>
            <h1>NO RESULTS FOUND. TRY ANOTHER TERM OR BROWSE SOME PROJECTS BELOW. </h1>
                    <?php $args=array( //Loop 2
                        //'post_type' => array ('news','project'), 
                        'post_type' => 'project', 
                        'posts_per_page' => 9,
                        'orderby' => 'rand',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'season',
                                'field'    => 'slug',
                                'terms'    => 's04',
                            ),
                        ),
                        'post_status' => 'publish',
                        'post__not_in' => $exclude
                    ); 
                    $myloop = new WP_Query($args);
                   
                    if($myloop->have_posts()) : while($myloop->have_posts()) :
                    $myloop->the_post();
                    ?>
                    <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' ); $url = $thumb['0']; ?>

                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 no-column">
                        <div class="item box fleft fwidth">
                            <div class="meta">
                                <span class="box fleft">
                                    <span class="supporters"><?php echo support_count(get_the_ID());?> Votes</span>
                                </span>
                                <?php $global_post_id = get_the_ID();?>
                                <span class="box fright season">
                                <?php $terms = get_the_terms( $global_post_id, 'season' );?><?php if($terms){ ?><?php $term = array_shift($terms);?><span class="season <?php echo $term->slug;?>"> <?php echo $term->name;?> </span><?php } ?>
                                </span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="photo box fleft fwidth" style="background-image:url(<?php echo get('visuals_project_display');?>);">
                                <?php if( has_term('yes','funded', $global_post_id) ) {?><span class="funded">GRANT RECIPIENT</span><?php } else {?><? } ?>

                                <a href="<?php the_permalink(); ?>">&nbsp;</a>
                            </div>

                            <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                            <div class="clearfix"></div>


                                <div class="excerpt">
                                    <?php echo get('describe_project');?>
                                    <a href="<?php the_permalink(); ?>">more</a>
                                </div>
                            <!--comentar votos-->
                            <?/* <span class="supporters"><?php echo support_count(get_the_ID());?> Votes</span> */?>
                            <div class="clearfix"></div>

                            <?php if(get_the_tag_list()) { echo get_the_tag_list('<ul class="post-tags-front"><li>','</li><li>','</li></ul>'); } ?>
                        </div>
                    </div>

                    
                    <?php endwhile; endif; ?>
                    <?php wp_reset_query(); ?>
                    
                <?php } ?>
            <?php } else { ?>
            <h1>Browse more projects</h1>
            <?php } 
            if($projects->have_posts()) : while($projects->have_posts()) :
                $projects->the_post();
            ?>
            <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' ); $url = $thumb['0']; ?>

            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 no-column">
                <div class="item box fleft fwidth">
                    <div class="meta">
                        <span class="box fleft">
                            <span class="supporters"><?php echo support_count(get_the_ID());?> Votes</span>
                        </span>
                        <span class="box fright season">
                        <?php $terms = get_the_terms( $post->ID, 'season' );?><?php if($terms){ ?><?php $term = array_shift($terms);?><span class="season <?php echo $term->slug;?>"> <?php echo $term->name;?> </span><?php } ?>
                        </span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="photo box fleft fwidth" style="background-image:url(<?php echo get('visuals_project_display');?>);">
                        <?php if( has_term('yes','funded', $post->ID) ) {?><span class="funded">GRANT RECIPIENT</span><?php } else {?><? } ?>

                        <a href="<?php the_permalink(); ?>">&nbsp;</a>
                    </div>

                    <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                    <div class="clearfix"></div>


                        <div class="excerpt">
                            <?php echo get('describe_project');?>
                            <a href="<?php the_permalink(); ?>">more</a>
                        </div>
                    <!--comentar votos-->
                    <?/* <span class="supporters"><?php echo support_count(get_the_ID());?> Votes</span> */?>
                    <div class="clearfix"></div>

                    <?php if(get_the_tag_list()) { echo get_the_tag_list('<ul class="post-tags-front"><li>','</li><li>','</li></ul>'); } ?>
                </div>
            </div>

            
            <?php endwhile; endif; ?>
            <?php wp_reset_query(); ?>
            
                          
        <div class="clearfix"></div>
        </div>
         <?php
        // pager
        if($projects->max_num_pages>1){?>
            <ul class="pagination">
            <?php
            for($i=1;$i<=$projects->max_num_pages;$i++){ ?>
                <li class="<?php echo ($paged==$i)? 'active':'';?>"><a href="<?php echo get_pagenum_link($i);?>"><?php echo $i;?></a></li>
                <?php } ?>
            </ul>
        <?php } ?>
        </div>
    </div>
</div>




<?php get_footer(); ?>