
<?/**/?>
<?php if(isset($_GET['draft'])) publish_to_draft();?>
<?php if(!is_role('project') && !is_role('administrator')) wp_redirect(get_permalink(4));?>
<?php if(!have_draft()) wp_redirect(get_permalink(12)); ?>



<?php get_header(); ?>

<div class="box fwidth fleft supertitulo">
    <div class="container">
        <div class="col-lg-12">
            <h1>Introduce Yourself</h1>
        </div>
    </div>
</div>
<div id="modal-guidance" class="modal fade guidance-module" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div id="guidance" class="container-fluid">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <h3>Guidance Mode</h3>
                <?php the_field('people_intro', '21'); ?>
                <br>
                <h4>Why is this important?</h4>
                <?php the_field('people_important', '21'); ?>
</div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                            <?php the_field('people_final_words', '21'); ?>

                            <div class="toolshed-box box fwidth fleft">
                            <?php 
                            $posts = get_field('people_choose_tool','21');
                            if( $posts ): ?>
                                <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                                    <?php setup_postdata($post); ?>
                                    <div class="toolbox" style="background-color:<?php the_field('color_toolshed'); ?>;">
                                        <a href="<?php the_field('pdf_link_toolshed'); ?>" target="_blank"><?php the_title(); ?></a>
                                    </div>
                                <?php endforeach; ?>
                                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                            <?php endif; ?>
                            </div>
</div>                
        </div>
    </div>
  </div>
</div>



<div id="container-create" class="box fwidth fleft">
    
  <section id="create" class="container">

    <div class="form-create col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
    	<form id="pctform" name="team">
        	<?php $project = get_project();?>
            
            
            
            <div class="modulo box fwidth fleft team_1">
                <div class="container-fluid">
                    <div class="col-lg-3 col-md-3 col-xs-12 no-column">
                        <div class="container-fluid">
                            <div class="col-lg-12">
                                <h3>What is your age? <span>*</span></h3>
                                <div class="input-field">
                                    <input type="number" rel="requerido" maxlength="2" min="1" max="99" length="2"  name="team_project_old"  oninput="maxLengthCheck(this)" value="<?php echo $project['team_project_old'];?>">
                                    <label for="team_project_old">Only Numbers</label>
                                </div>
                        
                            </div>
                        </div>    
                    </div>
                    <div class="col-lg-5 col-md-5 col-xs-12 no-column location">
                        <div class="container-fluid">
                            <div class="col-lg-12">
                                <h3>Where are you Located? <span>*</span></h3>
                                <div class="input-field">
                                   
                                    <select class="fa-select material" name="team_project_location" rel="requerido">
                                        <option value="" disabled selected>Choose location</option>
                                        <option value="ACT" <?php if($project['team_project_location']=='ACT') echo "selected";?>>ACT</option>
                                        <option value="NWS" <?php if($project['team_project_location']=='NWS') echo "selected";?>>NSW</option>
                                        <option value="NT" <?php if($project['team_project_location']=='NT') echo "selected";?>>NT</option>
                                        <option value="QLD" <?php if($project['team_project_location']=='QLD') echo "selected";?>>QLD</option>
                                        <option value="SA" <?php if($project['team_project_location']=='SA') echo "selected";?>>SA</option>
                                        <option value="TAS" <?php if($project['team_project_location']=='TAS') echo "selected";?>>TAS</option>
                                        <option value="VIC" <?php if($project['team_project_location']=='VIC') echo "selected";?>>VIC</option>
                                        <option value="WA"  <?php if($project['team_project_location']=='WA') echo "selected";?>>WA</option>
                            
                                    </select> 
                                </div>
                            </div>
                        </div>    
                    </div>
                    
                    <div class="col-lg-4 col-md-4 col-xs-12 no-column">
                        <div class="container-fluid">
                            <div class="col-lg-12 check-caja">
                                <h3 style="width:100%;">Which gender do you feel best represents you? <span>*</span></h3>
                                    <p>
                                        <input type="radio" name="team_project_sex" value="Female" id="female-team" <?php if($project['team_project_sex']=="Female") echo "checked";?>>
                                        <label for="female-team">Female</label>
                                    </p>
                                    <p>
                                        <input type="radio" name="team_project_sex" value="Male" id="male-team" <?php if($project['team_project_sex']=="Male") echo "checked";?>>
                                        <label for="male-team">Male</label>
                                    </p>
                                    <p>
                                        <input type="radio" name="team_project_sex" value="X" id="x-team" <?php if($project['team_project_sex']=="X") echo "checked";?>>
                                        <label for="x-team">X</label>
                                    </p>
                                    <p>
                                        <input type="radio" name="team_project_sex" value="NotSay" id="not-team" <?php if($project['team_project_sex']=="NotSay") echo "checked";?>>
                                        <label for="not-team">I would rather not say</label>
                                    </p>
                        
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
            <!--/modulo-->

            <div class="modulo box fwidth fleft">
                <div class="container-fluid">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <h3>What is your role in this project?</h3>
                        <div class="input-field">
                            <input type="text" maxlength="20" name="team_project_role" value="<?php echo $project['team_project_role'];?>" >
                            <label for="name_project">Name your role</label>
                        </div>
                        <div class="aso-tool box fleft fwidth text-left">
                            <p><i class="fa fa-wrench"></i> If you need guidance, use the <a href="<?php echo get_option('siteurl'); ?>/wp-content/uploads/2015/03/stirtools_people_lvl1.pdf" target="_blank">Personal profile tool</a></p>
                        </div>
                    
                    </div>
                    <div class="text-side col-lg-3 col-md-3 col-sm-3 col-xs-12 text-right">
                        <?php the_field('people_role', '21'); ?>
                    </div>
                </div>
            </div>            
            
            <div class="modulo box fwidth fleft">
                <div class="container-fluid">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <h3 style="width:60%;">What are the strengths that you bring to the project?</h3>
                        <div class="input-field">
                            <textarea type="text" name="team_project_skills" class="materialize-textarea" length="100" maxlength="100"><?php echo reverse_wpautop($project['team_project_skills']);?></textarea>
                            <label for="team_project_skills">max. 100 characters</label>
                        </div>
                    <?php /*<input type="text" placeholder="max. 20 characters" maxlength="20" value="<?php echo $project['post_title'];?>" name="post_title" > */?>
                    </div>
                    <div class="text-side col-lg-3 col-md-3 col-sm-3 col-xs-12 text-right">
                        <?php the_field('people_skills', '21'); ?>
                    </div>
                </div>
            </div>            


            <div class="modulo box fwidth fleft">
                <div class="container-fluid">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <h3>Add your profile picture</h3>
                        
                        <div class="file-field input-field">
                          <div class="btn">
                            <span>File</span>
                            <input type="file" name="team_project_profile_picture" name="team_project_profile_picture" <?php if(empty($project['team_project_profile_picture'])):?> <?php endif;?>>
                          </div>
                          <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                          </div>
                        </div>                        
                        <div class="cache" rel="team_project_profile_picture">
                            <?php if(!empty($project['team_project_profile_picture'])){?>
                            <img src="<?php echo $project['team_project_profile_picture'];?>" width="200">
                            <br/><br>
                            <?php } ?>
                        </div>
                        <div class="aso-tool box fleft fwidth text-left">
                            <p><i class="fa fa-wrench"></i> Check out the <a href="<?php echo get_option('siteurl'); ?>/wp-content/uploads/2015/03/stirtools_people_lvl2.pdf" target="_blank">Profile Pic Tool</a> for tips</p>
                        </div>
                    </div>
                    <div class="text-side col-lg-3 col-md-3 col-sm-3 col-xs-12 text-right">
                        <?php the_field('people_profile_picture', '21'); ?>
                    </div>
                </div>
            </div>  


            <div class="modulo box fwidth fleft">
                <div class="container-fluid">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <h3>Where can people find out more about you?</h3>
                        <div class="input-field">
                            <input type="text" name="team_project_find_people" value="<?php echo get('team_project_find_people');?>" >
                            <label for="name_project">Add a URL where we can know more about you. Start with "http://"</label>
                        </div>
                    </div>
                    <div class="text-side col-lg-3 col-md-3 col-sm-3 col-xs-12 text-right">
                        <?php the_field('people_about_you', '21'); ?>
                    </div>
                </div>
            </div>
            <!--/modulo-->

            
            <!-- NUEVA -->
            <div class="modulo box fwidth fleft">
                <div class="container-fluid">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <h3>Are you looking for any collaborators?</h3>
                        <div class="input-field">
                            <input type="text" name="team_project_team_collaborators" value="<?php echo $project['team_project_team_collaborators'];?>">
                            <label for="team_collaborators">What do you need? If you don't please leave empty</label>
                        </div>
                    </div>
                    <div class="text-side col-lg-3 col-md-3 col-sm-3 col-xs-12 text-right">
                        <?php the_field('people_collaborators', '21'); ?>
                    </div>
                </div>
            </div>
            <!--/modulo-->

            <!-- NUEVA -->
            <div class="modulo box fwidth fleft">
                <div class="container-fluid">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <h3 style="width:100%;">If you are looking for collaborators, what is the best email for them to contact you?</h3>
                        <div class="input-field">
                            <input type="email" name="team_project_team_email" class="validate" value="<?php echo $project['team_project_team_email'];?>" >
                            <label for="team_email">Write a valid email</label>
                        </div>
                    </div>
                    <div class="text-side col-lg-3 col-md-3 col-sm-3 col-xs-12 text-right">
                        <?php the_field('people_your_email', '21'); ?>
                    </div>
                </div>
            </div>
            <!--/modulo-->

            
            <!--nuevo: es un input-file para subir una foto del carnet, no se manifiesta en el single-->
            <?php /*
            <div class="modulo box fwidth fleft">
                <div class="container-fluid">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <h3>Please provide an image of your Proof of I.D</h3>
                        
                        <div class="file-field input-field">
                          <div class="btn">
                            <span>File</span>
                            <input type="file" name="team_project_proof_id" <?php if(empty($project['team_project_proof_id'])) { ?> <?php }?>>
                          </div>
                          <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                          </div>
                        </div>                        
                        <div class="cache" rel="team_project_proof_id">
                            <?php if(!empty($project['team_project_proof_id'])){?>
                            <img src="<?php echo $project['team_project_proof_id'];?>" width="200">
                            <br/><br>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="text-side col-lg-3 col-md-3 col-sm-3 col-xs-12 text-right">
                        <?php the_field('people_proof_id', '21'); ?>
                    </div>
                </div>
            </div>  
            */ ?> 
            
            <div class="modulo box fwidth fleft">
                <div class="container-fluid">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Add team members</h3>
                        
                        <section class="box fleft fwidth">
                        <a class="btn btn-md btn-primary atm" data-duplicate-add="teammember">Add a Team Member</a>
                        
                        <div class="clearfix"></div>
						
						<?php 
							if(is_array($project['members'])){
								$contador =0;
								foreach($project['members'] as $key =>$member){
								
								
						?>
						<div class="col-md-4 col-sm-4 col-xs-12 card-team-member" data-duplicate="teammember" data-duplicate-min="0">
							<a class="btn btn-sm btn-danger" data-duplicate-remove="teammember"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
							<div class="card">
								<div class="card-content">
									
									<div class="form-group profile">
										<label>Profile Picture</label>
										<input type="file" style="margin-bottom:40px;" name="other_member_project_picture[]"  >
										<?php if(!empty($member['other_member_project_picture'])){?>
										<div class="cache" rel="other_member_project_picture">
											<img src="<?php echo $member['other_member_project_picture'];?>" width="200">
										</div>
										<input type="hidden" name="file[other_member_project_picture][]" value="<?php echo basename( $member['other_member_project_picture']);?>">
										<?php }else {?>
										<input type="hidden" name="file[other_member_project_picture][]" value="">
										<div class="cache" rel="other_member_project_picture"></div>
										<?php } ?>
									</div>
									
									<div class="form-group">
										<label>Role</label>
										<input type="text" class="form-control" placeholder="Role of the team member" name="other_member_project_role[]" value="<?php echo $member['other_member_project_role'];?>">
									</div>
									<div class="form-group">    
										<label>Age</label>
										<input type="number" class="form-control" placeholder="#" maxlength="2" min="1" max="99" length="2" oninput="maxLengthCheck(this)" name="other_member_project_age[]" value="<?php echo $member['other_member_project_age'];?>">
									</div>

									<div class="form-group">
										<label>Gender</label>
										<select name="other_member_project_gender[]">
											<option value="Female" <?php if ($member['other_member_project_gender']=='Female')echo "selected";?>>Female</option>
											<option value="Male"  <?php if ($member['other_member_project_gender']=='Male')echo "selected";?>>Male</option>
											<option value="X"  <?php if ($member['other_member_project_gender']=='X')echo "selected";?>>X</option>
											<option value="NotSay"  <?php if ($member['other_member_project_gender']=='NotSay')echo "selected";?>>I would rather not say</option>
										</select>
									</div>
									<div class="form-group">
										<label>Skills [Separated by enter]</label>
										<div class="clearfix"></div>
										<input type="text" class="form-control" data-role="tagsinput" rel="tagsinput" name="other_member_project_skills[]" value="<?php echo $member['other_member_project_skills'];?>">
										<div class="clearfix"></div>
									</div>

								</div>
							</div>
						</div>
							<?php }
							} else { ?>
						<div class="col-md-4 col-sm-4 col-xs-12 card-team-member" data-duplicate="teammember" data-duplicate-min="0">
							<a class="btn btn-sm btn-danger" data-duplicate-remove="teammember"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
						
							<div class="card">
								<div class="card-content">
									
									<div class="form-group profile">
										<label>Profile Picture</label>
										<input type="file" style="margin-bottom:40px;" name="other_member_project_picture[]">
										<input type="hidden" name="file[other_member_project_picture][]" value="">
										<div class="cache" rel="other_member_project_picture"></div>
									</div>
									
									<div class="form-group">
										<label>Role</label>
										<input type="text" class="form-control" name="other_member_project_role[]" placeholder="Role of the team member">
									</div>
									<div class="form-group">    
										<label>Age</label>
										<input type="number" class="form-control" placeholder="#" maxlength="2" min="1" max="99" length="2" oninput="maxLengthCheck(this)" name="other_member_project_age[]">
									</div>

									<div class="form-group">
										<label>Gender</label>
										<select name="other_member_project_gender[]">
											<option value="Female" >Female</option>
											<option value="Male">Male</option>
											<option value="X" >X</option>
											<option value="NotSay">I would rather not say</option>
										</select>
									</div>
									<div class="form-group">
										<label>Skills [Separated by enter]</label>
										<div class="clearfix"></div>
										<input type="text" class="form-control" data-role="tagsinput"  name="other_member_project_skills[]">
										<div class="clearfix"></div>
									</div>

								</div>
							</div>
						</div>
						<?php } ?>
						
                        </section>
                        
                        <div class="clearfix"></div>
                        
                        <?/*
                        <div class="team-members box fleft fwidth <?php if($project['member_project_other_people']=="Yes") echo " visible";?>" >
               
                            

                            
                            <div class="clearfix"></div>
                            
                            
                            
                        
                
                        <div class="col-lg-6">
                            <button type="button" id="insert-more" class="button tiny radius agrega">Add another</button>
                        </div>
                        <div class="col-lg-6">
                            <button type="button" id="remove-more" class="button tiny radius quita">Remove last</button>
                        </div>
                        </div>
                        
                    </div>
                    
                    <div class="text-side col-lg-3 col-md-3 col-sm-3 col-xs-12 text-right">
                        &nbsp;
                    </div>
                    */?>
                </div>
            </div>
            <!--/modulo-->
        

            
            
        </form>    
    </div>
  </section>

<?php include 'save-bar.php' ?>
    
    
</div>  

<?php get_footer(); ?>