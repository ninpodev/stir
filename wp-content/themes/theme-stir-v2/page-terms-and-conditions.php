<?php get_header(); ?>
<div class="box fwidth fleft supertitulo">
    <div class="container">
        <div class="col-lg-12">
            <h1>Terms and Conditions</h1>
        </div>
    </div>
</div>

<div class="page-wrapper box fleft fwidth">
<section id="page" class="container">
    <div id="terms" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-column">
			<?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>            
            
            <div class="item-term box fwidth fleft">
            	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <?php the_field('introduction_text'); ?>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div id="slider-toolshed" class="flexslider">
                        <ul class="slides">
                        	<?php if( have_rows('slider_introduction') ): while ( have_rows('slider_introduction') ) : the_row(); ?>
                            <li><img src="<?php the_sub_field('slider_introduction_image'); ?>" alt=""></li>
                            <?php endwhile; else : endif; ?>  
                        </ul>
                    </div>
                </div>
            </div>
            <!--item term-->
            
			<div class="clearfix"></div>
            <div class="item-term box fwidth fleft">
            	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <?php the_field('accounts_text'); ?>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <?php the_field('accounts_key_points_text'); ?>
                </div>
            </div>
            <!--item term-->



			<div class="clearfix"></div>
            <div class="item-term box fwidth fleft">
            	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <?php the_field('projects_and_eligibility_text'); ?>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <?php the_field('projects_and_eligibility_key_points'); ?>
                </div>
            </div>
            <!--item term-->


			<div class="clearfix"></div>
            <div class="item-term box fwidth fleft">
            	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <?php the_field('voting_text'); ?>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <?php the_field('voting_key_points'); ?>
                </div>
            </div>
            <!--item term-->

			<div class="clearfix"></div>
            <div class="item-term box fwidth fleft">
            	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <?php the_field('receiving_a_grant_text'); ?>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <?php the_field('receiving_a_grant_key_points'); ?>
                </div>
            </div>
            <!--item term-->

			
            <div class="clearfix"></div>
            <div class="item-term box fwidth fleft">
            	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <?php the_field('the_spirit_of_stir_text'); ?>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <?php the_field('the_spirit_of_stir_key_points'); ?>
                </div>
            </div>
            <!--item term-->


            <div class="clearfix"></div>
            <div class="item-term box fwidth fleft">
            	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <?php the_field('intellectual_property_text'); ?>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <?php the_field('intellectual_property_key_points'); ?>
                </div>
            </div>
            <!--item term-->

            <div class="clearfix"></div>
            <div class="item-term box fwidth fleft">
            	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <?php the_field('additional_terms_text'); ?>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <?php the_field('additional_terms_key_points'); ?>
                </div>
            </div>
            <!--item term-->

            
            
            
            <?php endwhile; ?>
            <?php else : ?>
            <?php endif; ?>
            
            
            
            
			<?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>            
            <?php the_content(); ?>
            <?php endwhile; ?>
            <?php else : ?>
            <?php endif; ?>
            
        </div>
    <!--/desc box-->
</section>
</div>
  

<?php get_footer(); ?>