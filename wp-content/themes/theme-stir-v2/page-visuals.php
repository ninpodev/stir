<?php if(isset($_GET['draft'])) publish_to_draft();?>
<?php if(!is_role('project') && !is_role('administrator')) wp_redirect(get_permalink(4));?>
<?php if(!have_draft()) wp_redirect(get_permalink(12)); ?>
<?php get_header(); ?>

<div class="box fwidth fleft supertitulo">
    <div class="container">
        <div class="col-lg-12">
            <h1>Add Some Visuals</h1>
        </div>
    </div>
</div>

<div id="modal-guidance" class="modal fade guidance-module" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

        <div id="guidance" class="container-fluid">
            
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <h3>Guidance Mode</h3>
                        <?php the_field('visuals_intro', '19'); ?>
                        <br>
                        <h4>Why is this important?</h4>
                        <?php the_field('visuals_important', '19'); ?>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <?php the_field('visuals_finals_words', '19'); ?>
                        <div class="toolshed-box box fwidth fleft">
                            <?php 
                            $posts = get_field('visuals_choose_tool','19');
                            if( $posts ): ?>
                                <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                                    <?php setup_postdata($post); ?>
                                    <div class="toolbox" style="background-color:<?php the_field('color_toolshed'); ?>;">
                                        <a href="<?php the_field('pdf_link_toolshed'); ?>" target="_blank"><?php the_title(); ?></a>
                                    </div>
                                <?php endforeach; ?>
                                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                            <?php endif; ?>
                        </div>
                    </div>
            
        </div>
        
    </div>
  </div>
</div>



<div id="container-create" class="box fwidth fleft">
  <section id="create" class="container">
      
    <div class="form-create col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
    	<form id="pctform" name="visuals">
        	<?php $project = get_project();?>
            
            
            <div class="modulo box fwidth fleft">
                <div class="container-fluid">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <h3>Does your project have a logo?</h3>
                        <div class="input-field col s6">
                            
                        <div class="file-field input-field">
                          <div class="btn">
                            <span>Upload a Logo</span>
                            <input type="file" value="uplaod a logo" name="logo" >
                          </div>
                          <div class="file-path-wrapper">
                            <input class="file-path" type="text">
                          </div>
                        </div>
                        <div class="clear"></div>
                        
                        </div>
                        <div id="logo-dinamico" class="box fleft fwidth">
                            <?php if(!empty($project['logo'])){?>
                            <div class="cache-display">
                                <img src="<?php echo $project['logo'];?>" width="200">  
                                <input type="hidden" value="<?php echo basename($project['logo']);?>"  name="file[logo]"> 
                            </div>
                            <?php }else{
                            ?>
                            <div class="cache-display"></div>
                            <?php                    
                            } ?>
                            
                        </div>
                        <div class="aso-tool box fleft fwidth text-left">
                            <p><i class="fa fa-wrench"></i> Use the <a href="<?php echo get_option('siteurl'); ?>/wp-content/uploads/2015/03/stirtools_basics_lvl3.pdf" target="_blank">Logo Specs tool</a> for some tips.</p>
                        </div>
                    </div>
                    <div class="text-side col-lg-3 col-md-3 col-sm-3 col-xs-12 text-right">
                        <?php the_field('basics_logo', '14'); ?>
                    </div>
                </div>
            </div>
            <!--modulo-->

            
            
            
            <div class="modulo box fwidth fleft">
                <div class="container-fluid">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <h3>Would you like to add a video?</h3>
                        <p>
                            <input type="radio" name="video" value="videono" id="video_0" checked="checked">
                            <label for="video_0">No</label>
                        </p>
                        <p>
                            <input type="radio" name="video" value="videoyes" id="video_1" <?php if(!empty($project['visuals_project_url_video'])) echo "checked";?>>
                            <label for="video_1">Yes</label>
                        </p>
                        
                        <?php /*
                        <div class="aso-tool box fleft fwidth text-left">
                            <p><i class="fa fa-wrench"></i> Use the <a>Video Storytelling Tool</a> to help you get people really excited about your project.</p>
                        </div>
                        */?>

                        
                        <div id="video-si" class="box fwidth fleft" <?php if(!empty($project['visuals_project_url_video'])) echo "style='display:block;'";?>>
                            
                            <div class="input-field col s6">
                                <input type="text" name="visuals_project_url_video" value="<?php echo $project['visuals_project_url_video'];?>">
                                <label for="visuals_project_url_video">Video URL</label>
                            </div>                            
                        </div>
            
                    </div>
                    <div class="text-side col-lg-3 col-md-3 col-sm-3 col-xs-12 text-right">
                        <p>Upload your video to a streaming service such as Youtube and paste the URL here. The video will take the place of your display photo.</p>
                    </div>
                </div>
            </div>
            <!--/modulo-->
            
            
            
            <div id="find-info" class="modulo box fwidth fleft">
                <div class="container-fluid">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <h3>Can people find more info on the project?</h3>
                        
                        <table style="width:100%;" id="mytable-links">
                            <tbody>
                        <?php   if(is_array($project['visuals_project_find_project'])){
                                foreach($project['visuals_project_find_project'] as $key =>$value){
                                $uri =$project['visuals_project_find_project'][$key];
                                $url = parse_url($uri);
                                $domain = preg_replace('/(www(\.)|\.(\w|\d)*)/','',$url["host"]);                
                                if ( $domain !== "linkedin" && $domain != "twitter" && $domain != "google" 
                                   && $domain != "youtube" && $domain != "pinterest" && $domain != "soundcloud"
                                  && $domain != "instagram" && $domain != "behance" && $domain != "wordpress" 
                                  && $domain != "facebook" ){
                                    $domain  = "other";
                                } 
                        ?>
                    <tr <?php if($key!=0) echo "class='nueva-link'";?>>
                        <td>
                            <div class="input-field">
                            <input type="text" name="visuals_project_find_project[]" value="<?php echo $project['visuals_project_find_project'][$key];?>">
                                <label for="visuals_project_find_project">Add Link (Include HTTP://)</label>
                            </div>
                        </td>
                        <td>
                            <div class="input-field">
                         <select class="fa-select material">
                                <option <?php if($domain=='facebook') echo 'selected';?>>Facebook</option>
                                <option <?php if($domain=='linkedin') echo 'selected';?>>Linked-In</option>
                                <option <?php if($domain=='twitter') echo 'selected';?>>Twitter</option>
                                <option <?php if($domain=='google') echo 'selected';?>>Google+</option>
                                <option <?php if($domain=='youtube') echo 'selected';?>>YouTube</option>
                                <option <?php if($domain=='pinterest') echo 'selected';?>>Pinterest</option>
                                <option <?php if($domain=='souncloud') echo 'selected';?>>Souncloud</option>
                                <option <?php if($domain=='instagram') echo 'selected';?>>Instagram</option>
                                <option <?php if($domain=='behance') echo 'selected';?>>Behance</option>
                                <option <?php if($domain=='kickstarter') echo 'selected';?>>Kickstarter</option>
                                <option <?php if($domain=='other') echo 'selected';?>>Other</option>
                            </select> 
                            </div>
						</td>
                    </tr>
                    <?php   } 
                        }else{
                    ?>
                        <tr>
                            <td>
                                <div class="input-field">
                                <input type="text" name="visuals_project_find_project[]" >
                                    <label for="visuals_project_find_project">Add Link</label>
                                </div>
                            </td>
                            <td>
                                <div class="input-field">
                                    <select class="fa-select material">
                                        <option>Facebook</option>
                                        <option>Linked-In</option>
                                        <option>Twitter</option>
                                        <option>Google+</option>
                                        <option>YouTube</option>
                                        <option>Pinterest</option>
                                        <option>Souncloud</option>
                                        <option>Instagram</option>
                                        <option>Behance</option>
                                        <option>Kickstarter</option>
                                        <option>Other</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    <?php       
                        }
                    ?>
                </tbody>
                        </table>
                        <div class="clearfix"></div><br>
                        <div class="col-lg-2 col-xs-6 pl0">
                            <button type="button" id="remove-more-links" class="waves-effect waves-light btn quita">Remove last</button>
                        </div>
                        <div class="col-lg-2 col-xs-6">
                            <button type="button" id="insert-more-links" class="waves-effect waves-light btn agrega">Add another</button>
                        </div>                        
                    </div>
                    <div class="text-side col-lg-3 col-md-3 col-sm-3 col-xs-12 text-right">
                        <?php the_field('visuals_project_info', '19'); ?>
                    </div>
                </div>
            </div>    
                        
            
            
        </form>    
    </div>
  </section>

    <?php include 'save-bar.php' ?>
    
    
    
</div>
<?php get_footer(); ?>