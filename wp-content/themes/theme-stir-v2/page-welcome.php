

<?php get_header(); ?>

<? /*<?php wp_redirect_register();?> */?>

<section id="welcome" class="box fwidth">    
    <div class="intro box fleft fwidth">
        <div class="table">
            <div class="table-cell">
                <div class="container">
                    <div class="col-lg-10 col-lg-offset-1 text-center">
                        <h1>Welcome To Stir</h1>
                        <p>You are about to begin creating a project. The whole process should take between 20 to 30 minutes. Each step will provide you with tools along the way.</p>
                        <p class="sub">Here you will find a quick overview of the process. <br><a href="#get-started">Or you can skip ahead to get started.</a></p>
                    </div>
                </div> 
            </div>
        </div>
        
    </div>
    <div class="clearfix"></div>
    <div class="indicators box fleft fwidth">
        <div class="table">
            <div class="table-cell">
                <div class="container">
                    <div class="col-lg-10 col-lg-offset-1">
                        <div class="item box fleft fwidth">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 text-right img">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/ico-tool-welcome.svg" alt="">
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
                                <p>These will open a pop-out with some exercises to help you with part of your project.</p>
                            </div>
                        </div>
                        <div class="item box fleft fwidth img">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4 text-right img">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/ico-guidance-welcome.svg" alt="">
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-8">
                                <p>This will open up a window with tips 
and additional advice.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <div class="clearfix"></div>
    <div class="stages box fleft fwidth">
        <div class="table">
            <div class="table-cell">
                <div class="container">
                    <div class="col-lg-10 col-lg-offset-1">
                        <p class="title">The project creation process contains five stages.</p>
                        <div class="item box fleft fwidth">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 text-right img">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/ico-thebasics-welcome.png" alt="">
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9">
                                <h3>The Basics</h3>
                                <p>Start with an overview that people can share easily.</p>
                            </div>
                        </div>
                        <div class="item box fleft fwidth">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 text-right img">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/ico-thedetails-welcome.png" alt="">
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9">
                                <h3>The Details</h3>
                                <p>Explain the project and it's current state.</p>
                            </div>
                        </div>
                        <div class="item box fleft fwidth">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 text-right img">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/ico-thevisuals-welcome.png" alt="">
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9">
                                <h3>The Visuals</h3>
                                <p>Provide photos and images that show off the project.</p>
                            </div>
                        </div>
                        <div class="item box fleft fwidth">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 text-right img">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/ico-theteam-welcome.png" alt="">
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9">
                                <h3>The Team</h3>
                                <p>Introduce the people and skills behind the project.</p>
                            </div>
                        </div>
                        <div class="item box fleft fwidth">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 text-right img">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/ico-theplan-welcome.png" alt="">
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9">
                                <h3>The Plan</h3>
                                <p>Imagine where the project could go in the future.</p>
                            </div>
                        </div>
                        <div class="item box fleft fwidth">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 text-right img">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/ico-hit-welcome.png" alt="">
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9">
                                <p class="section-title">Then, hit the publish button to make it live.</p>
                                <p>You can always come back to edit or improve your project, so <span>don’t worry if it isn’t perfect.</span></p>
                            </div>
                        </div>
                        <div class="item box fleft fwidth">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 text-right img">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/ico-share-welcome.png" alt="">
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9">
                                <p>Share it as much as you can to get comments and votes.</p>
                            </div>
                        </div>
                        <div class="item item-last* box fleft fwidth">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 text-right img">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/ico-chest-welcome.png" alt="">
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-9">
                                <p><span>On the 23th of June</span>, the ten projects with the most votes will each receive a $1000 grant!</p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="pre-cta box fleft fwidth text-center">
                    <p class="subtitle">If you want to know more, <a href="#">check the FAQ.</a></p>
                    <h1>Ready?</h1>
                </div>
            </div>
        </div>
    </div>

    
    
    <div class="clearfix"></div>    
    <div id="get-started" class="col-lg-10 col-lg-offset-1 col-xs-12 text-center">

        <?/*
        <?php if ( is_user_logged_in() ) { ?>
        
        
      <?php   $current_user = wp_get_current_user();?>
            <p>Hello <?php echo $current_user->user_login;?></p>
        
            <?php if(have_draft()){?>
            //si tiene un draft no publicado o en edición

        <?php } else if(user_publishproject()){ ?>
        //si tiene un projecto publicado
        
            <?php  } else{?>
            //si no tiene nada, para crear proyecto

        <?php ; } else{?>
        //si no está logueado    
        
            
        
        <? } ?>     
        
        */?>
        
        <form class="box fleft fwidth text-center" name="create">
            <div class="title">
                <h2>Get Started</h2>
            </div>
            
            <div class="input-field">      
                <input type="text" id="inputDatabaseName" required name="post_title">
                <span class="bar"></span>
                <label>What's the name of your project? you can change this later</label>
                <div class="clearfix"></div>
                
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pl0">
                    <div class="tool-intro box fleft fwidth">
                        <span class="subinput tool-link box fleft text-left animated slideInDown"><i class="fa fa-wrench"></i>&nbsp;<a href="http://causeastir.com.au/wp-content/uploads/2015/03/stirtools_basics_lvl1.pdf" target="_blank">project naming tool</a></span>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr0">
                    <span class="subinput available box fright " rel="message"  style="display:none;">
                        <i class="fa fa-check-circle" ></i> 
                        That’s a great name
                    </span>
                    <span class="subinput not-available box fright"  style="display:none;">
                        <i class="fa fa-close"></i> 
                        Sorry, that name is already taken
                    </span>
                </div>
            </div>
            <div class="clearfix"></div>
            
            
            
            <div class="title-section box fleft fwidth">
                <p>Choose the area that best represents your project</p>
            </div>
            <div id="checkboxesarea" class="container-fluid">

                <div class="col-xs-12 form-inline">
                    
                    <div class="clearfix"></div>
                    <?php $terms = get_terms( 'area', array( 'hide_empty' => false) );?>
                    <?php foreach($terms as $term){?>
                    <div class="form-group">
                        <input type="checkbox" name="area[]" value="<?php echo $term->term_id;?>" id="<?php echo $term->slug;?>" autocomplete="off" />
                        <div class="btn-group">
                            <label for="<?php echo $term->slug;?>" class="btn btn-default">
                                <span class="glyphicon glyphicon-ok"></span>
                                <span> </span>
                            </label>
                            <label for="<?php echo $term->slug;?>" class="label-kind">
                                <?php echo $term->name;?>
                            </label>
                        </div>
                    </div>
                    <?php } ?>
                    
                </div>
            </div>
            
            
            
            <div class="box fleft fwidth mensajes">
                
                <!--<div class="alert" role="alert">
                    <p>Lorem Ipsum dolor sit amet bla bla bla</p>
                </div>--> 
                
            </div>
            <div class="box fleft fwidth text-center">
                <input class="big-submit" type="button" rel="button" data-page="create" data-option=""   value="[sign up to continue]">
            </div>
            
        </form>
        
    </div>
    
</section>


    
<?php get_footer(); ?> 