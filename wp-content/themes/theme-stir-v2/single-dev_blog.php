<?php get_header(); ?>

    <section id="blog" class="container-fluid no-column">
    <?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?> 
        
      <article class="all-content container-fluid">
        
          
        <div class="modulo-texto centered-title col-lg-10 col-lg-offset-1 text left">
            <br><br><br><br><br>
            <h1><?php the_title(); ?></h1>
            <span class="date">
                <?php the_time('d'); ?>.<?php the_time('m'); ?>.<?php the_time('Y'); ?>
            </span>
              <div class="clearfix"></div>
<?php if ( get_field( 'authorblog' ) ): ?>
<span class="autor">Words:<?php the_field('authorblog', $post->ID) ?></span>
<?php else: // field_name returned false ?>
<span class="autor">Words:<?php the_author_link(); ?></span>
<?php endif; // end of if field_name logic ?>
        </div>
        <!--modulo centered title-->
          <div class="clearfix"></div>
          <div class="modulo-texto paragraph col-lg-10 col-lg-offset-1 text-left">
              <br><br>
                <?php the_content(); ?>
            </div>
        
        </article>
        <?php endwhile; ?>
        <?php else : ?>
        <?php endif; ?> 

    </section>
    <div class="clearfix"></div>

            <div id="content-tiles" class="container-fluid">
            <div class="col-lg-2 no-column">
                <h3>From <br>the blog</h3>
            </div>
            <div class="col-lg-10 filter text-left">
                <p>Filter Topics</p>
                <ul>
                    <?
                    // your taxonomy name
                    $tax = 'type_of_content';
                    // get the terms of taxonomy
                    $terms = get_terms( $tax, [
                    'hide_empty' => true, // do not hide empty terms
                    ]);
                    // loop through all terms
                    foreach( $terms as $term ) {
                    // if no entries attached to the term
                    if( 0 == $term->count )
                    // display only the term name
                    echo '<li>' . $term->name . '</li>';
                    // if term has more than 0 entries
                    elseif( $term->count > 0 )
                    // display link to the term archive
                    echo '<li><a href="'. get_term_link( $term ) .'">'. $term->name .'</a></li>';
                    }?>
                </ul>
            </div>
            <div class="clearfix"></div>
                
                <?php $args=array( //Loop 2
                            //'post_type' => array ('news','project'), 
                            'post_type' => 'news', 
                            'posts_per_page' => 4,
                            'taxonomy' => array ('journal','podcast'/*'tools-skill','stir-story'*/),
                            //'term' => 'yes',
                            //'orderby' => 'rand',
                        );
                        $myloop = new WP_Query($args);
                        if($myloop->have_posts()) : while($myloop->have_posts()) :
                        $myloop->the_post();
                    ?>
                <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' ); $url = $thumb['0']; ?>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 no-column">
                    
                    
                    <div class="item box fleft fwidth">
                        <div class="meta">
                            <span class="box fleft">
                                <?php   // Get terms for post
                                $terms = get_the_terms( $post->ID , 'type_of_content' );
                                // Loop over each item since it's an array
                                if ( $terms != null ){
                                foreach( $terms as $term ) {
                                // Print the name method from $term which is an OBJECT
                                print $term->slug ;
                                // Get rid of the other data stored in the object, since it's not needed
                                unset($term);
                                } } ?>
                            </span>
                            <span class="box fright">
                                <?php the_time('d'); ?>.<?php the_time('m'); ?>.<?php the_time('Y'); ?>
                            </span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="photo box fleft fwidth" style="background-image:url(<?=$url?>);">
                            <a href="<?php the_permalink(); ?>">&nbsp;</a>
                        </div>
                        
                        <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                        <div class="clearfix"></div>
                        <p class="excerpt"><?php echo excerpt(17); ?><a href="<?php the_permalink(); ?>">more</a></p>
                        <div class="clearfix"></div>
                        
                        <?php
if(get_the_tag_list()) {
    echo get_the_tag_list('<ul class="post-tags"><li>','</li><li>','</li></ul>');
}
?>        
                    </div>
                    
                     <?php /*
                     
                    <?php  if ( has_term( 'podcast', 'type_of_content' )  ) { ?>
                    <?php } elseif ( has_term( 'journal', 'type_of_content' ) ) { ?>
                    <?php } elseif ( has_term( 'stir-story', 'type_of_content' ) ) { ?>
                    <?php } elseif ( has_term( 'skill-share', 'type_of_content' ) ) { ?>
                    <?php } elseif ( 'project' == get_post_type() ) { ?>
                    <?php } else {?>
                    <?php } ?>
                    
                    
                  <? */?>
                    
                    
                </div>

                <?php endwhile; endif; ?>
                <?php wp_reset_query(); ?>
                
                
            </div>
            <div class="clearfix"></div>
            
            <div id="project-tiles" class="container">
                <div class="co-lg-12 col-md-12 col-sm-12 filter">
                    <h3>Browse Creative Projects</h3>
                </div>
                
                <?php $args=array( //Loop 2
                    'post_type' => 'project', 
                    'posts_per_page' => 4,
                    'orderby' => 'rand',
                );
                $myloop = new WP_Query($args);
                if($myloop->have_posts()) : while($myloop->have_posts()) :
                $myloop->the_post();
                ?>
                <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' ); $url = $thumb['0']; ?>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 no-column">
                    <div class="item box fleft fwidth">
                        <div class="meta">
                            <span class="box fleft">
                                &nbsp;
                            </span>
                            <span class="box fright season">
                                
                            <?php $terms = get_the_terms( $global_post_id, 'season' );?><?php if($terms){ ?><?php $term = array_shift($terms);?><span class="season <?php echo $term->slug;?>"> <?php echo $term->name;?></span> <?php } ?>
                                
                            </span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="photo box fleft fwidth" style="background-image:url(<?php echo get('visuals_project_display');?>);">
                            <?php if( has_term('yes','funded', $global_post_id) ) {?><span class="funded">GRANT RECIPIENT</span><?php } else {?><? } ?>
                            
                            <a href="<?php the_permalink(); ?>">&nbsp;</a>
                        </div>
                        
                        <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                        <div class="clearfix"></div>
                        
                        
                            <div class="excerpt">
                                <?php echo get('describe_project');?>
                                <a href="<?php the_permalink(); ?>">more</a>
                            </div>
                        <span class="supporters"><?php echo support_count(get_the_ID());?> supporters</span>
                        <div class="clearfix"></div>
                        
                        <?php if(get_the_tag_list()) { echo get_the_tag_list('<ul class="post-tags"><li>','</li><li>','</li></ul>'); } ?>
                    </div>
                </div>

                <?php endwhile; endif; ?>
                <?php wp_reset_query(); ?>
                
                <div class="box fleft fwidth caja-boton text-center">
                    <a href="<?php echo get_option('siteurl'); ?>/support" class="small-cta">[more projects]</a>
                </div>
                
            </div>            
            


<?php get_footer(); ?>