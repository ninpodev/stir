<?php get_header(); ?>

<?php
	wp_reset_query();
	wp_reset_postdata();
	global $post;
	$global_post_id = $post->ID;
?>

<?php /*
<?php wp_reset_query();
        wp_reset_postdata();
?>

<!--abre loop-->
<?php if(have_posts()): the_post();
      $color = @strtolower(get('choose_a_colour'));
?> 
<?php endif;?>
<!--/-->

<!--ID del proyecto OBSOLETO-->
<?php echo get_the_ID();?>

<!--Titulo del proyecto-->
<?php the_title();?>

<!--Excerpt del proyecto-->
<?php echo get('describe_project');?>
<?php echo get('describe');?>

    
<!--Logo del proyecto OBSOLETO-->    
<?php 
$logo = get('logo_project');
if(!empty($logo)) {?>
<img src="<?php echo get('logo_project');?>" alt="" width="400">
<?php } else { ?>
<img src="<?php echo esc_url( get_template_directory_uri() );?>/img/project-logo.jpg" alt="">
<?php } ?>   


<!--imágenes del proyecto-->
<?php 
 $aditional = get_order_field('visuals_project_aditional_photos');
 $imagen = get('visuals_project_display');
?>
<div id="slider-featured" class="flexslider" style="margin-bottom:30px; margin-top:20px;">
    <ul class="slides">
    <?php if(!empty($imagen)){ ?>
        <li><img src="<?php echo $imagen;?>" width="450"></li>
    <?php } ?>
    <?php
        foreach($aditional as $key){ ?>
        <li><img src="<?php echo get('visuals_project_aditional_photos',1,$key);?>" width="450"></li>
    <?php   } ?>
    </ul>
</div>

<!--video del proyecto-->
<div class="video col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <?php echo getFrameVideo(get('visuals_project_url_video'));?> 		        
</div>

<!--descripción detallada del proyecto-->
<?php echo get('detail_of_project_detailed');?>           

<!--why you should support this project?-->
<?php echo get('plan_project_support');?>

<!--how will money be used?-->
<?php echo get('plan_project_stir_money');?>

<!--where will the project go in the future?-->
<?php echo get('plan_project_future');?>
            

<!--who is behind this project?-->
<?php  $teams = get_order_group('team_project_old'); foreach($teams as $team){ ?>
<div class="item-proj" style="background-image:url(<?php echo get('team_project_profile_picture',$team);?>);">

    <div class="info bg-<?php echo $color;?>"  <?php if(empty($color)){?> style="background-color:#ef4136;"<?php } ?>>
        <aside class="txt">
            <p><?php echo get('team_project_role',$team);?></p>
            <span>Age: <?php echo  get('team_project_old',$team);?>, Gender: <?php echo  get('team_project_sex',$team);?></span>
            <span><?php echo get('team_project_skills',$team);?>; </span>
        </aside>
    </div>
</div>
<?php } ?>

<!--otros miembros-->
<?php
$teams = get_order_group('other_member_project_role');
foreach($teams as $team){
    $role = get('other_member_project_role',$team);
    $age  = get('other_member_project_age',$team);
     if(!empty($role) && !empty($age)){
?>
<div class="item-proj" style="background-image:url(<?php echo get('other_member_project_picture',$team);?>);">
<div class="info bg-<?php echo $color;?>"  <?php if(empty($color)){?> style="background-color:#ef4136;"<?php } ?>>
    <aside class="txt">
        <p><?php echo get('other_member_project_role',$team);?></p>
        <span>Age: <?php echo  get('other_member_project_age',$team);?>, Gender: <?php echo  get('other_member_project_gender',$team);?></span>
        <span><?php echo get('other_member_project_skills',$team);?>; </span>
    </aside>
</div>
</div>
<?php } } ?>

                
<!--redes sociales-->
<?php 
$find = get_order_field('visuals_project_find_project');
foreach($find as $key ){ 
        $icons = array('facebook','linkedin','twitter','google'
            ,'youtube','pinterest','soundcloud','instagram','behance');
        $uri = get('visuals_project_find_project',1,$key);
        $url = parse_url($uri);
        $domain = preg_replace('/(www(\.)|\.(\w|\d)*)/','',$url["host"]);
        if($domain=='google') $domain="google-plus";
  if(!empty($domain)){
    ?> 
<li>
    <a href="<?php echo  get('visuals_project_find_project',1,$key);?>">
        <span class="fa-stack fa-lg">
          <i class="fa fa-square fa-stack-2x"></i>
          <i class="fa fa-<?php echo $domain;?> fa-stack-1x fa-inverse"></i>
        </span>
    </a>                    
</li>
<?php   } ?>
<?php } ?>

<!--support count-->
<?php $supportcount = support_count(get_the_ID());?>
<ul>
    <li>Supporters: <?php echo $supportcount;?></li>
    <li>Share:</li>
</ul>

    <!--Similar projects-->
<?php
    wp_reset_postdata();
    wp_reset_query();
    $project = new WP_Query(array('post_type'=>'project','post_status'=>'publish','posts_per_page'=>3,'orderby'=>'rand'));
    if($project->have_posts()) while($project->have_posts()): $project->the_post();
    $color = @strtolower(get('choose_a_colour'));

?>
<div class="item-proj" style="background-image:url(<?php echo get('visuals_project_display');?>);">
<div class="info bg-<?php echo $color;?>"  <?php if(empty($color)){?> style="background-color:#ef4136;"<?php } ?>>
    <span>Project <?php the_ID();?></span>
    <h4><a href="<?php echo get_permalink();?>"><?php the_title();?></a></h4>

    <aside class="txt">
       <?php echo get('describe_project');?>
        <span><?php echo support_count(get_the_ID());?> Supporters</span>
        <a href="<?php echo get_permalink();?>" class="boton-gris">view</a>
    </aside>
</div>
</div>
<?php endwhile;?>

*/
?>

<?php wp_reset_query();  wp_reset_postdata(); ?>

<!--abre loop-->
<?php if(have_posts()): the_post();
      $color = @strtolower(get('choose_a_colour'));
?> 
<?php endif;?>



<div id="modal-survey" class="modal fade survey-module" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div id="survey" class="box fleft fwidth">
                
            <div class="content box fleft fwidth">
                <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                    <h3>support this project</h3>
                </div>
                <div class="clearfix"></div>
                <div class="form col-lg-12 text-center">
                    <form name="support">
                    <div class="row">
                 <?php global $post;?>
                <input type="hidden" name="post_id" value="<?php echo $post->ID;?>" >
                </div>

                    
                    <div class="box fleft fwidth comentario text-center">
                        <p>Would you like to leave a comment? (optional)</p>
                        <textarea type="text" name="comment" class="materialize-textarea" length="100" maxlength="100" placeholder="Share why you decided to support this project. Your enthusiasm may cause others to support it!"></textarea>
                        <label for="comment">max. 100 characters</label>
                    </div>


                <?php if ( is_user_logged_in() ) { ?>

                <? } else { ?>
                        <div class="clearfix"></div>
                <div id="register-user-survey" class="container-fluid">
                    <div class="col-lg-6 text-left">
                        <p class="title-section">
                            Register using an username and email
                        </p>
                        <p>You will be requested to confirm your email to have your vote counted.</p><br>
                        <div class="row">
                            <div class="input-field col-lg-12">
                                <input placeholder="Choose a Username" id="user_name" type="text" class="validate" rel="requerido" name="user_login">
                            </div>
                            <div class="input-field col-lg-12">
                                <input id="user_email" type="email" placeholder="A Valid Email" class="validate" rel="requerido"  name="email">
                            </div>
                        </div>
                        <input type="submit" class="cta-stir" value="[Vote for this project]" rel="button" data-page="registersupport"  data-option="support" data-block="yes">
                    </div>
                    <div class="col-lg-6 text-left">
                        <p class="title-section">
                            One-Click vote and register with Facebook
                        </p>
                        <p>After logging in your vote will be added</p><br>
                        <div class="col-lg-12 slbtn">
                            <div id="fb-root"></div> 
                            <a class="btn btn-block btn-social btn-facebook" rel="social" data-red="facebook" data-action="support" href="#">
                            <span class="fa fa-facebook"></span> Register and Vote with Facebook
                            </a>
                        </div>  
                    </div>
                    
                    
                    
                </div> 
                                              
                        
                <? } ?>



                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <?php if ( is_user_logged_in() ) { ?>

                <input type="submit" class="cta-stir" value="[Vote for this project]" rel="button" data-page="support" data-action="" data-block="yes">
                <? } else { ?>
                
                
                <? } ?>
                </div>
                        
                <br/>
                <div class="clearfix"></div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mensaje-suuport text-center" rel="message"> 
                </div> 
                <div class="clearfix"></div><br><br>


                </form>
                </div>
            </div>
                
        </div>
    </div>
  </div>
</div>


<section id="single" class="box fleft fwidth">
<div id="skrollr-body">
    <div id="contenedor-slide" class="contenedor-slide box fwidth fleft bggif">
        <?php 
         $aditional = get_order_field('visuals_project_aditional_photos');
         $imagen = get('visuals_project_display');
        ?>        
        <div id="slide-single" class="flexslider">
            <ul class="slides">
                <?php if(!empty($imagen)){ ?>
                <li style="background-image:url(<?php echo $imagen;?>);">&nbsp;</li>            
                <?php } ?>
                <?php
                foreach($aditional as $key){ ?>
                <li style="background-image:url(<?php echo get('visuals_project_aditional_photos',1,$key);?>);">&nbsp;</li>
                <?php   } ?>
            </ul>
        </div>
        
        
    </div>
    
    <div class="clearfix"></div>
    <div id="excerpt" class="container">
        
        <!--si el logo existe-->
        
        <?php $logo = get('logo_project'); if(!empty($logo)) {?>
        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 logo-project">        
            <img src="<?php echo get('logo_project');?>" alt="">
        </div>
        <?php } else { ?>
        &nbsp;
        <?php } ?>   

        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">
            <div class="row">
                <div class="col-lg-12">
                    <h1><?php the_title();?></h1>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <?php echo get('describe_project');?> <?php echo get('describe');?>
<? /*
                    <?php
$timestamp = time();
$date_time = date("d-m-Y (D) H:i:s", $timestamp);
echo "Current date and local time on this server is $date_time";
?>
*/?>
                    
                </div>
            </div>
        </div>
        
        
    </div>
    
    <div id="share-support" class="box fwidth fleft">
        <div class="container">
            <div class="col col-lg-4 col-md-4 hidden-sm hidden-xs pl0">
                <div class="table">
                    <div class="table-cell">
                        <ul class="list-group">
                            <li class="first"><a class="list-group-item" href="#"><span class="hidden-sm hidden-xs">Share this</span></a></li>
                            <li><?php echo do_shortcode('[ssbp]'); ?></li>
                        </ul>
                    </div>
                </div>
                
            </div>
            

        
            <?php if(  has_term('s04','season', $global_post_id)) {?>
                <?php
                // The current date
                $date = time();
                // Static contest start date
                $contestStart = strtotime('2018-04-16 09:00:00');
                //$contestclose = strtotime('2018-01-06 00:00:01');
                // If current date is after contest start
                if ($date > $contestStart) { ?>
                <div class="col col-lg-5 col-md-5 col-sm-7 col-xs-6 text-center">
                    <div class="table">
                        <div class="table-cell">
                            <?php if ( is_user_logged_in() ) { ?>
                                <?php if( user_login_support_this_project($global_post_id) == false) {?>
                                    <button data-toggle="modal" data-target=".survey-module" data-keyboard="false" class="cta-stir" name="support" >Vote for this <span class="hidden-xs">Project</span></button>
                                <?php } else { ?>
                            <div class="col-lg-9 text-left" style="padding-top:2px;"> <small>You have already voted for this project, continue to help it by commenting. Look for this icon </small>
                            </div> 
                                    <div class="col-lg-3 text-center">
                                        
                                        <p style="margin-top:10px;"><img src="<?php echo esc_url( get_template_directory_uri() );?>/img/ico-abre-comentarios.png" alt=""></p>
                            </div> 
                            
                                <?php } ?>
                            <? } else { ?>
                                <button data-toggle="modal" data-target=".survey-module" data-keyboard="false" class="cta-stir" name="support" >Vote for this <span class="hidden-xs">Project</span></button>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <div class="col col-lg-3 col-md-3 col-sm-5 col-xs-6 text-center">
                <div class="table">
                    <div class="table-cell">
                        <ul class="list-group">
                            <li class="first">
                                <p class="season">
                                    <!--This is a-->
								<?php $terms = get_the_terms( $global_post_id, 'season' );?><?php if($terms){ ?><?php $term = array_shift($terms);?><span class="season <?php echo $term->slug;?>"> <?php echo $term->name;?> Project</span> <?php } ?> with <?php echo support_count(get_the_ID());?> Votes</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
                <? } else { ?>
                <div class="col col-lg-6 col-md-6 col-sm-7 col-xs-12 text-center">
                    <div class="table">
                        <div class="table-cell">
                            <ul class="list-group">
                                <li class="first">
                                    <p class="season"><span class="season summer-of-stir"> Vote for this <a href="<?php bloginfo('siteurl'); ?>/summer" target="_blank" title="Read more about Summer of Stir">Season 04</a> project</span> from 09:00AM 16TH April, 2018.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <? } ?>

             

            <?php }else {?>
            <div class="col col-lg-3 col-md-3 col-sm-5 col-xs-12 text-center">
                <div class="table">
                    <div class="table-cell">
                        <ul class="list-group">
                            <li class="first">
                                <p class="season">
                                    <!--This is a-->
								<?php $terms = get_the_terms( $global_post_id, 'season' );?><?php if($terms){ ?><?php $term = array_shift($terms);?><span class="season <?php echo $term->slug;?>"> <?php echo $term->name;?> Project</span> <?php } ?> with <?php echo support_count(get_the_ID());?> Supporters</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <?php } ?>            
        </div>
    </div>
    
    <!--share-support-->
    <div class="clearfix"></div>
	
    <!--contenido del proyecto-->
    <div class="container">
        <?php 
			$comment_type = 'project_support';

			if( is_user_logged_in() ) {	
				global $post;
				$post_id = $post->ID;
				$author_post = get_post_field( 'post_author', $post_id );
				$current_user = wp_get_current_user();
				
				if($current_user->ID == $author_post){
					$coments = get_comments( array( 'meta_query' => array(
						array( 'key' => '_type_', 'value'=> $comment_type,'compare'=>'='),
						array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
					),'order'=>'ASC', 'parent'=>0, 'post_id' => get_the_ID()) );
					$reminder = comment_reminder($coments);
					if(isset(end($coments)->comment_ID)){
						$coments_child = get_comments(array('parent'=> $coments->comment_ID,'order'=>'ASC' ));
					}
				} else{ 
					
					$coments = get_comments(array( 'meta_query' => array(
						array( 'key' => '_type_', 'value'=> $comment_type,'compare'=>'='),
						array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
					),'parent'=>0,'order'=>'ASC', 'post_id' => get_the_ID(),'author__in'=> array($current_user->ID)));
					
					$reminder = comment_reminder($coments);	
					if(isset(end($coments)->comment_ID)){
						$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
					}

				}
				
			}
		?>
        
        
        
        
        <div class="modulo-contenido box fwidth fleft detail-project">
            <div class="txt col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <?php echo get('detail_of_project_detailed');?>
                <div class="clearfix"></div>
                <?php echo get('detail_of_project_for_description');?>
            </div>
        </div>
        
        <div class="modulo-contenido box fwidth fleft">
            <div class="video col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2">
                <?php echo getFrameVideo(get('visuals_project_url_video'));?>
            </div>
        </div>
        
        
        
        <div class="modulo-contenido box fwidth fleft">
            <div class="txt col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<?php if( is_user_logged_in() ){?>
                <?php if ( ( count($coments) != 0 && $current_user->ID == $author_post )   ) { ?>
					<span class="icos-comentarios <?php if($reminder == "_true_"){ ?>comentario-con-respuesta<?php }else{ ?>sin-comentarios agregar-nuevo-comentario<?php } ?>" title="Add new comment">&nbsp;</span>
                <?php } else if($current_user->ID != $author_post && count($coments)!=0){ ?>
					<span class="icos-comentarios <?php if(isset($coments_child) && $reminder == "_true_"){ ?>comentario-con-respuesta<?php }else{ ?>comentario-sin-respuesta<?php } ?> " title="Add new comment">&nbsp;</span>
				<?php } else if($current_user->ID != $author_post && count($coments)==0) { ?>
					<span class="icos-comentarios agregar-nuevo-comentario" title="Add new comment">&nbsp;</span>
				<?php } ?>
			<?php } ?>
                <h3>Why you should support <br><?php the_title(); ?>?</h3>
                <?php echo get('plan_project_support');?>
                <ul class="fa-ul edit">
                    <!--<li><i class="fa-li fa fa-pencil"></i><span>Edit</span></li>-->
                </ul>
            </div>
            <div class="comentarios col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <!--cuando no hay comentarios-->
                <div id="comentario-para-moderar" class="modulo-comentarios fwidth fleft">
                    <span class="cerrar-modulo text-center"><i class="fa fa-times fa-lg"></i></span>
                <?php if(is_user_logged_in()){?>	    
                   	<?php if ( ( count($coments) != 0 && $current_user->ID == $author_post )   ) { ?>
                   		<?php if( $reminder == "_true_" ){ ?>
                   		<h6>There is a reply to your comment</h6>
                   		<?php } else{?>
                   		<h6>Review and reply your comment</h6>
                   		<?php } ?>
                   	<?php } else if($current_user->ID != $author_post && count($coments)!=0){ ?>
                   	 <?php if( $reminder == "_true_" ){ ?>
                   		<h6>There is a reply to your comment</h6>
                   	 <?php } else{ ?>
                   	 	<h6>No reply to your comment</h6>
                   	<?php  } ?>
                   	<?php } else if($current_user->ID != $author_post && count($coments)==0) { ?>
                    	<h6>Help the author by leaving a comment or question</h6>
                    <?php } ?>
						
					
						<?php if($current_user->ID != $author_post){?>
						<div class="contenedor-comentarios fleft fwidth">
							<!-- user visit -->
								
							<ul class="lista-comentarios hay-respuesta">
							<?php foreach($coments as $coment){ ?>
								<li>
									<?php echo get_avatar( $coment->user_id, 32 ); ?>
									<span class="nombre"><?php echo $coment->comment_author;?> Says: </span>
                                    <div class="clearfix"></div>
									<span class="comentario"><?php echo $coment->comment_content;?></span>
									<div class="clearfix"></div>
									<span class="fecha"> <?php echo $coment->comment_date;?></span>
								</li>
								<?php 
									$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
									foreach($coments_child as $child){	
								?>
								<li>
									<?php echo get_avatar( $child->user_id, 32 ); ?>
									<span class="nombre"><?php echo $child->comment_author;?> Says: </span>
                                    <div class="clearfix"></div>
									<span class="comentario"><?php echo $child->comment_content;?></span>
									<div class="clearfix"></div>
									<span class="fecha"> <?php echo $child->comment_date;?></span>
								</li>
								<?php } ?>
							<?php } ?>
							</ul>
							
							<?php if (count($coments)!=0 && count($coments_child) != 0 && !get_resolve($coment->comment_ID)){?>
							<form class="para-respuesta" name="project_support">
								<div class="input-field">
									<input type="hidden" name="id_project" value="<?php the_ID();?>">
									<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
									<textarea id="textarea2" class="materialize-textarea" name="comment"></textarea>
									<label for="textarea2">Reply</label>
								</div>
								<button class="btn waves-effect waves-light fright" type="submit" rel="comment" data-option="comment_project_reply">Reply <i class="fa fa-comments" aria-hidden="true"></i></button>
							</form>
							<?php } ?>
							
							<?php if(count($coments)==0 && !get_resolve($coment->comment_ID)){?>
							<form class="para-respuesta" name="project_support" >	
								<div class="input-field">
									<textarea id="textarea1" name="comment" class="materialize-textarea"></textarea>
									<input type="hidden" name="id_project" value="<?php the_ID();?>">
									<label for="textarea1">Leave your comment here</label>
								</div>
								
								<button class="btn waves-effect waves-light fleft" type="reset" name="action">Reset</button>
								<button class="btn waves-effect waves-light fright" type="submit" rel="comment"  data-option="comment_project">Submit <i class="fa fa-comments-o" aria-hidden="true"></i></button>
							</form>	
							<?php } ?>
						</div>
						<?php } ?>
					
						<!-- author del proyecto -->
						<?php if($current_user->ID == $author_post){ ?>
							<?php $coments = get_comments( array( 'meta_query' => array(
									array( 'key' => '_type_', 'value'=>'project_support','compare'=>'='),
									array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
									),'order'=>'ASC', 'parent'=>0, 'post_id' => get_the_ID()) );?>
							
							<?php foreach($coments as $coment){ ?>
							
							<div class="contenedor-comentarios fleft fwidth">
								<ul class="lista-comentarios hay-respuesta">
									<li>
										<?php echo get_avatar( $coment->user_id, 32 ); ?>
										<span class="nombre"><?php echo $coment->comment_author;?> Says: </span>
                                        <div class="clearfix"></div>
										<span class="comentario"><?php echo $coment->comment_content;?></span>
										<div class="clearfix"></div>
										<span class="fecha"> <?php echo $coment->comment_date;?></span>
									</li>
									<?php 
										$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
										foreach($coments_child as $child){	
									?>
									<li>
										<?php echo get_avatar( $child->user_id, 32 ); ?>
										<span class="nombre"><?php echo $child->comment_author;?> Says: </span>
                                        <div class="clearfix"></div>
										<span class="comentario"><?php echo $child->comment_content;?></span>
										<div class="clearfix"></div>
										<span class="fecha"> <?php echo $child->comment_date;?></span>
									</li>
									<?php } ?>
								</ul>	
								
								<?php $resolved = get_resolve($coment->comment_ID);
									if($resolved != true){?>
								<form class="para-respuesta" name="project_support">
									<div class="input-field">
										<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
										<input type="hidden" name="id_project" value="<?php the_ID();?>">
										<textarea class="materialize-textarea" name="comment"></textarea>
										<label>Reply</label>
									</div>
									
									<button class="btn waves-effect waves-light fleft resolver" type="reset"  rel="comment" data-option="comment_resolve">Resolve this thread <i class="fa fa-check" aria-hidden="true"></i></button>
									<button class="btn waves-effect waves-light fright" type="submit"   rel="comment" data-option="comment_project_reply">Reply <i class="fa fa-comments" aria-hidden="true"></i></button>
								</form>
								<?php } ?>
								<div class="helpful box fwidth fleft text-center" <?php if($resolved != true){?>style="display:none"<?php } ?>>
									<p>Was this person helpful for your project?</p>
									<div class="clearfix"></div>
									<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
									<button class="btn waves-effect waves-light helpful-button" rel="comment"  data-option="comment_helpful">Helpful</button>
									<button class="btn waves-effect waves-light not-helpful-button" rel="comment" data-option="comment_not_helpful">Not Helpful</button>
								</div>
							
							</div>
							<?php } ?>
						<!-- owner project -->
						<?php } ?>
				<?php } ?>	
                </div>
                <!--cuando no hay comentarios-->
            </div>
        </div>
        
        
        
        <?php if ( is_old_post(160) ) { ?>
        <?php } else { ?>
        
        <?php
			$comment_type = 'project_current_status';

			if( is_user_logged_in() ) {	
				global $post;
				$post_id = $post->ID;
				$author_post = get_post_field( 'post_author', $post_id );
				$current_user = wp_get_current_user();
				
				if($current_user->ID == $author_post){
					$coments = get_comments( array( 'meta_query' => array(
						array( 'key' => '_type_', 'value'=> $comment_type,'compare'=>'='),
						array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
					),'order'=>'ASC', 'parent'=>0, 'post_id' => get_the_ID()) );
					$reminder = comment_reminder($coments);
					if(isset(end($coments)->comment_ID)){
						$coments_child = get_comments(array('parent'=> $coments->comment_ID,'order'=>'ASC' ));
					}
				} else{ 
					
					$coments = get_comments(array( 'meta_query' => array(
						array( 'key' => '_type_', 'value'=> $comment_type,'compare'=>'='),
						array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
					),'parent'=>0,'order'=>'ASC', 'post_id' => get_the_ID(),'author__in'=> array($current_user->ID)));
					
					$reminder = comment_reminder($coments);	
					if(isset(end($coments)->comment_ID)){
						$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
					}

				}
				
			}
		?>
        <div class="modulo-contenido box fwidth fleft">
            <div class="txt col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <?php if( is_user_logged_in() ){?>
                <?php if ( ( count($coments) != 0 && $current_user->ID == $author_post )   ) { ?>
					<span class="icos-comentarios <?php if($reminder == "_true_"){ ?>comentario-con-respuesta<?php }else{ ?>sin-comentarios agregar-nuevo-comentario<?php } ?>" title="Add new comment">&nbsp;</span>
                <?php } else if($current_user->ID != $author_post && count($coments)!=0){ ?>
					<span class="icos-comentarios <?php if(isset($coments_child) && $reminder == "_true_"){ ?>comentario-con-respuesta<?php }else{ ?>comentario-sin-respuesta<?php } ?> " title="Add new comment">&nbsp;</span>
				<?php } else if($current_user->ID != $author_post && count($coments)==0) { ?>
					<span class="icos-comentarios agregar-nuevo-comentario" title="Add new comment">&nbsp;</span>
				<?php } ?>
			<?php } ?>
                <h3>What is the project’s current state?</h3>
                <?php echo get('plan_project_current_status');?>
                
                <ul class="fa-ul edit">
                    <!--<li><i class="fa-li fa fa-pencil"></i><span>Edit</span></li>-->
                </ul>
            </div>
            <div class="comentarios col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <!--cuando no hay comentarios-->
                <div id="comentario-para-moderar" class="modulo-comentarios fwidth fleft">
                    <span class="cerrar-modulo text-center"><i class="fa fa-times fa-lg"></i></span>
                <?php if(is_user_logged_in()){?>	    
                   	<?php if ( ( count($coments) != 0 && $current_user->ID == $author_post )   ) { ?>
                   		<?php if( $reminder == "_true_" ){ ?>
                   		<h6>There is a reply to your comment</h6>
                   		<?php } else{?>
                   		<h6>Review and reply your comment</h6>
                   		<?php } ?>
                   	<?php } else if($current_user->ID != $author_post && count($coments)!=0){ ?>
                   	 <?php if( $reminder == "_true_" ){ ?>
                   		<h6>There is a reply to your comment</h6>
                   	 <?php } else{ ?>
                   	 	<h6>No reply to your comment</h6>
                   	<?php  } ?>
                   	<?php } else if($current_user->ID != $author_post && count($coments)==0) { ?>
                    	<h6>Help the author by leaving a comment or question</h6>
                    <?php } ?>
						
					
						<?php if($current_user->ID != $author_post){?>
						<div class="contenedor-comentarios fleft fwidth">
							<!-- user visit -->
								
							<ul class="lista-comentarios hay-respuesta">
							<?php foreach($coments as $coment){ ?>
								<li>
									<?php echo get_avatar( $coment->user_id, 32 ); ?>
									<span class="nombre"><?php echo $coment->comment_author;?> Says: </span>
                                    <div class="clearfix"></div>
									<span class="comentario"><?php echo $coment->comment_content;?></span>
									<div class="clearfix"></div>
									<span class="fecha"> <?php echo $coment->comment_date;?></span>
								</li>
								<?php 
									$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
									foreach($coments_child as $child){	
								?>
								<li>
									<?php echo get_avatar( $child->user_id, 32 ); ?>
									<span class="nombre"><?php echo $child->comment_author;?> Says: </span>
                                    <div class="clearfix"></div>
									<span class="comentario"><?php echo $child->comment_content;?></span>
									<div class="clearfix"></div>
									<span class="fecha"> <?php echo $child->comment_date;?></span>
								</li>
								<?php } ?>
							<?php } ?>
							</ul>
							
							<?php if (count($coments)!=0 && count($coments_child) != 0 && !get_resolve($coment->comment_ID)){?>
							<form class="para-respuesta" name="<?php echo $comment_type;?>">
								<div class="input-field">
									<input type="hidden" name="id_project" value="<?php the_ID();?>">
									<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
									<textarea id="textarea2" class="materialize-textarea" name="comment"></textarea>
									<label for="textarea2">Reply</label>
								</div>
								<button class="btn waves-effect waves-light fright" type="submit" rel="comment" data-option="comment_project_reply">Reply <i class="fa fa-comments" aria-hidden="true"></i></button>
							</form>
							<?php } ?>
							
							<?php if(count($coments)==0 && !get_resolve($coment->comment_ID)){?>
							<form class="para-respuesta" name="<?php echo $comment_type;?>" >	
								<div class="input-field">
									<textarea id="textarea1" name="comment" class="materialize-textarea"></textarea>
									<input type="hidden" name="id_project" value="<?php the_ID();?>">
									<label for="textarea1">Leave your comment here</label>
								</div>
								
								<button class="btn waves-effect waves-light fleft" type="reset" name="action">Reset</button>
								<button class="btn waves-effect waves-light fright" type="submit" rel="comment"  data-option="comment_project">Submit <i class="fa fa-comments-o" aria-hidden="true"></i></button>
							</form>	
							<?php } ?>
						</div>
						<?php } ?>
					
						<!-- author del proyecto -->
						<?php if($current_user->ID == $author_post){ ?>
							<?php $coments = get_comments( array( 'meta_query' => array(
									array( 'key' => '_type_', 'value'=>$comment_type,'compare'=>'='),
									array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
									),'order'=>'ASC', 'parent'=>0, 'post_id' => get_the_ID()) );?>
							
							<?php foreach($coments as $coment){ ?>
							
							<div class="contenedor-comentarios fleft fwidth">
								<ul class="lista-comentarios hay-respuesta">
									<li>
										<?php echo get_avatar( $coment->user_id, 32 ); ?>
										<span class="nombre"><?php echo $coment->comment_author;?> Says: </span>
                                        <div class="clearfix"></div>
										<span class="comentario"><?php echo $coment->comment_content;?></span>
										<div class="clearfix"></div>
										<span class="fecha"> <?php echo $coment->comment_date;?></span>
									</li>
									<?php 
										$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
										foreach($coments_child as $child){	
									?>
									<li>
										<?php echo get_avatar( $child->user_id, 32 ); ?>
										<span class="nombre"><?php echo $child->comment_author;?> Says: </span>
                                        <div class="clearfix"></div>
										<span class="comentario"><?php echo $child->comment_content;?></span>
										<div class="clearfix"></div>
										<span class="fecha"> <?php echo $child->comment_date;?></span>
									</li>
									<?php } ?>
								</ul>	
								
								<?php $resolved = get_resolve($coment->comment_ID);
									if($resolved != true){?>
								<form class="para-respuesta" name="<?php echo $comment_type;?>">
									<div class="input-field">
										<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
										<input type="hidden" name="id_project" value="<?php the_ID();?>">
										<textarea class="materialize-textarea" name="comment"></textarea>
										<label>Reply</label>
									</div>
									
									<button class="btn waves-effect waves-light fleft resolver" type="reset"  rel="comment" data-option="comment_resolve">Resolve this thread <i class="fa fa-check" aria-hidden="true"></i></button>
									<button class="btn waves-effect waves-light fright" type="submit"   rel="comment" data-option="comment_project_reply">Reply <i class="fa fa-comments" aria-hidden="true"></i></button>
								</form>
								<?php } ?>
								<div class="helpful box fwidth fleft text-center" <?php if($resolved != true){?>style="display:none"<?php } ?>>
									<p>Was this person helpful for your project?</p>
									<div class="clearfix"></div>
									<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
									<button class="btn waves-effect waves-light helpful-button" rel="comment"  data-option="comment_helpful">Helpful</button>
									<button class="btn waves-effect waves-light not-helpful-button" rel="comment" data-option="comment_not_helpful">Not Helpful</button>
								</div>
							
							</div>
							<?php } ?>
						<!-- owner project -->
						<?php } ?>
				<?php } ?>	
                </div>
                <!--cuando no hay comentarios-->
            </div>
        </div>
        
        
        <?php } ?>
        
        
        
        
         <?php
			$comment_type = 'project_stir_money';

			if( is_user_logged_in() ) {	
				global $post;
				$post_id = $post->ID;
				$author_post = get_post_field( 'post_author', $post_id );
				$current_user = wp_get_current_user();
				
				if($current_user->ID == $author_post){
					$coments = get_comments( array( 'meta_query' => array(
						array( 'key' => '_type_', 'value'=> $comment_type,'compare'=>'='),
						array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
					),'order'=>'ASC', 'parent'=>0, 'post_id' => get_the_ID()) );
					$reminder = comment_reminder($coments);
					if(isset(end($coments)->comment_ID)){
						$coments_child = get_comments(array('parent'=> $coments->comment_ID,'order'=>'ASC' ));
					}
				} else{ 
					
					$coments = get_comments(array( 'meta_query' => array(
						array( 'key' => '_type_', 'value'=> $comment_type,'compare'=>'='),
						array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
					),'parent'=>0,'order'=>'ASC', 'post_id' => get_the_ID(),'author__in'=> array($current_user->ID)));
					
					$reminder = comment_reminder($coments);	
					if(isset(end($coments)->comment_ID)){
						$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
					}

				}
				
			}
		?>
        
        <div class="modulo-contenido box fwidth fleft">
            <div class="txt col-lg-8 col-md-8 col-sm-8 col-xs-12">
             <?php if( is_user_logged_in() ){?>
                <?php if ( ( count($coments) != 0 && $current_user->ID == $author_post )   ) { ?>
					<span class="icos-comentarios <?php if($reminder == "_true_"){ ?>comentario-con-respuesta<?php }else{ ?>sin-comentarios agregar-nuevo-comentario<?php } ?>" title="Add new comment">&nbsp;</span>
                <?php } else if($current_user->ID != $author_post && count($coments)!=0){ ?>
					<span class="icos-comentarios <?php if(isset($coments_child) && $reminder == "_true_"){ ?>comentario-con-respuesta<?php }else{ ?>comentario-sin-respuesta<?php } ?> " title="Add new comment">&nbsp;</span>
				<?php } else if($current_user->ID != $author_post && count($coments)==0) { ?>
					<span class="icos-comentarios agregar-nuevo-comentario" title="Add new comment">&nbsp;</span>
				<?php } ?>
			<?php } ?>
                <h3>Which resources do you need for the project?</h3>
                <?php echo get('plan_project_stir_money');?>
                <ul class="fa-ul edit">
                    <!--<li><i class="fa-li fa fa-pencil"></i><span>Edit</span></li>-->
                </ul>
            </div>
            <div class="comentarios col-lg-4 col-md-4 col-sm-4 col-xs-12">
                
               <div id="comentario-para-moderar" class="modulo-comentarios fwidth fleft">
                    <span class="cerrar-modulo text-center"><i class="fa fa-times fa-lg"></i></span>
                <?php if(is_user_logged_in()){?>	    
                   	<?php if ( ( count($coments) != 0 && $current_user->ID == $author_post )   ) { ?>
                   		<?php if( $reminder == "_true_" ){ ?>
                   		<h6>There is a reply to your comment</h6>
                   		<?php } else{?>
                   		<h6>Review and reply your comment</h6>
                   		<?php } ?>
                   	<?php } else if($current_user->ID != $author_post && count($coments)!=0){ ?>
                   	 <?php if( $reminder == "_true_" ){ ?>
                   		<h6>There is a reply to your comment</h6>
                   	 <?php } else{ ?>
                   	 	<h6>No reply to your comment</h6>
                   	<?php  } ?>
                   	<?php } else if($current_user->ID != $author_post && count($coments)==0) { ?>
                    	<h6>Help the author by leaving a comment or question</h6>
                    <?php } ?>
						
					
						<?php if($current_user->ID != $author_post){?>
						<div class="contenedor-comentarios fleft fwidth">
							<!-- user visit -->
								
							<ul class="lista-comentarios hay-respuesta">
							<?php foreach($coments as $coment){ ?>
								<li>
									<?php echo get_avatar( $coment->user_id, 32 ); ?>
									<span class="nombre"><?php echo $coment->comment_author;?> Says: </span>
                                    <div class="clearfix"></div>
									<span class="comentario"><?php echo $coment->comment_content;?></span>
									<div class="clearfix"></div>
									<span class="fecha"> <?php echo $coment->comment_date;?></span>
								</li>
								<?php 
									$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
									foreach($coments_child as $child){	
								?>
								<li>
									<?php echo get_avatar( $child->user_id, 32 ); ?>
									<span class="nombre"><?php echo $child->comment_author;?> Says: </span>
                                    <div class="clearfix"></div>
									<span class="comentario"><?php echo $child->comment_content;?></span>
									<div class="clearfix"></div>
									<span class="fecha"> <?php echo $child->comment_date;?></span>
								</li>
								<?php } ?>
							<?php } ?>
							</ul>
							
							<?php if (count($coments)!=0 && count($coments_child) != 0 && !get_resolve($coment->comment_ID)){?>
							<form class="para-respuesta" name="<?php echo $comment_type;?>">
								<div class="input-field">
									<input type="hidden" name="id_project" value="<?php the_ID();?>">
									<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
									<textarea id="textarea2" class="materialize-textarea" name="comment"></textarea>
									<label for="textarea2">Reply</label>
								</div>
								<button class="btn waves-effect waves-light fright" type="submit" rel="comment" data-option="comment_project_reply">Reply <i class="fa fa-comments" aria-hidden="true"></i></button>
							</form>
							<?php } ?>
							
							<?php if(count($coments)==0 && !get_resolve($coment->comment_ID)){?>
							<form class="para-respuesta" name="<?php echo $comment_type;?>" >	
								<div class="input-field">
									<textarea id="textarea1" name="comment" class="materialize-textarea"></textarea>
									<input type="hidden" name="id_project" value="<?php the_ID();?>">
									<label for="textarea1">Leave your comment here</label>
								</div>
								
								<button class="btn waves-effect waves-light fleft" type="reset" name="action">Reset</button>
								<button class="btn waves-effect waves-light fright" type="submit" rel="comment"  data-option="comment_project">Submit <i class="fa fa-comments-o" aria-hidden="true"></i></button>
							</form>	
							<?php } ?>
						</div>
						<?php } ?>
					
						<!-- author del proyecto -->
						<?php if($current_user->ID == $author_post){ ?>
							<?php $coments = get_comments( array( 'meta_query' => array(
									array( 'key' => '_type_', 'value'=>$comment_type,'compare'=>'='),
									array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
									),'order'=>'ASC', 'parent'=>0, 'post_id' => get_the_ID()) );?>
							
							<?php foreach($coments as $coment){ ?>
							
							<div class="contenedor-comentarios fleft fwidth">
								<ul class="lista-comentarios hay-respuesta">
									<li>
										<?php echo get_avatar( $coment->user_id, 32 ); ?>
										<span class="nombre"><?php echo $coment->comment_author;?> Says: </span><div class="clearfix"></div>
										<span class="comentario"><?php echo $coment->comment_content;?></span>
										<div class="clearfix"></div>
										<span class="fecha"> <?php echo $coment->comment_date;?></span>
									</li>
									<?php 
										$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
										foreach($coments_child as $child){	
									?>
									<li>
										<?php echo get_avatar( $child->user_id, 32 ); ?>
										<span class="nombre"><?php echo $child->comment_author;?> Says: </span>
                                        <div class="clearfix"></div>
										<span class="comentario"><?php echo $child->comment_content;?></span>
										<div class="clearfix"></div>
										<span class="fecha"> <?php echo $child->comment_date;?></span>
									</li>
									<?php } ?>
								</ul>	
								
								<?php $resolved = get_resolve($coment->comment_ID);
									if($resolved != true){?>
								<form class="para-respuesta" name="<?php echo $comment_type;?>">
									<div class="input-field">
										<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
										<input type="hidden" name="id_project" value="<?php the_ID();?>">
										<textarea class="materialize-textarea" name="comment"></textarea>
										<label>Reply</label>
									</div>
									
									<button class="btn waves-effect waves-light fleft resolver" type="reset"  rel="comment" data-option="comment_resolve">Resolve this thread <i class="fa fa-check" aria-hidden="true"></i></button>
									<button class="btn waves-effect waves-light fright" type="submit"   rel="comment" data-option="comment_project_reply">Reply <i class="fa fa-comments" aria-hidden="true"></i></button>
								</form>
								<?php } ?>
								<div class="helpful box fwidth fleft text-center" <?php if($resolved != true){?>style="display:none"<?php } ?>>
									<p>Was this person helpful for your project?</p>
									<div class="clearfix"></div>
									<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
									<button class="btn waves-effect waves-light helpful-button" rel="comment"  data-option="comment_helpful">Helpful</button>
									<button class="btn waves-effect waves-light not-helpful-button" rel="comment" data-option="comment_not_helpful">Not Helpful</button>
								</div>
							
							</div>
							<?php } ?>
						<!-- owner project -->
						<?php } ?>
				<?php } ?>	
                </div>
                <!--cuando no hay comentarios-->
                
                
            </div>
        </div>
        <?php
			$comment_type = 'project_future';

			if( is_user_logged_in() ) {	
				global $post;
				$post_id = $post->ID;
				$author_post = get_post_field( 'post_author', $post_id );
				$current_user = wp_get_current_user();
				
				if($current_user->ID == $author_post){
					$coments = get_comments( array( 'meta_query' => array(
						array( 'key' => '_type_', 'value'=> $comment_type,'compare'=>'='),
						array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
					),'order'=>'ASC', 'parent'=>0, 'post_id' => get_the_ID()) );
					$reminder = comment_reminder($coments);
					if(isset(end($coments)->comment_ID)){
						$coments_child = get_comments(array('parent'=> $coments->comment_ID,'order'=>'ASC' ));
					}
				} else{ 
					
					$coments = get_comments(array( 'meta_query' => array(
						array( 'key' => '_type_', 'value'=> $comment_type,'compare'=>'='),
						array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
					),'parent'=>0,'order'=>'ASC', 'post_id' => get_the_ID(),'author__in'=> array($current_user->ID)));
					
					$reminder = comment_reminder($coments);	
					if(isset(end($coments)->comment_ID)){
						$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
					}

				}
				
			}
		?>
        
        <div class="modulo-contenido box fwidth fleft">
            <div class="txt col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <?php if( is_user_logged_in() ){?>
            <?php if ( ( count($coments) != 0 && $current_user->ID == $author_post )   ) { ?>
				<span class="icos-comentarios <?php if($reminder == "_true_"){ ?>comentario-con-respuesta<?php }else{ ?>sin-comentarios agregar-nuevo-comentario<?php } ?>" title="Add new comment">&nbsp;</span>
            <?php } else if($current_user->ID != $author_post && count($coments)!=0){ ?>
				<span class="icos-comentarios <?php if(isset($coments_child) && $reminder == "_true_"){ ?>comentario-con-respuesta<?php }else{ ?>comentario-sin-respuesta<?php } ?> " title="Add new comment">&nbsp;</span>
			<?php } else if($current_user->ID != $author_post && count($coments)==0) { ?>
				<span class="icos-comentarios agregar-nuevo-comentario" title="Add new comment">&nbsp;</span>
			<?php } ?>
			<?php } ?>
                <h3>Where will <?php the_title(); ?> <br>go in the future?</h3>
                <?php echo get('plan_project_future');?>
                <ul class="fa-ul edit">
                    <!--<li><i class="fa-li fa fa-pencil"></i><span>Edit</span></li>-->
                </ul>
            </div>
            <div class="comentarios col-lg-4 col-md-4 col-sm-4 col-xs-12">
                
                <!--cuando hay comentarios visto desde el autor del proyecto-->
                <div id="comentario-para-moderar" class="modulo-comentarios fwidth fleft">
                    <span class="cerrar-modulo text-center"><i class="fa fa-times fa-lg"></i></span>
                <?php if(is_user_logged_in()){?>	    
                   	<?php if ( ( count($coments) != 0 && $current_user->ID == $author_post )   ) { ?>
                   		<?php if( $reminder == "_true_" ){ ?>
                   		<h6>There is a reply to your comment</h6>
                   		<?php } else{?>
                   		<h6>Review and reply your comment</h6>
                   		<?php } ?>
                   	<?php } else if($current_user->ID != $author_post && count($coments)!=0){ ?>
                   	 <?php if( $reminder == "_true_" ){ ?>
                   		<h6>There is a reply to your comment</h6>
                   	 <?php } else{ ?>
                   	 	<h6>No reply to your comment</h6>
                   	<?php  } ?>
                   	<?php } else if($current_user->ID != $author_post && count($coments)==0) { ?>
                    	<h6>Help the author by leaving a comment or question</h6>
                    <?php } ?>
						
					
						<?php if($current_user->ID != $author_post){?>
						<div class="contenedor-comentarios fleft fwidth">
							<!-- user visit -->
								
							<ul class="lista-comentarios hay-respuesta">
							<?php foreach($coments as $coment){ ?>
								<li>
									<?php echo get_avatar( $coment->user_id, 32 ); ?>
									<span class="nombre"><?php echo $coment->comment_author;?> Says: </span>
                                    <div class="clearfix"></div>
									<span class="comentario"><?php echo $coment->comment_content;?></span>
									<div class="clearfix"></div>
									<span class="fecha"> <?php echo $coment->comment_date;?></span>
								</li>
								<?php 
									$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
									foreach($coments_child as $child){	
								?>
								<li>
									<?php echo get_avatar( $child->user_id, 32 ); ?>
									<span class="nombre"><?php echo $child->comment_author;?> Says: </span>
                                    <div class="clearfix"></div>
									<span class="comentario"><?php echo $child->comment_content;?></span>
									<div class="clearfix"></div>
									<span class="fecha"> <?php echo $child->comment_date;?></span>
								</li>
								<?php } ?>
							<?php } ?>
							</ul>
							
							<?php if (count($coments)!=0 && count($coments_child) != 0 && !get_resolve($coment->comment_ID)){?>
							<form class="para-respuesta" name="<?php echo $comment_type;?>">
								<div class="input-field">
									<input type="hidden" name="id_project" value="<?php the_ID();?>">
									<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
									<textarea id="textarea2" class="materialize-textarea" name="comment"></textarea>
									<label for="textarea2">Reply</label>
								</div>
								<button class="btn waves-effect waves-light fright" type="submit" rel="comment" data-option="comment_project_reply">Reply <i class="fa fa-comments" aria-hidden="true"></i></button>
							</form>
							<?php } ?>
							
							<?php if(count($coments)==0 && !get_resolve($coment->comment_ID)){?>
							<form class="para-respuesta" name="<?php echo $comment_type;?>" >	
								<div class="input-field">
									<textarea id="textarea1" name="comment" class="materialize-textarea"></textarea>
									<input type="hidden" name="id_project" value="<?php the_ID();?>">
									<label for="textarea1">Leave your comment here</label>
								</div>
								
								<button class="btn waves-effect waves-light fleft" type="reset" name="action">Reset</button>
								<button class="btn waves-effect waves-light fright" type="submit" rel="comment"  data-option="comment_project">Submit <i class="fa fa-comments-o" aria-hidden="true"></i></button>
							</form>	
							<?php } ?>
						</div>
						<?php } ?>
					
						<!-- author del proyecto -->
						<?php if($current_user->ID == $author_post){ ?>
							<?php $coments = get_comments( array( 'meta_query' => array(
									array( 'key' => '_type_', 'value'=>$comment_type,'compare'=>'='),
									array( 'key' => '_closed_', 'value'=>'1','compare'=>'NOT EXISTS')
									),'order'=>'ASC', 'parent'=>0, 'post_id' => get_the_ID()) );?>
							
							<?php foreach($coments as $coment){ ?>
							
							<div class="contenedor-comentarios fleft fwidth">
								<ul class="lista-comentarios hay-respuesta">
									<li>
										<?php echo get_avatar( $coment->user_id, 32 ); ?>
										<span class="nombre"><?php echo $coment->comment_author;?> Says: </span>
                                        <div class="clearfix"></div>
										<span class="comentario"><?php echo $coment->comment_content;?></span>
										<div class="clearfix"></div>
										<span class="fecha"> <?php echo $coment->comment_date;?></span>
									</li>
									<?php 
										$coments_child = get_comments(array('parent'=> $coment->comment_ID,'order'=>'ASC' ));
										foreach($coments_child as $child){	
									?>
									<li>
										<?php echo get_avatar( $child->user_id, 32 ); ?>
										<span class="nombre"><?php echo $child->comment_author;?> Says: </span>
                                        <div class="clearfix"></div>
										<span class="comentario"><?php echo $child->comment_content;?></span>
										<div class="clearfix"></div>
										<span class="fecha"> <?php echo $child->comment_date;?></span>
									</li>
									<?php } ?>
								</ul>	
								
								<?php $resolved = get_resolve($coment->comment_ID);
									if($resolved != true){?>
								<form class="para-respuesta" name="<?php echo $comment_type;?>">
									<div class="input-field">
										<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
										<input type="hidden" name="id_project" value="<?php the_ID();?>">
										<textarea class="materialize-textarea" name="comment"></textarea>
										<label>Reply</label>
									</div>
									
									<button class="btn waves-effect waves-light fleft resolver" type="reset"  rel="comment" data-option="comment_resolve">Resolve this thread <i class="fa fa-check" aria-hidden="true"></i></button>
									<button class="btn waves-effect waves-light fright" type="submit"   rel="comment" data-option="comment_project_reply">Reply <i class="fa fa-comments" aria-hidden="true"></i></button>
								</form>
								<?php } ?>
								<div class="helpful box fwidth fleft text-center" <?php if($resolved != true){?>style="display:none"<?php } ?>>
									<p>Was this person helpful for your project?</p>
									<div class="clearfix"></div>
									<input type="hidden" name="id_comment" value="<?php echo $coment->comment_ID;?>">
									<button class="btn waves-effect waves-light helpful-button" rel="comment"  data-option="comment_helpful">Helpful</button>
									<button class="btn waves-effect waves-light not-helpful-button" rel="comment" data-option="comment_not_helpful">Not Helpful</button>
								</div>
							
							</div>
							<?php } ?>
						<!-- owner project -->
						<?php } ?>
				<?php } ?>	
                </div>
               
                <!--cuando hay comentarios sin respuesta visto desd el creador del comentario-->
                
            </div>
        </div>
        
        
        
        <div class="modulo-contenido box fwidth fleft">
            
                <?php 
                    $collab = get('team_project_team_collaborators');
                ?>
            
                <?php if(empty($collab)){?> 
                <?php } else { ?>
                        <div class="txt col-lg-3 col-md-3 col-sm-3 col-xs-12"> 
                            <h3><?php the_title(); ?> is looking for collaborators</h3>
                             
                            <?php if ( is_user_logged_in() ) { ?>
                <p>Required Skills: <?php echo get('team_project_team_collaborators');?></p>
                             <p>Contact at <a href="mailto:<?php echo get('team_project_team_email');?>"> <?php echo get('team_project_team_email');?></a></p>
                <? } else { ?>
                <p><small><a data-toggle="modal" data-target="#myModal">Log-in</a> or <a href="<?php echo get_option('siteurl'); ?>/register/?guest" target="_blank">Register <i class="fa fa-external-link fa-lg" aria-hidden="true"></i></a> to check contact details. </small></p>
                <? }?>          

                         </div>
                <?php } ?>
           
            
            <div class="txt col-lg-3 col-md-3 col-sm-3 col-xs-12">
                
                <h3>Find out more about <?php the_title(); ?></h3>
                <?php 
                    $find = get_order_field('visuals_project_find_project');
                    foreach($find as $key ){ 
                    $uri = get('visuals_project_find_project',1,$key);
                    $url = parse_url($uri);
                    $domain = preg_replace('/(www(\.)|\.(\w|\d)*)/','',$url["host"]);
					
					if ( $domain !== "linkedin" && $domain != "twitter" && $domain != "google" 
					   && $domain != "youtube" && $domain != "pinterest" && $domain != "soundcloud"
					  && $domain != "instagram" && $domain != "behance" && $domain != "wordpress" 
					  && $domain != "facebook" ){
						$domain  = "other";
					} else if($domain=='google') {
						$domain="google-plus";
					}
                    if(!empty($domain)){
                ?> 
                <ul class="redes">
                    <li>
                        <a href="<?php echo  get('visuals_project_find_project',1,$key);?>" target="_blank">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-square fa-stack-2x"></i>
                                <i class="fa fa-<?php echo $domain;?> fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>                    
                    </li>
                </ul>    
                <?php   } ?>
                <?php } ?>
                <div class="clearfix"></div>
                
                <ul class="fa-ul edit">
                    <!--<li><i class="fa-li fa fa-pencil"></i><span>Edit</span></li>-->
                </ul>
            </div>
            
            
            
            <div class="txt col-lg-6 col-md-6 col-sm-6 col-xs-12 who-is-behind">
                
                <h3>Who is behind <?php the_title(); ?>?</h3>
                
                
                
                <?php /* <?php
                    $teams = get_order_group('other_member_project_role');
                    foreach($teams as $team){
                    $role = get('other_member_project_role',$team);
                    $age  = get('other_member_project_age',$team);
                    if(!empty($role) && !empty($age)){
                ?>
                <div class="item-proj" style="background-image:url(<?php echo get('other_member_project_picture',$team);?>);">
                    <div class="info bg-<?php echo $color;?>"  <?php if(empty($color)){?> style="background-color:#ef4136;"<?php } ?>>
                    <aside class="txt">
                        <p><?php echo get('other_member_project_role',$team);?></p>
                        <span>Age: <?php echo  get('other_member_project_age',$team);?>, Gender: <?php echo  get('other_member_project_gender',$team);?></span>
                        <span><?php echo get('other_member_project_skills',$team);?>; </span>
                    </aside>
                    </div>
                </div>
                <?php } } ?>
                */ ?>
                
        <div class="clearfix"></div>
                
             
                <?php
                    $teams = get_order_group('team_project_old');
                    foreach($teams as $team){
                    $linkperso = get('team_project_find_people',$team);
                ?>
                 
                <div class="col-lg-5 col-md-4 col-sm-6 col-xs-12">
            <div class="card">
                <div class="card-image">
                  <img src="<?php echo get('team_project_profile_picture',$team);?>">
                </div>
                <div class="card-content">
                    <span class="card-title"><?php echo get('team_project_role',$team);?><br> <?php echo  get('team_project_old',$team);?></span>
                    <?/* <p><?php echo  get('team_project_sex',$team);?>, <?php echo  get('team_project_old',$team);?></p> */?>
                    <p>My Skills: <?php echo get('team_project_skills',$team);?></p>
                    <?php if(empty($linkperso)){?> 
            <?php } else { ?>
            <div id="finsingle" class="card-action">
                  <a href="<?php echo get('team_project_find_people',$team);?>" target="_blank">Find out more about me</a>
                </div>
            <?php } ?>
                </div>

            
              </div>
            </div>
                 
                <?php } ?>
                 
                 <?php                
                $teams = get_order_group('other_member_project_role');
                foreach($teams as $team){
                $role = get('other_member_project_role',$team);
                $age  = get('other_member_project_age',$team);
                if(!empty($role) && !empty($age)){
                ?>
                 
                 

                 <div class="col-lg-5 col-md-4 col-sm-6 col-xs-12">
                 <div class="card">
                <div class="card-image">
                  <img src="<?php echo get('other_member_project_picture',$team);?>" alt="">
                </div>
                <div class="card-content">
                    <span class="card-title"><?php echo get('other_member_project_role',$team);?></span>
                    <p><?php echo  get('other_member_project_gender',$team);?>, <?php echo  get('other_member_project_age',$team);?></p>
                    <br>
                    <p>My Skills: <?php echo get('other_member_project_skills',$team);?></p>
                </div>
              </div> 
            </div>
                 
                 
                
               <?php } } ?> 
                 
                 
                 
            
        
                
                       
                
                
                <div class="clearfix"></div>
                <ul class="fa-ul edit">
                    <!--<li><i class="fa-li fa fa-pencil"></i><span>Edit</span></li>-->
                </ul>
            </div>
            
        </div>
        
    </div>
    <!--/contenido del proyecto-->
    <div id="mobile-share" class="box fleft fwidth hidden-lg hidden-md hidden-sm">
        <?php echo do_shortcode('[ssbp]'); ?>
    </div>
    </div>
</section>
<?php wp_reset_query(); ?>

<div id="similar-projects" class="box fleft fwidth">

        <div class="container">
            <div id="project-tiles" class="container">
                <div class="co-lg-12 col-md-12 col-sm-12 filter">
                    <h3>Browse Creative Projects</h3>
                </div>
                
                <?php $args=array( //Loop 2
                    'post_type' => 'project', 
                    'posts_per_page' => 4,
                    'orderby' => 'rand',
                );
                $myloop = new WP_Query($args);
                if($myloop->have_posts()) : while($myloop->have_posts()) :
                $myloop->the_post();
                ?>
                <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '' ); $url = $thumb['0']; ?>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 no-column">
                    <div class="item box fleft fwidth">
                        <div class="meta">
                            <span class="box fleft">
                                &nbsp;
                            </span>
                            
                            <span class="box fright season">
                                
                            <?php $terms = get_the_terms( get_the_ID(), 'season' );?><?php if($terms){ ?><?php $term = array_shift($terms);?>
							<span class="season <?php echo $term->slug;?>"> <?php echo $term->name;?></span> <?php } ?>
                                
                            </span>
                            
                        </div>
                        <div class="clearfix"></div>
                        <div class="photo box fleft fwidth" style="background-image:url(<?php echo get('visuals_project_display');?>);">
                            <?php if( has_term('yes','funded', get_the_ID()) ) {?><span class="funded">GRANT RECIPIENT</span><?php } else {?><? } ?>
                            
                            <a href="<?php the_permalink(); ?>">&nbsp;</a>
                        </div>
                        
                        <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                        <div class="clearfix"></div>
                        
                        
                            <div class="excerpt">
                                <?php echo get('describe_project');?>
                                <a href="<?php the_permalink(); ?>">more</a>
                            </div>
                        <!--comentar votos-->
                        <?/*<span class="supporters"><?php echo support_count(get_the_ID());?> Votes</span>*/?>
                        
                        <div class="clearfix"></div>
                        
                        <?php if(get_the_tag_list()) { echo get_the_tag_list('<ul class="post-tags"><li>','</li><li>','</li></ul>'); } ?>
                    </div>
                </div>

                <?php endwhile; endif; ?>
                <?php wp_reset_query(); ?>
                
                <div class="box fleft fwidth caja-boton text-center">
                    <a href="<?php echo get_option('siteurl'); ?>/support" class="small-cta">[more projects]</a>
                </div>

                
                
                
            </div>
            
            
        </div>
    </div>


<?php get_footer(); ?>